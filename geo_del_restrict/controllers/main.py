from odoo import http,_
from odoo.http import request
import babel.messages.pofile
from odoo.http import request
import odoo.addons.web.controllers.main as main
from odoo.exceptions import ValidationError

class MyDataSet(main.DataSet):
	
	@http.route(['/web/dataset/call_kw', '/web/dataset/call_kw/<path:path>'], type='json', auth="user")
	def call_kw(self, model, method, args, kwargs, path=None):
		uid=request.session.uid
		if method=='unlink' and uid!=1:
			raise ValidationError(_("You have not access to delete record."))
		return super(MyDataSet, self).call_kw(model, method, args, kwargs, path)

