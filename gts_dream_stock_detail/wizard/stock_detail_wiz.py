from odoo import models, fields, api, _
from datetime import datetime, date, time, timedelta
from dateutil.relativedelta import relativedelta


class ProductReportWiz(models.TransientModel):
    _name = 'product.detail.wiz'

    from_date = fields.Date('Start Date', default=fields.Datetime.now)
    end_date = fields.Date('End Date', default=fields.Datetime.now)

    @api.multi
    def open_product_detail_table(self):
        report_search = self.env['product.detail'].search([])
        branches = self.env['res.branch'].search([])
        self._cr.execute('''
                            delete  
                            from product_detail
                            where branch_id in %s
                        ''', (tuple(branches.ids),))

        if self.from_date and self.end_date:
            tree_view_id = self.env.ref('gts_dream_stock_detail.view_product_detail_report_tree').id
            form_view_id = self.env.ref('gts_dream_stock_detail.view_product_detail_report_form').id
            graph_view_id = self.env.ref('gts_dream_stock_detail.view_product_detail_report_graph').id
            pivot_view_id = self.env.ref('gts_dream_stock_detail.view_product_detail_report_pivot').id
            search_view_ref = self.env.ref('gts_dream_stock_detail.view_product_detail_report_search', False)
            date_format = '%Y-%m-%d'
            first_day_ = datetime.strptime(self.from_date, date_format)
            sub_months = first_day_ + relativedelta(months=-1)
            sub_days = first_day_ + relativedelta(days=-1)
            last_day_ = datetime.strptime(self.end_date, date_format)
            first_day = first_day_
            day_last_strip = str(last_day_).strip(':0')
            last_day = last_day_
            last_day_it = day_last_strip + '23:59:59'

            self._cr.execute("""
                        
                    with product_opening as (
			            select
                            sl.branch_id as branch_id,
                            sil.product_id as product_id,
                            sum(sil.total_real_cost) as purchase_cost_last,
                            sum(sil.product_qty) as purchase_quantity_last,
                            0.0 as purchase_cost,
                            0.0 as purchase_value,
                            0.0 as purchase_quantity,
                            0.0 as outward_cost,
			                0.0 as outward_quantity,
                            0.0 as outward_value,
                            0.0 as closing_cost,
                            0.0 as closing_qty,
                            0.0 as theoretical_quantity,
                            0.0 as theoretical_cost,
                            pr.product_stage as product_stage
                            from stock_inventory_line sil
                            join stock_location sl on sl.id = sil.location_id
                            join res_branch br on br.id = sl.branch_id
                            join product_product pr on sil.product_id = pr.id
                            where sil.date_line between '%s' and '%s'
                            and sil.location_id in (16, 73, 79, 91, 85) and sil.state_line = 'done'
                            group by sl.branch_id, sil.product_id, pr.product_stage
                    ),                 
                                
                    product_detail_inward as (
                        select                            
                            sl.branch_id as branch_id,
                            sm.product_id as product_id,
                            0.0 as purchase_cost_last,
                            0.0 as purchase_quantity_last,
                            sum(sm.cost) as purchase_cost,
                            sum(sm.total_cost) as purchase_value,
                            sum(sm.product_uom_qty) as purchase_quantity,
                            0.0 as outward_cost,
                            0.0 as outward_quantity,
                            0.0 as outward_value,
                            0.0 as closing_cost,
                            0.0 as closing_qty,
                            0.0 as theoretical_quantity,
                            0.0 as theoretical_cost,
                            pr.product_stage as product_stage
                            from stock_move sm
                            join stock_location sl on sl.id = sm.location_dest_id
                            join res_branch br on br.id = sl.branch_id
                            join product_product pr on sm.product_id = pr.id
                            where location_dest_id in (16, 73, 79, 91, 85)
                            and picking_type_id in (1, 61, 79, 73,67)
                            and state = 'done'
                            and sm.date between '%s' and '%s'
                            group by sm.product_id, sl.branch_id, pr.product_stage
                    ),
                                                   
                    product_detail_outward as (
                        select
                            sl.branch_id as branch_id,
                            sm.product_id as product_id,
                            0.0 as purchase_cost_last,
                            0.0 as purchase_quantity_last,
                            0.0 as purchase_cost,
                            0.0 as purchase_value,
                            0.0 as purchase_quantity,
                            sum(sm.cost) as outward_cost,
                            sum(sm.product_uom_qty) as outward_quantity,
                            sum(sm.total_cost) as outward_value,
                            0.0 as closing_cost,
                            0.0 as closing_qty,
                            0.0 as theoretical_quantity,
                            0.0 as theoretical_cost,
                            pr.product_stage as product_stage
                            from stock_move sm
                            join stock_location sl on sl.id = sm.location_dest_id
                            join res_branch br on br.id = sl.branch_id
                            join product_product pr on sm.product_id = pr.id
                            where location_dest_id in (102, 100, 99, 95, 104)
                            and picking_type_id in (4, 64, 70, 76, 82)
                            and state = 'done'
                            and sm.date between '%s' and '%s'
                            group by sm.product_id, sl.branch_id, pr.product_stage
                    ),
                                       
                    closing_product_detail as (
                        select	
                            sl.branch_id as branch,
                            sil.product_id as product_id,
                            0.0 as purchase_cost_last,
                            0.0 as purchase_quantity_last,
                            0.0 as purchase_cost,
                            0.0 as purchase_value,
                            0.0 as purchase_quantity,
                            0.0 as outward_cost,
                            0.0 as outward_quantity,
                            0.0 as outward_value,
                            sum(sil.total_real_cost) as closing_cost,
                            sum(sil.product_qty) as closing_qty,
                            sum(sil.theoretical_qty) as theoretical_quantity,
                            sum(sil.total_theoretical_cost) as theoretical_cost,
                            pr.product_stage as product_stage
                            from stock_inventory_line sil
                            join stock_location sl on sl.id = sil.location_id
                            join res_branch br on br.id = sl.branch_id
                            join product_product pr on sil.product_id = pr.id
                            where sil.date_line between '%s' and '%s' 
                            and sil.location_id in (16, 73, 79, 91, 85) and sil.state_line = 'done'
                            group by sl.branch_id, sil.product_id, pr.product_stage
                    ),
                    final_data as (
                        select * from product_opening
                        UNION
                        select * from product_detail_inward
                        UNION
                        select * from product_detail_outward
                        UNION
                        select * from closing_product_detail
                        )
                                                          
                    
                    select row_number() OVER (ORDER BY product_id) AS id, branch_id, product_id,
                    purchase_cost_last as opening_value, purchase_quantity_last as opening_quantity, purchase_cost, purchase_value, purchase_quantity,
                    outward_cost, outward_quantity, outward_value, (purchase_quantity_last + purchase_quantity) - outward_quantity as theoretical_quantity, (purchase_cost_last + purchase_value ) - outward_value as theoretical_cost,
                    closing_cost as closing_value, closing_qty as closing_quantity, ((purchase_quantity_last + purchase_quantity) - outward_quantity) - closing_qty  as diff_quantity_ ,
                    ((purchase_cost_last + purchase_value ) - outward_value)- closing_cost as diff_cost, product_stage as product_stage_       
                    from final_data
                    

                    """%(sub_months, sub_days,
                         first_day, last_day,
                         first_day, last_day,
                         first_day, last_day
                         ))

            result = self._cr.fetchall()
            rows = result
            values = ', '.join(map(str, rows))
            sql = ("""INSERT INTO product_detail 
                        (id, branch_id, product_id, opening_value, opening_quantity, 
                        purchase_cost, purchase_value, purchase_quantity, outward_cost, outward_quantity, outward_value,
                        theoretical_quantity, theoretical_cost, closing_value, closing_quantity, diff_quantity_, diff_cost, product_stage_
                        ) VALUES {}""".format(values).replace('None', 'null')
                   )
            self._cr.execute(sql)
            action = {
                'type': 'ir.actions.act_window',
                'views': [
                    (tree_view_id, 'tree'), (form_view_id, 'form'),
                    (graph_view_id, 'graph'), (pivot_view_id, 'pivot')
                ],
                'view_mode': 'tree,form',
                'name': _('Stock Report : %s - %s' %(first_day.strftime('%d %b %y'),
                                                              last_day.strftime('%d %b %y'))),
                'res_model': 'product.detail',
                'search_view_id': search_view_ref and search_view_ref.id,
                'context': {'group_by' :['branch_id','product_id']}
            }
            return action