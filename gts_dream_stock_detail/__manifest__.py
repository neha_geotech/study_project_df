# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'GTS Stock Detail Report',
    'summary': 'Stock Detail Report',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'website': 'http://www.geotechnosoft.com',
    'category': 'Inventory',
    'version': '1.0',
    'depends': [
        'gts_dream_purchase'
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/security_view.xml',
        'wizard/stock_detail_wiz_view.xml',
        'report/stock_detail_view.xml',
    ],
    'installable': True,
}
