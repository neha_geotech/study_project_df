# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError


class ProductDetail(models.Model):
    '''
    This is report view table to show Stock Report
    '''
    _name = 'product.detail'

    @api.model
    def default_get(self, fields):
        raise ValidationError(_('No Record Create From this Option!'))
        res = super(ProductDetail, self).default_get(fields)
        return res

    product_id = fields.Many2one('product.product', string='Product')
    cost = fields.Float(string='Cost')
    total_cost = fields.Float(string='Total Cost')
    product_uom_qty = fields.Float(string='Quantity')
    location_dest_id = fields.Many2one('stock.location', string='Location')
    branch_id = fields.Many2one('res.branch', string='Branch', readonly=True)
    purchase_cost = fields.Float(string='Purchase Rate')
    purchase_quantity = fields.Float(string='Purchase Quantity')
    purchase_value = fields.Float(string='Purchase Value')
    outward_cost = fields.Float('Outward Rate')
    outward_quantity = fields.Float('Outward Quantity')
    outward_value = fields.Float('Outward Value')
    closing_quantity = fields.Float(string='Closing Quantity')
    closing_value = fields.Float(string='Closing Value')
    outward_cost_last = fields.Float('Outward Rate')
    outward_quantity_last = fields.Float('Outward Quantity')
    outward_value_last = fields.Float('Outward Value')
    purchase_cost_last = fields.Float(string='Purchase Rate')
    purchase_quantity_last = fields.Float(string='Purchase Quantity')
    purchase_value_last = fields.Float(string='Purchase Value')
    opening_quantity = fields.Float(string='Opening Quantity')
    opening_value = fields.Float(string='Opening Value')
    theoretical_quantity = fields.Float(string='Theoretical Quantity')
    theoretical_cost = fields.Float(string='Theoretical Value')
    real_quantity = fields.Float(string='Real Quantity')
    diff_quantity_ = fields.Float(string='Diff Quantity')
    diff_cost= fields.Float(string='Diff Value')
    product_stage_ = fields.Selection([
        ('raw_material', 'Raw Material'),
        ('wip', 'WIP'),
        ('semi_finished', 'Semi Finished'),
        ('reusable', 'Reusable')],
        string='Product Stage')
