from odoo import fields, models, api, _
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import Warning, UserError


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        res = super(SaleOrderLine, self).product_id_change()
        res['price_unit'] = self.order_id.amount_untaxed_new
        return res

    @api.multi
    def _prepare_invoice_line(self, qty):
        """
        Prepare the dict of values to create the new invoice line for a sales order line.

        :param qty: float quantity to invoice
        """
        self.ensure_one()
        res = {}
        account = self.product_id.property_account_income_id or self.product_id.categ_id.property_account_income_categ_id
        if not account:
            raise UserError(
                _('Please define income account for this product: "%s" (id:%d) - or for its category: "%s".') %
                (self.product_id.name, self.product_id.id, self.product_id.categ_id.name))

        fpos = self.order_id.fiscal_position_id or self.order_id.partner_id.property_account_position_id
        if fpos:
            account = fpos.map_account(account)

        res = {
            # 'name': self.name,
            'sequence': self.sequence,
            'origin': self.order_id.name,
            'account_id': account.id,
            'price_unit': self.order_id.amount_untaxed_new,
            'quantity': qty,
            'discount': self.discount,
            'uom_id': self.product_uom.id,
            'product_id': self.product_id.id or False,
            'layout_category_id': self.layout_category_id and self.layout_category_id.id or False,
            'invoice_line_tax_ids': [(6, 0, self.tax_id.ids)],
            'account_analytic_id': self.order_id.analytic_account_id.id,
            'analytic_tag_ids': [(6, 0, self.analytic_tag_ids.ids)],
            'hsn_id': self.hsn_id.id
        }
        if self.order_id.branch_id.id == 7:
            if self.order_id.product_id.product_tmpl_id.id == 7698:
                opportunity = ''
                venue = 'Venue'
                # date = 'Event Date'
                if self.order_id.opportunity_id:
                    opportunity = self.order_id.opportunity_id.name
                if self.order_id.venue:
                    venue = self.order_id.venue
                # if self.order_id.event_date:
                #     date = self.order_id.event_date
                res['name'] = 'Invoice for the following Services provided for the ' + str(
                    opportunity
                ) + ' at ' + str(venue)

            elif self.order_id.product_id.product_tmpl_id.id == 7601:
                opportunity = ''
                venue = 'Venue'
                # date = 'Event Date'
                if self.order_id.opportunity_id:
                    opportunity = self.order_id.opportunity_id.name
                if self.order_id.venue:
                    venue = self.order_id.venue
                # if self.order_id.event_date:
                #     date = self.order_id.event_date
                res['name'] = 'Invoice for the Provision of Supply of Furniture & Fixtures for the ' + str(
                    opportunity
                ) + ' at ' + str(venue)
        else:
            opportunity = ''
            venue = 'Venue'
            date = 'Event Date'
            if self.order_id.opportunity_id:
                opportunity = self.order_id.opportunity_id.name
            if self.order_id.venue:
                venue = self.order_id.venue
            if self.order_id.event_date:
                date = self.order_id.event_date
            res['name'] = 'Invoice for the following services provided for the ' + str(
                opportunity
            ) + ' Event Held at ' + str(venue) + ' on dated ' + str(
                date
            )
        return res


class SaleOrder(models.Model):
    _inherit = "sale.order"
    _order = 'create_date desc'

    @api.model
    def _default_warehouse_id(self):
        company = self.env.user.company_id.id
        if self.branch_id.id not in (6, 7, 8):
            warehouse_ids = self.env['stock.warehouse'].search([
                ('company_id', '=', company)
            ], limit=1)
            return warehouse_ids
        else:
            return 1

    @api.depends('order_line.price_total')
    def _amount_all(self):
        res = super(SaleOrder, self)._amount_all()
        for order in self:
            amount_tax_igst = amount_tax_sgst = amount_tax_cgst = 0.0
            for line in order.order_line:
                amount_tax_igst += line.igst
                if not line.hsn_id:
                    amount_tax_sgst += line.sgst
                    amount_tax_cgst += line.cgst
                    # amount_tax_igst = 0.0
                if line.hsn_id:
                    if order.partner_id.state_id and order.branch_id.state_id:
                        if order.partner_id.state_id == order.branch_id.state_id:
                            amount_tax_sgst += line.sgst
                            amount_tax_cgst += line.cgst
                            # amount_tax_igst = 0.0
                order.update({
                    'amount_tax_igst_ol': round(amount_tax_igst),
                    'amount_tax_sgst_ol': round(amount_tax_sgst),
                    'amount_tax_cgst_ol': round(amount_tax_cgst),
                    'amount_total': order.amount_untaxed + round(amount_tax_igst) + round(amount_tax_sgst) + round(amount_tax_cgst),
                })
        return res

    @api.depends('quotation_line.total')
    def _amount_all_new(self):
        """
        Intended to compute value of fields amount_untaxed_new, amount_tax_igst,
        amount_tax_sgst, amount_tax_cgst and amount_total_new
        """
        for order in self:
            amount_untaxed = amount_tax_sgst = amount_tax_cgst = 0.0
            for line in order.quotation_line:
                amount_untaxed += line.total
            amount_tax_igst = (amount_untaxed * 18) / 100
            if order.partner_id.state_id and order.branch_id.state_id:
                if order.partner_id.state_id.id == order.branch_id.state_id.id:
                    amount_tax_sgst += (amount_untaxed * 9) / 100
                    amount_tax_cgst += (amount_untaxed * 9) / 100
                    amount_tax_igst = 0.0
            res = {
                'amount_untaxed_new': amount_untaxed,
                'amount_tax_igst': round(amount_tax_igst),
                'amount_tax_sgst': round(amount_tax_sgst),
                'amount_tax_cgst': round(amount_tax_cgst),
            }
            if order.exclude_tax is True:
                res['amount_total_new'] = amount_untaxed
            else:
                res['amount_total_new'] = amount_untaxed + round(amount_tax_igst) + round(amount_tax_sgst) + round(amount_tax_cgst)
            order.update(res)

    @api.multi
    def _calc_exp_date(self):
        return datetime.now() + relativedelta(months=1)

    quotation_line = fields.One2many(
        'quotation.line',
        'sale_order_id',
        string='Quotation Lines'
    )
    validity_date = fields.Date(
        string='Expiration Date',
        readonly=True,
        default=_calc_exp_date,
    )
    proforma_seq = fields.Char(
        'Proforma',
        required=True,
        index=True,
        copy=False,
        default='New'
    )
    project_name = fields.Text(string='Project Name', track_visibility='onchange', default=' ')
    project_id = fields.Many2one('project.project', string='Project Name', track_visibility='onchange')
    project_owner = fields.Many2one('res.users', string='Project Owner', track_visibility='onchange')
    event_date = fields.Date('Event Date', track_visibility='onchange')
    setup_date = fields.Date('Setup Date', track_visibility='onchange')
    type_ = fields.Many2one('project.type', string='Project Type', track_visibility='onchange')
    venue = fields.Text(string='Venue',default='',track_visibility='onchange')
    activity_owner = fields.Char(string='Activity Owner',default='', track_visibility='onchange')
    hsn_code = fields.Char(string='HSN/SAC CODE',default=' ', track_visibility='onchange')
    df_gst_number = fields.Char(string='DF-GST NO.', track_visibility='onchange')
    client_gst_number = fields.Char(string='Client GST NO.', track_visibility='onchange')
    branch_code_ = fields.Char(string='Branch Code', related='branch_id.branch_code', track_visibility='onchange')
    revision_number = fields.Integer(
        'Revision Number',
        copy=False,
    )
    revision_date = fields.Date('Revision Date', copy=False, track_visibility='onchange')
    delivery_address = fields.Text('Delivery Address', default='', track_visibility='onchange')
    project_execution_date = fields.Date('Project Execution Date', track_visibility='onchange')
    # project_managers = fields.Char(related='opportunity_id.project_managers', string='Project Managers')
    date_deadline = fields.Date(related='analytic_account_id.date_deadline', string='Expected Closing Date', track_visibility='onchange')
    partner_ids = fields.Many2many(
        'res.partner',
        'rel_sale_partner_invoice',
        'sale_id',
        'partner_id',
        string='Associated Addresses',
    )
    import_quotation = fields.Boolean('Import Quotation', default=False, copy=False)
    quotation_revision = fields.Boolean('Quotation Revision', default=False, copy=False)
    analytic_account_id = fields.Many2one('account.analytic.account', 'Job Code', copy=False,
                                          oldname='project_id')

    amount_untaxed_new = fields.Monetary(
        string='Untaxed Amount',
        store=True,
        readonly=True,
        compute='_amount_all_new',
        track_visibility='onchange'
    )
    amount_tax_igst = fields.Monetary(
        string='IGST @18%',
        store=True,
        compute='_amount_all_new',
        track_visibility='onchange'
    )
    amount_tax_sgst = fields.Monetary(
        string='SGST @9%',
        store=True,
        compute='_amount_all_new',
        track_visibility='onchange'
    )
    amount_tax_cgst = fields.Monetary(
        string='CGST @9%',
        store=True,
        compute='_amount_all_new',
        track_visibility='onchange'
    )
    amount_total_new = fields.Monetary(
        string='Total',
        store=True,
        compute='_amount_all_new',
        track_visibility='onchange'
    )
    advance_required = fields.Float(
        string='Advance Required of Grand Total',
        track_visibility='onchange'
    )
    amount_tax_igst_ol = fields.Monetary(
        string='IGST',
        store=True,
        compute='_amount_all'
    )
    amount_tax_sgst_ol = fields.Monetary(
        string='SGST',
        store=True,
        compute='_amount_all'
    )
    amount_tax_cgst_ol = fields.Monetary(
        string='CGST',
        store=True,
        compute='_amount_all'
    )
    exclude_tax = fields.Boolean(
        string='Exclusive of Taxes',
        default=False, copy=False, index=True
    )
    bom_id = fields.Many2one(
        'mrp.bom', 'Bill of Material', copy=False
    )
    bom_created = fields.Boolean(
        'BOM Created', default=False, copy=False
    )
    job_code = fields.Boolean(
        'Job Code', default=False, copy=False
    )

    @api.multi
    def _prepare_invoice(self):
        res = super(SaleOrder, self)._prepare_invoice()
        res['sale_id'] = self.id
        res['job_code'] = self.analytic_account_id.id
        res['activity_owner'] = self.activity_owner
        res['venue'] = self.venue
        res['event_name'] = self.project_id.name
        return res

    @api.onchange('project_name', 'activity_owner', 'hsn_code', 'venue','delivery_address')
    def project_name_title(self):
        for rec in self:
            self.project_name = str(self.project_name).title()
            self.activity_owner = str(self.activity_owner).title()
            self.hsn_code = str(self.hsn_code).title()
            self.venue = str(self.venue).title()
            self.delivery_address = str(self.delivery_address).title()

    @api.onchange('payment_term_id', 'amount_total_new')
    def _advance_requierd(self):
        for order in self:
            adv_amt = 0.0
            if order.payment_term_id:
                search_id = self.env['account.payment.term.line'].search([
                    ('payment_id', '=', order.payment_term_id.id),
                    ('value', '=', 'percent')
                ])
                adv_amt = (order.amount_total_new * search_id.value_amount)/ 100
                order.advance_required = adv_amt

    @api.multi
    def action_confirm(self):
        rec = super(SaleOrder, self).action_confirm()
        if not self.order_line:
            raise UserError(_('Please add product in order lines to proceed further.'))
        self.write({
            'project_execution_date': fields.Datetime.now()
        })
        for data in self.picking_ids:
            data.update({'branch_id': self.branch_id.id,})
            data.move_lines.update({'branch_id': self.branch_id.id})
        if self.opportunity_id:
            for res in self:
                res.opportunity_id.planned_revenue = self.amount_untaxed_new

        return rec

    @api.onchange('partner_id')
    def onchange_partner_id(self):
        rec = super(SaleOrder, self).onchange_partner_id()
        list_ = []
        if self.partner_id.child_ids:
            for data in self.partner_id.child_ids:
                list_.append(data.id)
                partner_data = self.env['res.partner'].search([
                    ('id', 'in', list_)
                ])
                if partner_data:
                    self.update({'partner_ids': partner_data})
        return rec

    @api.model
    def create(self, vals):
        """
        Create sequence for sale.order
        """
        seq_obj = self.env['ir.sequence']
        branch_obj = self.env['res.branch']
        if 'branch_id' in vals:
            branch_id_ = branch_obj.search([
                ('id', '=', vals['branch_id'])
            ])
            seq_code = 'sale.' + branch_id_.branch_code
            proforma_seq_code = 'proforma.' + branch_id_.branch_code
            if 'date_order' in vals and branch_id_.branch_code:
                date_ = datetime.strptime(
                    vals['date_order'],
                    "%Y-%m-%d %H:%M:%S"
                )
                month_ = (date_.strftime("%b")).upper()
                year_ = date_.strftime("%y")
                vals['name'] = 'SO' + '/' + str(
                    branch_id_.branch_code
                ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                        seq_code
                    )
                vals['proforma_seq'] = 'PI' + '/' + str(
                    branch_id_.branch_code
                ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                    proforma_seq_code
                )
            if 'date_order' not in vals:
                if branch_id_.branch_code:
                    date_ = datetime.now()
                    month_ = (date_.strftime("%b")).upper()
                    year_ = date_.strftime("%y")
                    vals['name'] = 'SO' + '/' + str(
                        branch_id_.branch_code
                    ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                        seq_code
                    )
                    vals['proforma_seq'] = 'PI' + '/' + str(
                        branch_id_.branch_code
                    ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                        proforma_seq_code
                    )
            if not branch_id_.branch_code:
                raise Warning(_(
                    'Branch code is not defined for branch %s'
                ) % branch_id_.name)
        order_ = super(SaleOrder, self).create(vals)
        active_ids = self.env.context.get('active_ids')
        if active_ids:
            crm_id = self.env['crm.lead'].browse(active_ids)
            crm_id.write({
                'sale_id': order_.id,
            })
        return order_

    @api.multi
    def button_reset_quotation(self):
        self.write({
            'import_quotation': False,
            'quotation_revision': True,
        })
        self.quotation_line.unlink()

    @api.multi
    def action_quotation_send(self):
        '''
        This function opens a window to compose an email, with the edi sale template message loaded by default
        Passing customized template reference into the field template_id
        '''
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('gts_dream_quotation', 'qt_email_template_edi_sale')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = {
            'default_model': 'sale.order',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            'custom_layout': "sale.mail_template_data_notification_email_sale_order",
            'proforma': self.env.context.get('proforma', False),
            'force_email': True
        }
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.multi
    def button_create_bom(self):
        view_ref = self.env[
            'ir.model.data'
        ].get_object_reference(
            'mrp', 'mrp_bom_form_view'
        )
        view_id = view_ref[1] if view_ref else False
        if view_id:
            rec = {
                'type': 'ir.actions.act_window',
                'res_model': 'mrp.bom',
                'view_type': 'form',
                'view_mode': 'form',
                'view_id': view_id,
                'target': 'current',
                'context': {
                    'default_client_name': self.partner_id.id,
                    'default_branch_id': self.branch_id.id,
                    'default_sale_id': self.id,
                    'default_venue': self.venue,
                    'default_job_code': self.analytic_account_id.id,
                    'default_event_date': self.event_date,
                    'default_project_managers': self.opportunity_id.project_managers.id,
                    'default_product_tmpl_id': self.project_id.product_id.product_tmpl_id.id,
                    'default_product_id': self.project_id.product_id.id,
                }
            }
            return rec


class QuotationLine(models.Model):
    _name = 'quotation.line'
    _description = 'Quotation Line'
    _order = 'id'
    """
    QuotationLine class is to introduce quotation lines into Quotation form.
    Which allows to enter data as per user's ease.
    """
    date_order = fields.Datetime(
        string='Order Date',
        default=fields.Datetime.now,
        readonly=True)
    sale_order_id = fields.Many2one('sale.order', string='Order Reference')
    seq_number = fields.Char('S.No.')
    particulars = fields.Char(string='Particulars')
    description_of_goods = fields.Char(string='Description')
    length_ = fields.Char(string='L')
    width = fields.Char(string='W')
    height = fields.Char(string='H')
    size = fields.Char(string='Size')
    uom = fields.Text(string='UOM')
    quantity = fields.Float(string='Qty')
    days = fields.Integer(string='Days')
    rate = fields.Float(string='Rate')
    total = fields.Float(string='Amount')


class ResCompany(models.Model):
    _inherit = "res.company"

    pan = fields.Char(string='PAN No')
    active = fields.Boolean(string='Active', default=True)
    cin_no = fields.Char(string='CIN No')

