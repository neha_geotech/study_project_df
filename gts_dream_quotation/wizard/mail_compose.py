from odoo import api, fields, models


class MailComposer(models.TransientModel):
    _inherit = 'mail.compose.message'

    @api.multi
    def send_mail_action(self):
        res = super(MailComposer, self).send_mail_action()
        active_ids = self.env.context.get('active_ids')
        active_model = self.env.context.get('active_model')
        if active_model == 'sale.order':
            if active_ids:
                sale_id = self.env['sale.order'].browse(active_ids)[0]
                if sale_id.quotation_revision is True:
                    sale_id.write({
                        'quotation_revision': False,
                        'revision_number': sale_id.revision_number + 1,
                        'revision_date': fields.Datetime.now()
                    })
        return res
