# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

import xlrd
import tempfile
import base64
import os

from odoo import api, fields, models, _
from odoo.exceptions import Warning


class SaleQuotationWiz(models.TransientModel):
    _name = "sale.quotation.wiz"
    _description = "Sales Quotation Wizard"

    file_import = fields.Binary('Upload File')
    filename = fields.Char('File Name')

    sale_order_id = fields.Many2one('sale.order', string='Order Reference')

    """
    These below fields from 'file_field1' to 'file_fields12' 
    are to show the header values of imported file.
    """

    file_field1 = fields.Char(string='F1')
    file_field2 = fields.Char(string='F2')
    file_field3 = fields.Char(string='F3')
    file_field4 = fields.Char(string='F4')
    file_field5 = fields.Char(string='F5')
    file_field6 = fields.Char(string='F6')
    file_field7 = fields.Char(string='F7')
    file_field8 = fields.Char(string='F8')
    file_field9 = fields.Char(string='F9')
    file_field10 = fields.Char(string='F10')
    file_field11 = fields.Char(string='F11')
    file_field12 = fields.Char(string='F12')

    """
    These below fields from 'field_name1' to 'field_name12' 
    are to show the header values of imported file.
    """

    field_name1 = fields.Many2one(
        'ir.model.fields', 'Field Name', domain="[('model_id','=','quotation.line')]")
    field_name2 = fields.Many2one(
        'ir.model.fields', 'Field Name', domain="[('model_id','=','quotation.line')]")
    field_name3 = fields.Many2one(
        'ir.model.fields', 'Field Name', domain="[('model_id','=','quotation.line')]")
    field_name4 = fields.Many2one(
        'ir.model.fields', 'Field Name', domain="[('model_id','=','quotation.line')]")
    field_name5 = fields.Many2one(
        'ir.model.fields', 'Field Name', domain="[('model_id','=','quotation.line')]")
    field_name6 = fields.Many2one(
        'ir.model.fields', 'Field Name', domain="[('model_id','=','quotation.line')]")
    field_name7 = fields.Many2one(
        'ir.model.fields', 'Field Name', domain="[('model_id','=','quotation.line')]")
    field_name8 = fields.Many2one(
        'ir.model.fields', 'Field Name', domain="[('model_id','=','quotation.line')]")
    field_name9 = fields.Many2one(
        'ir.model.fields', 'Field Name', domain="[('model_id','=','quotation.line')]")
    field_name10 = fields.Many2one(
        'ir.model.fields', 'Field Name', domain="[('model_id','=','quotation.line')]")
    field_name11 = fields.Many2one(
        'ir.model.fields', 'Field Name', domain="[('model_id','=','quotation.line')]")
    field_name12 = fields.Many2one(
        'ir.model.fields', 'Field Name', domain="[('model_id','=','quotation.line')]")

    @api.onchange('file_import')
    def onchange_file_import(self):
        """
        Shows the fields of file (to be imported) header.
        """
        if self.file_import:
            tmp_dir = tempfile.mkdtemp()
            excel_file = base64.decodebytes(bytes(self.file_import, 'utf-8'))
            file_path = os.path.join(tmp_dir, 'test.xls')
            fp = open(file_path, 'wb')
            fp.write(excel_file)
            fp.close()
            book = xlrd.open_workbook(file_path)
            sh = book.sheet_by_index(0)
            cells = sh.row_values(0)
            if len(cells) < 12:
                raise Warning(_('Incorrect file format. \
                    Please make sure that the file contains 12 headers.'))
            self.file_field1 = cells[0]
            self.file_field2 = cells[1]
            self.file_field3 = cells[2]
            self.file_field4 = cells[3]
            self.file_field5 = cells[4]
            self.file_field6 = cells[5]
            self.file_field7 = cells[6]
            self.file_field8 = cells[7]
            self.file_field9 = cells[8]
            self.file_field10 = cells[9]
            self.file_field11 = cells[10]
            self.file_field12 = cells[11]

    @api.multi
    def read_file(self):
        """
        Creates Quotation line by using the values of selected file
        """
        active_ids = self.env.context.get('active_ids')
        if active_ids:
            sale_id = self.env['sale.order'].browse(active_ids)
            sale_id.write({
                'import_quotation': True,
            })
        quotation_env = self.env['quotation.line']
        active_ids = self.env.context.get('active_ids', [])
        tmp_dir = tempfile.mkdtemp()
        excel_file = base64.decodebytes(self.file_import)
        file_path = os.path.join(tmp_dir, 'test.xls')
        fp = open(file_path, 'wb')
        fp.write(excel_file)
        fp.close()
        book = xlrd.open_workbook(file_path)
        sh = book.sheet_by_index(0)
        line_val = {}
        for line in range(1, sh.nrows):
            row = sh.row_values(line)
            if self.field_name1:
                line_val[self.field_name1.name] = row[0] or False
            if self.field_name2:
                line_val[self.field_name2.name] = row[1] or False
            if self.field_name3:
                line_val[self.field_name3.name] = row[2] or False
            if self.field_name4:
                line_val[self.field_name4.name] = row[3] or False
            if self.field_name5:
                line_val[self.field_name5.name] = row[4] or False
            if self.field_name6:
                line_val[self.field_name6.name] = row[5] or False
            if self.field_name7:
                line_val[self.field_name7.name] = row[6] or False
            if self.field_name8:
                line_val[self.field_name8.name] = row[7] or False
            if self.field_name9:
                line_val[self.field_name9.name] = row[8] or False
            if self.field_name10:
                line_val[self.field_name10.name] = row[9] or False
            if self.field_name11:
                line_val[self.field_name11.name] = row[10] or False
            if self.field_name12:
                line_val[self.field_name12.name] = row[11] or False
            if len(line_val) == 0:
                raise Warning(_('You have not selected any fields \
                    to import values into Quotation Lines.'))
            line_val['sale_order_id'] = active_ids[0] or False
            if 'seq_number' in line_val:
                line_val['seq_number'] = str(
                    line_val['seq_number']
                ).rstrip('0').rstrip('.')
            if 'particulars' in line_val:
                line_val['particulars'] = str(
                    line_val['particulars']
                ).title()
                if line_val['particulars'] == 'False':
                    line_val['particulars'] = ''
            if 'description_of_goods' in line_val:
                line_val['description_of_goods'] = str(
                    line_val['description_of_goods']
                ).title()
                if line_val['description_of_goods'] == 'False':
                    line_val['description_of_goods'] = ''
            if 'seq_number' in line_val:
                if line_val['seq_number'] == 'False':
                    line_val['seq_number'] = ''
            if 'uom' in line_val:
                line_val['uom'] = str(
                    line_val['uom']
                ).upper()
                if line_val['uom'] == 'FALSE':
                    line_val['uom'] = ''
            if 'length_' in line_val:
                line_val['length_'] = str(
                    line_val['length_']
                ).upper()
                if line_val['length_'] == 'FALSE':
                    line_val['length_'] = ''
            if 'width' in line_val:
                line_val['width'] = str(
                    line_val['width']
                ).upper()
                if line_val['width'] == 'FALSE':
                    line_val['width'] = ''
            if 'height' in line_val:
                line_val['height'] = str(
                    line_val['height']
                ).upper()
                if line_val['height'] == 'FALSE':
                    line_val['height'] = ''
            if 'size' in line_val:
                line_val['size'] = str(
                    line_val['size']
                ).upper()
                if line_val['size'] == 'FALSE':
                    line_val['size'] = ''
            if 'total' in line_val:
                if type(line_val['total']) not in (float, bool):
                    raise Warning(_('Please enter proper format for field Amount'))
            if 'quantity' in line_val:
                if type(line_val['quantity']) not in (float, bool):
                    raise Warning(_('Please enter proper format for field Quantity'))
            if 'rate' in line_val:
                if type(line_val['rate']) not in (float, bool):
                    raise Warning(_('Please enter proper format for field Rate'))
            quotation_env.create(line_val)
