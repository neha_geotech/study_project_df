
from odoo import api, fields, models, _


class tree_columns_setup(models.Model):
	_name = 'tree.columns.setup'
	_description = """This class used to store the user related tree view column data"""

	name = fields.Char('Name')
	xml_id = fields.Char('XML ID')
	user_id = fields.Many2one('res.users', 'User')
	res_model = fields.Char('Model')
	column_lines = fields.One2many('tree.columns.setup.line', 'tree_column_setup_id',
								   'Column Line', ondelete='cascade')

	@api.multi
	def get_columns(self, recs):
		shown_columns = []
		try:
			domain = [('xml_id', '=', recs['xml_id']), ('user_id', '=', recs['user_id']),
					  ('res_model', '=', recs['res_model'])]
			find_rec = self.find_view_filter(domain)
			if len(find_rec) > 0:
				for rec in find_rec.column_lines:
					shown_columns.append(rec.name)
			return shown_columns
		except:
			print('Found Exception in method get_columns')
			pass

	@api.multi
	def find_view_filter(self, domain=[]):
		if len(domain) > 0:
			return self.sudo().search(domain)
		else:
			return []

	@api.multi
	def create_update(self, recs, columns):
		col_line = self.env['tree.columns.setup.line']
		domain = [('xml_id', '=', recs['xml_id']), ('user_id','=', recs['user_id']),
				  ('res_model', '=', recs['res_model'])]
		find_rec = self.find_view_filter(domain)
		if len(find_rec) < 1:
			rec_id = self.sudo().create(recs)
			for c in columns:
				col_line.sudo().create({'name': c, 'tree_column_setup_id': rec_id.id})
		else:
			existing_column = []
			for cl in find_rec.column_lines:
				existing_column.append(cl.name)
				if cl.name not in columns:
					cl.sudo().unlink()
			for c in columns:
				if c not in existing_column:
					col_line.sudo().create({'name': c, 'tree_column_setup_id': find_rec.id})
		return True


class tree_columns_setup_line(models.Model):
	_name = 'tree.columns.setup.line'

	tree_column_setup_id = fields.Many2one('tree.column.setup', 'Tree Column')
	name = fields.Char('Name')
