
{
    'name': 'Dynamic Tree View',
    'version': '11.0.0.0.1',
    'author': 'Geo Technosoft',
    'category': 'Tools',
    'website': 'https://www.geotechnosoft.com',
    'summary': 'Dynamic List View',
    'description': """
        This module allow users to select fields that will show in tree view.
        Tree fields can hide and show on user's selection.
        Dynamic list view columns view fields hide columns show columns visible fields
    """,
    'sequence': 1,
    'depends': ['web'],
    'data': [
        'security/ir.model.access.csv',
        'views/tree_button.xml'
    ],
    'qweb': ['static/src/xml/tree_view_button_view.xml'],
    'images': ['static/description/banner.png'],
    'price': 20.00,
    'currency': 'EUR',
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'OPL-1',
}
