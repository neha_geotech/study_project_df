odoo.define('dynamic_tree_view.shcolumns', function (require) {
"use strict";

var core = require('web.core');
var ListView = require('web.ListView');
var ListController = require('web.ListController');
//var Model = require('web.DataModel');
var rpc = require('web.rpc');
var QWeb = core.qweb;

ListController.include({
	init: function(viewInfo, params) {
		this._super.apply(this, arguments);
		var self = this;
        self.newCols = [];
        self.get_stored_columns(); //will check first if columns are available
        },

    renderButtons: function($node) {
        var self = this;
        this._super($node);
            this.$buttons.find('.oe_select_columns').click(this.proxy('my_setup_columns'));
            this.$buttons.find('.oe_dropdown_btn').click(this.proxy('hide_show_columns'));
            this.$buttons.find('.oe_dropdown_menu').click(this.proxy('stop_event'));
    },
        
    get_stored_columns: function(){
        var self = this;
        //var toolbarAction = self.toolbarActions.action[0];
        var context = self.initialState.getContext();
        var data_args = {'name': self.modelName, 'xml_id': context.params['action'], 'res_model':self.modelName,'user_id': context.uid};
        var def = self._rpc({
            model: 'tree.columns.setup',
            method: 'get_columns',
            args: [[],data_args],
        });
        $.when(def).pipe(function(shwn_cols){
            self.stored_columns = shwn_cols;
            if(shwn_cols.length > 0){
                //console.info('Customized Columns Loaded.')
                self.hide_show_columns('first'); //if found column then set list columns 
            }
        });
    },

    create_update: function(){
        var self = this;
        var context = self.initialState.getContext();
        //var data_args = {'name': toolbarAction.res_model +'_'+ toolbarAction.view_id[1], 'xml_id': toolbarAction.view_id[1], 'res_model':toolbarAction.res_model,'user_id': context.uid};
        var data_args = {'name': self.modelName, 'xml_id': context.params['action'], 'res_model':self.modelName,'user_id': context.uid};
        var def = self._rpc({
            model: 'tree.columns.setup',
            method: 'create_update',
            args: [[],data_args, self.newCols],
        });
        $.when(def).pipe(function(shwn_cols){
            self.newCols = [];
        });
    },

    checkVisibilty: function (col){
        var self = this;
        if(self.stored_columns.length > 1){
            if(self.stored_columns.indexOf(col.name) > -1){
                return true
            }
            else{
                return false
            }
        }
        else{
            if(Object.keys(col).indexOf('modifiers') > -1){
                if((col.modifiers.invisible && col.modifiers.invisible == 1) || (col.invisible && col.invisible == 1)){
                    return false;
                }
                else{
                    return true;
                }
            }else{
                return true;
            }
        }
    },

    my_setup_columns: function (fields, grouped) {
        $("#showcb").show();
        var getcb = document.getElementById('showcb');
        var self = this;
        var cols_list = [];
        var col_obj = {};
        var initialState = self.initialState;
        var visible_cols_obj = _.filter(self.renderer.columns, function(col){
            cols_list.push(col.name);
            col_obj[col.name] = col;
        });
        
        this.visible_columns = _.filter(initialState.fieldsInfo.list, function (f) {
            var firstcheck = document.getElementById(f.name);
            
            if(firstcheck == null)
            {
                var column = self.initialState.fields[f.name]
                var li= document.createElement("li");
                var description = document.createTextNode(column.string);
                var checkbox = document.createElement("input");
                checkbox.id = f.name;
                checkbox.type = "checkbox";
                checkbox.name = "cb";
                if(self.checkVisibilty(f)){
                    checkbox.checked = true;
                }
                else{
                    checkbox.checked = false
                }
                li.appendChild(checkbox);
                li.appendChild(description);
                getcb.appendChild(li);
            }
            else{
                
                if(self.checkVisibilty(f))
                {
                 firstcheck.checked = true;
                }
                else
                {
                  firstcheck.checked = false;
                }
                
            }
            
        });
     
	},
    stop_event : function(e)
      {
          e.stopPropagation();
      },

    hide_show_columns : function(modelCall){
    	var self = this;
    	$("#showcb").hide();
    	var column_ids = [];
        if(modelCall != 'first'){
        	_.each($('.oe_dropdown_menu li input'), function(c){
        		var ch = $(c);
        		if(typeof ch != 'undefined'){
        			if($(ch)[0].checked){
        				column_ids.push($(ch)[0].id);
        			}
        		}
        	});
        }
        else{
            column_ids = self.stored_columns;
        }
        _.each(self.renderer.arch.children, function(col){
            if(column_ids.indexOf(col.attrs['name']) > -1){
                if(Object.keys(col.attrs).indexOf('modifiers') > -1){
                    self.setVisible(col,modelCall);
                }else{
                    col.attrs['modifiers'] = {};
                    self.setVisible(col,modelCall);
                }
            }
            else{
                if(Object.keys(col.attrs).indexOf('modifiers') > -1){
                    self.setInvisible(col);
                }else{
                    col.attrs['modifiers'] = {};
                    self.setInvisible(col);
                }
            }
        });
        if(modelCall != 'first'){
            self.create_update();
        }
        
    },

    updateStoredColumns: function(col, state){
        var self = this;
        var idx = self.stored_columns.indexOf(col.attrs.name);
        if( idx > -1){
            if(state == 'invisible'){
                self.stored_columns.splice(idx,1);
            }
        }else{
            if(state == 'visible'){
                self.stored_columns.push(col.attrs.name);
            }
        }
    },

    setVisible: function(col, modelCall){
        var self = this;
        col.attrs['invisible'] = 0;
        col.attrs.modifiers['invisible'] = false;
        col.attrs.modifiers['column_invisible'] = false;
        self.update(self, {reload:true});
        if(modelCall != 'first'){
            self.newCols.push(col.attrs.name);
        }
        self.updateStoredColumns(col, 'visible');
    },

    setInvisible: function(col){
        var self = this;
        col.attrs['invisible'] = 1;
        col.attrs.modifiers['invisible'] = true;
        col.attrs.modifiers['column_invisible'] = true
        self.update(self, {reload:true});
        self.updateStoredColumns(col, 'invisible');
    }

});

    $(document).click(function(){
      $("#showcb").hide();
    });

});
