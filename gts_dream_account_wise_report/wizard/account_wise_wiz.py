from odoo import models, fields, api, _
from datetime import date, timedelta, datetime


class AccountWiseWiz(models.TransientModel):
    _name = 'account.wise.wiz'

    from_date = fields.Date('Start Date', default=fields.Datetime.now)
    end_date = fields.Date('End Date', default=fields.Datetime.now)

    @api.multi
    def open_account_wise(self):
        report_search = self.env['debtor.creditor.account'].search([])
        if report_search:
            report_search.unlink()
        accounts_ = self.env['account.account'].search([])
        self._cr.execute('''
                            delete
                            from debtor_creditor_account
                            where account in %s
                        ''', (tuple(accounts_.ids),))
        if self.from_date and self.end_date:
            tree_view_id = self.env.ref('gts_dream_account_wise_report.view_debtor_creditor_report_tree_account').id
            form_view_id = self.env.ref('gts_dream_account_wise_report.view_debtor_creditor_report_form_account').id
            graph_view_id = self.env.ref('gts_dream_account_wise_report.view_debtor_creditor_report_graph_account').id
            pivot_view_id = self.env.ref('gts_dream_account_wise_report.view_debtor_creditor_report_pivot_account').id
            search_view_ref = self.env.ref('gts_dream_account_wise_report.view_debtor_creditor_report_search_account', False)

            date_format = '%Y-%m-%d'
            first_day_ = datetime.strptime(self.from_date, date_format)
            last_day_ = datetime.strptime(self.end_date, date_format)
            first_day = first_day_
            last_day = last_day_

            all_accounts = self.env['account.account'].search([
                '|','|','|','|','|','|',
                ('name', '=', 'Imprest A/C'),
                ('name', '=', 'Debtors'),
                ('name', '=', 'Creditors For Capital Goods'),
                ('name', '=', 'Creditors For Expenses'),
                ('name', '=', 'Creditors For Purchases'),
                ('name', '=', 'Security Deposit Rent'),
                ('name', '=', 'Other Security Deposit')
                ])

            bank_accounts = self.env['account.account'].search([
                ('group_id.name', '=', 'Bank Accounts'),
                ('name', '!=', 'Bank')
            ])
            bank_all = []
            bank = []
            for data in bank_accounts:
                bank.append(data.id)
                bank_all.append(data.id)

            cash_accounts = self.env['account.account'].search([
                ('group_id.name', '=', 'Cash-In-Hand'),
                ('name', '!=', 'Dummy Cash')
            ])
            cash = []
            for res in cash_accounts:
                cash.append(res.id)
                bank_all.append(res.id)
            for rec in all_accounts:
                bank_all.append(rec.id)

            self._cr.execute("""
                with account_ as (
                    select
                    ac.id as account,
                    ac.group_id as account_group,
                    ac.user_type_id as user_type_id
                    from account_account ac
                    where ac.id in %s
                    group by ac.id
                ),
                opening_bank_balance as (
                    select
                    al.account_id as account,
                    ac.user_type_id as user_type_id,
                    ac.group_id as account_group,
                    sum(al.balance) as opening_bank_balance
                    from account_move_line al
                    join account_account ac on ac.id = al.account_id
                    left join account_move am on am.id = al.move_id
                    where am.state = 'posted'
                    and al.account_id in %s 
                    and al.date < '%s'
                    group by al.account_id, ac.user_type_id, ac.group_id
                ),
                data_opening_bank_balance as (
                    select
                    account_.account,
                    account_.user_type_id,
                    account_.account_group,
                    sum(obb.opening_bank_balance) as opening_bank_balance
                    from account_
                    left join opening_bank_balance obb on account_.account = obb.account
                    group by account_.account, account_.user_type_id, account_.account_group
                ),
                closing_bank_balance as (
                    select
                    al.account_id as account,
                    ac.user_type_id as user_type_id,
                    ac.group_id as account_group,
                    sum(al.balance) as closing_bank_balance
                    from account_move_line al
                    join account_account ac on ac.id = al.account_id
                    left join account_move am on am.id = al.move_id
                    where am.state = 'posted'
                    and al.account_id in %s 
                    and al.date <= '%s'
                    group by al.account_id, ac.user_type_id, ac.group_id
                ),
                data_closing_bank_balance as (
                    select
                    data_opening_bank_balance.account,
                    data_opening_bank_balance.user_type_id,
                    data_opening_bank_balance.account_group,
                    data_opening_bank_balance.opening_bank_balance as opening_bank_balance,
                    sum(cbb.closing_bank_balance) as closing_bank_balance
                    from data_opening_bank_balance
                    left join closing_bank_balance cbb on data_opening_bank_balance.account = cbb.account
                    group by data_opening_bank_balance.account,
                    data_opening_bank_balance.user_type_id,
                    data_opening_bank_balance.user_type_id,
                    data_opening_bank_balance.opening_bank_balance,
                    data_opening_bank_balance.account_group
                ),
                opening_imprest as (
                    select
                    al.account_id as account,
                    ac.user_type_id as user_type_id,
                    ac.group_id as account_group,
                    sum(al.balance) as opening_imprest
                    from account_move_line al
                    join account_account ac on ac.id = al.account_id
                    left join account_move am on am.id = al.move_id
                    where am.state = 'posted'
                    and ac.name = 'Imprest A/C'
                    and al.date < '%s'
                    group by al.account_id, ac.user_type_id, ac.group_id
                ),
                data_opening_imprest as (
                    select
                    data_closing_bank_balance.account,
                    data_closing_bank_balance.user_type_id,
                    data_closing_bank_balance.account_group,
                    data_closing_bank_balance.opening_bank_balance as opening_bank_balance,
                    data_closing_bank_balance.closing_bank_balance as closing_bank_balance,
                    sum(oi.opening_imprest) as opening_imprest
                    from data_closing_bank_balance
                    left join opening_imprest oi on data_closing_bank_balance.account = oi.account
                    group by data_closing_bank_balance.account,
                    data_closing_bank_balance.user_type_id,
                    data_closing_bank_balance.opening_bank_balance,
                    data_closing_bank_balance.closing_bank_balance,
                    data_closing_bank_balance.account_group
                ),
                closing_imprest as (
                    select
                    al.account_id as account,
                    ac.user_type_id as user_type_id,
                    ac.group_id as account_group,
                    sum(al.balance) as closing_imprest
                    from account_move_line al
                    join account_account ac on ac.id = al.account_id
                    left join account_move am on am.id = al.move_id
                    where am.state = 'posted'
                    and ac.name = 'Imprest A/C'
                    and al.date <= '%s'
                    group by al.account_id, ac.user_type_id, ac.group_id
                ),
                data_closing_imprest as (
                    select
                    data_opening_imprest.account,
                    data_opening_imprest.user_type_id,
                    data_opening_imprest.account_group,
                    data_opening_imprest.opening_bank_balance as opening_bank_balance,
                    data_opening_imprest.closing_bank_balance as closing_bank_balance,
                    data_opening_imprest.opening_imprest as opening_imprest,
                    sum(ci.closing_imprest) as closing_imprest
                    from data_opening_imprest
                    left join closing_imprest ci on data_opening_imprest.account = ci.account
                    group by data_opening_imprest.account,
                    data_opening_imprest.user_type_id,
                    data_opening_imprest.opening_bank_balance,
                    data_opening_imprest.closing_bank_balance,
                    data_opening_imprest.opening_imprest,
                    data_opening_imprest.account_group
                ),
                opening_cash_in_hand as (
                    select
                    al.account_id as account,
                    ac.user_type_id as user_type_id,
                    ac.group_id as account_group,
                    sum(al.balance) as opening_cash_in_hand
                    from account_move_line al
                    join account_account ac on ac.id = al.account_id
                    left join account_move am on am.id = al.move_id
                    where am.state = 'posted'
                    and al.account_id in %s 
                    and al.date < '%s'
                    group by al.account_id, ac.user_type_id, ac.group_id
                ),
                data_opening_cash_in_hand as (
                    select
                    data_closing_imprest.account,
                    data_closing_imprest.user_type_id,
                    data_closing_imprest.account_group,
                    data_closing_imprest.opening_bank_balance as opening_bank_balance,
                    data_closing_imprest.closing_bank_balance as closing_bank_balance,
                    data_closing_imprest.opening_imprest as opening_imprest,
                    data_closing_imprest.closing_imprest as closing_imprest,
                    sum(ocih.opening_cash_in_hand) as opening_cash_in_hand
                    from data_closing_imprest
                    left join opening_cash_in_hand ocih on data_closing_imprest.account = ocih.account
                    group by data_closing_imprest.account,
                    data_closing_imprest.user_type_id,
                    data_closing_imprest.opening_bank_balance,
                    data_closing_imprest.closing_bank_balance,
                    data_closing_imprest.opening_imprest,
                    data_closing_imprest.closing_imprest,
                    data_closing_imprest.account_group
                ),
                closing_cash_in_hand as (
                    select
                    al.account_id as account,
                    ac.user_type_id as user_type_id,
                    ac.group_id as account_group,
                    sum(al.balance) as closing_cash_in_hand
                    from account_move_line al
                    join account_account ac on ac.id = al.account_id
                    left join account_move am on am.id = al.move_id
                    where am.state = 'posted'
                    and al.account_id in %s 
                    and al.date <= '%s'
                    group by al.account_id, ac.user_type_id, ac.group_id
                ),
                data_closing_cash_in_hand as (
                    select
                    data_opening_cash_in_hand.account,
                    data_opening_cash_in_hand.user_type_id,
                    data_opening_cash_in_hand.account_group,
                    data_opening_cash_in_hand.opening_bank_balance as opening_bank_balance,
                    data_opening_cash_in_hand.closing_bank_balance as closing_bank_balance,
                    data_opening_cash_in_hand.opening_imprest as opening_imprest,
                    data_opening_cash_in_hand.closing_imprest as closing_imprest,
                    data_opening_cash_in_hand.opening_cash_in_hand as opening_cash_in_hand,
                    sum(ccih.closing_cash_in_hand) as closing_cash_in_hand
                    from data_opening_cash_in_hand
                    left join closing_cash_in_hand ccih on data_opening_cash_in_hand.account = ccih.account
                    group by data_opening_cash_in_hand.account,
                    data_opening_cash_in_hand.user_type_id,
                    data_opening_cash_in_hand.opening_bank_balance,
                    data_opening_cash_in_hand.closing_bank_balance,
                    data_opening_cash_in_hand.opening_imprest,
                    data_opening_cash_in_hand.closing_imprest,
                    data_opening_cash_in_hand.opening_cash_in_hand,
                    data_opening_cash_in_hand.account_group
                ),
                opening_debtors as (
                    select
                    al.account_id as account,
                    ac.user_type_id as user_type_id,
                    ac.group_id as account_group,
                    sum(al.balance) as opening_debtors
                    from account_move_line al
                    left join account_move am on am.id = al.move_id
                    join account_account ac on ac.id = al.account_id
                    where am.state = 'posted'
                    and ac.name = 'Debtors'
                    and al.date < '%s'
                    group by al.account_id, ac.user_type_id, ac.group_id
                ),
                data_opening_debtors as (
                    select
                    data_closing_cash_in_hand.account,
                    data_closing_cash_in_hand.user_type_id,
                    data_closing_cash_in_hand.account_group,
                    data_closing_cash_in_hand.opening_bank_balance as opening_bank_balance,
                    data_closing_cash_in_hand.closing_bank_balance as closing_bank_balance,
                    data_closing_cash_in_hand.opening_imprest as opening_imprest,
                    data_closing_cash_in_hand.closing_imprest as closing_imprest,
                    data_closing_cash_in_hand.opening_cash_in_hand as opening_cash_in_hand,
                    data_closing_cash_in_hand.closing_cash_in_hand as closing_cash_in_hand,
                    sum(od.opening_debtors) as opening_debtors
                    from data_closing_cash_in_hand
                    left join opening_debtors od on data_closing_cash_in_hand.account = od.account
                    group by data_closing_cash_in_hand.account,
                    data_closing_cash_in_hand.user_type_id,
                    data_closing_cash_in_hand.opening_bank_balance,
                    data_closing_cash_in_hand.closing_bank_balance,
                    data_closing_cash_in_hand.opening_imprest,
                    data_closing_cash_in_hand.closing_imprest,
                    data_closing_cash_in_hand.opening_cash_in_hand,
                    data_closing_cash_in_hand.closing_cash_in_hand,
                    data_closing_cash_in_hand.account_group
                ),
                closing_debtors as (
                    select
                    al.account_id as account,
                    ac.user_type_id as user_type_id,
                    ac.group_id as account_group,
                    sum(al.balance) as closing_debtors
                    from account_move_line al
                    left join account_move am on am.id = al.move_id
                    join account_account ac on ac.id = al.account_id
                    where am.state = 'posted'
                    and ac.name = 'Debtors'
                    and al.date <= '%s'
                    group by al.account_id, ac.user_type_id, ac.group_id
                ),
                data_closing_debtors as (
                    select
                    data_opening_debtors.account,
                    data_opening_debtors.user_type_id,
                    data_opening_debtors.account_group,
                    data_opening_debtors.opening_bank_balance as opening_bank_balance,
                    data_opening_debtors.closing_bank_balance as closing_bank_balance,
                    data_opening_debtors.opening_imprest as opening_imprest,
                    data_opening_debtors.closing_imprest as closing_imprest,
                    data_opening_debtors.opening_cash_in_hand as opening_cash_in_hand,
                    data_opening_debtors.closing_cash_in_hand as closing_cash_in_hand,
                    data_opening_debtors.opening_debtors as opening_debtors,
                    sum(cd.closing_debtors) as closing_debtors
                    from data_opening_debtors
                    left join closing_debtors cd on data_opening_debtors.account = cd.account
                    group by data_opening_debtors.account,
                    data_opening_debtors.user_type_id,
                    data_opening_debtors.opening_bank_balance,
                    data_opening_debtors.closing_bank_balance,
                    data_opening_debtors.opening_imprest,
                    data_opening_debtors.closing_imprest,
                    data_opening_debtors.opening_cash_in_hand,
                    data_opening_debtors.closing_cash_in_hand,
                    data_opening_debtors.opening_debtors,
                    data_opening_debtors.account_group
                ),
                opening_creditors as (
                    select
                    al.account_id as account,
                    ac.user_type_id as user_type_id,
                    ac.group_id as account_group,
                    sum(al.balance) * -1 as opening_creditors
                    from account_move_line al
                    left join account_move am on am.id = al.move_id
                    join account_account ac on ac.id = al.account_id
                    where am.state = 'posted'
                    and ac.name in ('Creditors For Capital Goods', 'Creditors For Expenses', 'Creditors For Purchases')
                    and al.date < '%s'
                    group by al.account_id, ac.user_type_id, ac.group_id
                ),
                data_opening_creditors as (
                    select
                    data_closing_debtors.account,
                    data_closing_debtors.user_type_id,
                    data_closing_debtors.account_group,
                    data_closing_debtors.opening_bank_balance as opening_bank_balance,
                    data_closing_debtors.closing_bank_balance as closing_bank_balance,
                    data_closing_debtors.opening_imprest as opening_imprest,
                    data_closing_debtors.closing_imprest as closing_imprest,
                    data_closing_debtors.opening_cash_in_hand as opening_cash_in_hand,
                    data_closing_debtors.closing_cash_in_hand as closing_cash_in_hand,
                    data_closing_debtors.opening_debtors as opening_debtors,
                    data_closing_debtors.closing_debtors as closing_debtors,
                    sum(oc.opening_creditors) as opening_creditors
                    from data_closing_debtors
                    left join opening_creditors oc on data_closing_debtors.account = oc.account
                    group by data_closing_debtors.account,
                    data_closing_debtors.user_type_id,
                    data_closing_debtors.opening_bank_balance,
                    data_closing_debtors.closing_bank_balance,
                    data_closing_debtors.opening_imprest,
                    data_closing_debtors.closing_imprest,
                    data_closing_debtors.opening_cash_in_hand,
                    data_closing_debtors.closing_cash_in_hand,
                    data_closing_debtors.opening_debtors,
                    data_closing_debtors.closing_debtors,
                    data_closing_debtors.account_group
                ),
                closing_creditors as (
                    select
                    al.account_id as account,
                    ac.user_type_id as user_type_id,
                    ac.group_id as account_group,
                    sum(al.balance) as closing_creditors_wo_uncleared
                    from account_move_line al
                    left join account_move am on am.id = al.move_id
                    join account_account ac on ac.id = al.account_id
                    where am.state = 'posted'
                    and ac.name in ('Creditors For Capital Goods', 'Creditors For Expenses', 'Creditors For Purchases')
                     and al.date <= '%s'
                    group by al.account_id, ac.user_type_id, ac.group_id
                ),
                data_closing_creditors as (
                    select
                    data_opening_creditors.account,
                    data_opening_creditors.user_type_id,
                    data_opening_creditors.account_group,
                    data_opening_creditors.opening_bank_balance as opening_bank_balance,
                    data_opening_creditors.closing_bank_balance as closing_bank_balance,
                    data_opening_creditors.opening_imprest as opening_imprest,
                    data_opening_creditors.closing_imprest as closing_imprest,
                    data_opening_creditors.opening_cash_in_hand as opening_cash_in_hand,
                    data_opening_creditors.closing_cash_in_hand as closing_cash_in_hand,
                    data_opening_creditors.opening_debtors as opening_debtors,
                    data_opening_creditors.closing_debtors as closing_debtors,
                    data_opening_creditors.opening_creditors as opening_creditors,
                    sum(cc.closing_creditors_wo_uncleared) as closing_creditors_wo_uncleared
                    from data_opening_creditors
                    left join closing_creditors cc on data_opening_creditors.account = cc.account
                    group by data_opening_creditors.account,
                    data_opening_creditors.user_type_id,
                    data_opening_creditors.opening_bank_balance,
                    data_opening_creditors.closing_bank_balance,
                    data_opening_creditors.opening_imprest,
                    data_opening_creditors.closing_imprest,
                    data_opening_creditors.opening_cash_in_hand,
                    data_opening_creditors.closing_cash_in_hand,
                    data_opening_creditors.opening_debtors,
                    data_opening_creditors.closing_debtors,
                    data_opening_creditors.opening_creditors,
                    data_opening_creditors.account_group
                ),
                opening_security_rent as (
                    select
                    al.account_id as account,
                    ac.user_type_id as user_type_id,
                    ac.group_id as account_group,
                    sum(al.balance) as opening_security_rent
                    from account_move_line al
                    left join account_move am on am.id = al.move_id
                    join account_account ac on ac.id = al.account_id
                    where am.state = 'posted'
                    and ac.name in ('Security Deposit Rent','Other Security Deposit')
                    and al.date < '%s'
                    group by al.account_id, ac.user_type_id, ac.group_id
                ),
                data_opening_security_rent as (
                    select
                    data_closing_creditors.account,
                    data_closing_creditors.user_type_id,
                    data_closing_creditors.account_group,
                    data_closing_creditors.opening_bank_balance as opening_bank_balance,
                    data_closing_creditors.closing_bank_balance as closing_bank_balance,
                    data_closing_creditors.opening_imprest as opening_imprest,
                    data_closing_creditors.closing_imprest as closing_imprest,
                    data_closing_creditors.opening_cash_in_hand as opening_cash_in_hand,
                    data_closing_creditors.closing_cash_in_hand as closing_cash_in_hand,
                    data_closing_creditors.opening_debtors as opening_debtors,
                    data_closing_creditors.closing_debtors as closing_debtors,
                    data_closing_creditors.opening_creditors as opening_creditors,
                    data_closing_creditors.closing_creditors_wo_uncleared as closing_creditors_wo_uncleared,
                    sum(os.opening_security_rent) as opening_security_rent
                    from data_closing_creditors
                    left join opening_security_rent os on data_closing_creditors.account = os.account
                    group by data_closing_creditors.account,
                    data_closing_creditors.user_type_id,
                    data_closing_creditors.opening_bank_balance,
                    data_closing_creditors.closing_bank_balance,
                    data_closing_creditors.opening_imprest,
                    data_closing_creditors.closing_imprest,
                    data_closing_creditors.opening_cash_in_hand,
                    data_closing_creditors.closing_cash_in_hand,
                    data_closing_creditors.opening_debtors,
                    data_closing_creditors.closing_debtors,
                    data_closing_creditors.opening_creditors,
                    data_closing_creditors.closing_creditors_wo_uncleared,
                    data_closing_creditors.account_group
                ),
                closing_security_rent as (
                    select
                    al.account_id as account,
                    ac.user_type_id as user_type_id,
                    ac.group_id as account_group,
                    sum(al.balance) as closing_security_rent
                    from account_move_line al
                    left join account_move am on am.id = al.move_id
                    join account_account ac on ac.id = al.account_id
                    where am.state = 'posted'
                    and ac.name in ('Security Deposit Rent','Other Security Deposit')
                    and al.date <= '%s'
                    group by al.account_id, ac.user_type_id, ac.group_id
                ),
                data_closing_security_rent as (
                    select
                    data_opening_security_rent.account,
                    data_opening_security_rent.user_type_id,
                    data_opening_security_rent.account_group,
                    data_opening_security_rent.opening_bank_balance as opening_bank_balance,
                    data_opening_security_rent.closing_bank_balance as closing_bank_balance,
                    data_opening_security_rent.opening_imprest as opening_imprest,
                    data_opening_security_rent.closing_imprest as closing_imprest,
                    data_opening_security_rent.opening_cash_in_hand as opening_cash_in_hand,
                    data_opening_security_rent.closing_cash_in_hand as closing_cash_in_hand,
                    data_opening_security_rent.opening_debtors as opening_debtors,
                    data_opening_security_rent.closing_debtors as closing_debtors,
                    data_opening_security_rent.opening_creditors as opening_creditors,
                    data_opening_security_rent.closing_creditors_wo_uncleared as closing_creditors_wo_uncleared,
                    data_opening_security_rent.opening_security_rent as opening_security_rent,
                    sum(cs.closing_security_rent) as closing_security_rent
                    from data_opening_security_rent
                    left join closing_security_rent cs on data_opening_security_rent.account = cs.account
                    group by data_opening_security_rent.account,
                    data_opening_security_rent.user_type_id,
                    data_opening_security_rent.opening_bank_balance,
                    data_opening_security_rent.closing_bank_balance,
                    data_opening_security_rent.opening_imprest,
                    data_opening_security_rent.closing_imprest,
                    data_opening_security_rent.opening_cash_in_hand,
                    data_opening_security_rent.closing_cash_in_hand,
                    data_opening_security_rent.opening_debtors,
                    data_opening_security_rent.closing_debtors,
                    data_opening_security_rent.opening_creditors,
                    data_opening_security_rent.closing_creditors_wo_uncleared,
                    data_opening_security_rent.opening_security_rent,
                    data_opening_security_rent.account_group
                )
                select row_number() OVER (ORDER BY account) AS id,
                account, user_type_id, opening_bank_balance, closing_bank_balance, opening_imprest,
                closing_imprest, opening_security_rent, closing_security_rent, opening_cash_in_hand, closing_cash_in_hand, opening_debtors,
                closing_debtors, opening_creditors, closing_creditors_wo_uncleared, account_group
                from data_closing_security_rent
            """ % (tuple(bank_all),tuple(bank), first_day, tuple(bank), last_day, first_day, last_day,
                   tuple(cash), first_day, tuple(cash), last_day, first_day, last_day,
                   first_day, last_day,
                   first_day,
                   last_day))

            result = self._cr.fetchall()
            rows = result
            values = ', '.join(map(str, rows))
            sql = ("""INSERT INTO debtor_creditor_account 
                        (id, account, user_type_id, opening_bank_balance, closing_bank_balance,
                        opening_imprest, closing_imprest, opening_security_rent, closing_security_rent, opening_cash_in_hand,
                        closing_cash_in_hand, opening_debtors, closing_debtors,
                        opening_creditors, closing_creditors_wo_uncleared, account_group
                        ) VALUES {}""".format(values).replace('None', 'null')
                   )
            self._cr.execute(sql)

            action = {
                'type': 'ir.actions.act_window',
                'views': [
                    (tree_view_id, 'tree'), (form_view_id, 'form'),
                    (graph_view_id, 'graph'), (pivot_view_id, 'pivot')
                ],
                'view_mode': 'tree,form',
                'name': _('Debtors & Creditors Report Account Wise: %s - %s' %(first_day.strftime('%d %b %y'),
                                                                  last_day.strftime('%d %b %y'))),
                'res_model': 'debtor.creditor.account',
                'search_view_id': search_view_ref and search_view_ref.id,
                'context': {'group_by': ['account_group']}
            }
            return action