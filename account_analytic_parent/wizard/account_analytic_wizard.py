# -*- coding: utf-8 -*-
# Copyright 2017 Tecnativa - Vicent Cubells
# Copyright 2017 Tecnativa - David Vidal
# License AGPL-3 - See http://www.gnu.org/licenses/agpl-3.0.html
# @api.multi
#     def return_action_to_open_travel(self):
#         """ This opens the xml view specified in xml_id for the current vehicle """
#         self.ensure_one()
#         xml_id = self.env.context.get('xml_id')
#         if xml_id:
#             res = self.env['ir.actions.act_window'].for_xml_id('serp_travel_expense', xml_id)
#             res.update(
#                 context=dict(self.env.context, default_vehicle_id=self.id),
#                 domain=[('vehicle_id', '=', self.id)]
#             )
#             return res
#         return False

#     @api.multi
#     def action_open_task(self):
#         ''' This function returns an action that display open form for curerent task.'''
#         action = self.env.ref('project.action_view_task')
#         result = action.read()[0]
#         result['context'] = {}
# #        task_ids = sum([task.id for task in self], [])
#         res = self.env.ref('project.view_task_form2', False)
#         result['views'] = [(res and res.id or False, 'form')]
#         result['res_id'] = self.id or False
#         return result

from odoo import api, fields, models

class AccountAnalyticChart(models.TransientModel):
    _name = 'account.analytic.chart'
    _description = 'Account Analytic Chart'

    from_date = fields.Date(string='From')
    to_date = fields.Date(string='To')

    @api.multi
    def analytic_account_chart_open_window(self):
        self.ensure_one()
        context = dict(self.env.context or {})
        mod_obj = self.env['ir.model.data']
        act_obj = self.env['ir.actions.act_window']
        # result = mod_obj.get_object_reference('account_analytic_parent',
        #                                       'view_account_analytic_account_report')
        # id = result and result[1] or False
        # result = dict(act_obj.with_context(context).browse(id)._context or {})
        # if self.from_date:
        #     result.update({'from_date': self.from_date})
        # if self.to_date:
        #     result.update({'to_date': self.to_date})
        # result['context'] = str(result)
        # ctx = result['context']
        # action = self.env['ir.actions.act_window'].for_xml_id(
        #     'account_analytic_parent', 'action_analytic_account_report')
        # result.update(action)
        # # result = dict(result.items() | action.items())
        # result['context'] = ctx

        action = self.env.ref('account_analytic_parent.action_analytic_account_report')
        result = action.read()[0]
        result['context'] = {}
        #        task_ids = sum([task.id for task in self], [])
        res = self.env.ref('account_analytic_parent.view_account_analytic_account_report1', False)
        print ("=========",res)
        result['view_id'] = res.id
        result['view_type'] = 'tree'
        # result.update(res)
        # result['views'] = [[res.id, 'tree']]
        # result['res_id'] = self.id or False
        return result
        # return result


'''
@api.model
    def setting_init_company_action(self):
        """ Called by the 'Company Data' button of the setup bar."""
        company = self.env.user.company_id
        view_id = self.env.ref('account.setup_view_company_form').id
        return {'type': 'ir.actions.act_window',
                'name': _('Company Data'),
                'res_model': 'res.company',
                'target': 'new',
                'view_mode': 'form',
                'res_id': company.id,
                'views': [[view_id, 'form']],
        }


'''
