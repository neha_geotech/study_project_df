# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'GTS Dream CRM',
    'summary': 'CRM and Project Changes',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'website': 'http://www.geotechnosoft.com',
    'category': 'CRM',
    'version': '11.0.1.0.0',
    'depends': [
        'crm', 'project', 'mail', 'sale', 'branch', 'hr_timesheet', 'gts_dream_purchase', 'base', 'analytic','product', 'hr', 'account'
    ],
    'data': [
        'security/crm_security.xml',
        'security/ir.model.access.csv',
        'data/project_mail_template_data.xml',
        'data/auth_signup_data.xml',
        'data/project_sequence.xml',
        'views/config_view.xml',
        'views/task_email.xml',
        'views/res_partner_view.xml',
        'views/crm_lead_view.xml',
        'views/project_view.xml',
    ],
    'installable': True,
}
