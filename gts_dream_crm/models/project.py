# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

from odoo import fields, models, api, tools, _
from datetime import datetime,date
from odoo.exceptions import Warning


class ProjectProject(models.Model):
    _inherit = 'project.project'

    project_name = fields.Char('Project Name')
    project_seq = fields.Char(
        'Reference',
        required=True,
        index=True,
        copy=False,
        default='New'
    )
    branch_id = fields.Many2one('res.branch', string='Branch')
    opportunity_id = fields.Many2one(
        'crm.lead',
        string='Related Opportunity',
    )
    date_deadline = fields.Date(related='opportunity_id.date_deadline', string='Expected Closing Date')

    quotation_id = fields.Many2one(
        'sale.order',
        string='Related Quotation',
    )
    product_id = fields.Many2one('product.product', 'Product Name')

    @api.multi
    def name_get(self):
        """
        Return the complete name
        """
        res = []
        for project in self:
            name = ''
            # name = ' [ ' + str(project.name) + ' ] '
            if project.project_name:
                name = str(project.project_name)
            res.append((project.id, name))
        return res

    @api.model
    def create(self, vals):
        """
        Writing fields of crm.lead and create sequence for project.project
        """
        seq_obj = self.env['ir.sequence']
        branch_obj = self.env['res.branch']
        if 'branch_id' in vals:
            branch_id_ = branch_obj.search([
                ('id', '=', vals['branch_id'])
            ])
            self = self.with_context(branch=branch_id_.id)
            seq_code = 'project.' + branch_id_.branch_code
            if branch_id_.branch_code:
                date_ = datetime.now()
                month_ = (date_.strftime("%b")).upper()
                year_ = date_.strftime("%y")
                vals['project_seq'] = 'PC' + '/' + str(
                    branch_id_.branch_code
                ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                    seq_code
                )
            if not branch_id_.branch_code:
                raise Warning(_(
                    'Branch code is not defined for branch %s'
                ) % branch_id_.name)
        # automatic addition of projects into stages by using case_default field
        project_ids = []
        stage_obj = self.env['project.task.type']
        project = super(ProjectProject, self).create(vals)
        stage_id = stage_obj.search([('case_default', '=', True)])
        project_ids.append(project.id)
        stage_id.update({'project_ids': [(6, 0, project_ids)]})
        # ###########################
        dict_ = {
            'name': project.name,
            'active': True,
            'categ_id': 2378,
            'expense_type':2,
            'state': 'confirmed',
            'type': 'consu'
        }
        product_id = self.env['product.product'].create(dict_)
        project.write({'product_id': product_id.id})
        active_ids = self.env.context.get('active_ids')
        if active_ids:
            crm_id = self.env['crm.lead'].browse(active_ids)
            crm_id.write({
                'project_id': project.id,
                'project_managers': project.user_id.id,
                'related_job_code': project.analytic_account_id.id,
                'product_id': project.product_id.id,
                'stage_id': 4
            })
        if project.quotation_id:
            project.quotation_id.write({
                'analytic_account_id': project.analytic_account_id.id,
                'project_id': project.id,
                'job_code': True
            })

        return project


class Task(models.Model):
    _inherit = "project.task"
    _order = 'create_date desc'

    # @api.depends('your_date_field', 'date_deadline')
    # def _compute_number_of_delays_days(self):
    #     date_format = '%Y-%m-%d'
    #     for res in self:
    #         if res.your_date_field and res.date_deadline:
    #             a = datetime.strptime(res.date_deadline, date_format)
    #             b = datetime.strptime(res.your_date_field, date_format)
    #             c = a - b
    #             res.delays = c.days

    priority = fields.Selection([
        ('0', 'Low'),
        ('1', 'Normal'),
        ('2', 'High'),
        ('3', 'Very High')
    ], default='0', index=True, string="Priority")
    achivements = fields.Char(string='% Achivements')
    task_done_date = fields.Date(string='Task Done Date')
    delays = fields.Char(string='Delays/Days Left')
    stage = fields.Char(related='stage_id.name')
    date_deadline = fields.Date(string='Deadline', index=True, copy=False, track_visibility='onchange')

    @api.multi
    def write(self, vals):
        res = super(Task, self).write(vals)
        if 'stage_id' in vals:
            if vals.get('stage_id') == 21:
                self.achivements = '100%'
                res = super(Task, self).write(vals)
        return res

    @api.model
    def create(self, vals):
        res = super(Task, self).create(vals)
        dict_ = {}
        for rec in res:
            if rec.active is True:
                if rec.date_deadline:
                    dict_['phone_number'] = rec.user_id.partner_id.mobile
                    dict_['content'] = "HI {name},\nA NEW TASK '{taskname}' HAS BEEN ASSIGNED " \
                                       "TO YOU WITH DEADLINE OF '{deadline}'.".format(
                        name=rec.user_id.name.upper(),
                        taskname=rec.name.upper(),
                        deadline=rec.date_deadline
                    )
                    sms = self.env['sms.sms'].create(dict_)
                    sms.send()
        return res

    @api.model
    def get_done_days(self):
        search_id = self.env['project.project'].search([])
        date_format = '%Y-%m-%d %H:%M:%S'
        date_format_ = '%Y-%m-%d'
        today_date = fields.Datetime.now()
        for data in search_id:
            search_id_task = self.env['project.task'].search([('project_id', '=', data.id)])
            template_obj = self.env['mail.template'].sudo().search([('name', '=', 'Task Done')], limit=1)
            rec = {}
            body = ''
            for res in search_id_task:
                if res.stage != 'Done' and res.active is True:
                    if res.date_deadline:
                        a = datetime.strptime(res.date_deadline, date_format_).date()
                        b = datetime.strptime(today_date, date_format).date()
                        c = a - b
                        res.delays = c.days
                        rec['phone_number'] = res.user_id.partner_id.mobile
                        if c.days in [-1, -2, -3, -4, -5, -6, -7] or c.days > 0:
                            if c.days != -1 and c.days in [-2, -3, -4, -5, -6, -7]:

                                rec['content'] = "HI {name},\nYOUR TASK '{taskname}' " \
                                                 "IS OVERDUE BY {overduedays} DAYS.".format(
                                    name=res.user_id.name.upper(), overduedays=c.days*-1, taskname=res.name.upper()
                                )
                                body = "HI {name},\nYOUR TASK '{taskname}' " \
                                                 "IS OVERDUE BY {overduedays} DAYS.".format(
                                    name=res.user_id.name.upper(), overduedays=c.days*-1, taskname=res.name.upper()
                                )

                            if c.days == -1:
                                rec['content'] = "HI {name},\nYOUR TASK '{taskname}' IS " \
                                                 "OVERDUE BY ONE DAY.".format(
                                    name=res.user_id.name.upper(), taskname=res.name.upper()
                                )
                                body = "HI {name},\nYOUR TASK '{taskname}' IS " \
                                                 "OVERDUE BY ONE DAY.".format(
                                    name=res.user_id.name.upper(), taskname=res.name.upper()
                                )
                            if c.days > 0 and c.days != 1:
                                rec['content'] = "HI {name},\n{Oneday} DAYS " \
                                                 "LEFT TO COMPLETE YOUR TASK '{taskname}'.".format(
                                    name=res.user_id.name.upper(), Oneday=c.days, taskname=res.name.upper()
                                )
                                body = "HI {name},\n{Oneday} DAYS " \
                                                 "LEFT TO COMPLETE YOUR TASK '{taskname}'.".format(
                                    name=res.user_id.name.upper(), Oneday=c.days, taskname=res.name.upper()
                                )
                            if c.days == 1:
                                rec['content'] = "HI {name},\nTODAY IS THE " \
                                                 "LAST DAY TO COMPLETE YOUR TASK '{taskname}'.".format(
                                    name=res.user_id.name.upper(), taskname=res.name.upper()
                                )
                                body = "HI {name},\nTODAY IS THE " \
                                                 "LAST DAY TO COMPLETE YOUR TASK '{taskname}'.".format(
                                    name=res.user_id.name.upper(), taskname=res.name.upper()
                                )
                            sms = self.env['sms.sms'].create(rec)
                            sms.send()
                            if template_obj:
                                mail_values = {
                                    'subject': template_obj.subject,
                                    'body_html': body,
                                    'email_to': res.user_id.partner_id.email,
                                    'email_from': template_obj.email_from,
                                }
                                # print('----------', res.user_id.partner_id.email,)
                                create_and_send_email = self.env['mail.mail'].create(mail_values).send()


class ProjectTaskType(models.Model):
    _inherit = "project.task.type"


    case_default= fields.Boolean(
        'Default for New Projects', help="If you check this field, this "
                                         "stage will be proposed by default on each new "
                                         "project. It will not assign this stage to existing projects."
    ) # Adding this field to avail the functionality of automatic addition of stages into project tasks