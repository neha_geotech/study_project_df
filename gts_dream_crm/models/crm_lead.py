# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

from odoo import fields, models, api, _
from datetime import datetime,date
from odoo.exceptions import UserError


class CrmLead(models.Model):
    _inherit = 'crm.lead'
    _order = 'create_date desc'

    @api.multi
    @api.depends('tag_ids','project_type_ids','company_type_id')
    def _get_tag_name(self):
        for res in self:
            _name = ''
            _project = ''
            _company_type=''
            for data in res.tag_ids:
                _name += str(data.name + '  ')
            for data in res.project_type_ids:
                _project += str(data.name + '  ')
            for data in res.company_type_id:
                _company_type += str(data.name + '  ')
            res.tags = _name
            res._project_type = _project
            res._company_type_new = _company_type

    @api.multi
    def invoice_value_(self):
        for data in self:
            if data.related_job_code:
                invoice_ids = self.env['account.invoice'].search(
                    [('job_code', '=', data.related_job_code.id), ('type', '=', 'out_invoice'), ('state', 'in', ('open', 'paid'))]
                )
                sum_ = 0.0
                for rec in invoice_ids:
                    sum_ += rec.amount_untaxed
                data.update({
                    'invoice_value': sum_,

                })

    @api.multi
    def _compute_total_internal_transfer(self):
        for data in self:
            sum_ = 0.0
            if data.related_job_code:
                analytic_line_ids = self.env['account.analytic.line'].search(
                    [('account_id', '=', data.related_job_code.id)]
                )
                for line in analytic_line_ids:
                    if line.amount < 0.0 and line.product_id.id is not False and line.expense_type.id == 1:
                        sum_ += line.amount

                data.update({
                    'total_it_value': sum_ * -1,

                })

    @api.multi
    def calculate_booked_nm_it(self):
        sum_ = 0.0
        for res in self:
            sum_ = res.analytic_value + res.total_it_value
            res.update({
                'total_nm_it': sum_
            })

    @api.depends('quotation_value_crm', 'bom_value')
    def _get_bom_percentage(self):
        for data in self:
            if data.sale_id.state == 'sale':
                val = 0.0
                if data.quotation_value_crm != 0.0:
                    val = (data.bom_value / data.quotation_value_crm) * 100
                data.update({
                    'bom_percentage': val
                })

    @api.depends('quotation_value_crm', 'analytic_value')
    def _get_analytic_percentage(self):
        for data in self:
            val = 0.0
            if data.quotation_value_crm != 0.0:
                val = (data.analytic_value / data.quotation_value_crm) * 100
            data.update({
                'analytic_per': val
            })

    @api.depends('purchase_value', 'analytic_value')
    def _get_purchase_percentage(self):
        for data in self:
            val = 0.0
            if data.analytic_value != 0.0:
                val = (data.purchase_value / data.analytic_value) * 100
            data.update({
                'purchase_per': val
            })

    @api.multi
    def total_quotation_value(self):
        for data in self:
            if data.related_job_code:
                order_ids = self.env['sale.order'].search(
                    [('analytic_account_id', '=', data.related_job_code.id)]
                )
                sum_ = 0.0
                for rec in order_ids:
                    sum_ += rec.amount_untaxed_new
                data.update({
                    'quotation_value_crm': sum_,

                })

    @api.depends('invoice_value')
    def _calc_filter(self):
        for data in self:
            if data.invoice_value > 0:
                data.invoice_filter = 'is_greater_zero'
            else:
                data.invoice_filter = 'is_zero'

    project_type_ids = fields.Many2many(
        'project.type',
        'partner_project_type_rel_lead',
        'crm_id',
        'pro_type_id', string='Project Type'
    )
    _project_type = fields.Char('Project Type', compute='_get_tag_name', store=True)
    date_deadline = fields.Date('Expected Closing', track_visibility='onchange')
    _company_type_new = fields.Char('Company Type', compute='_get_tag_name', store=True)
    tags = fields.Char('Tags', compute='_get_tag_name', store=True)
    project_id = fields.Many2one('project.project', string='Related Project', copy=False)
    sale_id = fields.Many2one('sale.order', string='Related Quotation', copy=False)
    project_type = fields.Many2one('project.type', string='Project Type')
    project_managers = fields.Many2one('res.users', string='Project Managers', copy=False)
    event_date = fields.Date(string='Event Date')
    setup_date = fields.Date(string='Setup Date')
    venue = fields.Text(string='Venue')
    branch_id = fields.Many2one('res.branch', 'Branch')
    hsn_code = fields.Char('Code', related='project_type.code', readonly=True)
    gst_no = fields.Char('GST No', related='partner_id.vat', readonly=True)
    sale_number = fields.Integer(
        compute='_compute_sale_amount_total',
        string="Number of Quotations", store=True
    )
    bom_created = fields.Boolean('BOM Created', default=False, copy=False)
    related_bom = fields.Many2one('mrp.bom', 'Bill of Material', copy=False)
    related_job_code = fields.Many2one('account.analytic.account', 'Job Code', copy=False)
    product_id = fields.Many2one('product.product', 'Product Name', copy=False)
    stage = fields.Selection([
        ('mail_profile', 'Mail-Profile'),
        ('mail_followup', 'Mail-Followup'),
        ('call_followup', 'Call-Follwup'),
        ('meeting_intro', 'Meeting-Intro'),
        ('enquiry_followup', 'Enquiry-Followup'),
        ('meeting_project', 'Meeting-Project')],
        string='Stage')
    company_type_id = fields.Many2one('res.company.type', string='Company Type', copy=False)
    trade = fields.Text(string='Trade')
    invoice_value = fields.Float(compute='invoice_value_')
    quotation_value_crm = fields.Float(compute='total_quotation_value')
    analytic_value = fields.Float(compute='_compute_total_analytic')
    total_it_value = fields.Float(compute='_compute_total_internal_transfer')
    total_nm_it = fields.Float(compute='calculate_booked_nm_it')
    analytic_per = fields.Float(compute='_get_analytic_percentage')
    purchase_value = fields.Float(compute='_compute_total_purchase')
    booked_mat = fields.Float(compute='_compute_total_move')
    purchase_per = fields.Float(compute='_get_purchase_percentage')
    bom_value = fields.Float(compute='_compute_total_mrp')
    bom_percentage = fields.Float(compute='_get_bom_percentage')
    stage_name = fields.Char(related='stage_id.name')
    delays_crm = fields.Char(compute='_closing_date')
    invoice_filter = fields.Selection([
        ('is_zero', 'Is Zero'),
        ('is_greater_zero', 'Is Greater Then Zero')
    ], string='Untaxed Invoice', compute='_calc_filter', store=True)


    # @api.multi
    # def button_create_bom(self):
    #     view_ref = self.env[
    #         'ir.model.data'
    #     ].get_object_reference(
    #         'mrp', 'mrp_bom_form_view'
    #     )
    #     view_id = view_ref[1] if view_ref else False
    #     if view_id:
    #         rec = {
    #             'type': 'ir.actions.act_window',
    #             'res_model': 'mrp.bom',
    #             'view_type': 'form',
    #             'view_mode': 'form',
    #             'view_id': view_id,
    #             'target': 'current',
    #             'context': {
    #                 'default_client_name': self.partner_id.id,
    #                 'default_branch_id': self.branch_id.id,
    #                 'default_opportunity_id': self.id,
    #                 'default_venue': self.venue,
    #                 'default_job_code': self.related_job_code.id,
    #                 'default_event_date': self.event_date,
    #                 # 'default_date_deadline': self.date_deadline,
    #                 'default_project_managers': self.project_managers.id,
    #                 'default_product_tmpl_id': self.product_id.product_tmpl_id.id,
    #                 'default_product_id': self.product_id.id
    #             }
    #         }
    #         return rec

    @api.model
    def _closing_date(self):
        date_format = '%Y-%m-%d %H:%M:%S'
        date_format_ = '%Y-%m-%d'
        today_date = fields.Datetime.now()
        rec = {}
        for data in self:
            if data.stage_name != 'Won' and data.active is True:
                if data.date_deadline:
                    a = datetime.strptime(data.date_deadline, date_format_).date()
                    b = datetime.strptime(today_date, date_format).date()
                    c = a - b
                    data.delays_crm = c.days
                    msg_date = datetime.strptime(data.date_deadline, date_format_).strftime('%d-%m-%Y')
                    rec['phone_number'] = data.user_id.partner_id.mobile
                    if c.days in [-1, -2, -3, 1, 2, 3]:
                        if c.days in [-1, -2, -3]:
                            rec['content'] = "HI {name},\nEXPECTED CLOSING DATE OF " \
                                             "OPPORTUNITY ' {opportunity}' HAS BEEN PAST " \
                                   "BY {days} DAYS, PLEASE TAKE APPROPRIATE ACTION.".format(
                                name=data.user_id.name.upper(), opportunity=data.name.upper(), days=c.days*-1
                            )
                        if c.days in [1, 2, 3]:
                            rec['content'] = "HI {name},\nEXPECTED CLOSING DATE OF THE OPPORTUNITY ' {opportunity}' " \
                                             "IS OF {dateclose}, PLEASE TAKE " \
                                             "THE APPROPRIATE ACTION i.e., MARK IT WON/LOST OR " \
                                             "SHIFT THE EXPECTED CLOSING DATE.".format(
                                name=data.user_id.name.upper(), opportunity=data.name.upper(), dateclose=msg_date
                            )
                        sms = self.env['sms.sms'].create(rec)
                        sms.send()

    @api.multi
    def action_set_won(self):
        self = self.with_context(flag=True)
        # res = super(CrmLead, self).action_set_won()
        view_ref = self.env[
            'ir.model.data'
        ].get_object_reference(
            'project', 'project_project_view_form_simplified'
        )
        view_id = view_ref[1] if view_ref else False
        # return res
        for lead in self:
            if lead.sale_id:
                if lead.sale_id.state == 'sale':
                    if not lead.related_job_code:
                        if view_id:
                            rec = {
                                'type': 'ir.actions.act_window',
                                'res_model': 'project.project',
                                'view_type': 'form',
                                'view_mode': 'form',
                                'view_id': view_id,
                                'target': 'current',
                                'context': {
                                    'default_project_name': lead.name,
                                    'default_branch_id': lead.branch_id.id,
                                    'default_opportunity_id': lead.id,
                                    'default_quotation_id': lead.sale_id.id,
                                }
                            }
                            return rec
                    stage_id = lead._stage_find(domain=[('probability', '=', 100.0), ('on_change', '=', True)])
                    if lead.related_job_code:
                        lead.write({'stage_id': stage_id.id, 'probability': 100})
                    if lead.user_id and lead.team_id and lead.planned_revenue:
                        query = """
                            SELECT
                                SUM(CASE WHEN user_id = %(user_id)s THEN 1 ELSE 0 END) as total_won,
                                MAX(CASE WHEN date_closed >= CURRENT_DATE - INTERVAL '30 days' AND user_id = %(user_id)s THEN planned_revenue ELSE 0 END) as max_user_30,
                                MAX(CASE WHEN date_closed >= CURRENT_DATE - INTERVAL '7 days' AND user_id = %(user_id)s THEN planned_revenue ELSE 0 END) as max_user_7,
                                MAX(CASE WHEN date_closed >= CURRENT_DATE - INTERVAL '30 days' AND team_id = %(team_id)s THEN planned_revenue ELSE 0 END) as max_team_30,
                                MAX(CASE WHEN date_closed >= CURRENT_DATE - INTERVAL '7 days' AND team_id = %(team_id)s THEN planned_revenue ELSE 0 END) as max_team_7
                            FROM crm_lead
                            WHERE
                                type = 'opportunity'
                            AND
                                active = True
                            AND
                                probability = 100
                            AND
                                DATE_TRUNC('year', date_closed) = DATE_TRUNC('year', CURRENT_DATE)
                            AND
                                (user_id = %(user_id)s OR team_id = %(team_id)s)
                        """
                        lead.env.cr.execute(query, {'user_id': lead.user_id.id,
                                                    'team_id': lead.team_id.id})
                        query_result = self.env.cr.dictfetchone()

                        message = False
                        if query_result['total_won'] == 1:
                            message = _('Go, go, go! Congrats for your first deal.')
                        elif query_result['max_team_30'] == lead.planned_revenue:
                            message = _('Boom! Team record for the past 30 days.')
                        elif query_result['max_team_7'] == lead.planned_revenue:
                            message = _('Yeah! Deal of the last 7 days for the team.')
                        elif query_result['max_user_30'] == lead.planned_revenue:
                            message = _('You just beat your personal record for the past 30 days.')
                        elif query_result['max_user_7'] == lead.planned_revenue:
                            message = _('You just beat your personal record for the past 7 days.')

                        if message:
                            return {
                                'effect': {
                                    'fadeout': 'slow',
                                    'message': message,
                                    'img_url': '/web/image/%s/%s/image' % (lead.team_id.user_id._name,
                                                                           lead.team_id.user_id.id) if lead.team_id.user_id.image else '/web/static/src/img/smile.svg',
                                    'type': 'rainbow_man',
                                }
                            }
                else:
                    raise UserError(_('Please confirm sale before marking Won.'))
            else:
                raise UserError(_('Please create quotation for the opportunity before marking Won.'))
        return True

    @api.multi
    def write(self, vals):
        res = super(CrmLead, self).write(vals)
        if not self.sale_id:
            if 'stage_id' in vals:
                if vals['stage_id'] == 4:
                    raise UserError(_('Please create quotation for the opportunity before marking Won.'))
        if 'flag' not in self._context:
            if 'default_project_name' not in self._context:
                if 'stage_id' in vals:
                    if vals['stage_id'] == 4:
                        raise UserError(_('You can not drag opportunity to won stage. \
                        Please click on Mark Won button to move the opportunity in won stage.'))
        return res