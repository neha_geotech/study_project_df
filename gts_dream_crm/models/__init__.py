# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

from . import config
from . import res_partner
from . import crm_lead
from . import project
