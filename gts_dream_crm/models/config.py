# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

from odoo import fields, models


class ProjectType(models.Model):
    _name = 'project.type'

    name = fields.Char(string='Name', required=True)
    code = fields.Char(string='HSN/SAC Code', required=True)
    company_id = fields.Many2one(comodel_name='res.company', string='Company', required=True,
        default=lambda self: self.env.user.company_id)


class CompanyType(models.Model):
    _name = 'res.company.type'

    name = fields.Char(string='Name', required=True)
