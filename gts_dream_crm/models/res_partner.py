# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

from odoo import fields, models, api, _


class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.multi
    @api.depends('project_type_ids', 'competitors', 'category_id')
    def _project_type_name(self):
        for res in self:
            _name = ''
            _tag = ''
            _competitors_name = ''
            for data in res.project_type_ids:
                _name += str(data.name + '  ')
            for data in res.competitors:
                _competitors_name += str(data.name + '  ')
            for data in res.category_id:
                _tag += str(data.name + '  ')
            res._project_type = _name
            res._competitors = _competitors_name
            res._tags = _tag

    @api.multi
    @api.depends('client_service','customers')
    def _get_client_service(self):
        for res in self:
            _name = ''
            _customers = ''
            for data in res.client_service:
                _name += str(data.name + '  ')
            for data in res.customers:
                _customers += str(data.name + ' ')
            res.client_service_ = _name
            res.customers_ = _customers

    job_id = fields.Many2one('hr.job', 'Job Position')
    client_service_ = fields.Char('Client Service', compute='_get_client_service', store=True)
    customers_ = fields.Char('Customers', compute='_get_client_service', store=True)
    project_type_ids = fields.Many2many(
        'project.type', 'partner_project_type_rel', 'partner_id',
        'pro_type_id', string='Project Type')
    trade = fields.Text(string='Trade')
    company_type_id = fields.Many2one('res.company.type', string='Company Type')
    rating = fields.Selection([('A-1', 'A-1'), ('A-2', 'A-2'), ('A-3', 'A-3'), ('A-BL', 'A-BL'),
        ('B-1', 'B-1'), ('B-2', 'B-2'), ('B-3', 'B-3'), ('B-BL', 'B-BL'),
        ('C-1', 'C-1'), ('C-2', 'C-2'), ('C-3', 'C-3'), ('C-BL', 'C-BL')], 'Rating')
    competitors = fields.Many2many('res.partner', 'partner_competitors_rel', 'partner_id',
        'competitor_id', string='Competitors')
    stage = fields.Selection([
        ('mail_profile', 'Mail-Profile'),
        ('mail_followup', 'Mail-Followup'),
        ('call_followup', 'Call-Follwup'),
        ('meeting_intro', 'Meeting-Intro'),
        ('enquiry_followup', 'Enquiry-Followup'),
        ('meeting_project', 'Meeting-Project')],
        string='Stage')
    user_id = fields.Many2one('res.users', string='Salesperson', index=True, track_visibility='onchange',
                              default=lambda self: self.env.user)
    client_service = fields.Many2many('res.users', 'client_service_relation', 'client_id', 'client_service_id', string='Client Service')
    customers = fields.Many2many('res.partner', 'customer_relation', 'customer_id', 'customer_service_id',
                                      string='Customers')
    _competitors = fields.Char('Competitors', compute='_project_type_name', store=True)
    _tags = fields.Char('Tags', compute='_project_type_name', store=True)
    name = fields.Char(index=True, default='')
    street = fields.Char(default='')
    street2 = fields.Char(default='')
    city = fields.Char(default='')
    vendor_type = fields.Selection([
        ('manufacturer', 'Manufacturer'),
        ('wholesaler', 'Wholesaler'),
        ('retailer', 'Retailer')
    ],
    string='Vendor Type')
    _product_type = fields.Selection([
        ('consu', 'Consumable'),
        ('service', 'Service'),
        ('stockable', 'Stockable Product')
    ])
    parallel_vendor = fields.Many2many('res.partner', 'partner_vendor_rel', 'partner_id_',
                                   'vendor_id_', string='Parallel Vendor')
    parent_id_ = fields.Many2one('product.category', 'Parent Category', index=True)
    categ_id_ = fields.Many2one('product.category', 'Product Category')
    department_id = fields.Many2one('hr.department', string='Department')
    branch_id = fields.Many2one('res.branch', string='Branch')
    _project_type = fields.Char(string='Project Type', compute='_project_type_name', store=True)
    is_an_executive = fields.Boolean(string='Is an Executive')
    msme_registration = fields.Boolean(string='Is MSME Registerd')
    msme_reg = fields.Char(string='MSME Reg. No')
    id_proof = fields.Selection(string="ID Proof", selection=[('aadhar_card', 'Aadhar Card'),
                                                              ('voter_id', 'Voter ID Card'),
                                                              ('driving_license', 'Driving license'),
                                                              ('pan_card', 'PAN Card'),
                                                              ('passport', 'Passport No')])
    id_proof_number = fields.Char(string='ID proof No.')

    @api.onchange('street', 'street2', 'city', 'name')
    def _compute_address_upper(self):
        for rec in self:
            rec.street = str(rec.street).title()
            rec.street2 = str(rec.street2).title()
            rec.city = str(rec.city).title()
            rec.name = str(rec.name).title()

    @api.multi
    def write(self, vals):
        res = super(ResPartner, self).write(vals)
        search_id = self.env['hr.employee'].search([('address_id', '=', self.id)])
        search_id.get_employee_details()
        return res