# -*- coding: utf-8 -*-

from odoo import api, fields, models


class CrmLeadLost(models.TransientModel):
    _inherit = 'crm.lead.lost'

    @api.multi
    def action_lost_reason_apply(self):
        rec = super(CrmLeadLost, self).action_lost_reason_apply()
        leads = self.env['crm.lead'].browse(self.env.context.get('active_ids'))
        if leads.sale_id:
            leads.sale_id.state = 'cancel'
        if leads.related_bom:
            leads.related_bom.active = False
        if leads.related_job_code:
            leads.related_job_code.active = False

        return rec
