# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _


class Partner(models.Model):
    _inherit = "res.partner"

    forecast_lines = fields.One2many('forecast.line', 'partner_id', string='Forecast Lines')
