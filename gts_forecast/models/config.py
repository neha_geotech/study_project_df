# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields, _

import calendar
import datetime

MONTH_LIST = [
    (1, 'January'),
    (2, 'February'),
    (3, 'March'),
    (4, 'April'),
    (5, 'May'),
    (6, 'June'),
    (7, 'July'),
    (8, 'August'),
    (9, 'September'),
    (10, 'October'),
    (11, 'November'),
    (12, 'December')
]


class forecastLine(models.Model):
    _name = 'forecast.line'
    _description = 'Forecasting'

    from_date = fields.Date('From Date')
    to_date = fields.Date('To Date')
    month = fields.Selection(MONTH_LIST, string='Month')
    year = fields.Integer('Year')
    forecast_amount = fields.Float('Amount')
    partner_id = fields.Many2one('res.partner', 'Shopping Mall')

    def _check_unique_line(self):
        for cur_rec in self:
            exist = self.search([('id', '!=', cur_rec.id), ('from_date', '=', cur_rec.from_date),
                                 ('partner_id', '=', cur_rec.partner_id.id)])
            if exist:
                return False
            exist = self.search([('id', '!=', cur_rec.id), ('to_date', '=', cur_rec.to_date),
                                 ('partner_id', '=', cur_rec.partner_id.id)])

            if exist:
                return False
        return True

    def _check_from_date(self):
        for cur_rec in self:
            from_date = datetime.datetime.strptime(cur_rec.from_date, "%Y-%m-%d")
            first_day = datetime.date(from_date.year, from_date.month, 1)
            if cur_rec.from_date != first_day.strftime('%Y-%m-%d'):
                return False
        return True

    def _check_to_date(self):
        for cur_rec in self:
            to_date = datetime.datetime.strptime(cur_rec.to_date, "%Y-%m-%d")
            start, num_days = calendar.monthrange(to_date.year, to_date.month)
            last_day = datetime.date(to_date.year, to_date.month, num_days)
            if cur_rec.to_date != last_day.strftime('%Y-%m-%d'):
                return False
        return True

    _constraints = [
        (_check_unique_line,
         _('There exists a forecast line for this date range! '), ['from_date', 'to_date']),
        (_check_from_date,
         _('Invalid From Date, It should be start of the month !'), ['from_date']),
        (_check_to_date,
         _('Invalid To Date, It should be end of the month !'), ['to_date'])
    ]
