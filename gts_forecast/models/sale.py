# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    from_date = fields.Date('From Date', required=True, default=fields.Datetime.now)
    to_date = fields.Date('To Date', required=True)
