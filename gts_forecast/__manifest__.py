# -*- coding: utf-8 -*-
# Part of GTS. See LICENSE file for full copyright and licensing details.
{
    'name' : 'GTS Forecast',
    'version' : '11.0',
    'summary': 'Module to provide forecast functionality',
    'author' : "Geo Technosoft",
    'sequence': 30,
    'description': """ """,
    'category' : 'Budget',
    'website': 'www.geotechnosoft.com',
    'depends' : ['base', 'sale', 'sales_team', 'product', 'crm', 'mail', 'sale_crm'],
    'data': [
        'security/security.xml',
        'views/res_partner_view.xml',
#        'views/sale_view.xml',
        'report/forecast_summary_report_views.xml',
        'security/ir.model.access.csv'
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}