# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, tools

MONTH_LIST = [
    (1, 'January'),
    (2, 'February'),
    (3, 'March'),
    (4, 'April'),
    (5, 'May'),
    (6, 'June'),
    (7, 'July'),
    (8, 'August'),
    (9, 'September'),
    (10, 'October'),
    (11, 'November'),
    (12, 'December')
]


class ForecastReport(models.Model):
    """ Forecast Analysis """
    _name = "forecast.summary.report"
    _auto = False
    _description = "Forecast Analysis"
    _rec_name = 'id'

    date_order = fields.Date('Order Date', readonly=True)
    # month = fields.Selection(MONTH_LIST, string='Month', readonly=True)
    # year = fields.Char('Year', readonly=True)
    partner_id = fields.Many2one('res.partner', 'Customer', readonly=True)
    parent_id = fields.Many2one('res.partner', 'Group Company', readonly=True)
    product_id = fields.Many2one('product.product', 'Product', readonly=True)
    team_id = fields.Many2one('crm.team', 'Sales Team', readonly=True)
    # user_id = fields.Many2one('res.users', 'Sales Person', readonly=True)
    lead_id = fields.Many2one('crm.lead', "Lead", readonly=True)
    # company_id = fields.Many2one('res.company', 'Company', readonly=True)
    forecast_amount = fields.Float('Forecast', readonly=True)
    total_amount = fields.Float('Total Revenue', readonly=True)
    difference_amount = fields.Float('Difference', readonly=True)
    achievement_per = fields.Float('Achievement %', readonly=True)
    rating = fields.Char('Rating', readonly=True)
    sales_person = fields.Many2one('res.users', 'Sales Person', readonly=True)

    def init(self):
        tools.drop_view_if_exists(self._cr, 'forecast_summary_report')
        self._cr.execute("""
            CREATE VIEW forecast_summary_report AS (
                with forecast_data AS  (
                    select
                    fl.partner_id as partner_id,
                    rp.parent_id as parent_id,
                    fl.from_date as date_order,
                    fl.forecast_amount as forecast_amount,
                    0.0 AS difference_amount,
                    null::integer AS product_id,
                    0.0 AS total_amount,
                    null::integer AS lead_id,
                    null::integer AS company_id,
                    null::integer AS team_id,
                    rp.rating as rating,
                    rp.user_id as sales_person
                    from forecast_line fl
                    join res_partner as rp on rp.id = fl.partner_id
                    where partner_id is not null
                ),
                order_line_data AS (
                    select
                    so.partner_id as partner_id,
                    rp.parent_id as parent_id,
                    date_order::date as date_order,
                    0.0 AS forecast_amount,
                    0.0 AS difference_amount,
                    soline.product_id as product_id,
                    so.amount_untaxed_new AS total_amount,
                    so.opportunity_id AS lead_id,
                    so.company_id AS company_id,
                    so.team_id AS team_id,
                    rp.rating as rating,
                    rp.user_id as sales_person
                    from sale_order_line soline
                    LEFT JOIN sale_order so ON (soline.order_id = so.id)
                    join res_partner as rp on rp.id = so.partner_id
                    where so.partner_id is not null and so.state = 'sale'
                ),
                report_data AS (
                    select * from forecast_data
                    UNION
                    select * from order_line_data
                )
                select ROW_NUMBER() OVER () AS id, partner_id, parent_id, date_order, lead_id, team_id,
                forecast_amount, product_id, total_amount, rating, sales_person, 
                (total_amount - forecast_amount) as difference_amount
                from report_data


            )""")
