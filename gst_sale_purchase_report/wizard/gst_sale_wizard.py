from odoo import api, models, fields, _

import xlsxwriter
import base64
import datetime
from datetime import date


class WizardGstSale(models.TransientModel):
    _name = 'gst.sale'

    start_date = fields.Date('From Date')
    end_date = fields.Date('To Date')
    branch_id = fields.Many2one('res.branch', 'Branch')

    @api.multi
    def print_sale_gst_report(self, vals):
        f_name = '/tmp/gstsale.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        worksheet = workbook.add_worksheet('GST Sale')
        worksheet.set_column('A:N', 12)
        date_format = workbook.add_format({'num_format': 'd-mmm-yyyy',
                                           'align': 'center'})
        bold_size_format = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        bold_size_format.set_font_size(12)
        align_value = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter'})

        domain = [('type', 'in', ['out_invoice']),('invoice_id.state', '!=', 'draft')]
        if self.start_date and self.end_date:
            domain += [('date_invoice', '>=', self.start_date),
                ('date_invoice', '<=', self.end_date)
            ]
        if self.start_date and not self.end_date:
            domain += [('date_invoice', '>=', self.start_date),
                ('date_invoice', '<=', fields.Datetime.now())
            ]
        if self.branch_id:
            domain += [
                ('branch_id', '=', self.branch_id.id)
            ]
        if self.branch_id and self.start_date and self.end_date:
            domain += [
                ('date_invoice', '>=', self.start_date),
                ('date_invoice', '<=', self.end_date),
                ('branch_id', '=', self.branch_id.id)
                 ]

        invoice_search = self.env['account.invoice.line'].search(domain)

        row = 1
        new_row = row + 1

        worksheet.write('A%s' % (row), 'Date', bold_size_format)
        worksheet.write('B%s' % (row), 'Customer Name', bold_size_format)
        worksheet.write('C%s' % (row), 'Customer Invoice No.', bold_size_format)
        worksheet.write('D%s' % (row), 'Product',bold_size_format)
        worksheet.write('E%s' % (row), 'HSN/SAC', bold_size_format)
        worksheet.write('F%s' % (row), 'GST No.', bold_size_format)
        worksheet.write('G%s' % (row), 'State Code', bold_size_format)
        worksheet.write('H%s' % (row), 'Rate of Purchase', bold_size_format)
        worksheet.write('I%s' % (row), 'Qty', bold_size_format)
        worksheet.write('J%s' % (row), 'Basic Amt.', bold_size_format)
        worksheet.write('K%s' % (row), 'IGST', bold_size_format)
        worksheet.write('L%s' % (row), 'CGST', bold_size_format)
        worksheet.write('M%s' % (row), 'SGST', bold_size_format)
        worksheet.write('N%s' % (row), 'Total Amount.', bold_size_format)

        sub_total = cgst = sgst = igst = grand_total = 0.0
        for obj in invoice_search:
            state_ = str(obj.partner_id.state_id.state_code) + '-' + str(obj.partner_id.state_id.name)
            if obj.date_invoice:
                inv_date = datetime.datetime.strptime(str(obj.date_invoice), '%Y-%m-%d')
            else:
                inv_date = ''
            worksheet.write('A%s' % (new_row), inv_date, date_format)
            worksheet.write('B%s' % (new_row), obj.invoice_id.partner_id.name, align_value)
            # if obj.invoice_id.number:
            worksheet.write('C%s' % (new_row), obj.invoice_id.number, align_value)
            # else:
            #     worksheet.write('C%s' % (new_row), 'Draft Invoice', align_value)
            worksheet.write('D%s' % (new_row), obj.name, align_value)
            if obj.hsn_id.name:
                worksheet.write('E%s' % (new_row), obj.hsn_id.name, align_value)
            else:
                worksheet.write('E%s' % (new_row), '-', align_value)
            if obj.partner_id.vat:
                worksheet.write('F%s' % (new_row), obj.partner_id.vat, align_value)
            else:
                worksheet.write('F%s' % (new_row), '-', align_value)

            worksheet.write('G%s' % (new_row), state_, align_value)
            worksheet.write('H%s' % (new_row), obj.price_unit, align_value)

            worksheet.write('I%s' % (new_row), obj.quantity, align_value)
            worksheet.write('J%s' % (new_row), obj.price_subtotal, align_value)
            worksheet.write('K%s' % (new_row), obj.igst, align_value)
            worksheet.write('L%s' % (new_row), obj.cgst, align_value)
            worksheet.write('M%s' % (new_row), obj.sgst, align_value)
            total = obj.price_subtotal+obj.igst+obj.cgst+obj.sgst
            worksheet.write('N%s' % (new_row), total, align_value)
            new_row += 1
            sub_total += obj.price_subtotal
            cgst += obj.cgst
            sgst += obj.sgst
            igst += obj.igst
            grand_total += total
        new_row += 2
        worksheet.write('H%s' % (new_row), 'Total', bold_size_format)
        worksheet.write('J%s' % (new_row), sub_total, bold_size_format)
        worksheet.write('K%s' % (new_row), igst, bold_size_format)
        worksheet.write('L%s' % (new_row), cgst, bold_size_format)
        worksheet.write('M%s' % (new_row), sgst, bold_size_format)
        worksheet.write('N%s' % (new_row), grand_total, bold_size_format)

        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'GST Sale Report'
        dt = 'From_' + str(self.start_date) + '' + '_To_' + str(self.end_date)
        out_wizard = self.env['xlsx.output'].create({
            'name': name+ '.xlsx',
            'xls_output': base64.encodebytes(data)
        })
        view_id = self.env.ref('geo_gst.xlsx_output_form').id
        return {
            'type': 'ir.actions.act_window',
            'name': _(name),
            'res_model': 'xlsx.output',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }
