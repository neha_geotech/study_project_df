from odoo import api, models, fields, _

import xlsxwriter
import base64
import xlwt
from xlwt import *
from xlwt import Workbook,easyxf
import datetime
from datetime import date


class WizardGstPurchase(models.TransientModel):
    _name = 'gst.purchase'

    start_date = fields.Date('From Date')
    end_date = fields.Date('To Date')
    branch_id = fields.Many2one('res.branch', 'Branch')

    @api.multi
    def print_purchase_gst_report(self, vals):
        f_name = '/tmp/gstpurchase.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        worksheet = workbook.add_worksheet('GST Purchase')
        worksheet.set_column('A:N', 12)
        date_format = workbook.add_format({'num_format': 'd-mmm-yyyy',
                                           'align': 'center'})
        bold_size_format = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        bold_size_format.set_font_size(12)
        align_value = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter'})
        domain = [('type', 'in', ['in_invoice'])]
        account_id = self.env['account.account'].search([('name', '=', 'Creditors For Purchases')], limit=1)
        if self.start_date and self.end_date:
            domain += [('date_invoice', '>=', self.start_date),
                ('date_invoice', '<=', self.end_date)
            ]
        if self.start_date and not self.end_date:
            domain += [('date_invoice', '>=', self.start_date),
                ('date_invoice', '<=', fields.Datetime.now())
            ]
        if self.branch_id:
            domain += [
                ('branch_id', '=', self.branch_id.id)
                ]
        if self.branch_id and self.start_date and self.end_date:
            domain += [
                  ('date_invoice', '>=', self.start_date),
                  ('date_invoice', '<=', self.end_date),
                  ('branch_id', '=', self.branch_id.id)
                      ]
        # ================================domain1 for gst report from move line========
        domain1 = []
        if self.start_date and self.end_date:
            domain1 += [('date', '>=', self.start_date),
                ('date', '<=', self.end_date)
            ]
        if self.start_date and not self.end_date:
            domain1 += [('date', '>=', self.start_date),
                ('date', '<=', fields.Datetime.now())
            ]
        # if self.branch_id:
        #     domain1 += [
        #         ('branch_id', '=', self.branch_id.id)
        #         ]
        if self.branch_id and self.start_date and self.end_date:
            domain1 += [
                  ('date', '>=', self.start_date),
                  ('date', '<=', self.end_date)

                      ]

        invoice_search = self.env['account.invoice.line'].search(domain)
        moves = self.env['account.move'].search(domain1)
        row = 1
        new_row = row + 1

        worksheet.write('A%s' % (row), 'Date', bold_size_format)
        worksheet.write('B%s' % (row), 'Customer Name', bold_size_format)
        worksheet.write('C%s' % (row), 'Customer Invoice No.', bold_size_format)
        worksheet.write('D%s' % (row), 'Product', bold_size_format)
        worksheet.write('E%s' % (row), 'HSN/SAC', bold_size_format)
        worksheet.write('F%s' % (row), 'GST No.', bold_size_format)
        worksheet.write('G%s' % (row), 'Rate of Purchase', bold_size_format)
        worksheet.write('H%s' % (row), 'Qty', bold_size_format)
        worksheet.write('I%s' % (row), 'Basic Amt.', bold_size_format)
        worksheet.write('J%s' % (row), 'IGST', bold_size_format)
        worksheet.write('K%s' % (row), 'CGST', bold_size_format)
        worksheet.write('L%s' % (row), 'SGST', bold_size_format)
        worksheet.write('M%s' % (row), 'Total Amount.', bold_size_format)

        sub_total = cgst = sgst = igst = grand_total = 0.0
        for obj in invoice_search:
            if obj.invoice_id.state in ('open','paid'):
                if obj.date_invoice:
                    inv_date = datetime.datetime.strptime(str(obj.date_invoice), '%Y-%m-%d')
                else:
                    inv_date = ''
                worksheet.write('A%s' % (new_row), inv_date, date_format)
                worksheet.write('B%s' % (new_row), obj.invoice_id.partner_id.name, align_value)
                if obj.invoice_id.number:
                    worksheet.write('C%s' % (new_row), obj.invoice_id.number, align_value)
                else:
                    worksheet.write('C%s' % (new_row), 'Draft Invoice', align_value)
                worksheet.write('D%s' % (new_row), obj.product_id.name, align_value)
                if obj.hsn_id.name:
                    worksheet.write('E%s' % (new_row), obj.hsn_id.name, align_value)
                else:
                    worksheet.write('E%s' % (new_row), '-', align_value)
                if obj.partner_id.vat:
                    worksheet.write('F%s' % (new_row), obj.partner_id.vat, align_value)
                else:
                    worksheet.write('F%s' % (new_row), '-', align_value)
                worksheet.write('G%s' % (new_row), obj.price_unit, align_value)

                worksheet.write('H%s' % (new_row), obj.quantity, align_value)
                worksheet.write('I%s' % (new_row), obj.price_subtotal, align_value)
                worksheet.write('J%s' % (new_row), obj.igst, align_value)
                worksheet.write('K%s' % (new_row), obj.cgst, align_value)
                worksheet.write('L%s' % (new_row), obj.sgst, align_value)
                total = obj.price_subtotal+obj.igst+obj.cgst+obj.sgst
                worksheet.write('M%s' % (new_row), total, align_value)
                new_row += 1

                sub_total += obj.price_subtotal
                cgst += obj.cgst
                sgst += obj.sgst
                igst += obj.igst
                grand_total += total
        new_row = new_row + 1
        igst_lst = []
        scgst_lst = []
        ac_amount_lst = []
        ac_amount_lst1 = []
        for mv in moves:
            for ml in mv.line_ids:
                if not ml.invoice_id:
                    if self.branch_id:
                        if ml.branch_id == self.branch_id:
                        # if ml.account_id.name == 'Creditors For Expenses' or ml.account_id.name == 'Creditors For Purchases'\
                        #         or ml.account_id.name == 'Rent Payable' or ml.account_id.name == 'Creditors For Capital Goods'\
                        #         or ml.account_id.name == 'Electricity Expenses Payable':
                        #     ac_amount_lst.append(ml.credit)

                            if ml.account_id.name == 'Input IGST':
                                igst_lst.append(ml.debit)
                                igst += ml.debit

                            if ml.account_id.name == 'Input CGST':
                                scgst_lst.append(ml.debit)
                                cgst += ml.debit

                            if ml.account_id.name == 'Input SGST':
                                scgst_lst.append(ml.debit)
                                sgst += ml.debit
                    else:

                        if ml.account_id.name == 'Input IGST':
                            igst_lst.append(ml.debit)
                            igst += ml.debit

                        if ml.account_id.name == 'Input CGST':
                            scgst_lst.append(ml.debit)
                            cgst += ml.debit

                        if ml.account_id.name == 'Input SGST':
                            scgst_lst.append(ml.debit)
                            sgst += ml.debit
            if igst_lst:
                # if ac_amount_lst:
                #     worksheet.write('I%s' % (new_row), ac_amount_lst[0], align_value)
                # else:
                #     worksheet.write('I%s' % (new_row), 0, align_value)
                worksheet.write('J%s' % (new_row), igst_lst[0] or False, align_value)
                worksheet.write('K%s' % (new_row), 0 or False, align_value)
                worksheet.write('L%s' % (new_row), 0 or False, align_value)
                worksheet.write('A%s' % (new_row), ml.date, date_format)
                worksheet.write('B%s' % (new_row), ml.partner_id.name, align_value)
                worksheet.write('C%s' % (new_row), ml.move_id.name, align_value)
                scgst_lst = []
                igst_lst = []
                ac_amount_lst = []
                new_row = new_row + 1
            if scgst_lst:
                # if ac_amount_lst:
                #     worksheet.write('I%s' % (new_row), ac_amount_lst[0], align_value)
                # else:
                #     worksheet.write('I%s' % (new_row), 0, align_value)
                worksheet.write('J%s' % (new_row), 0 or False, align_value)
                worksheet.write('K%s' % (new_row), scgst_lst[0] or False, align_value)
                worksheet.write('L%s' % (new_row), scgst_lst[1] or False, align_value)
                worksheet.write('A%s' % (new_row), ml.date, date_format)
                worksheet.write('B%s' % (new_row), ml.partner_id.name, align_value)
                worksheet.write('C%s' % (new_row), ml.move_id.name, align_value)
                scgst_lst = []
                igst_lst = []
                new_row = new_row + 1
                ac_amount_lst = []
        new_row += 2
        worksheet.write('G%s' % (new_row), 'Total', bold_size_format)
        worksheet.write('I%s' % (new_row), sub_total, bold_size_format)
        worksheet.write('J%s' % (new_row), igst, bold_size_format)
        worksheet.write('K%s' % (new_row), cgst, bold_size_format)
        worksheet.write('L%s' % (new_row), sgst, bold_size_format)
        worksheet.write('M%s' % (new_row), grand_total, bold_size_format)

        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'GST Purchase Report'
        dt = 'From_' + str(self.start_date) + '' + '_To_' + str(self.end_date)
        out_wizard = self.env['xlsx.output'].create({'name': name + '.xlsx',
                                                     'xls_output': base64.encodebytes(data)})
        view_id = self.env.ref('geo_gst.xlsx_output_form').id
        return {
            'type': 'ir.actions.act_window',
            'name': _(name),
            'res_model': 'xlsx.output',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }
