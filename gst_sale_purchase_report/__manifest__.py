# -*- coding: utf-8 -*-
##############################################################################
#
#    India-GST
#
#    GEOTECHNO SOFT Pvt. Ltd.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{

    'name': 'India-GST',
    'description': "Goods & Service-Tax.",
    'version': '1.0.1',
    'category': 'Accounting',
    'author': 'Geo Technosoft Pvt Ltd.',
    'website' : 'https://www.geotechnosoft.com',
    'summary': 'Indian GST Reports',
    'license': 'AGPL-3',
    'depends': ['sale','purchase','account','sale_management', 'account_invoicing'],
    'data': [
        
        'wizard/gst_sale_wizard.xml',
        'wizard/gst_purchase_wizard.xml',
        'wizard/gst_credit_note_view.xml',
        'wizard/gst_debit_note_view.xml',
        'views/purchase_order_line_report.xml',
    ],
    'images': ['static/description/banner.png'],
}
