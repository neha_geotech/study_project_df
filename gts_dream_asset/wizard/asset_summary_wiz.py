from odoo import api, fields, models,_
import xlsxwriter
import base64
import xlwt
from xlwt import *
from xlwt import Workbook,easyxf
import datetime
from datetime import date

class AssetSummary(models.TransientModel):
    _name = 'asset.summary'

    from_date = fields.Date('From Date')
    end_date = fields.Date('To Date')

    @api.multi
    def export_summary_report(self):
        f_name = '/tmp/summaryreport.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        worksheet = workbook.add_worksheet('Asset Summary Report')
        worksheet.set_column('A:N', 12)
        date_format = workbook.add_format({'num_format': 'd-mmm-yyyy',
                                           'align': 'center'})
        bold_size_format = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        bold_size_format.set_font_size(12)
        align_value = workbook.add_format({
            'align': 'left',
            'valign': 'vcenter'})

        row = 4
        new_row = row + 1
        worksheet.merge_range('A1:B2', "Date From", bold_size_format)
        worksheet.merge_range('E1:F2', "Date To", bold_size_format)
        worksheet.merge_range('C1:D2', self.from_date, date_format)
        worksheet.merge_range('G1:H2', self.end_date, date_format)
        worksheet.write('A%s' % (row), 'Particulars', bold_size_format)
        worksheet.write('B%s' % (row), 'Opening Gross Block', bold_size_format)
        worksheet.write('C%s' % (row), 'Additions During Year', bold_size_format)
        worksheet.write('D%s' % (row), 'Sale/Adjustments Dusring year', bold_size_format)
        worksheet.write('E%s' % (row), 'Gross Block', bold_size_format)
        worksheet.write('F%s' % (row), 'Accumulated Depreciation', bold_size_format)
        worksheet.write('G%s' % (row), 'Depreciation For Year', bold_size_format)
        worksheet.write('H%s' % (row), 'Adjustments', bold_size_format)
        worksheet.write('I%s' % (row), 'Accumulated Depreciation Closing', bold_size_format)
        worksheet.write('J%s' % (row), 'Opening WDV', bold_size_format)
        worksheet.write('K%s' % (row), 'Closing WDV', bold_size_format)

        category = self.env['account.asset.category'].search([])
        asset_obj = self.env['account.asset.asset']
        closing_gross = accum_dep_closing = opening_wdv = closing_wdv = 0.0
        total = 0.0
        for categ in category:
            worksheet.write('A%s' % (new_row), categ.name, align_value)
            self._cr.execute('''
                                select sum(purchase_cost) 
                                from account_asset_asset
                                where date <= %s
                                AND category_id = %s
                                ''',
                             (self.from_date, categ.id))
            open_gross_bal = self._cr.fetchall()
            self._cr.execute('''
                                select sum(value) 
                                from account_asset_asset
                                where date >= %s
                                and date <= %s
                                AND category_id = %s
                                ''',
                             (self.from_date, self.end_date, categ.id))
            current_year_bal = self._cr.fetchall()
            self._cr.execute('''
                                select sum(value) 
                                from account_asset_asset
                                where date >= %s
                                and date <= %s
                                AND category_id = %s
                                and state = 'sale_dispose'
                                ''',
                             (self.from_date, self.end_date, categ.id))
            sale_current_year = self._cr.fetchall()
            self._cr.execute('''
                                select sum(deprecation) 
                                from account_asset_asset
                                where date < %s
                                AND category_id = %s
                                ''',
                             (self.from_date, categ.id))
            depreciation_opening = self._cr.fetchall()
            self._cr.execute('''
                                select sum(amount) 
                                from account_asset_depreciation_line
                                where depreciation_date >= %s
                                and depreciation_date_to <= %s
                                AND category_id = %s
                                ''',
                             (self.from_date, self.end_date, categ.id))
            depreciation_current_year = self._cr.fetchall()
            self._cr.execute('''
                                select sum(amount) 
                                from account_asset_depreciation_line
                                where depreciation_date >= %s
                                and depreciation_date <= %s
                                AND category_id = %s
                                and state = 'sale_dispose'
                                ''',
                             (self.from_date, self.end_date, categ.id))
            adjustment_year_bal = self._cr.fetchall()
            if not current_year_bal[0][0]:
                current_year = 0.0
            if not open_gross_bal[0][0]:
                open_gross = 0.0
            if not sale_current_year[0][0]:
                sale_year = 0.0
            if current_year_bal[0][0]:
                current_year = current_year_bal[0][0]
            if open_gross_bal[0][0]:
                open_gross = open_gross_bal[0][0]
            if sale_current_year[0][0]:
                sale_year = sale_current_year[0][0]
            closing_gross = (open_gross + current_year) - sale_year

            if not depreciation_opening[0][0]:
                dep_open = 0.0
            if not depreciation_current_year[0][0]:
                dep_current = 0.0
            if not adjustment_year_bal[0][0]:
                adj_bal = 0.0
            if depreciation_opening[0][0]:
                dep_open = depreciation_opening[0][0]
            if depreciation_current_year[0][0]:
                dep_current = depreciation_current_year[0][0]
            if adjustment_year_bal[0][0]:
                adj_bal = adjustment_year_bal[0][0]
            accum_dep_closing = (dep_open + dep_current) - adj_bal

            if not open_gross_bal[0][0]:
                wdv = 0
            if not depreciation_opening[0][0]:
                wdv1 = 0
            if open_gross_bal[0][0]:
                wdv = open_gross_bal[0][0]
            if depreciation_opening[0][0]:
                wdv1 = depreciation_opening[0][0]
            opening_wdv = wdv - wdv1

            if closing_gross > 0 or accum_dep_closing > 0:
                closing_wdv = closing_gross - accum_dep_closing

            worksheet.write('B%s' % (new_row), open_gross_bal[0][0] or 0, align_value)
            worksheet.write('C%s' % (new_row), current_year_bal[0][0] or 0, align_value)
            worksheet.write('D%s' % (new_row), sale_current_year[0][0] or 0, align_value)
            worksheet.write('E%s' % (new_row),  closing_gross, align_value)
            worksheet.write('F%s' % (new_row), depreciation_opening[0][0] or 0, align_value)
            worksheet.write('G%s' % (new_row), depreciation_current_year[0][0] or 0, align_value)
            worksheet.write('H%s' % (new_row), adjustment_year_bal[0][0] or 0, align_value)
            worksheet.write('I%s' % (new_row), accum_dep_closing, align_value)
            worksheet.write('J%s' % (new_row), opening_wdv, align_value)
            worksheet.write('K%s' % (new_row), closing_wdv, align_value)
            total = 0.0
            new_row += 1
        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'Asset Summary Report'
        dt = 'From_' + str(self.from_date) + '' + '_To_' + str(self.end_date)
        out_wizard = self.env['xlsx.output'].create({'name': name + '.xlsx',
                                                     'xls_output': base64.encodebytes(data)})
        view_id = self.env.ref('geo_gst.xlsx_output_form').id
        return {
            'type': 'ir.actions.act_window',
            'name': _(name),
            'res_model': 'xlsx.output',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }

