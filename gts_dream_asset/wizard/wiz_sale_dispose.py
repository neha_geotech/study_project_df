from odoo import api, fields, models,_
from odoo.tools import float_compare, float_is_zero


class SaleDisposeWiz(models.TransientModel):
    _name = 'sale.dispose.wiz'

    partner_id = fields.Many2one('res.partner', 'Partner')
    amount = fields.Float('Amount')
    sale_date = fields.Date('Sale Date')
    account_id = fields.Many2one('account.account', 'Account')

    @api.multi
    def sale_dispose(self):
        active_ids = self.env.context.get('active_ids')
        prec = self.env['decimal.precision'].precision_get('Account')
        account_move = self.env['account.move']
        move_line = self.env['account.move.line']
        ml_list1 = []
        if active_ids:
            asset_id = self.env['account.asset.asset'].browse(active_ids)
            asset_id.write({'state': 'sale_dispose'})
            company_currency = asset_id.company_id.currency_id
            current_currency = asset_id.currency_id
            amount = current_currency.with_context(date=self.sale_date).compute(self.amount, company_currency)
            asset_name = asset_id.name
            if asset_id.depreciation_line_ids:
                move_ids = asset_id._get_disposal_moves(self.amount, self.sale_date)
                if move_ids:
                    name = _('Disposal Move')
                    view_mode = 'form'
                    if len(move_ids) > 1:
                        name = _('Disposal Moves')
                        view_mode = 'tree,form'
                    return {
                        'name': name,
                        'view_type': 'form',
                        'view_mode': view_mode,
                        'res_model': 'account.move',
                        'type': 'ir.actions.act_window',
                        'target': 'current',
                        'res_id': move_ids[0],
                    }
            else:
                move_line_1 = {
                    'name': asset_name,
                    'account_id': asset_id.category_id.account_asset_id.id,
                    'debit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
                    'credit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
                    'journal_id': asset_id.category_id.journal_id.id,
                    'partner_id': asset_id.partner_id.id,
                    'analytic_account_id': asset_id.category_id.account_analytic_id.id if asset_id.category_id.type == 'sale' else False,
                    'currency_id':  company_currency.id,
                    'amount_currency': company_currency != current_currency and - 1.0 * amount or 0.0,
                    'branch_id': asset_id.branch_id.id
                }
                move_line_2 = {
                    'name': asset_name,
                    'account_id': asset_id.category_id.sale_account_id.id,
                    'credit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
                    'debit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
                    'journal_id': asset_id.category_id.journal_id.id,
                    'partner_id': asset_id.partner_id.id,
                    'analytic_account_id': asset_id.category_id.account_analytic_id.id if asset_id.category_id.type == 'purchase' else False,
                    'currency_id':  company_currency.id,
                    'amount_currency': company_currency != current_currency and amount or 0.0,
                    'branch_id': asset_id.branch_id.id
                }
                move_vals = {
                    'ref': 'Asset',
                    'date': self.sale_date,
                    'journal_id': asset_id.category_id.journal_id.id,
                    'job_code': asset_id.job_code.id,
                    'line_ids': [(0, 0, move_line_1), (0, 0, move_line_2)],
                }
                move_id = self.env['account.move'].create(move_vals)
                print(move_id)
                asset_id.write({'move_id': move_id.id, 'move_check': True})
                move_ids = move_id

                # line.write({'move_id': move_ids.id, 'move_check': True})
                # created_moves |= move

                if move_ids:
                    name = _('Disposal Move')
                    view_mode = 'form'
                    if len(move_ids) > 1:
                        name = _('Disposal Moves')
                        view_mode = 'tree,form'
                    return {
                        'name': name,
                        'view_type': 'form',
                        'view_mode': view_mode,
                        'res_model': 'account.move',
                        'type': 'ir.actions.act_window',
                        'target': 'current',
                        'res_id': move_ids[0].id,
                    }

