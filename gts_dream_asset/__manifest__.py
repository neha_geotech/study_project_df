{
    'name': 'Assets Dream Factory',
    'version': '1.0',
    'category': '',
    'sequence': 75,
    'summary': 'Dream Factory ',
    'description': "",
    'website': '',
    'depends': ['account','account_asset', 'branch','geo_gst'],
    'data': [
        'wizard/sale_dispose_wiz_view.xml',
        'wizard/asset_summary_view.xml',
        'views/asset_view.xml',
        'views/asset_category.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
