import calendar
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo.tools import float_compare, float_is_zero
from dateutil import parser
import math


class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'

    @api.multi
    @api.depends('value', 'limit_percentage')
    def _salvage_value(self):
        val = 0.0
        for rec in self:
            val = (rec.limit_percentage / 100) * rec.value
            rec.salvage_value = math.ceil(val)

    res_value = fields.Float(related='value_residual', store=True)
    branch_id = fields.Many2one('res.branch', 'Branch')
    purchase_cost = fields.Float('Purchase Cost')
    deprecation = fields.Float('Depreciation Cost')
    no_of_revise = fields.Integer('No of Revious Year')
    revise_date = fields.Date('Revise Date')
    revise_year_line = fields.One2many('revise.year.line', 'ac_asset_id')
    limit_percentage = fields.Float(string='Limit Percentage')
    up_to_use = fields.Date('Up To Use')
    job_code = fields.Many2one(
        'account.analytic.account',
        string='Job Code',

    )
    purchase_date = fields.Date('Purchase Date', default=fields.Datetime.now)
    salvage_value = fields.Float(string='Salvage Value', digits=0,
                                states={'draft': [('readonly', False)]}, )
    import_date = fields.Date('Import Date')
    date = fields.Date(string='Date', states={'draft': [('readonly', False)]},
                        oldname="purchase_date")
    state = fields.Selection([('draft', 'Draft'), ('open', 'Running'), ('sale_dispose', 'Sale'),
                              ('close', 'Close')], 'Status', required=True,
                             copy=False, default='draft',
                             help="When an asset is created, the status is 'Draft'.\n"
                                  "If the asset is confirmed, the status goes in 'Running' and the depreciation lines can be posted in the accounting.\n"
                                  "You can manually close an asset when the depreciation is over. If the last line of depreciation is posted, the asset automatically goes in that status.")

    # @api.onchange('import_date', 'date')
    @api.multi
    def onchange_method_number(self):
        if self.date and self.import_date:
            p_date = datetime.strptime(self.purchase_date, "%Y-%m-%d")
            imp_date = datetime.strptime(self.import_date, "%Y-%m-%d")

            if (imp_date.year - p_date.year) < self.category_id.method_number:
                self.method_number = abs(self.category_id.method_number - abs((imp_date.year - p_date.year)))
            else:
                self.method_number = self.category_id.method_number


    # @api.multi
    # def method_number_update(self):
    #     if self.date and self.import_date:
    #         date = datetime.strptime(self.date, "%Y-%m-%d")
    #         imp_date = datetime.strptime(self.import_date, "%Y-%m-%d")
    #         if (imp_date.year - date.year) > 0:
    #             self.method_number = abs(self.category_id.method_number - abs((imp_date.year - date.year)))
    #         else:
    #             self.method_number = self.category_id.method_number

    @api.multi
    def write(self, vals):
        res = super(AccountAssetAsset, self).write(vals)
        if not self.revise_year_line:
            if 'depreciation_line_ids' not in vals and 'state' not in vals:
                for rec in self:
                    rec.compute_depreciation_board()
        return res

    @api.onchange('purchase_date', 'up_to_use')
    def date_onchange(self):
        if self.purchase_date:
            # self.write({'date': self.purchase_date})
            self.date = self.purchase_date
        if self.up_to_use:
            # self.write({'date': self.up_to_use})
            self.date = self.up_to_use
        # if self.date:
        #     self.write({'date': self.date})

    @api.multi
    @api.depends('depreciation_line_ids.move_id')
    def _entry_count(self):
        for asset in self:
            res = self.env['account.asset.depreciation.line'].search_count(
                [('asset_id', '=', asset.id), ('move_id', '!=', False)])
            result = self.env['revise.year.line'].search_count(
                [('ac_asset_id', '=', asset.id), ('move_id', '!=', False)])
            asset.entry_count = res + result or 0

    @api.multi
    def open_entries(self):
        move_ids = []
        for asset in self:
            for depreciation_line in asset.depreciation_line_ids:
                if depreciation_line.move_id:
                    move_ids.append(depreciation_line.move_id.id)
            for year_line in asset.revise_year_line:
                if year_line.move_id:
                    move_ids.append(year_line.move_id.id)
        return {
            'name': _('Journal Entries'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', move_ids)],
        }

    def get_financial_year(self, datestring):
        date = datetime.strptime(datestring, "%Y-%m-%d").date()
        # initialize the current year
        year_of_date = date.year
        # initialize the current financial year start date
        financial_year_start_date = datetime.strptime(str(year_of_date) + "-04-01", "%Y-%m-%d").date()
        if date < financial_year_start_date:
            return 'March ' + '31 ' + str(
                financial_year_start_date.year)
        else:
            return 'March' +  '31 ' + str(
                financial_year_start_date.year + 1)

    @api.onchange('no_of_revise')
    def onchange_year_date(self):
        if self.no_of_revise:
            self.revise_date = fields.Datetime.now()

    @api.multi
    def update_revise_year(self):
        count = 0
        commands = []
        if self.revise_year_line:
            self.revise_year_line.unlink()
        if not self.no_of_revise:
            raise ValidationError(_('Please Fill Number of Revise Year!'))
        revise_date = datetime.strptime(self.revise_date, "%Y-%m-%d")
        rev_date = revise_date - relativedelta(days=1)

        find_fin_year = self.get_financial_year(str(rev_date.date()))
        date_object = parser.parse(find_fin_year)

        purchase_date = datetime.strptime(self.date, "%Y-%m-%d")
        line_diff = rev_date.year - purchase_date.year
        for line in self.depreciation_line_ids:
            count = count + 1
            line.check_boolean = True
            dep_date_to = datetime.strptime(line.depreciation_date_to, "%Y-%m-%d")
            dep_date_from = datetime.strptime(line.depreciation_date, "%Y-%m-%d")
            if dep_date_from.year == date_object.year-1 and dep_date_to.year == date_object.year:
                residual_amount = line.remaining_value
                current_date = line.depreciation_date_to
                break
        for dep_line in self.depreciation_line_ids:
            if dep_line.check_boolean == False:
                dep_line.unlink()
        if residual_amount or current_date:
            line_create_count = self.no_of_revise - line_diff
            revisedate = datetime.strptime(current_date, "%Y-%m-%d")
            update_date = 'April ' + '01 ' + str(revisedate.date().year)
            start_fin_date = parser.parse(update_date).date()
            financial_date = self.get_financial_year(str(start_fin_date))
            datetime_object = parser.parse(financial_date)
            count_days = start_fin_date - datetime_object.date()
            tot_year_days = 365 * line_create_count
            per_day_amt = residual_amount / tot_year_days
            depreciation_date = start_fin_date
            for year_line in range(0,line_create_count - 1):
                tot_year_balance = per_day_amt * abs(count_days.days)
                if datetime_object.year % 4:
                    tot_year_balance = tot_year_balance + per_day_amt
                residual_amount -= abs(tot_year_balance)
                vals = {
                    'ac_asset_id': self.id,
                    'remaining_value': residual_amount,
                    'amount': abs(tot_year_balance),
                    'depreciated_value': self.value - (self.salvage_value + residual_amount),
                    'depreciation_date': depreciation_date.strftime(DF),
                    'depreciation_date_to': datetime_object.date().strftime(DF)
                }
                commands.append((0, False, vals))
                tot_year_balance = 0.0
                update_date = 'April ' + '01 ' + str(datetime_object.date().year)
                depreciation_date = parser.parse(update_date).date()
                financial = self.get_financial_year(str(depreciation_date))
                datetime_object = parser.parse(financial)
                count_days = depreciation_date - datetime_object.date()
            last_date = datetime.strptime(self.date, "%Y-%m-%d")
            change_date = str(last_date.month) + ' ' + str(last_date.day) + ' ' + str(parser.parse(update_date).date().year + 1)
            diff_date = parser.parse(update_date).date() - (parser.parse(change_date).date() - relativedelta(days=1))

            balance = (per_day_amt * abs(diff_date.days)) + per_day_amt
            residual_amount -= abs(balance)
            vals = {
                'amount': abs(balance),
                'ac_asset_id': self.id,
                'remaining_value': residual_amount,
                'depreciated_value': self.value - (self.salvage_value + residual_amount),
                'depreciation_date': parser.parse(update_date).date(),
                'depreciation_date_to': parser.parse(change_date).date() - relativedelta(days=1),
            }
            commands.append((0, False, vals))
            self.write({'revise_year_line': commands})
        return True

    @api.multi
    def compute_depreciation_board(self):
        self.ensure_one()
        if not self.revise_year_line:
            posted_depreciation_line_ids = self.depreciation_line_ids.filtered(lambda x: x.move_check).sorted(
                key=lambda l: l.depreciation_date)
            unposted_depreciation_line_ids = self.depreciation_line_ids.filtered(lambda x: not x.move_check)

            # Remove old unposted depreciation lines. We cannot use unlink() with One2many field
            commands = [(2, line_id.id, False) for line_id in unposted_depreciation_line_ids]

            if self.value_residual > 0.0:
                amount_to_depr = residual_amount = self.value_residual
                if self.prorata:
                    # if we already have some previous validated entries, starting date is last entry + method perio
                    if posted_depreciation_line_ids and posted_depreciation_line_ids[-1].depreciation_date:
                        last_depreciation_date = datetime.strptime(posted_depreciation_line_ids[-1].depreciation_date,
                                                                   DF).date()
                        depreciation_date = last_depreciation_date + relativedelta(months=+self.method_period)
                    else:
                        depreciation_date = datetime.strptime(self._get_last_depreciation_date()[self.id], DF).date()
                else:
                    # depreciation_date = 1st of January of purchase year if annual valuation, 1st of
                    # purchase month in other cases
                    if self.method_period >= 12:
                        asset_date = datetime.strptime(self.date[:4] + '-01-01', DF).date()
                        current_date = datetime.strptime(str(self.date), "%Y-%m-%d").date()
                        financial_date = self.get_financial_year(self.date)
                        datetime_object = parser.parse(financial_date)
                        count_days = current_date - datetime_object.date()

                        count_days_remaning_month = current_date - datetime_object.date() # double for second line
                    else:
                        asset_date = datetime.strptime(self.date[:7] + '-01', DF).date()
                    # if we already have some previous validated entries, starting date isn't 1st January but last entry + method period
                    if posted_depreciation_line_ids and posted_depreciation_line_ids[-1].depreciation_date:
                        last_depreciation_date = datetime.strptime(posted_depreciation_line_ids[-1].depreciation_date,
                                                                   DF).date()
                        depreciation_date = last_depreciation_date + relativedelta(months=+self.method_period)
                    else:
                        depreciation_date = current_date
                day = depreciation_date.day
                month = depreciation_date.month
                year = depreciation_date.year
                total_days = (year % 4) and 365 or 366
                tot_year_days = 365 * self.method_number

                per_day_amt = self.value_residual / tot_year_days
                undone_dotation_number = self._compute_board_undone_dotation_nb(depreciation_date, total_days)

                for x in range(len(posted_depreciation_line_ids), undone_dotation_number):
                    sequence = x + 1
                    amount = self._compute_board_amount(sequence, residual_amount, amount_to_depr, undone_dotation_number,
                                                        posted_depreciation_line_ids, total_days, depreciation_date)
                    amount = self.currency_id.round(amount)
                    if float_is_zero(amount, precision_rounding=self.currency_id.rounding):
                        continue

                    tot_year_balance = per_day_amt * abs(count_days.days)

                    if datetime_object.year % 4:
                        tot_year_balance = tot_year_balance + per_day_amt
                    # else:
                    #     tot_year_balance = tot_year_balance + per_day_amt
                    residual_amount -= abs(tot_year_balance)

                    vals = {
                        'amount': abs(tot_year_balance),
                        'asset_id': self.id,
                        'sequence': sequence,
                        'name': (self.code or '') + '/' + str(sequence),
                        'remaining_value': residual_amount,
                        'depreciated_value': self.value - (self.salvage_value + residual_amount),
                        'depreciation_date': depreciation_date.strftime(DF),
                        'depreciation_date_to': datetime_object.date().strftime(DF)
                    }
                    commands.append((0, False, vals))
                    # Considering Depr. Period as months
                    depreciation_date = date(year, month, day) + relativedelta(months=+self.method_period)
                    day = depreciation_date.day
                    month = depreciation_date.month
                    year = depreciation_date.year
                    tot_year_balance = 0.0
                    update_date = 'April ' + '01 ' + str(datetime_object.date().year)
                    depreciation_date = parser.parse(update_date).date()
                    financial = self.get_financial_year(str(depreciation_date))
                    datetime_object = parser.parse(financial)
                    count_days = depreciation_date - datetime_object.date()

                if int((self.value - (self.salvage_value + residual_amount))) != int(self.value_residual):
                    previous_date = current_date - relativedelta(days=1)
                    fin_year = self.get_financial_year(self.date)
                    datetime_object = parser.parse(fin_year).date()
                    date_to = 'April ' + '01 ' + str(depreciation_date.year)
                    last_date = datetime.strptime(self.date, "%Y-%m-%d")
                    change_date = str(last_date.month) + ' ' + str(last_date.day) + ' ' + str(parser.parse(update_date).date().year + 1)
                    tot_days =  365 - abs(count_days_remaning_month.days)
                    tot_year_balance = per_day_amt * abs(tot_days)
                    if datetime_object.year % 4:
                        tot_year_balance = tot_year_balance - per_day_amt
                    vals = {
                        'amount': abs(tot_year_balance),
                        'asset_id': self.id,
                        'sequence': sequence,
                        'name': (self.code or '') + '/' + str(sequence),
                        'remaining_value': residual_amount,
                        'depreciated_value': self.value - (self.salvage_value + residual_amount),
                        'depreciation_date': parser.parse(date_to).date(),
                        'depreciation_date_to':parser.parse(change_date).date() - relativedelta(days=1)
                    }
                    commands.append((0, False, vals))
            self.write({'depreciation_line_ids': commands})

        return True

    def _get_disposal_moves(self, sale_amount, sale_date):
        move_ids = []
        for asset in self:
            unposted_depreciation_line_ids = asset.depreciation_line_ids.filtered(lambda x: not x.move_check)
            revise_year_line_ids = asset.revise_year_line.filtered(lambda x: not x.move_check)
            if unposted_depreciation_line_ids:
                old_values = {
                    'method_end': asset.method_end,
                    'method_number': asset.method_number,
                }

                # Remove all unposted depr. lines
                commands = [(2, line_id.id, False) for line_id in unposted_depreciation_line_ids]

                # Create a new depr. line with the residual amount and post it
                sequence = len(asset.depreciation_line_ids) - len(unposted_depreciation_line_ids) + 1
                today = datetime.today().strftime(DF)
                vals = {
                    'amount': sale_amount,
                    'asset_id': asset.id,
                    'sequence': sequence,
                    'name': (asset.code or '') + '/' + str(sequence),
                    'remaining_value': 0,
                    'depreciated_value': asset.value - asset.salvage_value,  # the asset is completely depreciated
                    'depreciation_date': sale_date,
                }
                commands.append((0, False, vals))
                asset.write({'depreciation_line_ids': commands, 'method_end': today})
                tracked_fields = self.env['account.asset.asset'].fields_get(['method_number', 'method_end'])
                changes, tracking_value_ids = asset._message_track(tracked_fields, old_values)
                if changes:
                    asset.message_post(subject=_('Asset sold or disposed. Accounting entry awaiting for validation.'), tracking_value_ids=tracking_value_ids)
                move_ids += asset.depreciation_line_ids[-1].create_move(post_move=False)
            if revise_year_line_ids:
                for line in asset.revise_year_line:
                    if line.move_check == False:
                        line.unlink()

        return move_ids

class ReviseYearLine(models.Model):
    _name = 'revise.year.line'

    ac_asset_id = fields.Many2one('account.asset.asset')
    depreciation_date = fields.Date('Depreciation Date From')
    depreciation_date_to = fields.Char('Depreciation Date To')
    amount = fields.Float('Depreciation')
    depreciated_value = fields.Float('Cumulative Depreciation')
    remaining_value = fields.Float('Residual')
    move_check = fields.Boolean(compute='_get_move_check')
    move_posted_check = fields.Boolean()
    move_id = fields.Many2one('account.move')
    parent_state = fields.Selection(related='ac_asset_id.state', string='State of Asset')

    @api.multi
    @api.depends('move_id')
    def _get_move_check(self):
        for line in self:
            line.move_check = bool(line.move_id)

    @api.multi
    @api.depends('move_id.state')
    def _get_move_posted_check(self):
        for line in self:
            line.move_posted_check = True if line.move_id and line.move_id.state == 'posted' else False

    @api.multi
    def create_move(self, post_move=True):
        created_moves = self.env['account.move']
        prec = self.env['decimal.precision'].precision_get('Account')
        for line in self:
            if line.move_id:
                raise UserError(_('This depreciation is already linked to a journal entry! Please post or delete it.'))
            category_id = line.ac_asset_id.category_id
            depreciation_date_to = self.env.context.get(
                'depreciation_date_to') or line.depreciation_date_to or fields.Date.context_today(self)
            company_currency = line.ac_asset_id.company_id.currency_id
            current_currency = line.ac_asset_id.currency_id
            amount = current_currency.with_context(date=depreciation_date_to).compute(line.amount, company_currency)
            # asset_name = line.ac_asset_id.name + ' (%s/%s)' % (len(line.ac_asset_id.revise_year_line))
            asset_name = 'Asset'
            move_line_1 = {
                'name': asset_name,
                'account_id': category_id.account_depreciation_id.id,
                'debit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
                'credit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
                'journal_id': category_id.journal_id.id,
                'partner_id': line.ac_asset_id.partner_id.id,
                'analytic_account_id': category_id.account_analytic_id.id if category_id.type == 'sale' else False,
                'currency_id': company_currency != current_currency and current_currency.id or False,
                'amount_currency': company_currency != current_currency and - 1.0 * line.amount or 0.0,
                'branch_id': line.ac_asset_id.branch_id.id
            }
            move_line_2 = {
                'name': asset_name,
                'account_id': category_id.account_depreciation_expense_id.id,
                'credit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
                'debit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
                'journal_id': category_id.journal_id.id,
                'partner_id': line.ac_asset_id.partner_id.id,
                'analytic_account_id': category_id.account_analytic_id.id if category_id.type == 'purchase' else False,
                'currency_id': company_currency != current_currency and current_currency.id or False,
                'amount_currency': company_currency != current_currency and line.amount or 0.0,
                'branch_id': line.ac_asset_id.branch_id.id
            }
            move_vals = {
                'ref': 'Asset',
                'date': depreciation_date_to or False,
                'journal_id': category_id.journal_id.id,
                'line_ids': [(0, 0, move_line_1), (0, 0, move_line_2)],
            }
            move = self.env['account.move'].create(move_vals)
            line.write({'move_id': move.id, 'move_check': True})
            created_moves |= move

        # if post_move and created_moves:
        #     created_moves.filtered(
        #         lambda m: any(m.revise_year_line.mapped('ac_asset_id.category_id.open_asset'))).post()
        return [x.id for x in created_moves]

    @api.multi
    def create_grouped_move(self, post_move=True):
        if not self.exists():
            return []

class AccountAssetDepreciationLines(models.Model):
    _inherit = 'account.asset.depreciation.line'

    depreciation_date_to = fields.Char('Depreciation Date To')
    check_boolean = fields.Boolean()
    category_id = fields.Many2one('account.asset.category',related="asset_id.category_id", store=True)
    state = fields.Selection(related="asset_id.state", store=True)

    @api.multi
    def create_move(self, post_move=True):
        created_moves = self.env['account.move']
        prec = self.env['decimal.precision'].precision_get('Account')
        for line in self:
            if line.move_id:
                raise UserError(_('This depreciation is already linked to a journal entry! Please post or delete it.'))
            category_id = line.asset_id.category_id
            depreciation_date_to = self.env.context.get(
                'depreciation_date_to') or line.depreciation_date_to or fields.Date.context_today(self)
            company_currency = line.asset_id.company_id.currency_id
            current_currency = line.asset_id.currency_id
            amount = current_currency.with_context(date=depreciation_date_to).compute(line.amount, company_currency)
            asset_name = line.asset_id.name + ' (%s/%s)' % (line.sequence, len(line.asset_id.depreciation_line_ids))
            move_line_1 = {
                'name': asset_name,
                'account_id': category_id.account_asset_id.id,
                'debit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
                'credit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
                'journal_id': category_id.journal_id.id,
                'partner_id': line.asset_id.partner_id.id,
                'analytic_account_id': category_id.account_analytic_id.id if category_id.type == 'sale' else False,
                'currency_id': company_currency != current_currency and current_currency.id or False,
                'amount_currency': company_currency != current_currency and - 1.0 * line.amount or 0.0,
                'branch_id': line.asset_id.branch_id.id
            }
            move_line_2 = {
                'name': asset_name,
                'account_id': category_id.account_depreciation_expense_id.id,
                'credit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
                'debit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
                'journal_id': category_id.journal_id.id,
                'partner_id': line.asset_id.partner_id.id,
                'analytic_account_id': category_id.account_analytic_id.id if category_id.type == 'purchase' else False,
                'currency_id': company_currency != current_currency and current_currency.id or False,
                'amount_currency': company_currency != current_currency and line.amount or 0.0,
                'branch_id': line.asset_id.branch_id.id
            }
            move_vals = {
                'ref': 'Asset',
                'date': depreciation_date_to or False,
                'journal_id': category_id.journal_id.id,
                'job_code': line.asset_id.job_code.id,
                'line_ids': [(0, 0, move_line_1), (0, 0, move_line_2)],
            }
            move = self.env['account.move'].create(move_vals)
            line.write({'move_id': move.id, 'move_check': True})
            created_moves |= move

        if post_move and created_moves:
            created_moves.filtered(
                lambda m: any(m.asset_depreciation_ids.mapped('asset_id.category_id.open_asset'))).post()
        return [x.id for x in created_moves]