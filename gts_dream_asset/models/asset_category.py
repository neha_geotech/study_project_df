from odoo import api, fields, models

class AccountAssetCategory(models.Model):
    _inherit = 'account.asset.category'

    sale_account_id = fields.Many2one('account.account', 'Sale Account')
