# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft

from odoo import api, fields, models, tools


class StockReport(models.Model):
    '''
    This is report view table to show Stock Report
    '''

    _name = 'stock.report'
    _rec_name = 'branch'

    branch = fields.Many2one('res.branch', string='Branch', readonly=True)
    opening_stock = fields.Float(string='1 OP Stock Real', readonly=True)
    total_purchase = fields.Float(string='2 Total Purchase', readonly=True)
    total_consumption_it = fields.Float(string='3 Consumption IT', readonly=True)
    total_actual = fields.Float(string='4 CL Stock Theoretical 1+2-3', readonly=True)
    difference_consumption = fields.Float(string='5 Inventory Adjustment 6-4', readonly=True)
    closing_stock_physical = fields.Float(string='6 CL Stock-Real', readonly=True)
    cl_op_stock = fields.Float(string='7 CL-OP Stock', readonly=True)
    non_material_skilled_exp = fields.Float(string='8 Non-Material & Skilled Exp.', readonly=True)
    total_booked = fields.Float(string='Total Booked 2+8', readonly=True)
    return_ = fields.Float(string='Return', readonly=True)
    recycled_use = fields.Float(string='Recycled Use', readonly=True)


class StockMoveLine(models.Model):
    '''
    This is report view table to show Stock Report
    '''

    _inherit = 'stock.move.line'

    cost = fields.Float(string='Cost', related='move_id.cost', store=True)
    total_cost = fields.Float(string='Total Cost', related='move_id.total_cost', store=True)
