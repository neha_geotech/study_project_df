from odoo import models, fields, api, _
from datetime import date, timedelta, datetime


class StockReportWiz(models.TransientModel):
    _name = 'stock.report.wiz'

    from_date = fields.Date('Start Date', default=fields.Datetime.now)
    end_date = fields.Date('End Date', default=fields.Datetime.now)

    @api.multi
    def open_stock_table(self):
        branches = self.env['res.branch'].search([])
        self._cr.execute('''
                            delete  
                            from stock_report
                            where branch in %s
                        ''', (tuple(branches.ids),))
        if self.from_date and self.end_date:
            tree_view_id = self.env.ref('gts_dream_stock_report.view_stock_report_tree').id
            form_view_id = self.env.ref('gts_dream_stock_report.view_stock_report_form').id
            graph_view_id = self.env.ref('gts_dream_stock_report.view_stock_report_graph').id
            pivot_view_id = self.env.ref('gts_dream_stock_report.view_stock_report_pivot').id
            search_view_ref = self.env.ref('gts_dream_stock_report.view_stock_report_search', False)
            date_format = '%Y-%m-%d'
            first_day_ = datetime.strptime(self.from_date, date_format)
            last_day_ = datetime.strptime(self.end_date, date_format)
            first_day = first_day_
            day_last_strip = str(last_day_).strip(':0')
            last_day = last_day_
            last_day_it = day_last_strip + '23:59:59'
            purchase_accounts = self.env['account.account'].search([
                ('group_id.name', '=', 'Material Expenses'),
            ])
            mat = []
            for data in purchase_accounts:
                mat.append(data.id)
            nonmat_accounts = self.env['account.account'].search(['|',
                ('group_id.name', '=', 'Non-Material Expenses'),
                ('group_id.name', '=', 'Skilled & Labour')
            ])
            non_mat = []
            for rec in nonmat_accounts:
                non_mat.append(rec.id)
            return_ = []
            return_move = self.env['stock.picking'].search([
                ('location_id.manufacturing_location', '=', True),
                ('location_dest_id.usage', '=', 'internal'),
            ])
            for res in return_move:
                return_.append(res.id)
            self._cr.execute("""
                with branch_ as (
                    select
                    br.id as branch
                    from res_branch br
                    group by br.id
                ),
                total_purchase as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) as total_purchase
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    left join account_move am on am.id = al.move_id
                    where am.state = 'posted'
                    and al.date between '%s' and '%s'
                     and al.account_id in %s
                    group by al.branch_id
                ),
                data_total_purchase as (
                    select
                    branch_.branch,
                    sum(tp.total_purchase) as total_purchase
                    from branch_
                    left join total_purchase tp on branch_.branch = tp.branch
                    group by branch_.branch
                ),
                total_consumption_it as (
                    select
                    sp.branch_id as branch,
                    sum(sp.total_it_amount) as total_consumption_it
                    from stock_picking sp
                    join res_branch br on br.id = sp.branch_id 
                    where sp.create_date between '%s' and '%s'
                    and sp.location_id in (16, 85, 91, 79, 73)
                    and sp.state = 'done'
                    group by sp.branch_id
                ),
                data_total_consumption_it as (
                    select
                    data_total_purchase.branch,
                    data_total_purchase.total_purchase as total_purchase,
                    sum(it.total_consumption_it) as total_consumption_it
                    from data_total_purchase
                    left join total_consumption_it it on data_total_purchase.branch = it.branch
                    group by data_total_purchase.branch, data_total_purchase.total_purchase
                ),
                non_material_skilled_exp as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) as non_material_skilled_exp
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    left join account_move am on am.id = al.move_id
                    where am.state = 'posted'
                    and al.date between '%s' and '%s'
                    and al.account_id in %s
                    group by al.branch_id
                ),
                data_non_material_skilled_exp as (
                    select
                    data_total_consumption_it.branch,
                    data_total_consumption_it.total_purchase as total_purchase,
                    data_total_consumption_it.total_consumption_it as total_consumption_it,
                    sum(nm.non_material_skilled_exp) as non_material_skilled_exp
                    from data_total_consumption_it
                    left join non_material_skilled_exp nm on data_total_consumption_it.branch = nm.branch
                    group by data_total_consumption_it.branch, data_total_consumption_it.total_purchase,
                    data_total_consumption_it.total_consumption_it
                ),
                return_ as (
                    select
                    sp.branch_id as branch,
                    sum(sp.total_amt) as return_
                    from stock_picking sp
                    join res_branch br on br.id = sp.branch_id
                    left join stock_move sm on sp.id = sm.picking_id
                    where sm.create_date between '%s' and '%s'
                    and sp.id in %s
                    and sp.state = 'done'
                    group by sp.branch_id
                ),
                data_return_ as (
                    select
                    data_non_material_skilled_exp.branch,
                    data_non_material_skilled_exp.total_purchase as total_purchase,
                    data_non_material_skilled_exp.total_consumption_it as total_consumption_it,
                    data_non_material_skilled_exp.non_material_skilled_exp as non_material_skilled_exp,
                    sum(re.return_) as return_
                    from data_non_material_skilled_exp
                    left join return_ re on data_non_material_skilled_exp.branch = re.branch
                    group by data_non_material_skilled_exp.branch, data_non_material_skilled_exp.total_purchase,
                    data_non_material_skilled_exp.total_consumption_it, data_non_material_skilled_exp.non_material_skilled_exp
                ),
                opening_stock as (
                    select
                    sl.branch_id as branch,
                    sum(sm.total_cost) as opening_stock
                    from stock_move sm
                    join stock_location sl on sl.id = sm.location_dest_id
                    join res_branch br on br.id = sl.branch_id
                    where sm.date < '%s'
                    and sm.location_dest_id in (16, 85, 91, 79, 73) and sm.state = 'done'
                    and picking_type_id = 1
                    group by sl.branch_id
                ),
                data_opening_stock as (
                    select
                    data_return_.branch,
                    data_return_.total_purchase as total_purchase,
                    data_return_.total_consumption_it as total_consumption_it,
                    data_return_.non_material_skilled_exp as non_material_skilled_exp,
                    data_return_.return_ as return_,
                    sum(os.opening_stock) as opening_stock
                    from data_return_
                    left join opening_stock os on data_return_.branch = os.branch
                    group by data_return_.branch, data_return_.total_purchase,
                    data_return_.total_consumption_it, data_return_.non_material_skilled_exp,
                    data_return_.return_
                ),
                closing_stock_physical as (
                    select
                    sl.branch_id as branch,
                    sum(sil.total_real_cost) as closing_stock_physical
                    from stock_inventory_line sil
                    join stock_location sl on sl.id = sil.location_id
                    join res_branch br on br.id = sl.branch_id
                    left join stock_inventory si on si.id = sil.inventory_id
                    where si.date < '%s'
                    and si.location_id in (16, 85, 91, 79, 73) and si.state = 'done'
                    group by sl.branch_id
                ),
                data_closing_stock as (
                    select
                    data_opening_stock.branch,
                    data_opening_stock.total_purchase as total_purchase,
                    data_opening_stock.total_consumption_it as total_consumption_it,
                    data_opening_stock.non_material_skilled_exp as non_material_skilled_exp,
                    data_opening_stock.return_ as return_,
                    data_opening_stock.opening_stock as opening_stock,
                    sum(cs.closing_stock_physical) as closing_stock_physical
                    from data_opening_stock
                    left join closing_stock_physical cs on data_opening_stock.branch = cs.branch
                    group by data_opening_stock.branch, data_opening_stock.total_purchase,
                    data_opening_stock.total_consumption_it, data_opening_stock.non_material_skilled_exp,
                    data_opening_stock.return_, data_opening_stock.opening_stock
                )
                select row_number() OVER (ORDER BY branch) AS id,
                branch, total_purchase, total_consumption_it, non_material_skilled_exp, return_,
                opening_stock,
                (total_consumption_it + non_material_skilled_exp) as total_booked,
                ((opening_stock + total_purchase) - total_consumption_it) as total_actual, closing_stock_physical, 
                (closing_stock_physical - (opening_stock - (total_consumption_it + total_purchase))) as difference_consumption,
                (closing_stock_physical - opening_stock) as cl_op_stock
                
                from data_closing_stock
            """ %(first_day, last_day, tuple(mat),
                  first_day, last_day_it,
                  first_day, last_day, tuple(non_mat),
                  first_day, last_day, tuple(return_),
                  first_day,
                  first_day))

            result = self._cr.fetchall()
            rows = result
            values = ', '.join(map(str, rows))
            sql = ("""INSERT INTO stock_report 
                    (id, branch, total_purchase, total_consumption_it,
                    non_material_skilled_exp, return_, opening_stock,
                    total_booked, total_actual, closing_stock_physical, cl_op_stock, difference_consumption
                    ) VALUES {}""".format(values).replace('None', 'null')
                   )
            self._cr.execute(sql)

            action = {
                'type': 'ir.actions.act_window',
                'views': [
                    (tree_view_id, 'tree'), (form_view_id, 'form'),
                    (graph_view_id, 'graph'), (pivot_view_id, 'pivot')
                ],
                'view_mode': 'tree,form',
                'name': _('Stock Report'),
                'res_model': 'stock.report',
                'search_view_id': search_view_ref and search_view_ref.id,
            }
            return action