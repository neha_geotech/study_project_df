# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'GTS Stock Report',
    'summary': 'Variance Report',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'website': 'http://www.geotechnosoft.com',
    'category': 'Inventory',
    'version': '1.0',
    'depends': [
        'gts_dream_purchase', 'stock', 'base', 'gts_dream_material'
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/stock_report_security.xml',
        'wizard/stock_report_wiz_view.xml',
        'report/stock_report_view.xml',
    ],
    'installable': True,
}
