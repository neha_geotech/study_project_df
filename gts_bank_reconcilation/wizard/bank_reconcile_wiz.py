from odoo import models, fields, api, _
from odoo.exceptions import UserError
from datetime import datetime, date
import calendar
from dateutil.relativedelta import relativedelta


class FilterBankDate(models.TransientModel):
    _name = "filter.bank.date"

    bank_reco_info_id = fields.Many2one('bank.reco.info')
    filter_bank_date_line = fields.One2many(
        'filter.bank.date.line',
        'filter_bank_date_id',
        'Filter Bank Date'
    )

    @api.model
    def default_get(self, fields):
        res = super(FilterBankDate, self).default_get(fields)
        context = self._context
        bank_reco_info_id = context.get('active_id')
        bank_reco_info = self.env['bank.reco.info']
        if not bank_reco_info_id:
            return res
        id_lines = []
        if 'material_id' in fields:
            res.update({'bank_reco_info_id': bank_reco_info_id})
        if 'filter_bank_date_line' in fields:
            bank_reco_info_obj = bank_reco_info.search([('id', '=', bank_reco_info_id)])
            for line in bank_reco_info_obj.bank_reco_info_line:
                if line.bank_date:
                    continue
                prd = {
                    'date': line.date_,
                    'partner_id': line.partner_id.id,
                    'cheque_no': line.cheque_no,
                    'debit_amount': line.debit_amount,
                    'ref': line.ref,
                    'credit_amount': line.credit_amount,
                    'bank_reco_info_line_id': line.id,
                }
                id_lines.append((0, 0, prd))
            res.update({'filter_bank_date_line': id_lines})
        return res

    @api.multi
    def action_save(self):
        date_format = '%Y-%m-%d'
        context = self._context
        bank_reco_info_id = context.get('active_id')
        budget = self.env['crossovered.budget.lines']
        bank_reco_info_obj = self.env['bank.reco.info'].search([('id', '=', bank_reco_info_id)])
        wiz_line = 0
        filter = 0
        for wiz in self:
            wiz_line = wiz_line + 1
            for line in wiz.filter_bank_date_line:
                filter = filter + 1
                line.bank_reco_info_line_id.write({'bank_date': line.bank_date})
                line.bank_reco_info_line_id.move_line_id.write({'bank_date_': line.bank_date})
                if line.bank_date:
                    start_dt = datetime.strptime(str(line.bank_date), "%Y-%m-%d").date()
                    num_days = calendar.monthrange(start_dt.year, start_dt.month)
                    first_day = date(start_dt.year, start_dt.month, 1)
                    last_day = date(start_dt.year, start_dt.month, num_days[1])

                    if line.bank_reco_info_line_id.move_line_id.analytic_account_id.id:
                        budget_id = budget.search(
                            [('analytic_account_id', '=', line.bank_reco_info_line_id.move_line_id.analytic_account_id.id),
                             ('date_from', '=', first_day),
                             ('date_to', '=', last_day)])
                        if budget_id:
                            budget_id.practical_amount_ += line.bank_reco_info_line_id.credit_amount
                search_move_line_id = self.env['account.analytic.line'].search([('move_id', '=', line.bank_reco_info_line_id.move_line_id.id)])
                search_move_line_id.write({'bank_rec_date': line.bank_date,
                                           'vendor_payout_reco': True})
        print('filte', filter)
        print('wiz', wiz_line)
        search_id = self.env['bank.reco.info.line'].search([('bank_reco_info_id', '=', bank_reco_info_id)])
        search_count = 0
        line_count = 0
        # for res in search_id:
        #     search_count = search_count + 1
        #     for rec in res.move_line_id:
        #         line_count = line_count + 1
        #         rec.bank_date_ = res.bank_date
        #         search_move_line_id = self.env['account.analytic.line'].search([('move_id', '=', rec.id)])
        #         search_move_id = self.env['account.move'].search([('id', '=', rec.move_id.id)])
        #         search_move_line_id.write({'bank_rec_date': res.bank_date,
        #                               'vendor_payout_reco': True})
        #         data_c = 0
        #         for data in search_move_id.line_ids:
        #             data_c = data_c + 1
        #             if data.account_id.name == 'Creditors':
        #                 analytic_ids = self.env['account.analytic.line'].search([('move_id', '=', data.id)])
        #                 analytic_ids.write({'bank_rec_date': res.bank_date,
        #                                            'vendor_payout_reco': True})
        # print('search',search_count)
        # print('line',line_count)
        # print('data', data_c)
        return {'type': 'ir.actions.act_window_close'}


class FilterBankDateLine(models.TransientModel):
    _name = 'filter.bank.date.line'

    date = fields.Date('Date')
    filter_bank_date_id = fields.Many2one('filter.bank.date', 'Filter Date Info')
    bank_reco_info_line_id = fields.Many2one('bank.reco.info.line', 'Bank Reco Line')
    partner_id = fields.Many2one('res.partner', 'Partner')
    bank_date = fields.Date("Bank Receive Date")
    cheque_no = fields.Char("Cheque No", size=128)
    debit_amount = fields.Float("Debit Amount")
    credit_amount = fields.Float("Credit Amount")
    ref = fields.Char("Reference ", size=128)
    user_id = fields.Many2one("res.users", 'User', default=lambda self: self._uid)