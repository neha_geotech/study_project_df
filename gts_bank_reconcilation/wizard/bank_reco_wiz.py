from odoo import models, fields, api, _
from odoo.exceptions import UserError
from datetime import datetime


class RecoBankDate(models.TransientModel):
    _name = "reco.bank.date"

    bank_reco_info_id = fields.Many2one('bank.reco.info')
    reco_bank_date_line = fields.One2many(
        'reco.bank.date.line',
        'reco_bank_date_id',
        'Filter Bank Date'
    )

    @api.model
    def default_get(self, fields):
        res = super(RecoBankDate, self).default_get(fields)
        context = self._context
        bank_reco_info_id = context.get('active_id')
        bank_reco_info = self.env['bank.reco.info']
        if not bank_reco_info_id:
            return res
        id_lines = []
        if 'material_id' in fields:
            res.update({'bank_reco_info_id': bank_reco_info_id})
        if 'reco_bank_date_line' in fields:
            bank_reco_info_obj = bank_reco_info.search([('id', '=', bank_reco_info_id)])
            for line in bank_reco_info_obj.bank_reco_info_line:
                if line.bank_date:
                    prd = {
                        'date': line.date_,
                        'partner_id': line.partner_id.id,
                        'cheque_no': line.cheque_no,
                        'debit_amount': line.debit_amount,
                        'ref': line.ref,
                        'credit_amount': line.credit_amount,
                        'bank_date': line.bank_date,
                        'bank_reco_info_line_id': line.id,
                    }
                    id_lines.append((0, 0, prd))
            res.update({'reco_bank_date_line': id_lines})
        return res

    @api.multi
    def action_save(self):
        date_format = '%Y-%m-%d'
        context = self._context
        bank_reco_info_id = context.get('active_id')
        bank_reco_info_obj = self.env['bank.reco.info'].search([('id', '=', bank_reco_info_id)])
        for wiz in self:
            for line in wiz.reco_bank_date_line:
                line.bank_reco_info_line_id.write({'bank_date': line.bank_date})
        return {'type': 'ir.actions.act_window_close'}


class RecoBankDateLine(models.TransientModel):
    _name = 'reco.bank.date.line'

    date = fields.Date('Date')
    reco_bank_date_id = fields.Many2one('reco.bank.date', 'Filter Date Info')
    bank_reco_info_line_id = fields.Many2one('bank.reco.info.line', 'Bank Reco Line')
    partner_id = fields.Many2one('res.partner', 'Partner')
    bank_date = fields.Date("Bank Receive Date")
    cheque_no = fields.Char("Cheque No", size=128)
    debit_amount = fields.Float("Debit Amount")
    credit_amount = fields.Float("Credit Amount")
    ref = fields.Char("Reference ", size=128)
    user_id = fields.Many2one("res.users", 'User', default=lambda self: self._uid)