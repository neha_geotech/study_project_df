from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class BankRecoInfo(models.Model):
    _name = 'bank.reco.info'
    _rec_name = 'account_id'

    @api.depends('bank_reco_info_line.debit_amount', 'bank_reco_info_line.credit_amount')
    def _get_total_amount(self):
        for data in self:
            total_debit = total_credit = 0.0
            for line in data.bank_reco_info_line:
                if line.bank_date:
                    continue
                total_debit += line.debit_amount
                total_credit += line.credit_amount
                data.update({
                    'debit_total': total_debit,
                    'credit_total': total_credit,
                })

    @api.multi
    def _get_balance(self):
        for data in self:
            search_id = self.env['account.move.line'].search([
                ('account_id', '=', data.account_id.id),
                ('move_id.state', '=', 'posted')
            ])
            balance = 0.0
            for rec in search_id:
                balance += (rec.debit - rec.credit)
                diff = balance - (data.debit_total + data.credit_total)
                # if balance >= 0.0:
                #     diff = (balance * -1) + data.debit_total - data.credit_total
                data.update({
                    'balance_': balance,
                    'difference_': (balance - data.debit_total) + data.credit_total
                })




    bank_reco_info_line = fields.One2many(
        'bank.reco.info.line',
        'bank_reco_info_id', 'Bank line Info'
    )
    account_id = fields.Many2one('account.account', 'Account')
    company_id = fields.Many2one(
        'res.company',
        string='Company',
        required=True,
        default=lambda self: self.env['res.company']._company_default_get('bank.reco.info')
    )
    debit_total = fields.Float(compute='_get_total_amount', string='Total Debit')
    credit_total = fields.Float(compute='_get_total_amount', string='Total Credit')
    balance_ = fields.Float(string='Balance', compute='_get_balance')
    difference_ = fields.Float(string='Balance', compute='_get_balance')
    file_load = fields.Boolean("File Loaded", default=False)
    date_from = fields.Date('Date From')
    date_to = fields.Date('Date To')

    _sql_constraints = [
        ('uniq_account_id', 'unique(account_id)', 'Account is already selected')
    ]

    @api.multi
    def button_reset(self):
        for data in self:
            for res in data.bank_reco_info_line:
                if not res.bank_date:
                    res.unlink()
                    data.write({'file_load': False})
                else:
                    raise UserError(_('You can not reset entries which have been reconciled.'))


    @api.multi
    def action_confirm(self):
        account_move_line_obj = self.env['account.move.line']
        bank_reco_info_line_obj = self.env['bank.reco.info.line']
        for bank_reco in self:
            if bank_reco.account_id:
                account_move_line_ids = account_move_line_obj.search([
                    ('move_id.state', '=', 'posted'),
                    ('account_id', '=', bank_reco.account_id.id),
                    # ('move_id.date', '>=', bank_reco.date_from),
                    # ('move_id.date', '<=', bank_reco.date_to),
                ])
                if account_move_line_ids:
                    for move_line in account_move_line_ids:
                        bank_reco_info_line_ids = bank_reco_info_line_obj.search([
                            ('move_line_id', '=', move_line.id)
                        ])
                        if not bank_reco_info_line_ids:
                            self.env['bank.reco.info.line'].create({
                                'bank_reco_info_id': bank_reco.id,
                                'move_line_id': move_line.id,
                                'date_': move_line.date,
                                'partner_id': move_line.partner_id.id,
                                'ref': move_line.ref,
                                'debit_amount': move_line.debit or 0,
                                'credit_amount': move_line.credit or 0,
                                'cheque_no': move_line.payment_id and move_line.payment_id.check_number or 0
                            })
                            self.write({'file_load': True})
                        if bank_reco_info_line_ids:
                            bank_reco_info_line_ids.write({
                                'move_line_id': move_line.id,
                                'date_': move_line.date,
                                'partner_id': move_line.partner_id.id,
                                'ref': move_line.ref,
                                'debit_amount': move_line.debit or 0,
                                'credit_amount': move_line.credit or 0,
                                'cheque_no': move_line.payment_id and move_line.payment_id.check_number or 0
                            })

    @api.multi
    def button_reload(self):
        account_move_line_obj = self.env['account.move.line']
        bank_reco_info_line_obj = self.env['bank.reco.info.line']
        for bank_reco in self:
            if bank_reco.account_id:
                account_move_line_ids = account_move_line_obj.search([
                    ('move_id.state', '=', 'posted'),
                    ('account_id', '=', bank_reco.account_id.id),
                    # ('move_id.date', '>=', bank_reco.date_from),
                    # ('move_id.date', '<=', bank_reco.date_to),
                ])
                if account_move_line_ids:
                    for move_line in account_move_line_ids:
                        bank_reco_info_line_ids = bank_reco_info_line_obj.search([
                            ('move_line_id', '=', move_line.id)
                        ])
                        if bank_reco_info_line_ids:
                            bank_reco_info_line_ids.write({
                                'bank_reco_info_id': bank_reco.id,
                                'move_line_id': move_line.id,
                                'date_': move_line.date,
                                'partner_id': move_line.partner_id.id,
                                'ref': move_line.ref,
                                'debit_amount': move_line.debit or 0,
                                'credit_amount': move_line.credit or 0,
                                'cheque_no': move_line.payment_id and move_line.payment_id.check_number or 0
                            })

    @api.multi
    def unlink(self):
        res = super(BankRecoInfo, self).unlink()
        if res:
            raise UserError(_('Cannot Delete Bank Reconciliation Entry.'))
        return res


class BankRecoInfoLine(models.Model):
    _name='bank.reco.info.line'

    bank_reco_info_id = fields.Many2one('bank.reco.info', 'Bank Info')
    partner_id = fields.Many2one('res.partner', 'Partner')
    move_line_id = fields.Many2one('account.move.line','Account Move Line')
    ref = fields.Char("Reference ",size=128)
    date_ = fields.Date('Date')
    bank_date = fields.Date("Bank Receive Date")
    cheque_no = fields.Char("Cheque No",size=128)
    debit_amount = fields.Float("Debit Amount")
    credit_amount = fields.Float("Credit Amount")
