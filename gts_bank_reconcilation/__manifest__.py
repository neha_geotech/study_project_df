{
    'name': 'GTS Bank Reco',
    'version': '1.0',
    'category': '',
    'sequence': 75,
    'summary': 'Dream Factory ',
    'description': "",
    'website': '',
    'depends': ['account'],
    'data': [
        'security/ir.model.access.csv',
        'wizard/bank_reconcile_wiz_view.xml',
        'wizard/bank_reco_wiz.xml',
        'views/bank_reconcile_view.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
