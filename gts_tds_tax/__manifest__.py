# -*- coding: utf-8 -*-
{
    'name': "GTS TDS Tax",
    'version': '11.0.0.1',
    'category': 'Accounting',
    'sequence': 5,
    'summary': 'Apply TDS Tax on Customer or Vendor Invoice',
    'description': """
        This module provide feature to apply TDS Tax on Customer or Vendor Invoice.
    """,
    'author': "Geo Technosoft",
    'website': "https://www.geotechnosoft.com",
    'depends': ['account', 'account_invoicing'],
    'data': [
        'views/account_invoice_view.xml',
        'views/account_tax_view.xml',
    ],
    'price': 9.99,
    'currency': 'EUR',
    'license': 'OPL-1',
    'installable': True,
    'application': True,
}
