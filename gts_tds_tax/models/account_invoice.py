
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class TaxInvoice(models.Model):
    _inherit = "account.invoice"

    tds_tax_id = fields.Many2one('account.tax', string='TDS TAX', readonly=True,
                                 states={'draft': [('readonly', False)],
                                         'sent': [('readonly', False)]})
    tds_amount = fields.Monetary('Total TDS', compute='_compute_amount')

    @api.multi
    @api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount',
                 'tax_line_ids.amount_rounding', 'currency_id', 'company_id',
                 'date_invoice', 'type', 'tds_tax_id')
    def _compute_amount(self):
        for rec in self:
            res = super(TaxInvoice, rec)._compute_amount()
            if 'tds_tax_id' in rec:
                rec.calculate_tds_tax()
            rec.calculate_tds_tax()
            sign = rec.type in ['in_refund', 'out_refund'] and -1 or 1
            rec.amount_total_company_signed = rec.amount_total * sign
            rec.amount_total_signed = rec.amount_total * sign
        return res

    @api.multi
    def calculate_tds_tax(self):
        for rec in self:
            if rec.tds_tax_id and rec.tds_tax_id.amount > 0.0:
                if rec.tds_tax_id.amount_type == 'percent':
                    rec.tds_amount = (rec.amount_untaxed * rec.tds_tax_id.amount) / 100
                elif rec.tds_tax_id.amount_type == 'fixed':
                    rec.tds_amount = rec.tds_tax_id.amount
            else:
                rec.tds_amount = 0.0

    @api.model
    def invoice_line_move_line_get(self):
        res = super(TaxInvoice, self).invoice_line_move_line_get()
        if self.tds_tax_id and self.tds_tax_id.amount > 0.0:
            name = self.tds_tax_id and self.tds_tax_id.name
            name = name + " for " + (self.origin if self.origin else \
                                         ("Invoice No " + str(self.name)))
            if not self.tds_tax_id.account_id:
                raise UserError(_('Please configure TDS Account from TDS tax master'))
            if self.tds_tax_id.account_id and self.type in ("out_invoice", "out_refund"):
                dict = {
                    'invl_id': self.number,
                    'type': 'src',
                    'name': name,
                    'price_unit': self.tds_amount,
                    'quantity': 1,
                    'price': -(self.tds_amount),
                    'account_id': self.tds_tax_id.account_id and self.tds_tax_id.account_id.id,
                    'invoice_id': self.id,
                }
                res.append(dict)
            elif self.tds_tax_id.account_id and self.type in ("in_invoice", "in_refund"):
                dict = {
                    'invl_id': self.number,
                    'type': 'src',
                    'name': name,
                    'price_unit': self.tds_amount,
                    'quantity': 1,
                    'price': -(self.tds_amount),
                    'account_id': self.tds_tax_id.account_id and self.tds_tax_id.account_id.id,
                    'invoice_id': self.id,
                }
                res.append(dict)
        return res
