from odoo import models, fields, api, _
from odoo.exceptions import UserError, Warning
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT,DEFAULT_SERVER_DATE_FORMAT
from datetime import datetime
from num2words import num2words


class AccountPayment(models.Model):
    _inherit = "account.payment"

    @api.multi
    def do_print_checks(self):
        return self.env.ref('gts_cheque.action_cheque_templates').report_action(self)

    @api.multi
    def get_date(self):
        date_ = datetime.strptime(self.payment_date, '%Y-%m-%d')
        date = date_.strftime('%d%m%Y')
        date=date.replace('', ' '*20)
        return date

    partner_name = fields.Char(string='Personal Name')
    account_pay = fields.Boolean('A/C Pay')

    @api.onchange('amount')
    def _onchange_amount(self):

        res = super(AccountPayment, self)._onchange_amount()
        for payment in self:
            payment.check_amount_in_words = (num2words(
                payment.amount, ordinal=False, lang='en_IN')
            ).title() + ' Only'
        return res

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        rec = super(AccountPayment, self)._onchange_partner_id()
        list_ = []
        if self.partner_id:
            for data in self.partner_id:
                list_.append(data.id)
                partner_data = self.env['res.partner'].search([
                    ('id', 'in', list_)
                ])
                if partner_data:
                    self.update({'partner_name': data.name})
                if not partner_data:
                    self.update({'partner_name': ''})
        return rec
