{
    'name': "Cheque Print",
    'version': '11.0.0',
    'category': 'Custom',
    'description': """
        Module provides functionality to  cheque print
	""",

    'author': 'Geo Technosoft Pvt Ltd.',
    'website': 'https://www.geotechnosoft.com',
    'depends': ['base','account_invoicing','account_check_printing','account'],
    'init_xml': [],
    'data': [
        'report/report_cheque_templates.xml',


    ],

    'installable': True,

}
