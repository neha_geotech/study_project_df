# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'GTS Group Partner Report',
    'summary': 'Group Partner Report',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'website': 'http://www.geotechnosoft.com',
    'category': 'accounting',
    'version': '1.0',
    'depends': [
        'account', 'gts_dream_purchase', 'gts_dream_account', 'gts_dream_mrp'
    ],
    'data': [
        'security/ir.model.access.csv',
        'report/group_due_report_view.xml',
    ],
    'installable': True,
}
