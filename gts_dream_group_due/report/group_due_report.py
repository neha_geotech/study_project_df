# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from datetime import timedelta
from odoo.exceptions import UserError


class ResPartner(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'

    partner_group_due = fields.Char(
        compute='_compute_partner_group_due',
        help='The label to display on partner ledger button, in form view'
    )

    @api.depends('supplier', 'customer')
    def _compute_partner_group_due(self):
        for record in self:
                record.partner_group_due = _('Group Due')

    @api.multi
    def open_due_group(self):
        report_search = self.env['group.due.report'].search([])
        if report_search:
            report_search.unlink()
        where_clause = ''
        partners = []
        for data in self:
            if data.supplier:
                where_clause = 'al.user_type_id = 2'
            if data.customer:
                where_clause = 'al.user_type_id = 1'
            if data.supplier == data.customer:
                where_clause = 'al.user_type_id in (1, 2)'
            search_id = self.env['res.partner'].search([('parent_id', '=', data.id)])
            for res in search_id:
                partners.append(res.id)
            print('tuple(partners)', tuple(partners))
            if not search_id:
                raise UserError(_('Unable to show Group Due!'))
        self._cr.execute(
            """
            with data_partner as (
                select
                   al.id as account_move_line_id,
                   am.id as account_move_id,
                   al.partner_id as partner_id,
                   rp.parent_id as parent_id,
                   al.branch_id as branch_id,
                   al.invoice_id as invoice_id,
                   al.date_maturity as date_maturity,
                   al.delays as delays,
                   sum(al.amount_residual) as balance,
                   al.account_id as account_id,
                   al.date as date,
                   al.event_name as event_name,
                   al.venue as venue,
                   al.activity_owner as activity_owner,
                   0.0 as basic_amount_31,
                   al.basic_amount as basic_amount,
                   al.gst_amount as gst_amount
                from account_move_line al
                join res_partner rp on rp.id = al.partner_id
                left join account_move am on am.id = al.move_id
                where rp.employee is FALSE
                and am.state = 'posted'
                and al.partner_id in %s
                and %s
                and al.date > '2018-04-01'
                and al.reconciled is False
                group by al.id, am.id, rp.parent_id, al.partner_id
            ),
            data_partner_31 as (
                select
                   al.id as account_move_line_id,
                   am.id as account_move_id,
                   al.partner_id as partner_id,
                   rp.parent_id as parent_id,
                   al.branch_id as branch_id,
                   al.invoice_id as invoice_id,
                   al.date_maturity as date_maturity,
                   al.delays as delays,
                   sum(al.amount_residual) as balance,
                   al.account_id as account_id,
                   al.date as date,
                   al.event_name as event_name,
                   al.venue as venue,
                   al.activity_owner as activity_owner,
                   al.basic_amount as basic_amount_31,
                   0.0 as basic_amount,
                   al.gst_amount as gst_amount
                from account_move_line al
                join res_partner rp on rp.id = al.partner_id
                left join account_move am on am.id = al.move_id
                where rp.employee is FALSE
                and am.state = 'posted'
                and al.partner_id in %s
                and %s
                and al.date < '2018-04-01'
                and al.reconciled is False
                group by al.id, am.id, rp.parent_id, al.partner_id
            ),
            final_data as (
                    select * from data_partner
                    UNION
                    select * from data_partner_31
            )
            
            select row_number() OVER () AS id, account_move_line_id, account_move_id, partner_id, parent_id,
            branch_id, invoice_id, date_maturity, delays, balance, account_id, date, event_name, venue,
            activity_owner, basic_amount_31, basic_amount,
            gst_amount,(basic_amount + gst_amount) as total_amount from final_data
            """ % (
                tuple(partners), where_clause,
                tuple(partners), where_clause)
        )
        result = self._cr.fetchall()
        if result:
            rows = result
            values = ', '.join(map(str, rows))
            print('----vla', values)
            sql = ("""INSERT INTO group_due_report 
                                (id, account_move_line_id, account_move_id, partner_id, parent_id,
                    branch_id, invoice_id, date_maturity, delays, balance, account_id, date, event_name,
                 venue, activity_owner, basic_amount_31, basic_amount, gst_amount, total_amount
                                ) VALUES {}""".format(values).replace('None', 'null')
                   )
            self._cr.execute(sql)

        tree_view_id = self.env.ref('gts_dream_group_due.view_group_due_report_tree').id
        form_view_id = self.env.ref('gts_dream_group_due.view_group_due_report_form').id
        graph_view_id = self.env.ref('gts_dream_group_due.view_group_due_report_graph').id
        pivot_view_id = self.env.ref('gts_dream_group_due.view_group_due_report_pivot').id
        search_view_ref = self.env.ref('gts_dream_group_due.view_group_due_report_search', False)
        action = {
            'type': 'ir.actions.act_window',
            'views': [
                (tree_view_id, 'tree'),
                (pivot_view_id, 'pivot'),
                (form_view_id, 'form'),
                (graph_view_id, 'graph')
            ],
            'view_mode': 'tree,form',
            'name': _('Group Due Report'),
            'res_model': 'group.due.report',
            'search_view_id': search_view_ref and search_view_ref.id,
            'context': {'group_by': ['parent_id', 'partner_id']}
        }
        return action


class GroupDueReport(models.Model):
    _name = 'group.due.report'
    _rec_name = 'parent_id'

    partner_id = fields.Many2one(
        'res.partner',
        string='Partner', readonly=True
    )
    parent_id = fields.Many2one(
        'res.partner',
        string='Group Partner', readonly=True
    )
    account_move_id = fields.Many2one(
        'account.move', string='Journal Entry',
        readonly=True
    )
    account_move_line_id = fields.Many2one(
        'account.move.line', string='Journal Item',
        readonly=True
    )
    account_id = fields.Many2one(
        'account.account', string='Account',
        readonly=True
    )
    branch_id = fields.Many2one(
        'res.branch', string='Branch',
        readonly=True
    )
    invoice_id = fields.Many2one(
        'account.invoice', string='Invoice Number',
        readonly=True
    )
    basic_amount_31 = fields.Float('Amt with Tax Till 31 Mar18', readonly=True)
    basic_amount = fields.Float('FY 18-19 Basic Amount', readonly=True)
    gst_amount = fields.Float('GST Amount', readonly=True)
    total_amount = fields.Float('Total Amount', readonly=True)
    balance = fields.Float('Total Due', readonly=True)
    event_date = fields.Date('Event Date', readonly=True)
    date = fields.Date('Date', readonly=True)
    delays = fields.Integer('Due Days', readonly=True)
    date_maturity = fields.Date('Due Date', readonly=True)
    event_name = fields.Char('Event Name', readonly=True)
    venue = fields.Text('Venue', readonly=True)
    activity_owner = fields.Char('Activity Owner', readonly=True)


