# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'GTS Job Code Detail',
    'summary': 'Job Code Detail Report',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'website': 'http://www.geotechnosoft.com',
    'category': 'Manufacturing',
    'version': '1.0',
    'depends': [
        'account', 'mrp', 'analytic', 'gts_dream_mrp','gts_dream_purchase'
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/job_code_detail_security.xml',
        'report/job_code_detail_view.xml',
    ],
    'installable': True,
}
