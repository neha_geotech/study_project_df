# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError


class JobCodeDetailReport(models.Model):
    _name = 'job.code.detail.report'
    _auto = False
    _rec_name = 'job_code'

    @api.model
    def default_get(self, fields):
        raise ValidationError(_('No Record Create From this Option!'))
        res = super(JobCodeDetailReport, self).default_get(fields)
        return res

    product_id = fields.Many2one('product.product', string='Product')
    job_code = fields.Many2one(
        'account.analytic.account',
        string='Job Code',
    )
    planned_amount = fields.Float(string='1 Planned Amount', readonly=True)
    total_qty = fields.Float(string='Total Quantity', readonly=True)
    expense_type = fields.Many2one(
        'mrp.bom.expense',
        string='Expense Type',
        readonly=True
    )
    actual_qty = fields.Float(string='Actual Quantity', readonly=True)
    actual_amount = fields.Float(string='Paid Bills', readonly=True)
    open_expense = fields.Float(string='2 Open Expense', readonly=True)
    open_expense_mat = fields.Float(string='Open Exp M', readonly=True)
    recycle_value = fields.Float(string='Recycle Value', readonly=True)
    return_value = fields.Float(string='Return Value', readonly=True)
    est_value = fields.Float(string='Estimated Value', readonly=True)
    payment_received = fields.Float(string='Payment Received', readonly=True)
    average_cost = fields.Float(string='Average Cost', readonly=True)
    branch_id = fields.Many2one(
        'res.branch',
        string='Branch',
        readonly=True
    )
    booked_expense = fields.Float(string='3 Booked Expense', readonly=True)
    date_closing = fields.Date(string='Expected Closing Date', readonly=True)
    open_invoice = fields.Float(string='Open Bills', readonly=True)
    internal_trans = fields.Float(string="4 Internal Transfer", readonly=True)
    ecs_value = fields.Float(string="ECS Value", readonly=True)
    account_move_id = fields.Many2one('account.move', string='Journal Entry', readonly=True)
    account_move_line_id = fields.Many2one('account.move.line', string='Journal Item', readonly=True)
    cost_center = fields.Float(string='5 Cost Center 2+3', readonly=True)
    diff_it = fields.Float(string='Difference 1-IT', readonly=True)
    diff_cc = fields.Float(string='Difference 1-CC', readonly=True)

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'job_code_detail_report')
        self._cr.execute("""
                CREATE or REPLACE VIEW job_code_detail_report AS (
                    with mrp_line as (
                        select
                            mbl.return_value as return_value,
                            mbl.product_id as product_id,
                            mb.job_code as job_code,
                            mbl.planned_amount_bom as planned_amount,
                            mbl.product_qty as total_qty,
                            mbl.expense_type as expense_type,
                            0.0 as booked_expense,
                            0.0 as open_expense,
                            0.0 as est_value,
                            0.0 as payment_received,
                            mbl.amount as average_cost,
                            mb.branch_id as branch_id,
                            0.0 as actual_amount,
                            ac.date_deadline as date_closing,
                            0.0 as actual_qty,
                            0.0 as open_invoice,
                            0.0 as open_expense_mat,
                            0.0 as internal_trans,
                            mbl.ecs_value as ecs_value,
                            null::int as account_move_id,
                            null::int as account_move_line_id
                        from mrp_bom_line mbl
                        left join mrp_bom mb on mbl.bom_id = mb.id
                        join account_analytic_account ac on ac.id = mb.job_code
                        where mbl.bom_id is not null and ac.opportunity_id is not null
                ),
                analytic_line as (
                        select
                            0.0 as return_value,
                            al.product_id as product_id,
                            al.account_id as job_code,
                            0.0 as planned_amount,
                            0.0 as total_qty,
                            al.expense_type as expense_type,
                            -(al.amount) as booked_expense,
                            0.0 as open_expense,
                            0.0 as est_value,
                            0.0 as payment_received,
                            0.0 as average_cost,
                            null::int as branch_id,
                            0.0 as actual_amount,
                            ac.date_deadline as date_closing,
                            al.unit_amount as actual_qty,
                            0.0 as open_invoice,
                            0.0 as open_expense_mat,
                            0.0 as internal_trans,
                            0.0 as ecs_value,
                            null::int as account_move_id,
                            al.move_id as account_move_line_id
                        from account_analytic_line al
                        join account_analytic_account ac on ac.id = al.account_id
                        where al.amount < 0.0 and ac.opportunity_id is not null
                ),
                purchase_line as (
                        select
                            0.0 as return_value,
                            l.product_id as product_id,
                            po.job_code as job_code,
                            0.0 as planned_amount,
                            0.0 as total_qty,
                            l.expense_type as expense_type,
                            0.0 as booked_expense,
                            l.price_subtotal as open_expense,
                            0.0 as est_value,
                            0.0 as payment_received,
                            0.0 as average_cost,
                            null::int as branch_id,
                            0.0 as actual_amount,
                            ac.date_deadline as date_closing,
                            0.0 as actual_qty,
                            0.0 as open_invoice,
                            0.0 as open_expense_mat,
                            0.0 as internal_trans,
                            0.0 as ecs_value,
                            null::int as account_move_id,
                            null::int as account_move_line_id
                        from purchase_order_line l
                        left join purchase_order po on l.order_id = po.id
                        join account_analytic_account ac on ac.id = po.job_code
                        where l.order_id is not null
                        and po.invoice_status not in ('no', 'invoiced', 'paid', 'partially_paid')
                        and po.state != 'cancel' and ac.opportunity_id is not null
                ),
                booked_exp as (
                        select
                            0.0 as return_value,
                            ail.product_id as product_id,
                            ai.job_code as job_code,
                            0.0 as planned_amount,
                            0.0 as total_qty,
                            ail.expense_type as expense_type,
                            0.0 as booked_expense,
                            0.0 as open_expense,
                            0.0 as est_value,
                            0.0 as payment_received,
                            0.0 as average_cost,
                            null::int as branch_id,
                            ail.price_subtotal as actual_amount,
                            ac.date_deadline as date_closing,
                            0.0 as actual_qty,
                            0.0 as open_invoice,
                            0.0 as open_expense_mat,
                            0.0 as internal_trans,
                            0.0 as ecs_value,
                            null::int as account_move_id,
                            null::int as account_move_line_id
                        from account_invoice_line ail
                        left join account_invoice ai on ail.invoice_id = ai.id
                        join account_analytic_account ac on ac.id = ai.job_code
                        where ail.invoice_id is not null and ai.state = 'paid' and
                        ai.expense_type != 1
                        and ai.type = 'in_invoice' and ac.opportunity_id is not null
                    ),
                    open_invoices as (
                        select
                            0.0 as return_value,
                            ail.product_id as product_id,
                            ai.job_code as job_code,
                            0.0 as planned_amount,
                            0.0 as total_qty,
                            ail.expense_type as expense_type,
                            0.0 as booked_expense,
                            0.0 as open_expense,
                            0.0 as est_value,
                            0.0 as payment_received,
                            0.0 as average_cost,
                            null::int as branch_id,
                            0.0 as actual_amount,
                            ac.date_deadline as date_closing,
                            0.0 as actual_qty,
                            ail.price_subtotal as open_invoice,
                            0.0 as open_expense_mat,
                            0.0 as internal_trans,
                            0.0 as ecs_value,
                            null::int as account_move_id,
                            null::int as account_move_line_id
                        from account_invoice_line ail
                        left join account_invoice ai on ail.invoice_id = ai.id
                        join account_analytic_account ac on ac.id = ai.job_code
                        where ail.invoice_id is not null and ai.state in ('draft', 'open')
                        and ai.expense_type != 1
                        and ai.type = 'in_invoice' and ac.opportunity_id is not null
                    ),
                    internal_transfer_ as (
                        select
                            0.0 as return_value,
                            sm.product_id as product_id,
                            sm.job_code as job_code,
                            0.0 as planned_amount,
                            0.0 as total_qty,
                            null::int as expense_type,
                            0.0 as booked_expense,
                            0.0 as open_expense,
                            0.0 as est_value,
                            0.0 as payment_received,
                            0.0 as average_cost,
                            null::int as branch_id,
                            0.0 as actual_amount,
                            ac.date_deadline as date_closing,
                            0.0 as actual_qty,
                            0.0 as open_invoice,
                            0.0 as open_expense_mat,
                            sm.total_cost as internal_trans,
                            0.0 as ecs_value,
                            null::int as account_move_id,
                            null::int as account_move_line_id
                        from stock_move sm
                        join account_analytic_account ac on ac.id = sm.job_code
                        where sm.picking_type_id in (4, 41, 47, 53, 59, 65, 71, 77)
                        and sm.state = 'done' and ac.opportunity_id is not null
                        ),
                    final_data as (
                        select * from mrp_line
                        UNION
                        select * from analytic_line
                        UNION
                        select * from purchase_line
                        UNION
                        select * from booked_exp
                        UNION
                        select * from open_invoices
                        UNION
                        select * from internal_transfer_
                )
                    select row_number() OVER (ORDER BY job_code) AS id,
                    return_value, product_id,
                    job_code, planned_amount, total_qty,
                    expense_type, booked_expense,
                    open_expense, est_value, payment_received,
                    average_cost, branch_id,
                    actual_amount, actual_qty, date_closing, open_invoice,
                    open_expense_mat, internal_trans, ecs_value, account_move_id, account_move_line_id,
                    (booked_expense + open_expense) as cost_center,
                    (planned_amount - internal_trans) as diff_it,
                    (planned_amount - (booked_expense + open_expense)) as diff_cc
                    from final_data
                )
        """)
