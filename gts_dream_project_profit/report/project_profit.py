# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft

from odoo import api, fields, models, tools


class ProjectProfitabilityReport(models.Model):
    _name = 'project.profitability.report'
    _auto = False

    job_code = fields.Many2one(
        'account.analytic.account',
        string='Job Code',
    )
    est_value = fields.Float(string='Quotation Value', readonly=True)
    invoice_value = fields.Float(string='Invoice Value', readonly=True)
    amount = fields.Float(string='Amount', readonly=True)
    planned_amount = fields.Float(string='Planned Amount', readonly=True)
    open_expense = fields.Float(string='Open Expense', readonly=True)
    actual_amount = fields.Float(string='Actual Expense', readonly=True)
    payment_received = fields.Float(string='Payment Received', readonly=True)

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'project_profitability_report')
        self._cr.execute("""
                CREATE or REPLACE VIEW project_profitability_report AS (
                    with mrp_line as (
                        select
                            mb.job_code as job_code,
                            0 as est_value,
                            0 as invoice_value,
                            mbl.amount as amount,
                            mbl.planned_amount_bom as planned_amount,
                            0 as open_expense,
                            0 as actual_amount,
                            0 as diff_amount,
                            0 as payment_received
                        from mrp_bom_line mbl
                        left join mrp_bom mb on mbl.bom_id = mb.id where mbl.bom_id is not null
                ),
                    analytic_line as (
                        select
                            al.account_id as job_code,
                            0 as est_value,
                            0 as invoice_value,
                            0 as amount,
                            0 as planned_amount,
                            0 as open_expense,
                            al.amount as actual_amount,
                            0 as diff_amount,
                            0 as payment_received
                        from account_analytic_line al
                ),
                    purchase_line as (
                        select
                            po.job_code as job_code,
                            0 as est_value,
                            0 as invoice_value,
                            0 as amount,
                            0 as planned_amount,
                            l.price_subtotal as open_expense,
                            0 as actual_amount,
                            0 as diff_amount,
                            0 as payment_received
                        from purchase_order_line l
                        left join purchase_order po on l.order_id = po.id where l.order_id is not null and po.state in ('draft', 'sent', 'to_approve')
                ),
                    production_line as (
                        select
                            mp.job_code as job_code,
                            0 as est_value,
                            0 as invoice_value,
                            0 as amount,
                            0 as planned_amount,
                            0 as open_expense,
                            0 as actual_amount,
                            0 as diff_amount,
                            0 as payment_received
                        from stock_move sm
                        left join mrp_production mp on sm.raw_material_production_id = mp.id where sm.raw_material_production_id is not null
                ),
                    quotation_line as (
                        select
                            so.analytic_account_id as job_code,
                            so.amount_total_new as est_value,
                            0 as invoice_value,
                            0 as amount,
                            0 as planned_amount,
                            0 as open_expense,
                            0 as actual_amount,
                            0 as diff_amount,
                            0 as payment_received
                        from quotation_line ql
                        left join sale_order so on ql.sale_order_id = so.id where ql.sale_order_id is not null
                    ),
                    account_inv_line as (
                        select
                            ail.account_analytic_id as job_code,
                            0 as est_value,
                            sum(amount_total) as invoice_value,
                            0 as amount,
                            0 as planned_amount,
                            0 as open_expense,
                            0 as actual_amount,
                            0 as diff_amount,
                            0 as payment_received
                        from account_invoice_line ail
                        left join account_invoice ai on ail.invoice_id = ai.id where ail.invoice_id is not null
                        group by ail.account_analytic_id
                    ),
                    account_move as (
                        select
                            aml.analytic_account_id as job_code,
                            0 as est_value,
                            0 as invoice_value,
                            0 as amount,
                            0 as planned_amount,
                            0 as open_expense,
                            0 as actual_amount,
                            0 as diff_amount,
                            am.amount as payment_received
                        from account_move am
                        left join account_move_line aml on aml.move_id = am.id where aml.move_id is not null and aml.partner_id is not null
                    ),
                    final_data as (
                        select * from mrp_line
                        UNION
                        select * from analytic_line
                        UNION
                        select * from purchase_line
                        UNION
                        select * from production_line
                        UNION
                        select * from quotation_line
                        UNION
                        select * from account_inv_line
                        UNION
                        select * from account_move
                )
                    select row_number() OVER (ORDER BY job_code) AS id,
                    job_code, est_value, invoice_value, amount, planned_amount, open_expense,
                    actual_amount, (planned_amount + actual_amount) as diff_amount, payment_received
                    from final_data
                )
        """)
