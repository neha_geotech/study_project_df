# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError


class VarianceReport(models.Model):
    '''
    This is report view table to show Variance Report
    '''
    _name = 'variance.report'
    _auto = False
    _rec_name = 'job_code'

    job_code = fields.Many2one(
        'account.analytic.account',
        string='Job Code',
    )
    planned_amount = fields.Float(string='Planned Cost', readonly=True)
    actual_amount = fields.Float(string='Paid Bills', readonly=True)
    booked_expense = fields.Float(string='Booked Expense', readonly=True)
    open_expense = fields.Float(string='3 Open Exp NM', readonly=True)
    internal_trans_value = fields.Float(string='Internal Transfer', readonly=True)
    open_expense_material = fields.Float(string='4 Open Exp M', readonly=True)
    est_value = fields.Float(string='SO Value', readonly=True)
    invoice_value = fields.Float(string='Invoice Value', readonly=True)
    bom_percentage = fields.Float(string='% BOM', readonly=True, group_operator='avg')
    bom_state = fields.Selection([
                                    ('draft', 'Draft'),
                                    ('bom_confirm', 'Confirm'),
                                    ('bom_close', 'Close'),
                                    ('cancel', 'Cancelled')], string='Closing', readonly=True)
    variance = fields.Float(string='Variance PC-CC', readonly=True)
    variance_percentage = fields.Float(string='9 % Variance', readonly=True, group_operator='avg')
    difference = fields.Float(string='Difference IN-SO', readonly=True)
    difference_percentage = fields.Float(string='% Difference', readonly=True, group_operator='avg')
    total_cost = fields.Float(string='8 Total Cost Center 3+4+5+7', readonly=True)
    total_cost_booked = fields.Float(string='10 Total CC Booked 3+4+5+6', readonly=True)
    total_value = fields.Float(string='Booked Expense M', readonly=True)
    date_closing = fields.Date(string='Expected Closing Date', readonly=True)
    event_date = fields.Date(string='Event Date', readonly=True)
    open_invoice = fields.Float(string='Booked Bills', readonly=True)
    branch = fields.Many2one('res.branch', string='Branch', readonly=True)
    booked_expense_null = fields.Float(string='Booked Expense', readonly=True)
    booked_expense_final = fields.Float(string='Booked Expense', readonly=True)
    percent_cost_center = fields.Float(string='8 % CC', readonly=True, group_operator='avg')
    percent_cost_center_booked = fields.Float(string='10 CC %', readonly=True, group_operator='avg')
    opportunity = fields.Many2one('crm.lead', string='Opportunity', readonly=True)
    return_value = fields.Float(string='Return', readonly=True)
    recycle_value = fields.Float(string='Recycle', readonly=True)
    credit_note = fields.Float(string='Credit Note', readonly=True)
    purchase_value = fields.Float(string="PO", readonly=True)
    purchase_percentage = fields.Float(string='% PO', group_operator='avg')
    variance_booked_cc = fields.Float(string='11 Variance 2-10')
    variance_percentage_booked = fields.Float(string='11 % Variance', readonly=True, group_operator='avg')

    @api.model
    def default_get(self, fields):
        raise ValidationError(_('No Record Create From this Option!'))
        res = super(VarianceReport, self).default_get(fields)
        return res

    @api.model_cr
    def init(self):
        """
        Query to create table for variance report
        """
        tools.drop_view_if_exists(self._cr, 'variance_report')
        self._cr.execute("""
            CREATE or REPLACE VIEW variance_report AS (
                with analytic as (
                        select
                        cl.id as job_code, cl.date_deadline as date_closing,
                        cl.event_date as event_date,
                        cl.branch_id as branch, cl.opportunity_id as opportunity
                        from account_analytic_account cl
                        where cl.active = True and cl.opportunity_id is not null
                        group by cl.id, cl.date_deadline, cl.branch_id, cl.opportunity_id, cl.event_date
                    ),
                    mrp as (
                        select
                        mb.job_code as job_code, ac.date_deadline as date_closing,
                        ac.event_date as event_date,
                        ac.branch_id as branch, ac.opportunity_id as opportunity,
                        sum(mbl.planned_amount_bom) as planned_amount,
                        sum(mbl.return_value) as return_value,
                        sum(mbl.recycle_value) as recycle_value,
                        mb.state as bom_state
                        from mrp_bom mb
                        left join mrp_bom_line mbl on mb.id = mbl.bom_id
                        join account_analytic_account ac on ac.id = mb.job_code
                        where mb.active = True
                        group by mb.job_code, ac.date_deadline, ac.branch_id, ac.opportunity_id, ac.event_date, mb.state
                    ),
                    combo as (
                        select 
                        analytic.job_code as job_code, analytic.date_closing as date_closing,
                        analytic.event_date as event_date,
                        analytic.branch as branch, analytic.opportunity as opportunity,
                        sum(mr.planned_amount) as planned_amount,
                        sum(mr.return_value) as return_value,
                        sum(mr.recycle_value) as recycle_value,
                        mr.bom_state as bom_state
                        from analytic
                        left join mrp mr on analytic.job_code = mr.job_code
                        group by analytic.job_code, analytic.date_closing, analytic.branch,
                        analytic.opportunity, analytic.event_date, mr.bom_state
                    ),
                    confirm_sale as (
                        select so.analytic_account_id as job_code, ac.date_deadline as date_closing,
                        ac.event_date as event_date,
                        ac.branch_id as branch, ac.opportunity_id as opportunity,
                        sum(so.amount_untaxed_new) as est_value
                        from sale_order so
                        join account_analytic_account ac on ac.id = so.analytic_account_id
                        where so.state not in ('draft', 'sent', 'cancel')
                        group by so.analytic_account_id, ac.date_deadline, ac.branch_id,
                        ac.opportunity_id, ac.event_date
                    ),
                    sale as (
                        select 
                        combo.job_code as job_code, combo.date_closing as date_closing,
                        combo.event_date as event_date,
                        combo.branch as branch, combo.opportunity as opportunity,
                        combo.planned_amount as planned_amount,
                        combo.return_value as return_value,
                        combo.recycle_value as recycle_value,
                        sum(cs.est_value) as est_value,
                        combo.bom_state as bom_state
                        from combo
                        left join confirm_sale cs on combo.job_code = cs.job_code
                        group by combo.job_code, combo.date_closing, combo.planned_amount,
                        combo.return_value, combo.recycle_value,
                        combo.branch, combo.opportunity, combo.event_date, combo.bom_state
                    ),
                    pur_inv as (
                        select
                        po.job_code as job_code, ac.date_deadline as date_closing,
                        ac.event_date as event_date,
                        ac.branch_id as branch, ac.opportunity_id as opportunity,
                        sum(po.amount_untaxed) as open_expense
                        from  purchase_order po
                        join account_analytic_account ac on ac.id = po.job_code
                        where po.invoice_status not in ('no', 'invoiced', 'paid', 'partially_paid') and po.expense_type != 1 and po.state != 'cancel'
                        group by po.job_code, ac.date_deadline, ac.branch_id, ac.opportunity_id, ac.event_date
                   ),
                   data_ as (
                        select 
                        sale.job_code as job_code, sale.date_closing as date_closing,
                        sale.event_date as event_date,
                        sale.branch as branch, sale.opportunity as opportunity,
                        sale.planned_amount as planned_amount,
                        sale.return_value as return_value,
                        sale.recycle_value as recycle_value,
                        sale.est_value as est_value,
                        sum(p.open_expense) as open_expense,
                        sale.bom_state as bom_state
                        from sale
                        left join pur_inv p on sale.job_code = p.job_code
                        group by sale.job_code, sale.planned_amount,
                        sale.return_value, sale.recycle_value,
                        sale.est_value, sale.date_closing, sale.branch,
                        sale.opportunity, sale.event_date, sale.bom_state
                   ),
                   inv as (
                        select
                        acc_inv.job_code as job_code, ac.date_deadline as date_closing,
                        ac.event_date as event_date,
                        ac.branch_id as branch, ac.opportunity_id as opportunity,
                        sum(acc_inv.amount_untaxed) as invoice_value
                        from account_invoice acc_inv
                        join account_analytic_account ac on ac.id = acc_inv.job_code      
                        where acc_inv.type = 'out_invoice' and acc_inv.state != 'cancel'
                        group by acc_inv.job_code, ac.date_deadline, ac.branch_id, ac.opportunity_id, ac.event_date
                   ),
                   invoice as (
                        select
                        data_.job_code as job_code,
                        data_.date_closing as date_closing,
                        data_.event_date as event_date,
                        data_.branch as branch,
                        data_.opportunity as opportunity,
                        data_.planned_amount as planned_amount,
                        data_.return_value as return_value,
                        data_.recycle_value as recycle_value,
                        data_.est_value as est_value,
                        data_.open_expense as open_expense,
                        sum(i.invoice_value) as invoice_value,
                        data_.bom_state as bom_state
                        from data_
                        left join inv i on data_.job_code = i.job_code
                        group by data_.job_code, data_.planned_amount,
                        data_.return_value, data_.recycle_value,
                        data_.est_value,
                        data_.open_expense, data_.date_closing, data_.branch, data_.opportunity, data_.event_date,
                        data_.bom_state

                   ),
                   inv_val as (
                        select
                        ai.job_code as job_code,
                        ac.date_deadline as date_closing,
                        ac.event_date as event_date,
                        ac.branch_id as branch,
                        ac.opportunity_id as opportunity,
                        sum(ai.amount_untaxed) as open_invoice
                        from  account_invoice ai
                        join account_analytic_account ac on ac.id = ai.job_code 
                        where ai.state in ('draft', 'open') and ai.expense_type != 1 and ai.type = 'in_invoice'
                        group by ai.job_code, ac.date_deadline, ac.branch_id, ac.opportunity_id, ac.event_date
                    ),
                    inv_val_data as (
                        select 
                        invoice.job_code as job_code,
                        invoice.date_closing as date_closing,
                        invoice.event_date as event_date,
                        invoice.branch as branch,
                        invoice.opportunity as opportunity,
                        invoice.planned_amount as planned_amount,
                        invoice.return_value as return_value,
                        invoice.recycle_value as recycle_value,
                        invoice.est_value as est_value,
                        invoice.open_expense as open_expense,
                        invoice.invoice_value as invoice_value,
                        sum(iv.open_invoice) as open_invoice,
                        invoice.bom_state as bom_state
                        from invoice
                        left join inv_val iv on invoice.job_code = iv.job_code
                        group by invoice.job_code, invoice.planned_amount,
                        invoice.return_value, invoice.recycle_value,
                        invoice.est_value, invoice.open_expense, invoice.invoice_value,
                        invoice.date_closing, invoice.branch, invoice.opportunity, invoice.event_date, invoice.bom_state
                    ),
                    pur_book as (
                        select
                        ai.job_code as job_code,
                        ac.date_deadline as date_closing, ac.event_date as event_date, ac.branch_id as branch,
                        ac.opportunity_id as opportunity,
                        sum(ai.amount_untaxed) as actual_amount
                        from  account_invoice ai
                        join account_analytic_account ac on ac.id = ai.job_code 
                        where ai.state = 'paid' and ai.expense_type != 1 and ai.type = 'in_invoice'
                        group by ai.job_code, ac.date_deadline, ac.branch_id, ac.opportunity_id, ac.event_date
                    ),
                    data_update as (
                        select 
                        inv_val_data.job_code,
                        inv_val_data.date_closing,
                        inv_val_data.event_date,
                        inv_val_data.branch,
                        inv_val_data.opportunity as opportunity,
                        inv_val_data.planned_amount as planned_amount,
                        inv_val_data.return_value as return_value,
                        inv_val_data.recycle_value as recycle_value,
                        inv_val_data.est_value as est_value,
                        inv_val_data.open_expense as open_expense,
                        inv_val_data.invoice_value as invoice_value,
                        inv_val_data.open_invoice as open_invoice,
                        sum(pb.actual_amount) as actual_amount,
                        inv_val_data.bom_state as bom_state
                        from inv_val_data
                        left join pur_book pb on inv_val_data.job_code = pb.job_code
                        group by inv_val_data.job_code, inv_val_data.planned_amount,
                        inv_val_data.return_value, inv_val_data.recycle_value,
                        inv_val_data.est_value,
                        inv_val_data.open_expense, inv_val_data.invoice_value,
                        inv_val_data.date_closing, inv_val_data.open_invoice, inv_val_data.branch,
                        inv_val_data.opportunity, inv_val_data.event_date, inv_val_data.bom_state
                    ),
                    anl_data_up as (
                        select
                        al.account_id,
                        ac.date_deadline as date_closing, ac.event_date as event_date, ac.branch_id as branch,
                        ac.opportunity_id as opportunity,
                        sum(al.amount) * -1 as booked_expense_null
                        from account_analytic_line al
                        join account_analytic_account ac on ac.id = al.account_id
                        where al.amount < 0.0 and al.general_account_id is null
                        group by al.account_id, ac.date_deadline, ac.branch_id,
                        ac.opportunity_id, ac.event_date
                    ),
                    data_data as (
                        select 
                        data_update.job_code as job_code,
                        data_update.date_closing as date_closing,
                        data_update.event_date as event_date,
                        data_update.branch as branch,
                        data_update.opportunity as opportunity,
                        data_update.planned_amount as planned_amount,
                        data_update.return_value as return_value,
                        data_update.recycle_value as recycle_value,
                        data_update.est_value as est_value,
                        data_update.open_expense as open_expense,
                        data_update.invoice_value as invoice_value,
                        data_update.open_invoice as open_invoice,
                        data_update.actual_amount as actual_amount,
                        sum(pb.booked_expense_null) as booked_expense_null,
                        data_update.bom_state as bom_state
                        from data_update
                        left join anl_data_up pb on data_update.job_code = pb.account_id
                        group by data_update.job_code, data_update.planned_amount,
                        data_update.return_value, data_update.recycle_value,
                        data_update.est_value,
                        data_update.open_expense, data_update.invoice_value, data_update.actual_amount,
                        data_update.date_closing, data_update.open_invoice, data_update.branch,
                        data_update.opportunity, data_update.event_date, data_update.bom_state
                    ),
                    pur_inv_material as (
                        select
                        po.job_code as job_code, ac.date_deadline as date_closing,
                        ac.event_date as event_date,
                        ac.branch_id as branch,
                        ac.opportunity_id as opportunity,
                        sum(po.amount_untaxed) as open_expense_material
                        from  purchase_order po
                        join account_analytic_account ac on ac.id = po.job_code
                        where po.invoice_status not in ('no', 'invoiced', 'paid', 'partially_paid') and po.expense_type = 1 and po.state != 'cancel'
                        group by po.job_code, ac.date_deadline, ac.branch_id, ac.opportunity_id, ac.event_date
                   ),
                   data_pur_inv_material as (
                        select 
                        data_data.job_code as job_code,
                        data_data.date_closing as date_closing,
                        data_data.event_date as event_date,
                        data_data.branch as branch,
                        data_data.opportunity as opportunity,
                        data_data.planned_amount as planned_amount,
                        data_data.return_value as return_value,
                        data_data.recycle_value as recycle_value,
                        data_data.est_value as est_value,
                        data_data.open_expense as open_expense,
                        data_data.invoice_value as invoice_value,
                        data_data.open_invoice as open_invoice,
                        data_data.actual_amount as actual_amount,
                        data_data.booked_expense_null as booked_expense_null,
                        sum(pm.open_expense_material) as open_expense_material,
                        data_data.bom_state as bom_state
                        from data_data
                        left join pur_inv_material pm on data_data.job_code = pm.job_code
                        group by data_data.job_code, data_data.planned_amount,
                        data_data.return_value, data_data.recycle_value,
                        data_data.est_value,
                        data_data.open_expense, data_data.invoice_value, data_data.actual_amount,
                        data_data.booked_expense_null,
                        data_data.date_closing, data_data.open_invoice, data_data.branch,
                        data_data.opportunity, data_data.event_date, data_data.bom_state
                    ),
                    internal_trans as (
                        select
                        al.account_id,
                        ac.date_deadline as date_closing, ac.event_date as event_date, ac.branch_id as branch,
                        ac.opportunity_id as opportunity,
                        sum(al.amount) * -1 as internal_trans_value
                        from account_analytic_line al
                        join account_analytic_account ac on ac.id = al.account_id
                        where al.amount < 0.0 and al.product_id is not null
                        and al.expense_type = 1
                        group by al.account_id, ac.date_deadline, ac.branch_id,
                        ac.opportunity_id, ac.event_date
                    ),
                    data_internal_trans as (
                        select 
                        data_pur_inv_material.job_code as job_code,
                        data_pur_inv_material.date_closing as date_closing,
                        data_pur_inv_material.event_date as event_date,
                        data_pur_inv_material.branch as branch,
                        data_pur_inv_material.opportunity as opportunity,
                        data_pur_inv_material.planned_amount as planned_amount,
                        data_pur_inv_material.return_value as return_value,
                        data_pur_inv_material.recycle_value as recycle_value,
                        data_pur_inv_material.est_value as est_value,
                        data_pur_inv_material.open_expense as open_expense,
                        data_pur_inv_material.invoice_value as invoice_value,
                        data_pur_inv_material.open_invoice as open_invoice,
                        data_pur_inv_material.actual_amount as actual_amount,
                        data_pur_inv_material.booked_expense_null as booked_expense_null,
                        data_pur_inv_material.open_expense_material as open_expense_material,
                        sum(it.internal_trans_value) as internal_trans_value,
                        data_pur_inv_material.bom_state as bom_state
                        from data_pur_inv_material
                        left join internal_trans it on data_pur_inv_material.job_code = it.account_id
                        group by data_pur_inv_material.job_code, data_pur_inv_material.planned_amount,
                        data_pur_inv_material.return_value, data_pur_inv_material.recycle_value,
                        data_pur_inv_material.est_value,
                        data_pur_inv_material.open_expense, data_pur_inv_material.invoice_value,
                        data_pur_inv_material.actual_amount,
                        data_pur_inv_material.booked_expense_null,
                        data_pur_inv_material.open_expense_material,
                        data_pur_inv_material.date_closing, data_pur_inv_material.open_invoice,
                        data_pur_inv_material.branch, data_pur_inv_material.opportunity, data_pur_inv_material.event_date,
                        data_pur_inv_material.bom_state
                    ),
                    credit_note as (
                        select
                        ai.job_code as job_code,
                        ac.date_deadline as date_closing,
                        ac.event_date as event_date,
                        ac.branch_id as branch,
                        ac.opportunity_id as opportunity,
                        sum(ai.amount_untaxed) as credit_note
                        from account_invoice ai
                        join account_analytic_account ac on ac.id = ai.account_id
                        where ai.state in ('open', 'paid') and ai.type = 'out_refund'
                        group by ai.job_code, ac.date_deadline,
                        ac.branch_id, ac.opportunity_id, ac.event_date
                    ),
                    data_credit_note as (
                        select 
                        data_internal_trans.job_code as job_code,
                        data_internal_trans.date_closing as date_closing,
                        data_internal_trans.event_date as event_date,
                        data_internal_trans.branch as branch,
                        data_internal_trans.opportunity as opportunity,
                        data_internal_trans.planned_amount as planned_amount,
                        data_internal_trans.return_value as return_value,
                        data_internal_trans.recycle_value as recycle_value,
                        data_internal_trans.est_value as est_value,
                        data_internal_trans.open_expense as open_expense,
                        data_internal_trans.invoice_value as invoice_value,
                        data_internal_trans.open_invoice as open_invoice,
                        data_internal_trans.actual_amount as actual_amount,
                        data_internal_trans.booked_expense_null as booked_expense_null,
                        data_internal_trans.open_expense_material as open_expense_material,
                        data_internal_trans.internal_trans_value as internal_trans_value,
                        sum(cn.credit_note) as credit_note,
                        data_internal_trans.bom_state as bom_state
                        from data_internal_trans
                        left join credit_note cn on data_internal_trans.job_code = cn.job_code
                        group by data_internal_trans.job_code, data_internal_trans.planned_amount,
                        data_internal_trans.return_value, data_internal_trans.recycle_value,
                        data_internal_trans.est_value,
                        data_internal_trans.open_expense, data_internal_trans.invoice_value,
                        data_internal_trans.actual_amount,
                        data_internal_trans.booked_expense_null,
                        data_internal_trans.open_expense_material,
                        data_internal_trans.internal_trans_value,
                        data_internal_trans.date_closing, data_internal_trans.open_invoice,
                        data_internal_trans.branch, data_internal_trans.opportunity, data_internal_trans.event_date,
                        data_internal_trans.bom_state 
                    ),
                    purchase_order_ as (
                        select
                        po.job_code as job_code, ac.date_deadline as date_closing,
                        ac.event_date as event_date,
                        ac.branch_id as branch,
                        ac.opportunity_id as opportunity,
                        sum(po.amount_untaxed) as purchase_value
                        from  purchase_order po
                        join account_analytic_account ac on ac.id = po.job_code
                        where po.state != 'cancel'
                        group by po.job_code, ac.date_deadline, ac.branch_id, ac.opportunity_id, ac.event_date
                   ),
                   data_purchase_order as (
                        select
                        data_credit_note.job_code as job_code,
                        data_credit_note.date_closing as date_closing,
                        data_credit_note.event_date as event_date,
                        data_credit_note.branch as branch,
                        data_credit_note.opportunity as opportunity,
                        data_credit_note.planned_amount as planned_amount,
                        data_credit_note.return_value as return_value,
                        data_credit_note.recycle_value as recycle_value,
                        data_credit_note.est_value as est_value,
                        data_credit_note.open_expense as open_expense,
                        data_credit_note.invoice_value as invoice_value,
                        data_credit_note.open_invoice as open_invoice,
                        data_credit_note.actual_amount as actual_amount,
                        data_credit_note.booked_expense_null as booked_expense_null,
                        data_credit_note.open_expense_material as open_expense_material,
                        data_credit_note.internal_trans_value as internal_trans_value,
                        data_credit_note.credit_note as credit_note,
                        sum(po.purchase_value) as purchase_value,
                        data_credit_note.bom_state as bom_state
                        from data_credit_note
                        left join purchase_order_ po on data_credit_note.job_code = po.job_code
                        group by data_credit_note.job_code, data_credit_note.planned_amount,
                        data_credit_note.return_value, data_credit_note.recycle_value,
                        data_credit_note.est_value,
                        data_credit_note.open_expense, data_credit_note.invoice_value,
                        data_credit_note.actual_amount,
                        data_credit_note.booked_expense_null,
                        data_credit_note.open_expense_material,
                        data_credit_note.internal_trans_value,
                        data_credit_note.date_closing, data_credit_note.open_invoice,
                        data_credit_note.branch, data_credit_note.opportunity,
                        data_credit_note.credit_note, data_credit_note.event_date,
                        data_credit_note.bom_state
                    ),
                    stock_move_ as (
                        select
                        al.analytic_account_id as job_code, ac.date_deadline as date_closing,
                        ac.event_date as event_date,
                        ac.branch_id as branch,
                        ac.opportunity_id as opportunity,
                        sum(al.balance) as total_value
                        from account_move_line al
                        join account_analytic_account ac on ac.id = al.analytic_account_id
                        join account_account aa on aa.id = al.account_id
                        join account_group ag on ag.id = aa.group_id
                        left join account_move am on am.id = al.move_id
                        where aa.group_id = 47 and am.state = 'posted'
                        group by al.analytic_account_id, ac.date_deadline, ac.branch_id, ac.opportunity_id, ac.event_date
                    ),
                    data_stock_move as (
                        select
                        data_purchase_order.job_code as job_code,
                        data_purchase_order.date_closing as date_closing,
                        data_purchase_order.event_date as event_date,
                        data_purchase_order.branch as branch,
                        data_purchase_order.opportunity as opportunity,
                        data_purchase_order.planned_amount as planned_amount,
                        data_purchase_order.return_value as return_value,
                        data_purchase_order.recycle_value as recycle_value,
                        data_purchase_order.est_value as est_value,
                        data_purchase_order.open_expense as open_expense,
                        data_purchase_order.invoice_value as invoice_value,
                        data_purchase_order.open_invoice as open_invoice,
                        data_purchase_order.actual_amount as actual_amount,
                        data_purchase_order.booked_expense_null as booked_expense_null,
                        data_purchase_order.open_expense_material as open_expense_material,
                        data_purchase_order.internal_trans_value as internal_trans_value,
                        data_purchase_order.credit_note as credit_note,
                        data_purchase_order.purchase_value as purchase_value,
                        sum(sm.total_value) as total_value,
                        data_purchase_order.bom_state as bom_state
                        from data_purchase_order
                        left join stock_move_ sm on data_purchase_order.job_code = sm.job_code
                        group by data_purchase_order.job_code, data_purchase_order.planned_amount,
                        data_purchase_order.return_value, data_purchase_order.recycle_value,
                        data_purchase_order.est_value,
                        data_purchase_order.open_expense, data_purchase_order.invoice_value,
                        data_purchase_order.actual_amount,
                        data_purchase_order.booked_expense_null,
                        data_purchase_order.open_expense_material,
                        data_purchase_order.internal_trans_value,
                        data_purchase_order.date_closing, data_purchase_order.open_invoice,
                        data_purchase_order.branch, data_purchase_order.opportunity,
                        data_purchase_order.credit_note, data_purchase_order.event_date,
                        data_purchase_order.purchase_value,
                        data_purchase_order.bom_state
                    ),
                    anl_data as (
                        select
                        al.account_id,
                        ac.date_deadline as date_closing, ac.event_date as event_date,  ac.branch_id as branch,
                        ac.opportunity_id as opportunity,
                        sum(al.amount) * -1 as booked_expense
                        from account_analytic_line al
                        join account_analytic_account ac on ac.id = al.account_id
                        join account_account aa on aa.id = al.general_account_id
                        join account_group ag on ag.id = aa.group_id
                        where al.amount < 0.0
                        and  aa.group_id in (5,62) 
                        group by al.account_id, ac.date_deadline, ac.branch_id,
                        ac.opportunity_id, ac.event_date
                    ),
                    final_data as (
                        select
                        data_stock_move.job_code as job_code,
                        data_stock_move.date_closing as date_closing,
                        data_stock_move.event_date as event_date,
                        data_stock_move.branch as branch,
                        data_stock_move.opportunity as opportunity,
                        COALESCE(sum(data_stock_move.planned_amount), 0.0) as planned_amount,
                        COALESCE(sum(data_stock_move.return_value), 0.0) as return_value,
                        COALESCE(sum(data_stock_move.recycle_value), 0.0) as recycle_value,
                        COALESCE(sum(data_stock_move.open_expense), 0.0) as open_expense,
                        COALESCE(sum(data_stock_move.est_value), 0.0) as est_value, 
                        COALESCE(sum(data_stock_move.invoice_value), 0.0) as invoice_value,
                        COALESCE(sum(data_stock_move.open_invoice), 0.0) as open_invoice,
                        COALESCE(sum(data_stock_move.actual_amount), 0.0) as actual_amount,
                        COALESCE(sum(data_stock_move.booked_expense_null), 0.0) as booked_expense_null,
                        COALESCE(sum(data_stock_move.open_expense_material), 0.0) as open_expense_material,
                        COALESCE(sum(data_stock_move.internal_trans_value), 0.0) as internal_trans_value,
                        COALESCE(sum(data_stock_move.credit_note), 0.0) as credit_note,
                        COALESCE(sum(data_stock_move.purchase_value), 0.0) as purchase_value,
                        COALESCE(sum(data_stock_move.total_value), 0.0) as total_value,
                        COALESCE(sum(ad.booked_expense), 0.0) as booked_expense,
                        data_stock_move.bom_state as bom_state
                        from data_stock_move
                        left join anl_data ad on data_stock_move.job_code = ad.account_id
                        group by data_stock_move.job_code, data_stock_move.date_closing,
                        data_stock_move.branch, data_stock_move.opportunity, data_stock_move.event_date,
                        data_stock_move.bom_state
                )
                select
                row_number() OVER (ORDER BY job_code) AS id,
                job_code, date_closing, event_date, branch, opportunity, planned_amount,
                 bom_state,
                return_value, recycle_value,
                open_expense, est_value,
                invoice_value, booked_expense, actual_amount, open_invoice, total_value, booked_expense_null,
                open_expense_material, internal_trans_value,
                credit_note, purchase_value,
                (booked_expense_null + booked_expense) as booked_expense_final,
                (purchase_value / nullif((booked_expense_null + booked_expense), 0.0) * 100.0) as purchase_percentage,
                (open_expense + booked_expense + open_expense_material + internal_trans_value) as total_cost,
                (open_expense + booked_expense + open_expense_material + total_value) as total_cost_booked,
                (planned_amount - (open_expense + booked_expense + open_expense_material + total_value)) as variance_booked_cc,
                (((planned_amount - (open_expense + booked_expense + open_expense_material + total_value)) * 100) / nullif(planned_amount, 0)) as variance_percentage_booked,
                ((planned_amount / nullif(est_value, 0.0)) * 100.0) as bom_percentage,
                (((open_expense + booked_expense + booked_expense_null) / nullif(est_value, 0.0)) * 100.0) as percent_cost_center,
                (((open_expense + booked_expense + open_expense_material + total_value) / nullif(est_value, 0.0)) * 100.0) as percent_cost_center_booked,
                (planned_amount - (open_expense + booked_expense + booked_expense_null)) as variance,
                (((planned_amount - (open_expense + booked_expense + booked_expense_null)) * 100) / nullif(planned_amount, 0)) as variance_percentage,
                ((invoice_value - credit_note) - est_value) as difference,
                ((((invoice_value - credit_note) - est_value) * 100) / nullif(est_value, 0)) as difference_percentage
                from final_data
                )    
        """)
