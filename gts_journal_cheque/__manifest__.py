{
    'name': "Journal Cheque Print",
    'version': '11.0.0',
    'category': 'Custom',
    'description': """
        Module provides functionality to  cheque print
	""",

    'author': 'Geo Technosoft Pvt Ltd.',
    'website': 'https://www.geotechnosoft.com',
    'depends': ['base','account_invoicing','account_check_printing','account', 'gts_cheque'],
    'init_xml': [],
    'data': [
        'report/journal_cheque_templates.xml',
        'views/account_journal_view.xml',



    ],

    'installable': True,

}
