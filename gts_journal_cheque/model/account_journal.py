from odoo import models, fields, api, _
from odoo.exceptions import UserError, Warning
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT,DEFAULT_SERVER_DATE_FORMAT
from datetime import datetime
from num2words import num2words


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    @api.multi
    def do_print_checks(self):
        return self.env.ref('gts_cheque.action_cheque_templates').report_action(self)

    @api.depends('balance')
    def _compute_amount_total_words_in(self):
        for invoice in self:
            invoice.check_amount_in_words = (num2words(
                invoice.cheque_amount, ordinal=False, lang='en_IN')
                                            ).title() + ' Only'

    @api.multi
    def get_date(self):
        for data in self:
            date_ = datetime.strptime(data.cheque_date, '%Y-%m-%d')
            date = date_.strftime('%d%m%Y')
            date = date.replace('', ' '*20)
            return date

    partner_name = fields.Char(string='Personal Name')
    self_pay = fields.Boolean('A/C Pay')
    cheque_date = fields.Date(string='Date', default=datetime.now())
    check_amount_in_words = fields.Char(string='Amount in words', compute='_compute_amount_total_words_in')
    cheque_amount = fields.Float(string='Cheque Amount', compute='_onchange_credit')

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        # rec = super(AccountMoveLine, self)._onchange_partner_id()
        list_ = []
        if self.partner_id:
            for data in self.partner_id:
                list_.append(data.id)
                partner_data = self.env['res.partner'].search([
                    ('id', 'in', list_)
                ])
                if partner_data:
                    self.update({'partner_name': data.name})
                if not partner_data:
                    self.update({'partner_name': ''})
        # return rec

    @api.depends('balance')
    def _onchange_credit(self):
        for data in self:
            if data.balance < 0.0:
                bal = -1 * data.balance
                data.update({'cheque_amount':bal})
            else:
                data.update({'cheque_amount': data.balance})
