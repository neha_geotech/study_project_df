from odoo import api, models, fields, _
from odoo.exceptions import UserError
import xlrd
import math
import base64
import logging
from io import BytesIO
from odoo.tools import pycompat
import string
from datetime import datetime
logger = logging.getLogger('Import')

logger = logging.getLogger('Script')


class script_running(models.TransientModel):
    ''' Used for Script Running '''
    _name = 'script.running'

    sheet = fields.Binary('Sheet')
    branch_id = fields.Many2one('res.branch', 'Branch')

    @api.multi
    def update_entry(self):
        ''' method for create move line and move from assest'''
        assets = self.env['account.asset.asset']
        account_move = self.env['account.move']
        move_line = self.env['account.move.line']
        account = self.env['account.account']
        journal = self.env['account.journal']
        fixed_account_id = account.search([('name', '=', '044002 Fixed Assets')], limit=1)
        cash_account_id = account.search([('name', '=', 'Cash')], limit=1)
        dep_account_id = account.search([('name', '=', 'Depreciation')], limit=1)
        journal_id = journal.search([('name', '=', 'Asset Journal')], limit=1)

        ml_list1 = []
        ml_lst2 = []
        assets_id = assets.search([('date', '=', '2018-03-31')])

        for asset in assets_id:
            if asset.purchase_cost > 0.0:
                ml_list1.append((0, 0, {
                    'account_id': asset.category_id.account_asset_id.id,
                      'branch_id': asset.branch_id.id,
                      'name': 'opening assets',
                      'debit': asset.purchase_cost
                }))
                ml_list1.append((0, 0, {
                    'account_id': cash_account_id.id,
                    'branch_id': asset.branch_id.id,
                    'name': 'opening assets',
                    'credit': asset.purchase_cost
                }))
                move_id = account_move.create({'date': asset.date,
                                     'branch_id': asset.branch_id.id,
                                    'journal_id': journal_id.id,
                                    'ref': asset.name,
                                    'line_ids': ml_list1,
                                    'asset_id': asset.id,
                                    })
                move_id.post()
                print(move_id)
                ml_list1 = []
            if asset.deprecation > 0.0:

                ml_lst2.append((0, 0, {
                    'account_id': asset.category_id.account_asset_id.id,
                    'branch_id': asset.branch_id.id,
                    'name': 'opening assets',
                    'credit': asset.deprecation
                }))
                ml_lst2.append((0, 0, {
                    'account_id': asset.category_id.account_depreciation_expense_id.id,
                    'branch_id': asset.branch_id.id,
                    'name': 'opening assets',
                    'debit': asset.deprecation
                }))
                move_id = account_move.create({'date': asset.date,
                                               'branch_id': asset.branch_id.id,
                                               'journal_id': journal_id.id,
                                               'ref': asset.name,
                                               'asset_id': asset.id,
                                               'line_ids': ml_lst2})
                move_id.post()
                ml_lst2 = []

    @api.multi
    def import_asset(self):
        asset = self.env['account.asset.asset']
        category = self.env['account.asset.category']
        branch = self.env['res.branch']
        if not self.sheet:
            raise UserError(_('Please add file'))
        val = base64.decodestring(self.sheet)
        fp = BytesIO()
        fp.write(val)
        # f = open("test lot.txt", "w+")
        book = xlrd.open_workbook(file_contents=fp.getvalue())
        sh = book.sheet_by_index(0)
        count = 0
        logger.info('row count : %d', sh.nrows)
        asset.unlink()
        for line in range(1, sh.nrows):  # sh.nrows
            count = count + 1
            row = sh.row_values(line)
            mrp = 0.0
            p_date = isinstance(row[2], float)
            imp_date = isinstance(row[3], float)
            sys_date = isinstance(row[1], float)
            if p_date == True:
                seconds = (row[2] - 25569) * 86400.0
                date = datetime.utcfromtimestamp(seconds)
                purchase_date = date.strftime('%Y/%m/%d')
            if imp_date == True:
                seconds = (row[3] - 25569) * 86400.0
                date = datetime.utcfromtimestamp(seconds)
                import_date = date.strftime('%Y/%m/%d')
            if sys_date == True:
                seconds = (row[1] - 25569) * 86400.0
                date = datetime.utcfromtimestamp(seconds)
                s_date = date.strftime('%Y/%m/%d')
            branch_id = branch.search([('name', '=', row[0])])
            categ_id = category.search([('name', '=', row[5])])
            importdate = datetime.strptime(import_date, "%Y/%m/%d")
            p_date = datetime.strptime(purchase_date, "%Y/%m/%d")

            if (importdate.year - p_date.year) > 0:
                method_num = abs(categ_id.method_number - abs((importdate.year - p_date.year)))
            else:
                method_num = categ_id.method_number

            asset_id = asset.create({'name': row[4],
                        'branch_id': branch_id.id,
                        'category_id': categ_id.id,
                        'purchase_date': purchase_date,
                        'import_date': import_date,
                        'value': row[8],
                        'salvage_value': int(row[9]),
                        'deprecation': row[7],
                        'purchase_cost': row[6],
                        'date': s_date,

                        })
            asset_id._amount_residual()
            asset_id.onchange_method_number()

        return True

    @api.multi
    def receive_product(self):
        asset_ids = self.env['account.asset.asset'].search([])
        for res in asset_ids:
            asset_line = res.depreciation_line_ids
            if asset_line:
                for rec in asset_line:
                    print(res, rec)
                    if rec.depreciation_date_to == '2019-03-31' and not rec.move_id:

                       rec.create_move()
            res.validate()
        # moves_ids = self.env['account.move.line'].search([('product_id', '!=', False),
        #                                           ('debit', '>', 0.0), ('date', '>=', '2018-04-01'),
        #                                           ('journal_id', '!=', 1), ('account_id', '!=',32)])
        # count = 0
        # print(moves_ids)
        # for res in moves_ids:
        #     if res.product_id.categ_id.id == 2378:
        #         count += 1
        #         print(res, count, res.account_id)
        #         res.update({'account_id': res.product_id.categ_id.property_account_expense_categ_id.id})


        # moves = self.env['stock.move'].search([('purchase_line_id', '!=', False)])
        # count = 0
        # for data in moves:
        #     count +=1
        #     purchase_line_ids = self.env['purchase.order.line'].search([('id', '=', data.id)])
        #     line_count = 0
        #     for line in purchase_line_ids:
        #         line_count += 1
        #         data.purchase_price = line.price_unit
        #         data.purchase_cost_price = line.price_unit * data.quantity_done
        #     # # print(purchase_ids)
        #     # for pur in purchase_ids:
        #     #     data.write({'purchase_price': pur.price_unit})


    @api.multi
    def done_mo(self):
        ''' method for create MO from BOM'''
        mo = self.env['mrp.production']
        count = 0
        bom_ids = self.env['mrp.bom'].search([('related_mo', '=', False),
                                              ('event_date', '<=', '2019-01-31'),
                                              ('branch_id', '=', self.branch_id.id)])
        for bom in bom_ids:
            count = count+1
            mrp_produce = self.env['mrp.product.produce']
            # bom.button_create_production()

            bom.action_confirm()
            if not bom.product_id:
                continue
            mo_id = self.env['mrp.production'].with_context(active_ids=bom.id).create({'product_id': bom.product_id.id,
                               'client_name': bom.client_name.id,
                               'venue': bom.venue,
                               'project_managers': bom.project_managers.id,
                                'date_planned_start': fields.Datetime.now(),
                                'branch_id': bom.branch_id.id,
                                'process_stage': 'bom',
                                'bom_id': bom.id,
                                'product_uom_id': bom.product_id.uom_id.id
                               })
            if not mo_id.move_raw_ids:
                continue
            mo_id.onchange_product_id()
            context = self.env.context.copy() or {}
            context['active_id'] = mo_id.id
            context['active_model'] = 'mrp.production'
            action = mo_id.with_context(context).open_produce_product()
            action['context'] = context
            # create produce Wizard & Production as per LOT
            production = self.env['mrp.product.produce'].with_context(context).create({
                'product_id': mo_id.product_id.id,
                'product_qty': mo_id.product_qty,
            })
            # for Default functions
            production.with_context(context).default_get(
                ['product_id', 'product_qty', 'product_uom_id'])

            production.do_produce()
            mo_id.button_mark_done()
            moves = self.env['stock.move'].search([('name', '=', mo_id.name)])
            for mv in moves:
                if mo_id.expected_closing_date:
                    mv.date = mo_id.expected_closing_date

    @api.multi
    def update_late_date(self):
        mo = self.env['account.move'].search([('create_date','>=','2019-05-01'),
                                              ('date','<=','2019-04-30'),('date','>=','2019-04-01')])
        print(mo)
        print(len(mo))
        for m in mo:
            m.write({'is_late_entry': True})

    @api.multi
    def update_mo_date(self):
        mo = self.env['mrp.production'].search([('state', 'in', ('done', 'progress', 'planned'))])
        print(mo)
        for m in mo:
            if m.expected_closing_date:
                moves = self.env['stock.move'].search([('name', '=', m.name)])
                if moves:
                    for mv in moves:
                        mv.write({'date': m.expected_closing_date, 'picking_type_id': m.picking_type_id.id})


    @api.multi
    def update_po_date(self):
        purchase = self.env['purchase.order'].search([('date_order', '>=', '2019-03-01 00:00:00'),
                                                      ('date_order', '<=', '2019-03-31 23:59:59'),
                                                      ('state', '=', 'purchase')])
        print('purchase', len(purchase))
        for rec in purchase:
            move = self.env['stock.move'].search([('origin', '=', rec.name)])
            if move:
                for mv in move:
                    if rec.create_date:
                        mv.write({'date': rec.date_order})

    @api.multi
    def update_inventory_date(self):
        inventory = self.env['stock.inventory'].search([])
        for inv in inventory:
            moves = self.env['stock.move'].search([('inventory_id', '=', inv.id)])
            if moves:
                for mv in moves:
                    mv.write({'date': inv.date})