
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

MAP_INVOICE_TYPE_PARTNER_TYPE = {
    'out_invoice': 'customer',
    'out_refund': 'customer',
    'in_invoice': 'supplier',
    'in_refund': 'supplier',
}

MAP_INVOICE_TYPE_PAYMENT_SIGN = {
    'out_invoice': 1,
    'in_refund': -1,
    'in_invoice': -1,
    'out_refund': 1,
}


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    # bank_charge_account = fields.Many2one(
    #     'account.account', string='Bank Charge Account',
    #     required=True
    # )
    bank_charge = fields.Monetary('Bank Charge', copy=False)
    bank_charge_type = fields.Selection([
        ('include_in_payment', 'Include In Payment'),
        ('exclude_in_payment', 'Exclude In Payment')
    ], string='Bank Charge Type', copy=False, default='exclude_in_payment')
    note = fields.Text('Note')
    journal_type = fields.Char('Journal Type')
    tds_amount = fields.Float(string="TDS Amount", copy=False)
    invoice_ids = fields.Many2many(
        'account.invoice', 'account_invoice_payment_rel',
        'payment_id', 'invoice_id', string="Invoices", copy=False
    )
    # currency_id = fields.Many2one("res.currency", string="Currency")
    job_code = fields.Many2one(
        'account.analytic.account',
        string='Job Code', index=True, copy=False
    )
    reference = fields.Char(string='Reference')
    ref_date = fields.Date(string='Cheque Date')


    @api.onchange('journal_id')
    def _onchange_journal(self):
        res = super(AccountPayment, self)._onchange_journal()
        if self.journal_id:
            res.setdefault('value', {})['journal_type'] = self.journal_id.type
        else:
            res.setdefault('value', {})['journal_type'] = ''
        for data in self:
            if data.journal_id.type == 'bank':
                data.job_code = 590
            if data.journal_id.type == 'cash':
                data.job_code = 589
        return res

    def post(self):
        rec = super(AccountPayment, self).post()
        for res in self:
            if res.bank_charge > 0 and not res.journal_id.bank_charge_account:
                raise UserError(_("Please define Bank Charge Account in bank journal."))
            return rec


    # Set default bank charge type for the payment type
    # @api.onchange('payment_type')
    # def _onchange_charge_type(self):
    #     if not self.invoice_ids:
    #         if self.payment_type == 'inbound':
    #             self.bank_charge_type = 'include_in_payment'
    #         elif self.payment_type == 'outbound':
    #             self.bank_charge_type = 'exclude_in_payment'



    def _create_payment_entry(self, amount):
        """ Create a journal entry corresponding to a payment, if the payment references invoice(s) they are reconciled.
            Return the journal entry.
        """
        # code to select amount when bank charge type is  ('include in payment or exclude in payment')
        if self.payment_type in 'inbound':
            if self.bank_charge_type in 'include_in_payment':
                amount = amount
            else:
                amount = amount - (self.bank_charge + self.tds_amount)
        else:
            if self.bank_charge_type in 'include_in_payment':
                amount = amount
            else:
                amount = amount

        aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
        invoice_currency = False
        if self.invoice_ids and all([x.currency_id == self.invoice_ids[0].currency_id for x in self.invoice_ids]):
            #if all the invoices selected share the same currency, record the paiement in that currency too
            invoice_currency = self.invoice_ids[0].currency_id
        debit, credit, amount_currency, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(amount, self.currency_id, self.company_id.currency_id, invoice_currency)

        move = self.env['account.move'].create(self._get_move_vals())

        #Write line corresponding to invoice payment
        counterpart_aml_dict = self._get_shared_move_line_vals(debit, credit, amount_currency, move.id, False)
        counterpart_aml_dict.update(self._get_counterpart_move_line_vals(self.invoice_ids))
        counterpart_aml_dict.update({'currency_id': currency_id})
        counterpart_aml = aml_obj.create(counterpart_aml_dict)

        #Reconcile with the invoices
        if self.payment_difference_handling == 'reconcile' and self.payment_difference:
            writeoff_line = self._get_shared_move_line_vals(0, 0, 0, move.id, False)
            amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(self.payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)[2:]
            # the writeoff debit and credit must be computed from the invoice residual in company currency
            # minus the payment amount in company currency, and not from the payment difference in the payment currency
            # to avoid loss of precision during the currency rate computations. See revision 20935462a0cabeb45480ce70114ff2f4e91eaf79 for a detailed example.
            total_residual_company_signed = sum(invoice.residual_company_signed for invoice in self.invoice_ids)
            total_payment_company_signed = self.currency_id.with_context(date=self.payment_date).compute(self.amount, self.company_id.currency_id)
            if self.invoice_ids[0].type in ['in_invoice', 'out_refund']:
                amount_wo = total_payment_company_signed - total_residual_company_signed
            else:
                amount_wo = total_residual_company_signed - total_payment_company_signed
            # Align the sign of the secondary currency writeoff amount with the sign of the writeoff
            # amount in the company currency
            if amount_wo > 0:
                debit_wo = amount_wo
                credit_wo = 0.0
                amount_currency_wo = abs(amount_currency_wo)
            else:
                debit_wo = 0.0
                credit_wo = -amount_wo
                amount_currency_wo = -abs(amount_currency_wo)
            writeoff_line['name'] = self.writeoff_label
            writeoff_line['account_id'] = self.writeoff_account_id.id
            writeoff_line['debit'] = debit_wo
            writeoff_line['credit'] = credit_wo
            writeoff_line['amount_currency'] = amount_currency_wo
            writeoff_line['currency_id'] = currency_id
            writeoff_line = aml_obj.create(writeoff_line)
            if counterpart_aml['debit'] or writeoff_line['credit']:
                counterpart_aml['debit'] += credit_wo - debit_wo
            if counterpart_aml['credit'] or writeoff_line['debit']:
                counterpart_aml['credit'] += debit_wo - credit_wo
            counterpart_aml['amount_currency'] -= amount_currency_wo

        #Write counterpart lines
        if not self.currency_id.is_zero(self.amount):
            if not self.currency_id != self.company_id.currency_id:
                amount_currency = 0
            liquidity_aml_dict = self._get_shared_move_line_vals(credit, debit, -amount_currency, move.id, False)
            liquidity_aml_dict.update(self._get_liquidity_move_line_vals(-amount))
            aml_obj.create(liquidity_aml_dict)

        # code  to check bank charge and creating bank charge entry in payment move
        # code to convert currency of bank charge monetary field
        # bank charge apply in inbound type payment
            if self.payment_type in 'inbound':
                if self.bank_charge > 0.0:
                    if not self.journal_id.bank_charge_account:
                        raise ValidationError(_('Please define bank charge account in payment journal !'))
                    for line in move.line_ids:
                        if self.currency_id != self.company_id.currency_id:
                            if line.debit > 0.0:
                                conv_bank_currency = aml_obj.compute_amount_fields(
                                    self.bank_charge, self.currency_id,
                                    self.company_id.currency_id, invoice_currency
                                )
                                conv_tds_currency = aml_obj.compute_amount_fields(self.tds_amount, self.currency_id,
                                                                                   self.company_id.currency_id,
                                                                                   invoice_currency)
                                line.debit = line.debit - (conv_bank_currency[0]+conv_tds_currency[0])
                                line.copy(default={
                                    'debit': conv_bank_currency[0],
                                    'account_id': self.journal_id.bank_charge_account.id,
                                    'amount_currency': self.bank_charge,
                                    'payment_id': self.id,
                                     })
                                line.copy(default={
                                    'debit': conv_tds_currency[0],
                                    'account_id': self.journal_id.tds_recivable_account.id,
                                    'amount_currency': self.tds_amount,
                                    'payment_id': self.id,
                                })
                        else:
                            if line.debit > 0.0:
                                line.debit = line.debit - (self.bank_charge + self.tds_amount)
                                line.copy(default={
                                    'debit': self.bank_charge,
                                    'account_id': self.journal_id.bank_charge_account.id,
                                    'payment_id': self.id,
                                    })
                                line.copy(default={
                                    'debit': self.tds_amount,
                                    'account_id': self.journal_id.tds_recivable_account.id,
                                    'payment_id': self.id,
                                })
            #outbound
            else:
                if self.bank_charge_type in 'exclude_in_payment':
                    if self.bank_charge > 0.0:
                        if not self.journal_id.bank_charge_account:
                            raise ValidationError(_('Please define bank charge account in payment journal !'))
                        for line in move.line_ids:
                            if self.currency_id != self.company_id.currency_id:
                                if line.credit > 0.0:
                                    conv_bank_currency = aml_obj.compute_amount_fields(self.bank_charge, self.currency_id,
                                                        self.company_id.currency_id, invoice_currency)
                                    conv_tds_currency = aml_obj.compute_amount_fields(self.tds_amount, self.currency_id,
                                                        self.company_id.currency_id, invoice_currency)
                                    line.credit = line.credit - conv_tds_currency[0]
                                    line.credit = line.credit + conv_bank_currency[0]
                                    line.copy(default={
                                        'credit': conv_tds_currency[0],
                                        'account_id': self.journal_id.tds_payable_account.id,
                                        'amount_currency': self.tds_amount,
                                        'payment_id': self.id,
                                    })
                                if line.debit > 0.0:
                                    line.debit = line.debit
                                    line.copy(default={
                                        'debit': conv_bank_currency[0],
                                        'account_id': self.journal_id.bank_charge_account.id,
                                        'amount_currency': self.tds_amount,
                                        'payment_id': self.id,
                                    })
                            else:
                                if line.credit > 0.0:
                                    line.credit = line.credit - self.tds_amount
                                    line.credit = line.credit + self.bank_charge
                                    line.copy(default={
                                        'credit': self.tds_amount,
                                        'account_id': self.journal_id.tds_payable_account.id,
                                        'payment_id': self.id,
                                    })
                                    # # line.credit = line.credit - self.tds_amount
                                    # # line.credit = self.bank_charge
                                    # line.copy(default={
                                    #     'credit': self.bank_charge,
                                    #     'account_id': self.journal_id.default_credit_account_id.id,
                                    #     'payment_id': self.id,
                                    #     'partner_id': False,
                                    # })
                                if line.debit > 0.0:
                                    line.debit = line.debit
                                    line.copy(default={
                                        'debit': self.bank_charge,
                                        'account_id': self.journal_id.bank_charge_account.id,
                                        'payment_id': self.id,
                                    })
                else:
                    if self.bank_charge > 0.0:
                        if not self.journal_id.bank_charge_account:
                            raise ValidationError(_('Please define bank charge account in payment journal !'))
                        for line in move.line_ids:
                            if self.currency_id != self.company_id.currency_id:
                                if line.credit > 0.0:
                                    conv_bank_currency = aml_obj.compute_amount_fields(self.bank_charge, self.currency_id,
                                                        self.company_id.currency_id, invoice_currency)
                                    conv_tds_currency = aml_obj.compute_amount_fields(self.tds_amount, self.currency_id,
                                                        self.company_id.currency_id, invoice_currency)
                                    line.credit = line.credit - (conv_bank_currency[0]+conv_tds_currency[0])
                                    line.copy(default={
                                        'credit': conv_bank_currency[0],
                                        'account_id': self.journal_id.bank_charge_account.id,
                                        'amount_currency': self.bank_charge,
                                        'payment_id': self.id,
                                    })
                                    line.copy(default={
                                        'credit': conv_tds_currency[0],
                                        'account_id': self.journal_id.tds_payable_account.id,
                                        'amount_currency': self.tds_amount,
                                        'payment_id': self.id,
                                    })
                            else:
                                if line.credit > 0.0:
                                    line.credit = line.credit - self.tds_amount
                                    line.copy(default={
                                        'credit': self.tds_amount,
                                        'account_id': self.journal_id.tds_payable_account.id,
                                        'payment_id': self.id,
                                    })
                                if line.debit > 0.0:
                                    line.debit = line.debit - self.bank_charge
                                    line.copy(default={
                                        'debit': self.bank_charge,
                                        'account_id': self.journal_id.bank_charge_account.id,
                                        'payment_id': self.id,
                                    })

        #validate the payment
        move.post()

        #reconcile the invoice receivable/payable line(s) with the payment
        self.invoice_ids.register_payment(counterpart_aml)

        return move

        # bank charge account field adding in account journal

    def _get_move_vals(self, journal=None):
        """ Return dict to create the payment move
        """
        journal = journal or self.journal_id
        if not journal.sequence_id:
            raise UserError(_('Configuration Error !'), _('The journal %s does not have a sequence, please specify one.') % journal.name)
        if not journal.sequence_id.active:
            raise UserError(_('Configuration Error !'), _('The sequence of journal %s is deactivated.') % journal.name)
        name = self.move_name or journal.with_context(ir_sequence_date=self.payment_date).sequence_id.next_by_id()
        return {
            'name': name,
            'date': self.payment_date,
            'ref': self.communication or '',
            'company_id': self.company_id.id,
            'journal_id': journal.id,
            'job_code': self.job_code.id
        }

    def _get_shared_move_line_vals(self, debit, credit, amount_currency, move_id, invoice_id=False):
        """ Returns values common to both move lines (except for debit, credit and amount_currency which are reversed)
        """
        return {
            'partner_id': self.payment_type in (
                'inbound', 'outbound'
            ) and self.env['res.partner']._find_accounting_partner(self.partner_id).id or False,
            'invoice_id': invoice_id and invoice_id.id or False,
            'move_id': move_id,
            'debit': debit,
            'credit': credit,
            'amount_currency': amount_currency or False,
            'payment_id': self.id,
            'analytic_account_id': self.job_code.id,
        }


class AccountJournal(models.Model):
    _inherit = "account.journal"

    bank_charge_account = fields.Many2one('account.account', string='Bank Charge Account')
    tds_payable_account = fields.Many2one('account.account', string='TDS Payable')
    tds_recivable_account = fields.Many2one('account.account', string='TDS Receivable')


class AccountRegisterPayments(models.TransientModel):
    _inherit = "account.register.payments"

    @api.onchange('journal_id')
    def _onchange_journal_id(self):
        res = super(AccountRegisterPayments, self)._onchange_journal_id()
        for data in self:
            if data.journal_id.type == 'bank':
                data.job_code = 590
            if data.journal_id.type == 'cash':
                data.job_code = 589
        return res

    bank_charge = fields.Monetary('Bank Charge', copy=False)
    partner_name = fields.Char(string='In Favour of')
    job_code = fields.Many2one(
        'account.analytic.account',
        string='Job Code', index=True, copy=False, default=False
    )
    reference = fields.Char(string='Reference')
    ref_date = fields.Date(string='Cheque Date')

    @api.multi
    def _prepare_payment_vals(self, invoices):
        # rec = super(AccountRegisterPayments, self)._prepare_payment_vals(invoices)
        '''Create the payment values.

        :param invoices: The invoices that should have the same commercial partner and the same type.
        :return: The payment values as a dictionary.
        '''
        amount = self._compute_payment_amount(invoices) if self.multi else self.amount
        payment_type = ('inbound' if amount > 0 else 'outbound') if self.multi else self.payment_type
        return {
            'journal_id': self.journal_id.id,
            'payment_method_id': self.payment_method_id.id,
            'payment_date': self.payment_date,
            'communication': self.communication,
            'invoice_ids': [(6, 0, invoices.ids)],
            'payment_type': payment_type,
            'reference': self.reference,
            'ref_date': self.ref_date,
            'amount': abs(amount),
            'currency_id': self.currency_id.id,
            'partner_id': invoices[0].commercial_partner_id.id,
            'partner_type': MAP_INVOICE_TYPE_PARTNER_TYPE[invoices[0].type],
            'check_number': self.check_number,
            'check_amount_in_words': self.check_amount_in_words,
            'partner_name': invoices[0].commercial_partner_id.name,
            'job_code': self.job_code.id,
            'bank_charge': self.bank_charge,
        }