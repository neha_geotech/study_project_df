# -*- encoding: utf-8 -*-
##############################################################################
#
#
##############################################################################
{
    'name': "Payment Bank Charge",
    'version': '11.0.0',
    'category': 'Custom',
    'description': """
        Module provides functionality to manage method operations

    """,
    'author': 'Geo Technosoft Pvt Ltd.',
    'website': 'https://www.geotechnosoft.com',
    'depends': ['account', 'account_invoicing', 'account_check_printing', 'gts_dream_purchase'],
    'init_xml': [],
    'data': [
        'views/payment_bank_charge_view.xml',
    ],
    'qweb': [],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
