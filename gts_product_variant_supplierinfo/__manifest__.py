# -*- coding: utf-8 -*-
{
    'name': 'Product supplier info per variant',
    'summary': 'Supplier info to product variant scope',
    'version': '11.0',
    'category': 'Product Management',
    'depends': [
        'purchase',
    ],
    'data': [
        'views/product_product_view.xml',
        'views/product_supplierinfo_view.xml',
    ],
    'installable': True,
}
