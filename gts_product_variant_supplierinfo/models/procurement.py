from odoo import api, models


class ProcurementOrder(models.Model):
    _inherit = 'procurement.order'

    # Do not call super as we do not want the native behaviour
    @api.model
    def _get_product_supplier(self, procurement):
        """returns the main supplier of the procurement's product
           given as argument"""
        product = procurement.product_id
        company_supplier = self.env['product.supplierinfo'].search([
            '|',
            '&',
            ('product_tmpl_id', '=', product.product_tmpl_id.id),
            ('product_id', '=', False),
            ('product_id', '=', product.id),
            ('company_id', '=', procurement.company_id.id),
        ], limit=1)
        if company_supplier:
            return company_supplier.name
        return procurement.product_id.seller_id
