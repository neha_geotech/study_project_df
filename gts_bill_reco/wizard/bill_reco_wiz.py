from odoo import models, fields, api, _


class BillRecoWiz(models.TransientModel):
    _name = "bill.reco.wiz"

    bill_reconcile_id = fields.Many2one('bill.reconcile')
    bill_reco_wiz_line = fields.One2many(
        'bill.reco.wiz.line',
        'bill_wiz_id',
        'Bill Information'
    )

    @api.model
    def default_get(self, fields):
        res = super(BillRecoWiz, self).default_get(fields)
        context = self._context
        bill_reco_id = context.get('active_id')
        bill_reco_obj = self.env['bill.reconcile']
        if not bill_reco_id:
            return res
        id_lines = []
        if 'bill_reco_wiz_line' in fields:
            bill_reco_ids = bill_reco_obj.search([('id', '=', bill_reco_id)])
            for line in bill_reco_ids.bill_reco_line:
                if line.reconcile:
                    prd = {
                        'date_': line.date_,
                        'reference': line.reference,
                        'total_amount': line.total_amount,
                        'pending_amount': line.pending_amount,
                        'reconcile': line.reconcile,
                        'bill_reco_line_id': line.id,
                    }
                    id_lines.append((0, 0, prd))
            res.update({'bill_reco_wiz_line': id_lines})
        return res

    @api.multi
    def action_save(self):
        for wiz in self:
            for line in wiz.bill_reco_wiz_line:
                line.bill_reco_line_id.write({'reconcile': line.reconcile})
                line.bill_reco_line_id.account_invoice_id.write({'reconciled': line.reconcile})
        return {'type': 'ir.actions.act_window_close'}


class BillRecoWizLine(models.TransientModel):
    _name = 'bill.reco.wiz.line'

    bill_wiz_id = fields.Many2one('bill.reco.wiz', 'Bill Reconcile')
    bill_reco_line_id = fields.Many2one('bill.reconcile.line', 'Bill Reco Line')
    reference = fields.Char('Reference Number')
    total_amount = fields.Float('Total Amount')
    pending_amount = fields.Float('Pending Amount')
    reconcile = fields.Boolean('Reconcile')
    date_ = fields.Date('Date')