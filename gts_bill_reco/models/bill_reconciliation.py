from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class BillReconcile(models.Model):
    _name = 'bill.reconcile'
    _rec_name = 'partner_id'

    # @api.depends('bill_reco_line.total_amount', 'bill_reco_line.pending_amount')
    # def _get_total_amount(self):
    #     for data in self:
    #         total_amt = total_pending = 0.0
    #         for line in data.bill_reco_line:
    #             total_amt += line.total_amount
    #             if line.reconcile:
    #                 continue
    #             total_pending += line.pending_amount
    #             data.update({
    #                 'amount_total': total_amt,
    #                 'pending_total': total_pending,
    #             })

    partner_id = fields.Many2one(
        'res.partner',
        string='Partner',
        required=True,
    )
    bill_reco_line = fields.One2many(
        'bill.reconcile.line',
        'bill_reco_id', 'Bill line Info'
    )
    date_from = fields.Date('Date From')
    date_to = fields.Date('Date To')
    file_load = fields.Boolean("File Loaded", default=False)
    amount_total = fields.Float(compute='_get_total_amount', string='Total')
    pending_total = fields.Float(compute='_get_total_amount', string='Total')

    _sql_constraints = [
        ('uniq_partner_id', 'unique(partner_id)', 'Partner is already selected')
    ]

    @api.multi
    def button_reset(self):
        for data in self:
            for res in data.bill_reco_line:
                if not res.reconcile:
                    res.unlink()
                    data.write({'file_load': False})
                else:
                    raise UserError(_('You can not reset entries which have been reconciled.'))

    @api.multi
    def action_confirm(self):
        account_invoice_obj = self.env['account.invoice']
        bank_reco_line_obj = self.env['bill.reconcile.line']
        for bill_reco in self:
            if bill_reco.partner_id:
                account_invoice_ids = account_invoice_obj.search([
                    ('state', '=', 'open'),
                    ('partner_id', '=', bill_reco.partner_id.id),
                ])
                if account_invoice_ids:
                    for data in account_invoice_ids:
                        bill_reco_line_ids = bank_reco_line_obj.search([
                            ('account_invoice_id', '=', data.id)
                        ])
                        if not bill_reco_line_ids:
                            self.env['bill.reconcile.line'].create({
                                'bill_reco_id': bill_reco.id,
                                'account_invoice_id': data.id,
                                'date_': data.date_invoice,
                                'reference': data.number,
                                'total_amount': data.amount_total,
                                'pending_amount': data.residual,
                                'partner_id': data.partner_id.id
                            })
                            self.write({'file_load': True})
                        if bill_reco_line_ids:
                            bill_reco_line_ids.write({
                                'account_invoice_id': data.id,
                                'date_': data.date_invoice,
                                'reference': data.number,
                                'total_amount': data.amount_total,
                                'pending_amount': data.residual,
                                'partner_id': data.partner_id.id
                            })

    @api.multi
    def unlink(self):
        res = super(BillReconcile, self).unlink()
        if res:
            raise UserError(_('Cannot Delete Bill Reconciliation Entry.'))
        return res


class BillReconcileLine(models.Model):
    _name = 'bill.reconcile.line'

    bill_reco_id = fields.Many2one('bill.reconcile', 'Bill Reco')
    reference = fields.Char('Reference Number')
    total_amount = fields.Float('Total Amount')
    pending_amount = fields.Float('Pending Amount')
    reconcile = fields.Boolean('Reconcile')
    date_ = fields.Date('Date')
    account_invoice_id = fields.Many2one('account.invoice', 'Account Invoice')
    partner_id = fields.Many2one(
        'res.partner',
        string='Partner',
    )