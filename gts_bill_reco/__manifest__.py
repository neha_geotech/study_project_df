{
    'name': 'GTS Bill Reconciliation',
    'version': '1.0',
    'category': '',
    'sequence': 75,
    'summary': 'Dream Factory ',
    'description': "",
    'website': '',
    'depends': ['account'],
    'data': [
        'security/ir.model.access.csv',
        'wizard/bill_reconciliation_wiz_view.xml',
        'wizard/bill_reco_view_wiz.xml',
        'views/bill_reconciliation_view.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
