# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'GTS Print Partner Ledger',
    'summary': 'Print Partner Ledger',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'website': 'http://www.geotechnosoft.com',
    'category': 'accounting',
    'version': '1.0',
    'depends': [
        'account', 'branch'
    ],
    'data': [
        'security/ir.model.access.csv',
        'report/print_partner_ledger.xml',
    ],
    'installable': True,
}
