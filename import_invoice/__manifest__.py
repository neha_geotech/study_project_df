
{
    'name': "Import invoice XLSX files",
    'version': '11.0.0',
    'category': 'Custom',
    'description': """
        Module provides ablility to import invoices and make related payments

    """,
    'author': 'Geo Technosoft Pvt Ltd.',
    'website': 'https://www.geotechnosoft.com',
    'depends': ['base', 'branch'],
    'init_xml': [],
    'data': [
        'wizard/invoice_import_wizard_view.xml',
    ],


    'qweb': [],
    'demo_xml': [],
    'test': [],
    'installable': True,
}