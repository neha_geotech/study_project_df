from odoo import api, fields, models, _
from odoo.exceptions import UserError

from datetime import datetime
from dateutil.relativedelta import relativedelta
import xlrd
import math
import base64
import logging
logger = logging.getLogger('Import')


class InvoiceImportWizard(models.TransientModel):
    _name = 'invoice.import.wizard'

    branch_id = fields.Many2one('res.branch', 'Branch', required=True)
    product_id = fields.Many2one('product.product', 'Product', required=True,
                                 help='Product to use in invoices')
    xlsx_file = fields.Binary("Upload File")
    filename = fields.Char('Filename')
    type = fields.Selection([('out_invoice', 'Customer Invoice'),
                             ('in_invoice', 'Vendor Invoice')], string="Invoice Type",
                            required=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, index=True,
                                 default=lambda self: self.env.user.company_id)

    @api.multi
    def _prepare_invoice_line(self, partner, price):
        self.ensure_one()
        res = {}
        account = self.product_id.property_account_income_id or\
                  self.product_id.categ_id.property_account_income_categ_id
        if self.type == 'in_invoice':
            account = self.product_id.property_account_expense_id or \
                      self.product_id.categ_id.property_account_expense_categ_id
        if not account:
            raise UserError(
                _('Please define income/expense account for this product: "%s" (id:%d) - or for its category: "%s".') %
                (self.product_id.name, self.product_id.id, self.product_id.categ_id.name))

        fpos = partner.property_account_position_id
        if fpos:
            account = fpos.map_account(account)

        res = {
            'name': self.product_id.name,
            'sequence': 1,
            'origin': 'Migrated',
            'account_id': account.id,
            'price_unit': price,
            'quantity': 1,
            'discount': 0,
            'uom_id': self.product_id.uom_id.id,
            'product_id': self.product_id.id or False
        }
        return res

    @api.multi
    def _prepare_invoice(self, partner, journal):
        self.ensure_one()
        account = partner.property_account_receivable_id
        fiscal_position = partner.property_account_position_id
        if self.type == 'in_invoice':
            account = partner.property_account_payable_id
        invoice_vals = {
            'name': '',
            'origin': 'Migrated',
            'type': self.type,
            'account_id': account.id,
            'partner_id': partner.id,
            'partner_shipping_id': partner.id,
            'journal_id': journal.id,
            'currency_id': journal.currency_id.id or self.company_id.currency_id.id,
            'comment': '',
            'payment_term_id': partner.property_payment_term_id and\
                               partner.property_payment_term_id.id or False,
            'fiscal_position_id': fiscal_position.id,
            'company_id': self.company_id.id,
            'user_id': self.env.user.id
        }
        return invoice_vals

    @api.multi
    def import_excel(self):
        '''Code to import invoice excel and generate a payment ,confirm the payment'''
        partner_obj = self.env['res.partner']
        invoice_obj = self.env['account.invoice']
        payment_obj = self.env['account.payment']
        journal_obj = self.env['account.journal']
        cash_journal = journal_obj.search([('type', '=', 'cash'),
                                           ('company_id', '=', self.company_id.id)])
        if not cash_journal:
            raise UserError(
                _('Please define an accounting cash journal for this company.'))
        cash_journal = cash_journal[0]
        journal_type = 'sale'
        if self.type == 'in_invoice':
            journal_type = 'purchase'
        journal_recs = journal_obj.search([('type', '=', journal_type),
                                           ('company_id', '=', self.company_id.id)])
        if not journal_recs:
            raise UserError(
                _('Please define an accounting %s journal for this company.') % (journal_type))
        journal_recs = journal_recs[0]
        if not self.xlsx_file:
            raise UserError(_('Please select Excel file'))
        file = base64.decodebytes(self.xlsx_file)
        date_invoice, name, partner_name, pending_amount = 0, 1, 2, 3
        file_path = '/tmp/invoice.xlsx'
        fp = open(file_path, 'wb')
        fp.write(file)
        fp.close()
        book = xlrd.open_workbook(file_path)
        sh = book.sheet_by_index(0)
        logger.info('row count : %d', sh.nrows)
        inv_ids = []
        for line in range(4, sh.nrows):  # sh.nrows
            row = sh.row_values(line)
            if row != '' and row[date_invoice]:
                # Getting dates from file
                serial = row[date_invoice]
                invoice_date = False
                try:
                    if serial and isinstance(serial, (int, float)):
                        seconds = (serial - 25569) * 86400.0
                        invoice_date = datetime.utcfromtimestamp(seconds).strftime('%Y-%m-%d')
                    elif serial:
                        serial = str(serial)
                        dt = str(serial).replace('/', '-')
                        invoice_date = datetime.strptime(dt, '%d-%m-%Y').strftime('%Y-%m-%d')
                except Exception as e:
                   pass
                # invoice_date = row[date_invoice]
                # date_due = datetime.strptime(invoice_date, "%Y-%m-%d") + relativedelta(days=30)
                partner = partner_obj.search([('name', '=', row[partner_name])], limit=1)

                # Creating Partner if not found
                if not partner:
                    partner = partner_obj.create({
                        'name': row[partner_name]
                    })
                print("*****************", row[pending_amount])
                # Creating invoice or payment
                if float(row[pending_amount]) > 0.0:
                    inv_vals = self._prepare_invoice(partner, journal_recs)
                    inv_line_vals = self._prepare_invoice_line(partner,
                                                               float(row[pending_amount]))
                    inv_vals['invoice_line_ids'] = [(0, 0, inv_line_vals)]
                    inv_vals['date_invoice'] = invoice_date
                    # inv_vals['date_due'] = date_due
                    inv_vals['branch_id'] = self.branch_id.id
                    inv_vals['origin'] = row[name]
                    invoice = invoice_obj.create(inv_vals)
                    invoice.action_invoice_open()
                    inv_ids.append(invoice.id)
                else:
                    # Create The payment
                    payment_amount = -float(row[pending_amount])
                    if self.type == 'in_invoice':
                        payment_amount = -payment_amount
                    if abs(payment_amount) > 0.00001:
                        partner_type = False
                        if partner:
                            if payment_amount < 0:
                                partner_type = 'supplier'
                            else:
                                partner_type = 'customer'

                        payment_methods = (payment_amount > 0) and\
                                          cash_journal.inbound_payment_method_ids or\
                                          cash_journal.outbound_payment_method_ids
                        currency = cash_journal.currency_id or self.company_id.currency_id
                        payment = payment_obj.create({
                            'payment_method_id': payment_methods and\
                                                 payment_methods[0].id or False,
                            'payment_type': payment_amount > 0 and 'inbound' or 'outbound',
                            'partner_id': partner.id or False,
                            'partner_type': partner_type,
                            'journal_id': cash_journal.id,
                            'payment_date': invoice_date,
                            'state': 'draft',
                            'branch_id': self.branch_id.id,
                            'currency_id': currency.id,
                            'amount': abs(payment_amount),
                            'communication': row[name],
                            'name': 'Draft Payment',
                        })
                        payment.post()
        return True
