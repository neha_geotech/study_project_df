# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'GTS IN vs OUT Report',
    'summary': 'IN vs OUT Report',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'website': 'http://www.geotechnosoft.com',
    'category': '',
    'version': '1.0',
    'depends': [
        'account', 'gts_dream_purchase'
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/in_out_report_security.xml',
        'wizard/in_vs_out_wiz_view.xml',
        'report/in_vs_out_report_view.xml',
    ],
    'installable': True,
}
