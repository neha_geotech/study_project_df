# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError


class InOutReport(models.Model):
    '''
    This is report view table to show Total In and Out Report
    '''
    _name = 'in.out.report'
    _rec_name = 'branch'

    @api.model
    def default_get(self, fields):
        raise ValidationError(_('No Record Create From this Option!'))
        res = super(InOutReport, self).default_get(fields)
        return res

    branch = fields.Many2one('res.branch', string='Branch', readonly=True)
    tot_untaxed_invoice = fields.Float(string='Total Untaxed Invoice', readonly=True)
    tot_taxed_invoice = fields.Float(string='Total Invoice With Tax', readonly=True)
    tot_untaxed_credit = fields.Float(string='Total Untaxed Credit', readonly=True)
    tot_taxed_credit = fields.Float(string='Total Credit With Tax', readonly=True)
    tot_credit = fields.Float(string='Total Credit', readonly=True)
    tot_recovery = fields.Float(string='Total Recovery', readonly=True)
    tot_gst = fields.Float(string='Total GST', readonly=True)
    percentage_recovery = fields.Float(string='% Recovery', readonly=True)
    tax_recovery = fields.Float(string='Recovery in Tax Form', readonly=True)
    scrap_sale = fields.Float(string='Scrap Sale', readonly=True)
    vendor_bill_with_tax = fields.Float(string='Total Vendor Bill Book with Tax', readonly=True)
    project_payout = fields.Float(string='Vendor Payout Others', readonly=True)
    project_payout_cash = fields.Float(string='Vendor Payout Cash', readonly=True)
    project_payout_imprest = fields.Float(string='Vendor Payout Imprest', readonly=True)
    admin_payout = fields.Float(string='Total Payout Admin', readonly=True)
    asset_payout = fields.Float(string='Current/Fixed Assets Payout', readonly=True)
    loan_repayment = fields.Float(string='Loan Repayment', readonly=True)
    tax_payout = fields.Float(string='Tax Payout', readonly=True)
    all_total = fields.Float(string='ALL Total Payout', readonly=True)
    tot_cash_trans = fields.Float(string='Total Fund Transfer', readonly=True)
    # tot_cash_payout = fields.Float(string='Total Cash Payout', readonly=True)
    percent_vendor_payout = fields.Float(string='% Vendor Payout', readonly=True)
    booked_gst = fields.Float(string='Total GST Booked', readonly=True)
    uncleared_cheques = fields.Float(string='Uncleared Cheques', readonly=True)