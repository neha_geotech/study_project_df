from odoo import models, fields, api, _
from datetime import date, timedelta, datetime


class InOutWiz(models.TransientModel):
    _name = 'in.out.wiz'

    from_date = fields.Date('Start Date', default=fields.Datetime.now)
    end_date = fields.Date('End Date', default=fields.Datetime.now)

    @api.multi
    def open_inout_table(self):
        report_search = self.env['in.out.report'].search([])
        branches = self.env['res.branch'].search([])
        self._cr.execute('''
                                    delete  
                                    from in_out_report
                                    where branch in %s
                                ''', (tuple(branches.ids),))
        if self.from_date and self.end_date:
            tree_view_id = self.env.ref('gts_in_out_report.view_in_out_report_tree').id
            form_view_id = self.env.ref('gts_in_out_report.view_in_out_report_form').id
            graph_view_id = self.env.ref('gts_in_out_report.view_in_out_report_graph').id
            pivot_view_id = self.env.ref('gts_in_out_report.view_in_out_report_pivot').id
            search_view_ref = self.env.ref('gts_in_out_report.view_in_out_report_search', False)
            date_format = '%Y-%m-%d'
            first_day_ = datetime.strptime(self.from_date, date_format)
            last_day_ = datetime.strptime(self.end_date, date_format)
            first_day = first_day_
            last_day = last_day_
            self._cr.execute("""
                with branch_ as (
                    select
                    br.id as branch
                    from res_branch br
                    group by br.id
                ),
                tot_untaxed_credit as (
                    select
                    ai.branch_id as branch,
                    sum(ai.amount_untaxed) as tot_untaxed_credit
                    from account_invoice ai
                    join res_branch br on br.id = ai.branch_id 
                    where ai.state in ('open', 'paid') and ai.type = 'out_refund'
                    and ai.date_invoice between '%s' and '%s'
                    group by ai.branch_id
                ),
                 data_untaxed_credit as (
                    select
                    branch_.branch,
                    sum(iv.tot_untaxed_credit) as tot_untaxed_credit
                    from branch_
                    left join tot_untaxed_credit iv on branch_.branch = iv.branch
                    group by branch_.branch
                ),
                 tot_taxed_credit as (
                    select
                    ai.branch_id as branch,
                    sum(ai.amount_total) as tot_taxed_credit
                    from account_invoice ai
                    join res_branch br on br.id = ai.branch_id 
                    where ai.state in ('open', 'paid') and ai.type = 'out_refund'
                    and ai.date_invoice between '%s' and '%s'
                    group by ai.branch_id
                ),
                
                data_tot_taxed_credit as (
                    select
                    data_untaxed_credit.branch,
                    data_untaxed_credit.tot_untaxed_credit as tot_untaxed_credit,
                    sum(ti.tot_taxed_credit) as tot_taxed_credit
                    from data_untaxed_credit
                    left join tot_taxed_credit ti on data_untaxed_credit.branch = ti.branch
                    group by data_untaxed_credit.branch, data_untaxed_credit.tot_untaxed_credit
                ),
                
                tot_untaxed_invoice as (
                    select
                    ai.branch_id as branch,
                    sum(ai.amount_untaxed) as tot_untaxed_invoice
                    from account_invoice ai
                    join res_branch br on br.id = ai.branch_id 
                    where ai.state in ('open', 'paid') and ai.type = 'out_invoice'
                    and ai.date_invoice between '%s' and '%s'
                    group by ai.branch_id
                ),
                data_untaxed_invoice as (
                    select
                    branch_.branch,
                    sum(iv.tot_untaxed_invoice) as tot_untaxed_invoice
                    from branch_
                    left join tot_untaxed_invoice iv on branch_.branch = iv.branch
                    group by branch_.branch
                ),
                tot_taxed_invoice as (
                    select
                    ai.branch_id as branch,
                    sum(ai.amount_total) as tot_taxed_invoice
                    from account_invoice ai
                    join res_branch br on br.id = ai.branch_id 
                    where ai.state in ('open', 'paid') and ai.type = 'out_invoice'
                    and ai.date_invoice between '%s' and '%s'
                    group by ai.branch_id
                ),
                data_tot_taxed_invoice as (
                    select
                    data_untaxed_invoice.branch,
                    data_untaxed_invoice.tot_untaxed_invoice as tot_untaxed_invoice,
                    sum(ti.tot_taxed_invoice) as tot_taxed_invoice
                    from data_untaxed_invoice
                    left join tot_taxed_invoice ti on data_untaxed_invoice.branch = ti.branch
                    group by data_untaxed_invoice.branch, data_untaxed_invoice.tot_untaxed_invoice
                ),
                vendor_bill_with_tax as (
                    select
                    ai.branch_id as branch,
                    sum(ai.amount_total) as vendor_bill_with_tax
                    from account_invoice ai
                    join res_branch br on br.id = ai.branch_id 
                    where ai.state in ('open', 'paid') and ai.type = 'in_invoice'
                    and ai.date_invoice between '%s' and '%s'
                    group by ai.branch_id
                ),
                data_vendor_bill_with_tax as (
                    select
                    data_tot_taxed_invoice.branch,
                    data_tot_taxed_invoice.tot_untaxed_invoice as tot_untaxed_invoice,
                    data_tot_taxed_invoice.tot_taxed_invoice as tot_taxed_invoice,
                    cr.tot_untaxed_credit as tot_untaxed_credit,
                    cr.tot_taxed_credit as tot_taxed_credit,
                    sum(vb.vendor_bill_with_tax) as vendor_bill_with_tax
                    from data_tot_taxed_invoice
                    LEFT join vendor_bill_with_tax vb on data_tot_taxed_invoice.branch = vb.branch
                    LEFT join data_tot_taxed_credit cr on cr.branch = vb.branch
                    group by data_tot_taxed_invoice.branch,
                    data_tot_taxed_invoice.tot_untaxed_invoice,
                    data_tot_taxed_invoice.tot_taxed_invoice, 
                    cr.tot_untaxed_credit, 
                    cr.tot_taxed_credit
                ),
                project_payout as (
                    select
                    al.branch_id as branch,
                    sum(al.debit) as project_payout
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    join account_account aa on aa.id = al.account_id
                    left join account_move am on am.id = al.move_id
                    join account_journal aj on aj.id = al.journal_id
                    where al.date between '%s' and '%s' and al.account_id in (20,952,953)
                    and am.state = 'posted' and aj.type != 'cash'
                    group by al.branch_id
                ),
                total_scrap_sale as (
                    select
                    al.branch_id as branch,
                    sum(al.debit) as project_payout
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    join account_account aa on aa.id = al.account_id
                    left join account_move am on am.id = al.move_id
                    join account_journal aj on aj.id = al.journal_id
                    where al.date between '%s' and '%s' and al.account_id in (20,952,953)
                    and am.state = 'posted' and aj.type != 'cash'
                    group by al.branch_id
                ),
                data_project_payout as (
                    select
                    data_vendor_bill_with_tax.branch,
                    data_vendor_bill_with_tax.tot_untaxed_invoice as tot_untaxed_invoice,
                    data_vendor_bill_with_tax.tot_taxed_invoice as tot_taxed_invoice,
                    data_vendor_bill_with_tax.tot_untaxed_credit as tot_untaxed_credit,
                    data_vendor_bill_with_tax.tot_taxed_credit as tot_taxed_credit,
                    data_vendor_bill_with_tax.vendor_bill_with_tax as vendor_bill_with_tax,
                    sum(pe.project_payout) as project_payout
                    from data_vendor_bill_with_tax
                    left join project_payout pe on data_vendor_bill_with_tax.branch = pe.branch
                    group by data_vendor_bill_with_tax.branch,
                    data_vendor_bill_with_tax.tot_untaxed_invoice,
                    data_vendor_bill_with_tax.tot_taxed_invoice,
                    data_vendor_bill_with_tax.tot_untaxed_credit,
                    data_vendor_bill_with_tax.tot_taxed_credit,
                    data_vendor_bill_with_tax.vendor_bill_with_tax
                ),
                project_payout_cash as (
                    select
                    al.branch_id as branch,
                    sum(al.debit) as project_payout_cash
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    join account_account aa on aa.id = al.account_id
                    left join account_move am on am.id = al.move_id
                    join account_journal aj on aj.id = al.journal_id
                    where al.date between '%s' and '%s' and al.account_id in (20,952,953)
                    and am.state = 'posted' and aj.type = 'cash'
                    group by al.branch_id
                ),
                project_payout_imprest as (
                    select
                    al.branch_id as branch,
                    sum(al.debit) as project_payout_imprest
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    join account_account aa on aa.id = al.account_id
                    left join account_move am on am.id = al.move_id
                    join account_journal aj on aj.id = al.journal_id
                    where al.date between '%s' and '%s' and al.account_id = 72
                    and am.state = 'posted' and aj.type != 'cash'
                    group by al.branch_id
                ),
                data_project_payout_cash as (
                    select
                    data_project_payout.branch,
                    data_project_payout.tot_untaxed_invoice as tot_untaxed_invoice,
                    data_project_payout.tot_taxed_invoice as tot_taxed_invoice,
                    data_project_payout.tot_untaxed_credit as tot_untaxed_credit,
                    data_project_payout.tot_taxed_credit as tot_taxed_credit,
                    data_project_payout.vendor_bill_with_tax as vendor_bill_with_tax,
                    data_project_payout.project_payout as project_payout,
                    sum(pe.project_payout_cash) as project_payout_cash,
                    sum(pi.project_payout_imprest) as project_payout_imprest
                    from data_project_payout
                    left join project_payout_cash pe on data_project_payout.branch = pe.branch
                    left join project_payout_imprest pi on data_project_payout.branch = pi.branch
                    group by data_project_payout.branch,
                    data_project_payout.tot_untaxed_invoice,
                    data_project_payout.tot_taxed_invoice,
                    data_project_payout.tot_untaxed_credit,
                    data_project_payout.tot_taxed_credit,
                    data_project_payout.vendor_bill_with_tax,
                    data_project_payout.project_payout
                ),
                admin_payout as (
                    select
                    cb.branch_id as branch,
                    sum(cbl.practical_amount_) as admin_payout
                    from crossovered_budget_lines cbl
                    left join crossovered_budget cb on cb.id = cbl.crossovered_budget_id
                    join res_branch br on br.id = cb.branch_id
                    join account_analytic_account aa on aa.id = cbl.analytic_account_id
                    where cbl.date_from >= '%s' and cbl.date_to <= '%s'
                    and cbl.budget_categ_id in (1, 2, 5)
                    group by cb.branch_id
                ),
                data_admin_payout as (
                    select
                    data_project_payout_cash.branch,
                    data_project_payout_cash.tot_untaxed_invoice as tot_untaxed_invoice,
                    data_project_payout_cash.tot_taxed_invoice as tot_taxed_invoice,
                    data_project_payout_cash.tot_untaxed_credit as tot_untaxed_credit,
                    data_project_payout_cash.tot_taxed_credit as tot_taxed_credit,
                    data_project_payout_cash.vendor_bill_with_tax as vendor_bill_with_tax,
                    data_project_payout_cash.project_payout as project_payout,
                    data_project_payout_cash.project_payout_cash as project_payout_cash,
                    data_project_payout_cash.project_payout_imprest as project_payout_imprest,
                    sum(ap.admin_payout) as admin_payout
                    from data_project_payout_cash
                    left join admin_payout ap on data_project_payout_cash.branch = ap.branch
                    group by data_project_payout_cash.branch,
                    data_project_payout_cash.tot_untaxed_invoice,
                    data_project_payout_cash.tot_taxed_invoice,
                    data_project_payout_cash.tot_untaxed_credit,
                    data_project_payout_cash.tot_taxed_credit,
                    data_project_payout_cash.vendor_bill_with_tax,
                    data_project_payout_cash.project_payout,
                    data_project_payout_cash.project_payout_cash,
                    data_project_payout_cash.project_payout_imprest
                ),
                asset_payout as (
                    select
                    cb.branch_id as branch,
                    sum(cbl.practical_amount_) as asset_payout
                    from crossovered_budget_lines cbl
                    left join crossovered_budget cb on cb.id = cbl.crossovered_budget_id
                    join res_branch br on br.id = cb.branch_id
                    join account_analytic_account aa on aa.id = cbl.analytic_account_id
                    where cbl.date_from >= '%s' and cbl.date_to <= '%s'
                    and cbl.budget_categ_id in (7, 8)
                    group by cb.branch_id
                ),
                data_asset_payout as (
                    select
                    data_admin_payout.branch,
                    data_admin_payout.tot_untaxed_invoice as tot_untaxed_invoice,
                    data_admin_payout.tot_taxed_invoice as tot_taxed_invoice,
                    data_admin_payout.tot_untaxed_credit as tot_untaxed_credit,
                    data_admin_payout.tot_taxed_credit as tot_taxed_credit,
                    data_admin_payout.vendor_bill_with_tax as vendor_bill_with_tax,
                    data_admin_payout.project_payout as project_payout,
                    data_admin_payout.project_payout_cash as project_payout_cash,
                    data_admin_payout.project_payout_imprest as project_payout_imprest,
                    data_admin_payout.admin_payout as admin_payout,
                    sum(ap.asset_payout) as asset_payout
                    from data_admin_payout
                    left join asset_payout ap on data_admin_payout.branch = ap.branch
                    group by data_admin_payout.branch,
                    data_admin_payout.tot_untaxed_invoice,
                    data_admin_payout.tot_taxed_invoice,
                    data_admin_payout.tot_untaxed_credit,
                    data_admin_payout.tot_taxed_credit,
                    data_admin_payout.vendor_bill_with_tax,
                    data_admin_payout.project_payout,
                    data_admin_payout.project_payout_cash,
                    data_admin_payout.project_payout_imprest,
                    data_admin_payout.admin_payout
                ),
                loan_repayment as (
                    select
                    cb.branch_id as branch,
                    sum(cbl.practical_amount_) as loan_repayment
                    from crossovered_budget_lines cbl
                    left join crossovered_budget cb on cb.id = cbl.crossovered_budget_id
                    join res_branch br on br.id = cb.branch_id
                    join account_analytic_account aa on aa.id = cbl.analytic_account_id
                    where cbl.date_from >= '%s' and cbl.date_to <= '%s'
                    and cbl.budget_categ_id = 6
                    group by cb.branch_id
                ),
                data_loan_repayment as (
                    select
                    data_asset_payout.branch,
                    data_asset_payout.tot_untaxed_invoice as tot_untaxed_invoice,
                    data_asset_payout.tot_taxed_invoice as tot_taxed_invoice,
                    data_asset_payout.tot_untaxed_credit as tot_untaxed_credit,
                    data_asset_payout.tot_taxed_credit as tot_taxed_credit,
                    data_asset_payout.vendor_bill_with_tax as vendor_bill_with_tax,
                    data_asset_payout.project_payout as project_payout,
                    data_asset_payout.project_payout_cash as project_payout_cash,
                    data_asset_payout.project_payout_imprest as project_payout_imprest,
                    data_asset_payout.admin_payout as admin_payout,
                    data_asset_payout.asset_payout as asset_payout,
                    sum(lr.loan_repayment) as loan_repayment
                    from data_asset_payout
                    left join loan_repayment lr on data_asset_payout.branch = lr.branch
                    group by data_asset_payout.branch,
                    data_asset_payout.tot_untaxed_invoice,
                    data_asset_payout.tot_taxed_invoice,
                    data_asset_payout.tot_untaxed_credit,
                    data_asset_payout.tot_taxed_credit,
                    data_asset_payout.vendor_bill_with_tax,
                    data_asset_payout.project_payout,
                    data_asset_payout.project_payout_cash,
                    data_asset_payout.project_payout_imprest,
                    data_asset_payout.admin_payout,
                    data_asset_payout.asset_payout
                ),
                tax_payout as (
                    select
                    cb.branch_id as branch,
                    sum(cbl.practical_amount_) as tax_payout
                    from crossovered_budget_lines cbl
                    left join crossovered_budget cb on cb.id = cbl.crossovered_budget_id
                    join res_branch br on br.id = cb.branch_id
                    join account_analytic_account aa on aa.id = cbl.analytic_account_id
                    where cbl.date_from >= '%s' and cbl.date_to <= '%s'
                    and cbl.budget_categ_id in (3, 4)
                    group by cb.branch_id
                ),
                data_tax_payout as (
                    select
                    data_loan_repayment.branch,
                    data_loan_repayment.tot_untaxed_invoice as tot_untaxed_invoice,
                    data_loan_repayment.tot_taxed_invoice as tot_taxed_invoice,
                    data_loan_repayment.tot_untaxed_credit as tot_untaxed_credit,
                    data_loan_repayment.tot_taxed_credit as tot_taxed_credit,
                    data_loan_repayment.vendor_bill_with_tax as vendor_bill_with_tax,
                    data_loan_repayment.project_payout as project_payout,
                    data_loan_repayment.project_payout_cash as project_payout_cash,
                    data_loan_repayment.project_payout_imprest as project_payout_imprest,
                    data_loan_repayment.admin_payout as admin_payout,
                    data_loan_repayment.asset_payout as asset_payout,
                    data_loan_repayment.loan_repayment as loan_repayment,
                    sum(tp.tax_payout) as tax_payout
                    from data_loan_repayment
                    left join tax_payout tp on data_loan_repayment.branch = tp.branch
                    group by data_loan_repayment.branch,
                    data_loan_repayment.tot_untaxed_invoice,
                    data_loan_repayment.tot_taxed_invoice,
                    data_loan_repayment.tot_untaxed_credit,
                    data_loan_repayment.tot_taxed_credit,
                    data_loan_repayment.vendor_bill_with_tax,
                    data_loan_repayment.project_payout,
                    data_loan_repayment.project_payout_cash,
                    data_loan_repayment.project_payout_imprest,
                    data_loan_repayment.admin_payout,
                    data_loan_repayment.asset_payout,
                    data_loan_repayment.loan_repayment
                ),
                all_total as (
                    select
                    cb.branch_id as branch,
                    sum(cbl.practical_amount_) as all_total
                    from crossovered_budget_lines cbl
                    left join crossovered_budget cb on cb.id = cbl.crossovered_budget_id
                    join res_branch br on br.id = cb.branch_id
                    join account_analytic_account aa on aa.id = cbl.analytic_account_id
                    where cbl.date_from >= '%s' and cbl.date_to <= '%s'
                    group by cb.branch_id
                ),
                data_all_total as (
                    select
                    data_tax_payout.branch,
                    data_tax_payout.tot_untaxed_invoice as tot_untaxed_invoice,
                    data_tax_payout.tot_taxed_invoice as tot_taxed_invoice,
                    data_tax_payout.tot_untaxed_credit as tot_untaxed_credit,
                    data_tax_payout.tot_taxed_credit as tot_taxed_credit,
                    data_tax_payout.vendor_bill_with_tax as vendor_bill_with_tax,
                    data_tax_payout.project_payout as project_payout,
                    data_tax_payout.project_payout_cash as project_payout_cash,
                    data_tax_payout.project_payout_imprest as project_payout_imprest,
                    data_tax_payout.admin_payout as admin_payout,
                    data_tax_payout.asset_payout as asset_payout,
                    data_tax_payout.loan_repayment as loan_repayment,
                    data_tax_payout.tax_payout as tax_payout,
                    sum(at.all_total) as all_total
                    from data_tax_payout
                    left join all_total at on data_tax_payout.branch = at.branch
                    group by data_tax_payout.branch,
                    data_tax_payout.tot_untaxed_invoice,
                    data_tax_payout.tot_taxed_invoice,
                    data_tax_payout.tot_untaxed_credit,
                    data_tax_payout.tot_taxed_credit,
                    data_tax_payout.vendor_bill_with_tax,
                    data_tax_payout.project_payout,
                    data_tax_payout.project_payout_cash,
                    data_tax_payout.project_payout_imprest,
                    data_tax_payout.admin_payout,
                    data_tax_payout.asset_payout,
                    data_tax_payout.loan_repayment,
                    data_tax_payout.tax_payout
                ),
                tot_recovery as (
                    select
                    ap.branch_id as branch,
                    sum(ap.amount) as tot_recovery
                    from account_payment ap
                    join res_branch br on br.id = ap.branch_id
                    where ap.payment_date between '%s' and '%s'
                    and ap.state != 'cancelled'
                    and ap.payment_type = 'inbound'
                    group by ap.branch_id
                ),
                data_tot_recovery as (
                    select
                    data_all_total.branch,
                    data_all_total.tot_untaxed_invoice as tot_untaxed_invoice,
                    data_all_total.tot_taxed_invoice as tot_taxed_invoice,
                    data_all_total.tot_untaxed_credit as tot_untaxed_credit,
                    data_all_total.tot_taxed_credit as tot_taxed_credit,
                    data_all_total.vendor_bill_with_tax as vendor_bill_with_tax,
                    data_all_total.project_payout as project_payout,
                    data_all_total.project_payout_cash as project_payout_cash,
                    data_all_total.project_payout_imprest as project_payout_imprest,
                    data_all_total.admin_payout as admin_payout,
                    data_all_total.asset_payout as asset_payout,
                    data_all_total.loan_repayment as loan_repayment,
                    data_all_total.tax_payout as tax_payout,
                    data_all_total.all_total as all_total,
                    sum(tr.tot_recovery) as tot_recovery
                    from data_all_total
                    left join tot_recovery tr on data_all_total.branch = tr.branch
                    group by data_all_total.branch,
                    data_all_total.tot_untaxed_invoice,
                    data_all_total.tot_taxed_invoice,
                    data_all_total.tot_untaxed_credit,
                    data_all_total.tot_taxed_credit,
                    data_all_total.vendor_bill_with_tax,
                    data_all_total.project_payout,
                    data_all_total.project_payout_cash,
                    data_all_total.project_payout_imprest,
                    data_all_total.admin_payout,
                    data_all_total.asset_payout,
                    data_all_total.loan_repayment,
                    data_all_total.tax_payout,
                    data_all_total.all_total
                ),
                tot_cash_trans as (
                    select
                    am.branch_id as branch,
                    sum(am.amount) as tot_cash_trans
                    from account_move am
                    join res_branch br on br.id = am.branch_id
                    where am.date between '%s' and '%s'
                    and am.job_code in (615, 614, 573, 619, 613, 612, 616)
                    group by am.branch_id
                ),
                data_tot_cash_trans as (
                    select
                    data_tot_recovery.branch,
                    data_tot_recovery.tot_untaxed_invoice as tot_untaxed_invoice,
                    data_tot_recovery.tot_taxed_invoice as tot_taxed_invoice,
                    data_tot_recovery.tot_untaxed_credit as tot_untaxed_credit,
                    data_tot_recovery.tot_taxed_credit as tot_taxed_credit,
                    data_tot_recovery.vendor_bill_with_tax as vendor_bill_with_tax,
                    data_tot_recovery.project_payout as project_payout,
                    data_tot_recovery.project_payout_cash as project_payout_cash,
                    data_tot_recovery.project_payout_imprest as project_payout_imprest,
                    data_tot_recovery.admin_payout as admin_payout,
                    data_tot_recovery.asset_payout as asset_payout,
                    data_tot_recovery.loan_repayment as loan_repayment,
                    data_tot_recovery.tax_payout as tax_payout,
                    data_tot_recovery.all_total as all_total,
                    data_tot_recovery.tot_recovery as tot_recovery,
                    sum(tc.tot_cash_trans) as tot_cash_trans
                    from data_tot_recovery
                    left join tot_cash_trans tc on data_tot_recovery.branch = tc.branch
                    group by data_tot_recovery.branch,
                    data_tot_recovery.tot_untaxed_invoice,
                    data_tot_recovery.tot_taxed_invoice,
                    data_tot_recovery.tot_untaxed_credit,
                    data_tot_recovery.tot_taxed_credit,
                    data_tot_recovery.vendor_bill_with_tax,
                    data_tot_recovery.project_payout,
                    data_tot_recovery.project_payout_cash,
                    data_tot_recovery.project_payout_imprest,
                    data_tot_recovery.admin_payout,
                    data_tot_recovery.asset_payout,
                    data_tot_recovery.loan_repayment,
                    data_tot_recovery.tax_payout,
                    data_tot_recovery.all_total,
                    data_tot_recovery.tot_recovery
                ),
                booked_gst as (
                    select
                    am.branch_id as branch,
                    sum(aml.balance) as booked_gst
                    from account_move_line aml
                    left join account_move am on am.id = aml.move_id
                    join res_branch br on br.id = am.branch_id
                    where aml.date between '%s' and '%s'
                    and aml.account_id in (4, 5, 6)
                    group by am.branch_id
                ),
                data_booked_gst as (
                    select
                    data_tot_cash_trans.branch,
                    data_tot_cash_trans.tot_untaxed_invoice as tot_untaxed_invoice,
                    data_tot_cash_trans.tot_taxed_invoice as tot_taxed_invoice,
                    data_tot_cash_trans.tot_untaxed_credit as tot_untaxed_credit,
                    data_tot_cash_trans.tot_taxed_credit as tot_taxed_credit,
                    data_tot_cash_trans.vendor_bill_with_tax as vendor_bill_with_tax,
                    data_tot_cash_trans.project_payout as project_payout,
                    data_tot_cash_trans.project_payout_cash as project_payout_cash,
                    data_tot_cash_trans.project_payout_imprest as project_payout_imprest,
                    data_tot_cash_trans.admin_payout as admin_payout,
                    data_tot_cash_trans.asset_payout as asset_payout,
                    data_tot_cash_trans.loan_repayment as loan_repayment,
                    data_tot_cash_trans.tax_payout as tax_payout,
                    data_tot_cash_trans.all_total as all_total,
                    data_tot_cash_trans.tot_recovery as tot_recovery,
                    data_tot_cash_trans.tot_cash_trans as tot_cash_trans,
                    sum(bg.booked_gst) as booked_gst
                    from data_tot_cash_trans
                    left join booked_gst bg on data_tot_cash_trans.branch = bg.branch
                    group by data_tot_cash_trans.branch,
                    data_tot_cash_trans.tot_untaxed_invoice,
                    data_tot_cash_trans.tot_taxed_invoice,
                    data_tot_cash_trans.tot_untaxed_credit,
                    data_tot_cash_trans.tot_taxed_credit,
                    data_tot_cash_trans.vendor_bill_with_tax,
                    data_tot_cash_trans.project_payout,
                    data_tot_cash_trans.project_payout_cash,
                    data_tot_cash_trans.project_payout_imprest,
                    data_tot_cash_trans.admin_payout,
                    data_tot_cash_trans.asset_payout,
                    data_tot_cash_trans.loan_repayment,
                    data_tot_cash_trans.tax_payout,
                    data_tot_cash_trans.all_total,
                    data_tot_cash_trans.tot_recovery,
                    data_tot_cash_trans.tot_cash_trans
                ),
                scrap_sale as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) * -1 as scrap_sale
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    join account_account aa on aa.id = al.account_id
                    left join account_move am on am.id = al.move_id
                    join account_journal aj on aj.id = al.journal_id
                    where al.date between '%s' and '%s' and al.account_id in (947)
                    and am.state = 'posted'
                    group by al.branch_id
                ),
                data_scrap_sale as (
                    select
                    data_booked_gst.branch,
                    data_booked_gst.tot_untaxed_invoice as tot_untaxed_invoice,
                    data_booked_gst.tot_taxed_invoice as tot_taxed_invoice,
                    data_booked_gst.tot_untaxed_credit as tot_untaxed_credit,
                    data_booked_gst.tot_taxed_credit as tot_taxed_credit,
                    data_booked_gst.vendor_bill_with_tax as vendor_bill_with_tax,
                    data_booked_gst.project_payout as project_payout,
                    data_booked_gst.project_payout_cash as project_payout_cash,
                    data_booked_gst.project_payout_imprest as project_payout_imprest,
                    data_booked_gst.admin_payout as admin_payout,
                    data_booked_gst.asset_payout as asset_payout,
                    data_booked_gst.loan_repayment as loan_repayment,
                    data_booked_gst.tax_payout as tax_payout,
                    data_booked_gst.all_total as all_total,
                    data_booked_gst.tot_recovery as tot_recovery,
                    data_booked_gst.tot_cash_trans as tot_cash_trans,
                    data_booked_gst.booked_gst as booked_gst,
                    sum(sc.scrap_sale) as scrap_sale
                    from data_booked_gst
                    left join scrap_sale sc on data_booked_gst.branch = sc.branch
                    group by data_booked_gst.branch,
                    data_booked_gst.tot_untaxed_invoice,
                    data_booked_gst.tot_taxed_invoice,
                    data_booked_gst.tot_untaxed_credit,
                    data_booked_gst.tot_taxed_credit,
                    data_booked_gst.vendor_bill_with_tax,
                    data_booked_gst.project_payout,
                    data_booked_gst.project_payout_cash,
                    data_booked_gst.project_payout_imprest,
                    data_booked_gst.admin_payout,
                    data_booked_gst.asset_payout,
                    data_booked_gst.loan_repayment,
                    data_booked_gst.tax_payout,
                    data_booked_gst.all_total,
                    data_booked_gst.tot_recovery,
                    data_booked_gst.tot_cash_trans,
                    data_booked_gst.booked_gst
                ),
                
                uncleared_cheques as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) * -1 as uncleared_cheques
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    join account_account aa on aa.id = al.account_id
                    left join account_move am on am.id = al.move_id
                    join account_journal aj on aj.id = al.journal_id
                    where al.date between '%s' and '%s' and al.account_id in (80, 81, 82, 83, 84, 85, 86, 133)
                    and am.state = 'posted' and al.bank_date_ is null
                    group by al.branch_id
                ),
                data_uncleared_cheques as (
                    select
                    data_scrap_sale.branch,
                    data_scrap_sale.tot_untaxed_invoice as tot_untaxed_invoice,
                    data_scrap_sale.tot_taxed_invoice as tot_taxed_invoice,
                    data_scrap_sale.tot_untaxed_credit as tot_untaxed_credit,
                    data_scrap_sale.tot_taxed_credit as tot_taxed_credit,
                    data_scrap_sale.vendor_bill_with_tax as vendor_bill_with_tax,
                    data_scrap_sale.project_payout as project_payout,
                    data_scrap_sale.project_payout_cash as project_payout_cash,
                    data_scrap_sale.project_payout_imprest as project_payout_imprest,
                    data_scrap_sale.admin_payout as admin_payout,
                    data_scrap_sale.asset_payout as asset_payout,
                    data_scrap_sale.loan_repayment as loan_repayment,
                    data_scrap_sale.tax_payout as tax_payout,
                    data_scrap_sale.all_total as all_total,
                    data_scrap_sale.tot_recovery as tot_recovery,
                    data_scrap_sale.tot_cash_trans as tot_cash_trans,
                    data_scrap_sale.booked_gst as booked_gst,
                    data_scrap_sale.scrap_sale as  scrap_sale,
                    sum(uc.uncleared_cheques) as uncleared_cheques
                    from data_scrap_sale
                    left join uncleared_cheques uc on data_scrap_sale.branch = uc.branch
                    group by data_scrap_sale.branch,
                    data_scrap_sale.tot_untaxed_invoice,
                    data_scrap_sale.tot_taxed_invoice,
                    data_scrap_sale.tot_untaxed_credit,
                    data_scrap_sale.tot_taxed_credit,
                    data_scrap_sale.vendor_bill_with_tax,
                    data_scrap_sale.project_payout,
                    data_scrap_sale.project_payout_cash,
                    data_scrap_sale.project_payout_imprest,
                    data_scrap_sale.admin_payout,
                    data_scrap_sale.asset_payout,
                    data_scrap_sale.loan_repayment,
                    data_scrap_sale.tax_payout,
                    data_scrap_sale.all_total,
                    data_scrap_sale.tot_recovery,
                    data_scrap_sale.tot_cash_trans,
                    data_scrap_sale.booked_gst,
                    data_scrap_sale.scrap_sale
                )
                
                
                select row_number() OVER (ORDER BY branch) AS id,
                branch, tot_untaxed_invoice, tot_taxed_invoice, tot_untaxed_credit, tot_taxed_credit,
                 (tot_untaxed_credit + tot_taxed_credit) as tot_credit,
                 vendor_bill_with_tax,
                project_payout, project_payout_cash, project_payout_imprest, admin_payout, asset_payout,
                loan_repayment, tax_payout, all_total, tot_recovery,
                ((tot_recovery * 18) / 100) as tax_recovery, scrap_sale,
                (tot_taxed_invoice - tot_untaxed_invoice) as tot_gst, tot_cash_trans,
                booked_gst, uncleared_cheques,
                ((tot_recovery * 100) / nullif((tot_taxed_invoice-tot_taxed_credit), 0)) as percentage_recovery,
                (((project_payout+project_payout_cash) * 100) / nullif(vendor_bill_with_tax, 0)) as percent_vendor_payout
                from data_uncleared_cheques
        """ % (first_day, last_day,
               first_day, last_day,
               first_day, last_day,
               first_day, last_day,
               first_day, last_day,
               first_day, last_day,
               first_day, last_day,
               first_day, last_day,
               first_day, last_day,
               first_day, last_day,
               first_day, last_day,
               first_day, last_day,
               first_day, last_day,
               first_day, last_day,
               first_day, last_day,
               first_day, last_day,
               first_day, last_day,
               first_day, last_day,
               first_day, last_day
               ))

            result = self._cr.fetchall()
            rows = result
            values = ', '.join(map(str, rows))
            sql = ("""INSERT INTO in_out_report 
                        (id, branch, tot_untaxed_invoice,
                        tot_taxed_invoice, tot_untaxed_credit,
                        tot_taxed_credit, tot_credit, vendor_bill_with_tax,
                        project_payout, project_payout_cash, project_payout_imprest, admin_payout, asset_payout,
                        loan_repayment, tax_payout, all_total, tot_recovery, tax_recovery, scrap_sale,
                        tot_gst, tot_cash_trans, booked_gst, uncleared_cheques, percentage_recovery, percent_vendor_payout
                        ) VALUES {}""".format(values).replace('None', 'null')
            )
            self._cr.execute(sql)

            action = {
                'type': 'ir.actions.act_window',
                'views': [
                    (tree_view_id, 'tree'), (form_view_id, 'form'),
                    (graph_view_id, 'graph'), (pivot_view_id, 'pivot')
                ],
                'view_mode': 'tree,form',
                'name': _('In Out Report: %s - %s' %(first_day.strftime('%d %b %y'), last_day.strftime('%d %b %y'))),
                'res_model': 'in.out.report',
                'search_view_id': search_view_ref and search_view_ref.id,
            }
            return action