# -*- encoding: utf-8 -*-
{
	"name": "GTS Inventory Force Date 11",
	"version": "1.1",
	"author": "Geotechnosoft Pvt.Ltd.",
	"website": "http://www.geotechnosoft.com",
	"sequence": 0,
	"depends": [
		"base","stock"
	],
	"category": "Stock",
	"complexity": "easy",
	'license': 'LGPL-3',
    'support': 'info@geotechnosoft.com',
	"description": """
	Add Force Date in Stock Inventory to update in all Stock MOve Lines
	""",
	"data": [
		'views/stock_inventory_view.xml',
	],
	"demo": [
	],
	"test": [
	],
    'images': ['static/description/banner.png'],
	"auto_install": False,
	"installable": True,
	"application": False,

}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
