from odoo import api, fields, models, _
from odoo.exceptions import UserError


class StockInventory(models.Model):
    _inherit = 'stock.inventory'

    force_date = fields.Datetime('Force Date', readonly=True,
                                 states={'draft': [('readonly', False)]})

    # Inherit: to update Move LInes Date with Force Date
    def action_done(self):
        res = super(StockInventory, self).action_done()
        if self.force_date:
            self._cr.execute("UPDATE stock_move_line set date = %s where move_id in %s",
                             (self.force_date, tuple(self.move_ids.ids, ),))
