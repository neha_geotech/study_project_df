# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'GTS Dream CRM',
    'summary': 'Additional Buttons',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'website': 'http://www.geotechnosoft.com',
    'category': 'CRM',
    'version': '11.0.1.0.0',
    'depends': [
        'crm', 'mrp','purchase','gts_dream_crm', 'gts_dream_purchase', 'analytic', 'account'
    ],
    'data': [
        'views/misc_view.xml',
        'views/crm_lead_view.xml',
    ],
    'installable': True,
}
