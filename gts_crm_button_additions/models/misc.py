# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

from odoo import api, fields, models, _


class AccountAnalyticLine(models.Model):
    _inherit = 'account.analytic.line'

    opportunity_id = fields.Many2one('crm.lead', related='account_id.opportunity_id', string='Opportunity')


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    opportunity_id = fields.Many2one('crm.lead', related='job_code.opportunity_id', string='Opportunity')


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    opportunity_id = fields.Many2one('crm.lead', related='job_code.opportunity_id', string='Opportunity')


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    opportunity_id = fields.Many2one('crm.lead', related='analytic_account_id.opportunity_id', string='Opportunity')