# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

from odoo import api, fields, models, _


class CrmLead(models.Model):
    _inherit = 'crm.lead'

    bom_number = fields.Integer(compute='_compute_total_mrp', string="Number of BOM")
    purchase_number = fields.Integer(compute='_compute_total_purchase', string="Number of Purchase")
    invoice_number = fields.Integer(compute='_compute_total_invoices', string="Number of Invoices")
    analytic_number = fields.Integer(compute='_compute_total_analytic', string="Number of Analytic Entries")
    move_number = fields.Integer(compute='_compute_total_move', string='Number of Move Lines')

    @api.multi
    def _compute_total_mrp(self):
        for data in self:
            nbr = 0
            sum_ = 0
            if data.related_job_code:
                bom_ids = self.env['mrp.bom'].search(
                    [('job_code', '=', data.related_job_code.id), ('active', '=', True)]
                )
                for res in bom_ids:
                    nbr += 1
                    for rec in res.bom_line_ids:
                        sum_ += rec.planned_amount_bom
                    data.bom_value = sum_
                data.bom_number = nbr


    @api.multi
    def _compute_total_invoices(self):
        for data in self:
            nbr = 0
            if data.related_job_code:
                invoice_ids = self.env['account.invoice'].search(
                    [('job_code', '=', data.related_job_code.id), ('type', '=', 'out_invoice')]
                )
                for inv in invoice_ids:
                    nbr += 1
                data.invoice_number = nbr

    # Booked NM: Group: Non-Material & Skilled
    @api.multi
    def _compute_total_analytic(self):
        for data in self:
            nbr = 0
            sum_ = 0.0
            if data.related_job_code:
                non_mat_lines = self.env['account.move.line'].search(
                    [('analytic_account_id', '=', data.related_job_code.id),
                    ('account_id.group_id', 'in', (5,62)),
                    ('move_id.state', '=', 'posted'),
                    ]
                )
                for line in non_mat_lines:
                    sum_ += line.balance

                data.update({
                    'analytic_value': sum_,
                    'analytic_number': len(non_mat_lines)

                })
    # @api.multi
    # def _compute_total_analytic(self):
    #     for data in self:
    #         nbr = 0
    #         sum_ = 0.0
    #         if data.related_job_code:
    #             analytic_line_ids = self.env['account.analytic.line'].search(
    #                 [('account_id', '=', data.related_job_code.id),
    #                 ('opportunity_id', '=', data.id),
    #                 ('general_account_id.group_id', 'in', (5,62)),
    #                 ('general_account_id.user_type_id.name', '!=', 'Bank and Cash')
    #                 ]
    #             )
    #             for line in analytic_line_ids:
    #                 sum_ += line.amount

    #             data.update({
    #                 'analytic_value': sum_ * -1,
    #                 'analytic_number': len(analytic_line_ids)

    #             })

    @api.multi
    def _compute_total_purchase(self):
        for data in self:
            nbr = 0
            sum_ = 0.0
            if data.related_job_code:
                purchase_line_ids = self.env['purchase.order'].search(
                    [('job_code', '=', data.related_job_code.id), ('state', 'in', ('open','purchase'))]
                )
                for line in purchase_line_ids:
                    nbr += 1
                    sum_ += line.amount_untaxed
                # data.purchase_number = nbr

                # for rec in purchase_line_ids:

                data.update({
                    'purchase_value': sum_,
                    'purchase_number': nbr

                })

    @api.multi
    def _compute_total_move(self):
        for data in self:
            nbr = 0
            sum_ = 0.0
            if data.related_job_code:
                move_line_ids = self.env['account.move.line'].search(
                    [('analytic_account_id', '=', data.related_job_code.id)]
                )
                for line in move_line_ids:
                    if line.account_id.group_id.id == 47 and line.move_id.state == 'posted':
                        nbr += 1
                        sum_ += line.balance
                data.update({
                    'booked_mat': sum_,
                    'move_number': nbr

                })



