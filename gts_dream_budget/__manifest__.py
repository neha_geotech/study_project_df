{
    'name': 'Budget Dream Factory',
    'version': '1.0',
    'category': '',
    'sequence': 75,
    'summary': 'Dream Factory ',
    'description': "",
    'website': '',
    'depends': ['account_budget', 'branch'],
    'data': [
        'security/ir.model.access.csv',
        'data/budget_data.xml',
        'views/budget_view.xml',
        'views/budget_categ_view.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
