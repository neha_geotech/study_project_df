from odoo import fields, models, api


class CrossoveredBudgetCategory(models.Model):
    _name = "crossovered.budget.category"
    _rec_name = 'category_name'
    _order = 'id'

    category_name = fields.Char('Name')

