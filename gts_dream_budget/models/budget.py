from odoo import fields, models, api


class CrossoveredBudget(models.Model):
    _inherit = "crossovered.budget"

    branch_id = fields.Many2one('res.branch', 'Branch')

    @api.onchange('crossovered_budget_line')
    def budget_line_change(self):
        for data in self.crossovered_budget_line:
            if self.branch_id:
                data.branch_id = self.branch_id.id

    @api.onchange('branch_id')
    def branch_change(self):
        for data in self.crossovered_budget_line:
            if self.branch_id:
                data.branch_id = self.branch_id.id

#     @api.multi
#     def _compute_difference_amount(self):
#         for line in self:
#             if line.practical_amount < 0:
#                 result = line.planned_amount + line.practical_amount
#             else:
#                 result = line.planned_amount - line.practical_amount
#             line.difference_amount = result
#
#     difference_amount = fields.Float(
#         compute='_compute_difference_amount',
#         string='Difference Amount',
#         digits=0
#     )
#
#     @api.multi
#     def _compute_practical_amount(self):
#         """
#         Making change into the Query
#         Using OR for general_account_id instead of AND
#         """
#         for line in self:
#             result = 0.0
#             acc_ids = line.general_budget_id.account_ids.ids
#             date_to = self.env.context.get('wizard_date_to') or line.date_to
#             date_from = self.env.context.get('wizard_date_from') or line.date_from
#             if line.analytic_account_id.id:
#                 self.env.cr.execute("""
#                         SELECT SUM(amount)
#                         FROM account_analytic_line
#                         WHERE account_id=%s
#                             AND (date between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd'))
#                             OR general_account_id=ANY(%s)""",
#                                     (line.analytic_account_id.id, date_from, date_to, acc_ids,))
#                 result = self.env.cr.fetchone()[0] or 0.0
#             line.practical_amount = result
#
#     @api.multi
#     def _compute_percentage(self):
#         for line in self:
#             if line.practical_amount != 0.00:
#                 if line.practical_amount < 0:
#                     line.percentage = ((line.planned_amount + line.practical_amount) / line.planned_amount) * 100
#                 else:
#                     line.percentage = ((line.planned_amount - line.practical_amount) / line.planned_amount) * 100
#             else:
#                 line.percentage = 0.00


class CrossoveredBudgetLines(models.Model):
    _inherit = "crossovered.budget.lines"
    _rec_name = 'crossovered_budget_id'

    balance = fields.Float(string='Balance', copy=False)
    balance_percentage = fields.Float(string='% Balance', copy=False)
    branch_id = fields.Many2one('res.branch', 'Branch')
    practical_amount_ = fields.Float(string='Practical Amount', copy=False)
    budget_categ_id = fields.Many2one('crossovered.budget.category', 'Budget Category')

    @api.model
    def _compute_practical_amount_schedule(self):
        search_id = self.env['crossovered.budget.lines'].search([])
        for line in search_id:
            result = 0.0
            # acc_ids = line.general_budget_id.account_ids.ids
            # date_to = self.env.context.get('wizard_date_to') or line.date_to
            # date_from = self.env.context.get('wizard_date_from') or line.date_from
            # if line.analytic_account_id.id:
            #     if line.analytic_account_id.vendor_payout == True:
            #         self.env.cr.execute("""
            #                 SELECT SUM(amount) * -1
            #                 FROM account_analytic_line
            #                 WHERE account_id=%s
            #                     AND vendor_payout_reco IS True
            #                     AND (date between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd'))
            #                     AND general_account_id=ANY(%s)""",
            #                             (line.analytic_account_id.id, date_from, date_to, acc_ids,))
            #         result = self.env.cr.fetchone()[0] or 0.0
            # if result:
            #     line.practical_amount_ = result
            total = line.planned_amount - line.practical_amount_
            line.balance = total
            if line.planned_amount != 0.0:
                c = line.balance * 100 / line.planned_amount
                line.balance_percentage = c





