# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'Budget Report',
    'summary': 'Budget Report',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'website': 'http://www.geotechnosoft.com',
    'category': 'Account',
    'version': '1.0',
    'depends': [
        'account', 'account_budget', 'branch'
    ],
    'data': [
        # 'security/ir.model.access.csv',
        'report/budget_report_view.xml',
    ],
    'installable': True,
}
