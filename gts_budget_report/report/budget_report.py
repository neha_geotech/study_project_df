# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft

from odoo import api, fields, models, tools
from datetime import datetime, date, timedelta


class BudgetReport(models.Model):
    '''
    This is report view table to show Variance Report
    '''
    _name = 'budget.report'
    _auto = False

    crossovered_budget_id = fields.Many2one('crossovered.budget', 'Budget')
    job_code = fields.Many2one('account.analytic.account', 'Job Code')
    general_budget_id = fields.Many2one('account.budget.post', 'Budgetary Position')
    date_from = fields.Date('Start Date')
    date_to = fields.Date('End Date')
    branch_id = fields.Many2one('res.branch', 'Branch')
    paid_date = fields.Date('Paid Date')
    planned_amount = fields.Float('Planned Amount')
    practical_amount = fields.Float(string='Practical Amount')
    balance = fields.Float(string='Balance')

    @api.model_cr
    def init(self):
        account_ids = self.env['crossovered.budget.lines'].search([])
        accounts = []
        for data in account_ids:
            accounts = data.general_budget_id.account_ids.ids

        """
        Query to create table for variance report
        """
        tools.drop_view_if_exists(self._cr, 'budget_report')
        self._cr.execute("""
            CREATE or REPLACE VIEW budget_report AS (
                with budget_lines as (
                select
                cbl.crossovered_budget_id as crossovered_budget_id,
                cbl.general_budget_id as general_budget_id,
                cbl.analytic_account_id as job_code,
                cbl.date_from as date_from,
                cbl.date_to as date_to,
                cb.branch_id as branch_id,
                sum(cbl.planned_amount) as planned_amount
                from crossovered_budget_lines cbl
                left join crossovered_budget cb on cb.id = cbl.crossovered_budget_id
                group by cbl.general_budget_id, cbl.analytic_account_id, cbl.date_from,
                cbl.date_to, cb.branch_id, cbl.crossovered_budget_id
            ),
                analytic_line as (
                select
                sum(al.amount) as practical_amount,
                al.account_id as job_code
                from account_analytic_line al
                left join crossovered_budget_lines cbl on cbl.analytic_account_id = al.account_id
                where al.general_account_id in (%s)
                and cbl.analytic_account_id = al.account_id
                group by al.account_id
            ),
            report_data as (
                select
                budget_lines.crossovered_budget_id as crossovered_budget_id,
                budget_lines.general_budget_id as general_budget_id,
                budget_lines.job_code as job_code,
                budget_lines.date_from as date_from,
                budget_lines.date_to as date_to,
                budget_lines.branch_id as branch_id,
                budget_lines.planned_amount as planned_amount,
                sum(a.practical_amount) as practical_amount
                from budget_lines
                left join analytic_line a on budget_lines.job_code = a.job_code
                group by budget_lines.general_budget_id, budget_lines.job_code,
                budget_lines.date_from, budget_lines.date_to, budget_lines.branch_id,
                budget_lines.planned_amount, budget_lines.crossovered_budget_id
            )
                select
                row_number() OVER () AS id, crossovered_budget_id, general_budget_id,
                job_code, date_from, date_to, branch_id, planned_amount, practical_amount
                from report_data
            )""" % tuple(accounts))
