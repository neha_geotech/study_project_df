from odoo import models, fields, api, _
from datetime import date, timedelta, datetime


class DebtorCreditorWiz(models.TransientModel):
    _name = 'debtor.creditor.wiz'

    from_date = fields.Date('Start Date', default=fields.Datetime.now)
    end_date = fields.Date('End Date', default=fields.Datetime.now)

    @api.multi
    def open_dr_cr_table(self):
        report_search = self.env['debtor.creditor.report'].search([])
        if report_search:
            report_search.unlink()
        branches = self.env['res.branch'].search([])
        self._cr.execute('''
                            delete  
                            from debtor_creditor_report
                            where branch in %s
                        ''', (tuple(branches.ids),))
        if self.from_date and self.end_date:
            tree_view_id = self.env.ref('gts_dream_debtor_creditor_report.view_debtor_creditor_report_tree').id
            form_view_id = self.env.ref('gts_dream_debtor_creditor_report.view_debtor_creditor_report_form').id
            graph_view_id = self.env.ref('gts_dream_debtor_creditor_report.view_debtor_creditor_report_graph').id
            pivot_view_id = self.env.ref('gts_dream_debtor_creditor_report.view_debtor_creditor_report_pivot').id
            search_view_ref = self.env.ref('gts_dream_debtor_creditor_report.view_debtor_creditor_report_search', False)

            tds_paid_ac = self.env['account.account'].search([('name', '=', 'TDS Paid')], limit=1)


            date_format = '%Y-%m-%d'
            first_day_ = datetime.strptime(self.from_date, date_format)
            last_day_ = datetime.strptime(self.end_date, date_format)
            first_day = first_day_
            last_day = last_day_
            year_date = last_day - timedelta(days=364)
            bank_accounts = self.env['account.account'].search([
                ('group_id.name', '=', 'Bank Accounts'),
                ('name', '!=', 'Bank')
            ])
            bank = []
            for data in bank_accounts:
                bank.append(data.id)
            cash_accounts = self.env['account.account'].search([
                ('group_id.name', '=', 'Cash-In-Hand'),
                ('name', '!=', 'Dummy Cash')
            ])
            cash = []
            for res in cash_accounts:
                cash.append(res.id)
            self._cr.execute("""
                with branch_ as (
                    select
                    br.id as branch
                    from res_branch br
                    group by br.id
                ),
                opening_bank_balance as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) as opening_bank_balance
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    left join account_move am on am.id = al.move_id
                    join account_account aa on aa.id = al.account_id
                    where am.state = 'posted'
                    and al.account_id in %s 
                    and al.date < '%s'
                    group by al.branch_id
                ),
                data_opening_bank_balance as (
                    select
                    branch_.branch,
                    sum(obb.opening_bank_balance) as opening_bank_balance
                    from branch_
                    left join opening_bank_balance obb on branch_.branch = obb.branch
                    group by branch_.branch
                ),
                closing_bank_balance as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) as closing_bank_balance
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    left join account_move am on am.id = al.move_id
                    join account_account aa on aa.id = al.account_id
                    where am.state = 'posted'
                    and al.account_id in %s 
                    and al.date <= '%s'
                    group by al.branch_id
                ),
                data_closing_bank_balance as (
                    select
                    data_opening_bank_balance.branch,
                    data_opening_bank_balance.opening_bank_balance as opening_bank_balance,
                    sum(cbb.closing_bank_balance) as closing_bank_balance
                    from data_opening_bank_balance
                    left join closing_bank_balance cbb on data_opening_bank_balance.branch = cbb.branch
                    group by data_opening_bank_balance.branch,
                    data_opening_bank_balance.opening_bank_balance
                ),
                opening_imprest as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) as opening_imprest
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    left join account_move am on am.id = al.move_id
                    join account_account aa on aa.id = al.account_id
                    where am.state = 'posted'
                    and aa.name = 'Imprest A/C'
                    and al.date < '%s'
                    group by al.branch_id
                ),
                data_opening_imprest as (
                    select
                    data_closing_bank_balance.branch,
                    data_closing_bank_balance.opening_bank_balance as opening_bank_balance,
                    data_closing_bank_balance.closing_bank_balance as closing_bank_balance,
                    sum(oi.opening_imprest) as opening_imprest
                    from data_closing_bank_balance
                    left join opening_imprest oi on data_closing_bank_balance.branch = oi.branch
                    group by data_closing_bank_balance.branch,
                    data_closing_bank_balance.opening_bank_balance,
                    data_closing_bank_balance.closing_bank_balance
                ),
                closing_imprest as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) as closing_imprest
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    left join account_move am on am.id = al.move_id
                    join account_account aa on aa.id = al.account_id
                    where am.state = 'posted'
                    and aa.name = 'Imprest A/C'
                    and al.date <= '%s'
                    group by al.branch_id
                ),
                data_closing_imprest as (
                    select
                    data_opening_imprest.branch,
                    data_opening_imprest.opening_bank_balance as opening_bank_balance,
                    data_opening_imprest.closing_bank_balance as closing_bank_balance,
                    data_opening_imprest.opening_imprest as opening_imprest,
                    sum(ci.closing_imprest) as closing_imprest
                    from data_opening_imprest
                    left join closing_imprest ci on data_opening_imprest.branch = ci.branch
                    group by data_opening_imprest.branch,
                    data_opening_imprest.opening_bank_balance,
                    data_opening_imprest.closing_bank_balance,
                    data_opening_imprest.opening_imprest
                ),
                opening_cash_in_hand as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) * -1 as opening_cash_in_hand
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    left join account_move am on am.id = al.move_id
                    join account_account aa on aa.id = al.account_id
                    where am.state = 'posted'
                    and al.account_id in %s 
                    and al.date < '%s'
                    group by al.branch_id
                ),
                data_opening_cash_in_hand as (
                    select
                    data_closing_imprest.branch,
                    data_closing_imprest.opening_bank_balance as opening_bank_balance,
                    data_closing_imprest.closing_bank_balance as closing_bank_balance,
                    data_closing_imprest.opening_imprest as opening_imprest,
                    data_closing_imprest.closing_imprest as closing_imprest,
                    sum(ocih.opening_cash_in_hand) as opening_cash_in_hand
                    from data_closing_imprest
                    left join opening_cash_in_hand ocih on data_closing_imprest.branch = ocih.branch
                    group by data_closing_imprest.branch,
                    data_closing_imprest.opening_bank_balance,
                    data_closing_imprest.closing_bank_balance,
                    data_closing_imprest.opening_imprest,
                    data_closing_imprest.closing_imprest
                ),
                closing_cash_in_hand as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) * -1 as closing_cash_in_hand
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    left join account_move am on am.id = al.move_id
                    join account_account aa on aa.id = al.account_id
                    where am.state = 'posted'
                    and al.account_id in %s 
                    and al.date <= '%s'
                    group by al.branch_id
                ),
                data_closing_cash_in_hand as (
                    select
                    data_opening_cash_in_hand.branch,
                    data_opening_cash_in_hand.opening_bank_balance as opening_bank_balance,
                    data_opening_cash_in_hand.closing_bank_balance as closing_bank_balance,
                    data_opening_cash_in_hand.opening_imprest as opening_imprest,
                    data_opening_cash_in_hand.closing_imprest as closing_imprest,
                    data_opening_cash_in_hand.opening_cash_in_hand as opening_cash_in_hand,
                    sum(ccih.closing_cash_in_hand) as closing_cash_in_hand
                    from data_opening_cash_in_hand
                    left join closing_cash_in_hand ccih on data_opening_cash_in_hand.branch = ccih.branch
                    group by data_opening_cash_in_hand.branch,
                    data_opening_cash_in_hand.opening_bank_balance,
                    data_opening_cash_in_hand.closing_bank_balance,
                    data_opening_cash_in_hand.opening_imprest,
                    data_opening_cash_in_hand.closing_imprest,
                    data_opening_cash_in_hand.opening_cash_in_hand
                ),
                opening_debtors as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) as opening_debtors
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    left join account_move am on am.id = al.move_id
                    join account_account aa on aa.id = al.account_id
                    where am.state = 'posted'
                    and aa.name = 'Debtors'
                    and al.date < '%s'
                    group by al.branch_id
                ),
                data_opening_debtors as (
                    select
                    data_closing_cash_in_hand.branch,
                    data_closing_cash_in_hand.opening_bank_balance as opening_bank_balance,
                    data_closing_cash_in_hand.closing_bank_balance as closing_bank_balance,
                    data_closing_cash_in_hand.opening_imprest as opening_imprest,
                    data_closing_cash_in_hand.closing_imprest as closing_imprest,
                    data_closing_cash_in_hand.opening_cash_in_hand as opening_cash_in_hand,
                    data_closing_cash_in_hand.closing_cash_in_hand as closing_cash_in_hand,
                    sum(od.opening_debtors) as opening_debtors
                    from data_closing_cash_in_hand
                    left join opening_debtors od on data_closing_cash_in_hand.branch = od.branch
                    group by data_closing_cash_in_hand.branch,
                    data_closing_cash_in_hand.opening_bank_balance,
                    data_closing_cash_in_hand.closing_bank_balance,
                    data_closing_cash_in_hand.opening_imprest,
                    data_closing_cash_in_hand.closing_imprest,
                    data_closing_cash_in_hand.opening_cash_in_hand,
                    data_closing_cash_in_hand.closing_cash_in_hand
                ),
                closing_debtors as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) as closing_debtors
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    left join account_move am on am.id = al.move_id
                    join account_account aa on aa.id = al.account_id
                    where am.state = 'posted'
                    and aa.name = 'Debtors'
                    and al.date <= '%s'
                    group by al.branch_id
                ),
                data_closing_debtors as (
                    select
                    data_opening_debtors.branch,
                    data_opening_debtors.opening_bank_balance as opening_bank_balance,
                    data_opening_debtors.closing_bank_balance as closing_bank_balance,
                    data_opening_debtors.opening_imprest as opening_imprest,
                    data_opening_debtors.closing_imprest as closing_imprest,
                    data_opening_debtors.opening_cash_in_hand as opening_cash_in_hand,
                    data_opening_debtors.closing_cash_in_hand as closing_cash_in_hand,
                    data_opening_debtors.opening_debtors as opening_debtors,
                    sum(cd.closing_debtors) as closing_debtors
                    from data_opening_debtors
                    left join closing_debtors cd on data_opening_debtors.branch = cd.branch
                    group by data_opening_debtors.branch,
                    data_opening_debtors.opening_bank_balance,
                    data_opening_debtors.closing_bank_balance,
                    data_opening_debtors.opening_imprest,
                    data_opening_debtors.closing_imprest,
                    data_opening_debtors.opening_cash_in_hand,
                    data_opening_debtors.closing_cash_in_hand,
                    data_opening_debtors.opening_debtors
                ),
                opening_creditors as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) * -1 as opening_creditors
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    left join account_move am on am.id = al.move_id
                    join account_account aa on aa.id = al.account_id
                    where am.state = 'posted'
                    and aa.name in ('Creditors For Capital Goods', 'Creditors For Expenses', 'Creditors For Purchases')
                    and al.date < '%s'
                    group by al.branch_id
                ),
                data_opening_creditors as (
                    select
                    data_closing_debtors.branch,
                    data_closing_debtors.opening_bank_balance as opening_bank_balance,
                    data_closing_debtors.closing_bank_balance as closing_bank_balance,
                    data_closing_debtors.opening_imprest as opening_imprest,
                    data_closing_debtors.closing_imprest as closing_imprest,
                    data_closing_debtors.opening_cash_in_hand as opening_cash_in_hand,
                    data_closing_debtors.closing_cash_in_hand as closing_cash_in_hand,
                    data_closing_debtors.opening_debtors as opening_debtors,
                    data_closing_debtors.closing_debtors as closing_debtors,
                    sum(oc.opening_creditors) as opening_creditors
                    from data_closing_debtors
                    left join opening_creditors oc on data_closing_debtors.branch = oc.branch
                    group by data_closing_debtors.branch,
                    data_closing_debtors.opening_bank_balance,
                    data_closing_debtors.closing_bank_balance,
                    data_closing_debtors.opening_imprest,
                    data_closing_debtors.closing_imprest,
                    data_closing_debtors.opening_cash_in_hand,
                    data_closing_debtors.closing_cash_in_hand,
                    data_closing_debtors.opening_debtors,
                    data_closing_debtors.closing_debtors
                ),
                closing_creditors as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) * -1 as closing_creditors_wo_uncleared
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    left join account_move am on am.id = al.move_id
                    join account_account aa on aa.id = al.account_id
                    where am.state = 'posted'
                    and aa.name in ('Creditors For Capital Goods', 'Creditors For Expenses', 'Creditors For Purchases')
                     and al.date <= '%s'
                    group by al.branch_id
                ),
                data_closing_creditors as (
                    select
                    data_opening_creditors.branch,
                    data_opening_creditors.opening_bank_balance as opening_bank_balance,
                    data_opening_creditors.closing_bank_balance as closing_bank_balance,
                    data_opening_creditors.opening_imprest as opening_imprest,
                    data_opening_creditors.closing_imprest as closing_imprest,
                    data_opening_creditors.opening_cash_in_hand as opening_cash_in_hand,
                    data_opening_creditors.closing_cash_in_hand as closing_cash_in_hand,
                    data_opening_creditors.opening_debtors as opening_debtors,
                    data_opening_creditors.closing_debtors as closing_debtors,
                    data_opening_creditors.opening_creditors as opening_creditors,
                    sum(cc.closing_creditors_wo_uncleared) as closing_creditors_wo_uncleared
                    from data_opening_creditors
                    left join closing_creditors cc on data_opening_creditors.branch = cc.branch
                    group by data_opening_creditors.branch,
                    data_opening_creditors.opening_bank_balance,
                    data_opening_creditors.closing_bank_balance,
                    data_opening_creditors.opening_imprest,
                    data_opening_creditors.closing_imprest,
                    data_opening_creditors.opening_cash_in_hand,
                    data_opening_creditors.closing_cash_in_hand,
                    data_opening_creditors.opening_debtors,
                    data_opening_creditors.closing_debtors,
                    data_opening_creditors.opening_creditors
                ),
                opening_security_rent as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) as opening_security_rent
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    left join account_move am on am.id = al.move_id
                    join account_account aa on aa.id = al.account_id
                    where am.state = 'posted'
                    and al.account_id in (540,1037)
                    and al.date < '%s'
                    group by al.branch_id
                ),
                data_opening_security_rent as (
                    select
                    data_closing_creditors.branch,
                    data_closing_creditors.opening_bank_balance as opening_bank_balance,
                    data_closing_creditors.closing_bank_balance as closing_bank_balance,
                    data_closing_creditors.opening_imprest as opening_imprest,
                    data_closing_creditors.closing_imprest as closing_imprest,
                    data_closing_creditors.opening_cash_in_hand as opening_cash_in_hand,
                    data_closing_creditors.closing_cash_in_hand as closing_cash_in_hand,
                    data_closing_creditors.opening_debtors as opening_debtors,
                    data_closing_creditors.closing_debtors as closing_debtors,
                    data_closing_creditors.opening_creditors as opening_creditors,
                    data_closing_creditors.closing_creditors_wo_uncleared as closing_creditors_wo_uncleared,
                    sum(os.opening_security_rent) as opening_security_rent
                    from data_closing_creditors
                    left join opening_security_rent os on data_closing_creditors.branch = os.branch
                    group by data_closing_creditors.branch,
                    data_closing_creditors.opening_bank_balance,
                    data_closing_creditors.closing_bank_balance,
                    data_closing_creditors.opening_imprest,
                    data_closing_creditors.closing_imprest,
                    data_closing_creditors.opening_cash_in_hand,
                    data_closing_creditors.closing_cash_in_hand,
                    data_closing_creditors.opening_debtors,
                    data_closing_creditors.closing_debtors,
                    data_closing_creditors.opening_creditors,
                    data_closing_creditors.closing_creditors_wo_uncleared
                ),
                closing_security_rent as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) as closing_security_rent
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    left join account_move am on am.id = al.move_id
                    join account_account aa on aa.id = al.account_id
                    where am.state = 'posted'
                    and al.account_id in (540,1037)
                    and al.date <= '%s'
                    group by al.branch_id
                ),
                data_closing_security_rent as (
                    select
                    data_opening_security_rent.branch,
                    data_opening_security_rent.opening_bank_balance as opening_bank_balance,
                    data_opening_security_rent.closing_bank_balance as closing_bank_balance,
                    data_opening_security_rent.opening_imprest as opening_imprest,
                    data_opening_security_rent.closing_imprest as closing_imprest,
                    data_opening_security_rent.opening_cash_in_hand as opening_cash_in_hand,
                    data_opening_security_rent.closing_cash_in_hand as closing_cash_in_hand,
                    data_opening_security_rent.opening_debtors as opening_debtors,
                    data_opening_security_rent.closing_debtors as closing_debtors,
                    data_opening_security_rent.opening_creditors as opening_creditors,
                    data_opening_security_rent.closing_creditors_wo_uncleared as closing_creditors_wo_uncleared,
                    data_opening_security_rent.opening_security_rent as opening_security_rent,
                    sum(cs.closing_security_rent) as closing_security_rent
                    from data_opening_security_rent
                    left join closing_security_rent cs on data_opening_security_rent.branch = cs.branch
                    group by data_opening_security_rent.branch,
                    data_opening_security_rent.opening_bank_balance,
                    data_opening_security_rent.closing_bank_balance,
                    data_opening_security_rent.opening_imprest,
                    data_opening_security_rent.closing_imprest,
                    data_opening_security_rent.opening_cash_in_hand,
                    data_opening_security_rent.closing_cash_in_hand,
                    data_opening_security_rent.opening_debtors,
                    data_opening_security_rent.closing_debtors,
                    data_opening_security_rent.opening_creditors,
                    data_opening_security_rent.closing_creditors_wo_uncleared,
                    data_opening_security_rent.opening_security_rent
                ),
                debtor_cycle as (
                    select
                    ai.branch_id as branch,
                    sum(ai.amount_total_signed) as debtor_cycle
                    from account_invoice ai
                    join res_branch br on br.id = ai.branch_id                    
                    where ai.state in ('open', 'paid')                    
                    and ai.date_invoice between '%s' and '%s'
                    and ai.type = 'out_invoice'
                    group by ai.branch_id
                ),
                total_sale_data as (
                    select
                    data_closing_security_rent.branch,
                    data_closing_security_rent.opening_bank_balance as opening_bank_balance,
                    data_closing_security_rent.closing_bank_balance as closing_bank_balance,
                    data_closing_security_rent.opening_imprest as opening_imprest,
                    data_closing_security_rent.closing_imprest as closing_imprest,
                    data_closing_security_rent.opening_cash_in_hand as opening_cash_in_hand,
                    data_closing_security_rent.closing_cash_in_hand as closing_cash_in_hand,
                    data_closing_security_rent.opening_debtors as opening_debtors,
                    data_closing_security_rent.closing_debtors as closing_debtors,
                    data_closing_security_rent.opening_creditors as opening_creditors,
                    data_closing_security_rent.closing_creditors_wo_uncleared as closing_creditors_wo_uncleared,
                    data_closing_security_rent.opening_security_rent as opening_security_rent,
                    data_closing_security_rent.closing_security_rent as closing_security_rent,
                    sum(sd.debtor_cycle) as debtor_cycle
                    from data_closing_security_rent
                    left join debtor_cycle sd on data_closing_security_rent.branch = sd.branch
                    group by data_closing_security_rent.branch,
                    data_closing_security_rent.opening_bank_balance,
                    data_closing_security_rent.closing_bank_balance,
                    data_closing_security_rent.opening_imprest,
                    data_closing_security_rent.closing_imprest,
                    data_closing_security_rent.opening_cash_in_hand,
                    data_closing_security_rent.closing_cash_in_hand,
                    data_closing_security_rent.opening_debtors,
                    data_closing_security_rent.closing_debtors,
                    data_closing_security_rent.opening_creditors,
                    data_closing_security_rent.closing_creditors_wo_uncleared,
                    data_closing_security_rent.opening_security_rent,
                    data_closing_security_rent.closing_security_rent
                ),
                creditor_cycle as (
                    select
                    ai.branch_id as branch,
                    sum(ai.amount_total_signed) as creditor_cycle
                    from account_invoice ai
                    join res_branch br on br.id = ai.branch_id                    
                    where ai.state in ('open', 'paid') 
                    and ai.type = 'in_invoice'                   
                    and ai.date_invoice between '%s' and '%s'
                    group by ai.branch_id
                ),
                total_purchase_data as (
                    select
                    total_sale_data.branch,
                    total_sale_data.opening_bank_balance as opening_bank_balance,
                    total_sale_data.closing_bank_balance as closing_bank_balance,
                    total_sale_data.opening_imprest as opening_imprest,
                    total_sale_data.closing_imprest as closing_imprest,
                    total_sale_data.opening_cash_in_hand as opening_cash_in_hand,
                    total_sale_data.closing_cash_in_hand as closing_cash_in_hand,
                    total_sale_data.opening_debtors as opening_debtors,
                    total_sale_data.closing_debtors as closing_debtors,
                    total_sale_data.opening_creditors as opening_creditors,
                    total_sale_data.closing_creditors_wo_uncleared as closing_creditors_wo_uncleared,
                    total_sale_data.opening_security_rent as opening_security_rent,
                    total_sale_data.closing_security_rent as closing_security_rent,
                    total_sale_data.debtor_cycle as debtor_cycle,
                    sum(cc.creditor_cycle) as creditor_cycle                    
                    from total_sale_data
                    left join creditor_cycle cc on total_sale_data.branch = cc.branch
                    group by total_sale_data.branch,
                    total_sale_data.opening_bank_balance,
                    total_sale_data.closing_bank_balance,
                    total_sale_data.opening_imprest,
                    total_sale_data.closing_imprest,
                    total_sale_data.opening_cash_in_hand,
                    total_sale_data.closing_cash_in_hand,
                    total_sale_data.opening_debtors,
                    total_sale_data.closing_debtors,
                    total_sale_data.opening_creditors,
                    total_sale_data.closing_creditors_wo_uncleared,
                    total_sale_data.opening_security_rent,
                    total_sale_data.closing_security_rent,
                    total_sale_data.debtor_cycle
                ),
                uncleard_cheque as (
                    select
                    al.branch_id as branch,
                    sum(al.debit) as uncleard_cheque
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    left join account_move am on am.id = al.move_id 
                    join account_account ac on ac.id = al.account_id                  
                    where am.state = 'posted'                                     
                    and al.date between '%s' and '%s'
                    and al.bank_date_ is null
                    and ac.id in (80,81,82,83,84,85,86,133,79)
                    group by al.branch_id
                ),
                uncleard_cheque_data as (
                    select
                    total_purchase_data.branch,
                    total_purchase_data.opening_bank_balance as opening_bank_balance,
                    total_purchase_data.closing_bank_balance as closing_bank_balance,
                    total_purchase_data.opening_imprest as opening_imprest,
                    total_purchase_data.closing_imprest as closing_imprest,
                    total_purchase_data.opening_cash_in_hand as opening_cash_in_hand,
                    total_purchase_data.closing_cash_in_hand as closing_cash_in_hand,
                    total_purchase_data.opening_debtors as opening_debtors,
                    total_purchase_data.closing_debtors as closing_debtors,
                    total_purchase_data.opening_creditors as opening_creditors,
                    total_purchase_data.closing_creditors_wo_uncleared as closing_creditors_wo_uncleared,
                    total_purchase_data.opening_security_rent as opening_security_rent,
                    total_purchase_data.closing_security_rent as closing_security_rent,
                    total_purchase_data.debtor_cycle as debtor_cycle,
                    total_purchase_data.creditor_cycle as creditor_cycle,
                    sum(uc.uncleard_cheque) as uncleard_cheque                  
                    from total_purchase_data
                    left join uncleard_cheque uc on total_purchase_data.branch = uc.branch
                    group by total_purchase_data.branch,
                    total_purchase_data.opening_bank_balance,
                    total_purchase_data.closing_bank_balance,
                    total_purchase_data.opening_imprest,
                    total_purchase_data.closing_imprest,
                    total_purchase_data.opening_cash_in_hand,
                    total_purchase_data.closing_cash_in_hand,
                    total_purchase_data.opening_debtors,
                    total_purchase_data.closing_debtors,
                    total_purchase_data.opening_creditors,
                    total_purchase_data.closing_creditors_wo_uncleared,
                    total_purchase_data.opening_security_rent,
                    total_purchase_data.closing_security_rent,
                    total_purchase_data.debtor_cycle,
                    total_purchase_data.creditor_cycle
                ),
                debtors as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) as debtors
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    left join account_move am on am.id = al.move_id 
                    join account_account ac on ac.id = al.account_id                  
                    where am.state = 'posted'                                     
                    and al.date <= '%s'
                    and ac.id = 3
                    group by al.branch_id
                ),
                debtors_data as (
                    select
                    uncleard_cheque_data.branch,
                    uncleard_cheque_data.opening_bank_balance as opening_bank_balance,
                    uncleard_cheque_data.closing_bank_balance as closing_bank_balance,
                    uncleard_cheque_data.opening_imprest as opening_imprest,
                    uncleard_cheque_data.closing_imprest as closing_imprest,
                    uncleard_cheque_data.opening_cash_in_hand as opening_cash_in_hand,
                    uncleard_cheque_data.closing_cash_in_hand as closing_cash_in_hand,
                    uncleard_cheque_data.opening_debtors as opening_debtors,
                    uncleard_cheque_data.closing_debtors as closing_debtors,
                    uncleard_cheque_data.opening_creditors as opening_creditors,
                    uncleard_cheque_data.closing_creditors_wo_uncleared as closing_creditors_wo_uncleared,
                    uncleard_cheque_data.opening_security_rent as opening_security_rent,
                    uncleard_cheque_data.closing_security_rent as closing_security_rent,
                    uncleard_cheque_data.debtor_cycle as debtor_cycle,
                    uncleard_cheque_data.creditor_cycle as creditor_cycle,
                    uncleard_cheque_data.uncleard_cheque as uncleard_cheque,
                    sum(dd.debtors) as debtors             
                    from uncleard_cheque_data
                    left join debtors dd on uncleard_cheque_data.branch = dd.branch
                    group by uncleard_cheque_data.branch,
                    uncleard_cheque_data.opening_bank_balance,
                    uncleard_cheque_data.closing_bank_balance,
                    uncleard_cheque_data.opening_imprest,
                    uncleard_cheque_data.closing_imprest,
                    uncleard_cheque_data.opening_cash_in_hand,
                    uncleard_cheque_data.closing_cash_in_hand,
                    uncleard_cheque_data.opening_debtors,
                    uncleard_cheque_data.closing_debtors,
                    uncleard_cheque_data.opening_creditors,
                    uncleard_cheque_data.closing_creditors_wo_uncleared,
                    uncleard_cheque_data.opening_security_rent,
                    uncleard_cheque_data.closing_security_rent,
                    uncleard_cheque_data.debtor_cycle,
                    uncleard_cheque_data.creditor_cycle,
                    uncleard_cheque_data.uncleard_cheque
                ),
                creditor as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) * -1 as creditor
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    left join account_move am on am.id = al.move_id 
                    join account_account ac on ac.id = al.account_id                  
                    where am.state = 'posted'                                     
                    and al.date <= '%s'
                    and ac.id in (20,952,953)
                    group by al.branch_id
                ),
                creditor_data as (
                    select
                    debtors_data.branch,
                    debtors_data.opening_bank_balance as opening_bank_balance,
                    debtors_data.closing_bank_balance as closing_bank_balance,
                    debtors_data.opening_imprest as opening_imprest,
                    debtors_data.closing_imprest as closing_imprest,
                    debtors_data.opening_cash_in_hand as opening_cash_in_hand,
                    debtors_data.closing_cash_in_hand as closing_cash_in_hand,
                    debtors_data.opening_debtors as opening_debtors,
                    debtors_data.closing_debtors as closing_debtors,
                    debtors_data.opening_creditors as opening_creditors,
                    debtors_data.closing_creditors_wo_uncleared as closing_creditors_wo_uncleared,
                    debtors_data.opening_security_rent as opening_security_rent,
                    debtors_data.closing_security_rent as closing_security_rent,
                    debtors_data.debtor_cycle as debtor_cycle,
                    debtors_data.creditor_cycle as creditor_cycle,
                    debtors_data.uncleard_cheque as uncleard_cheque,
                    debtors_data.debtors as debtors,
                    sum(cd.creditor) as creditor           
                    from debtors_data
                    left join creditor cd on debtors_data.branch = cd.branch
                    group by debtors_data.branch,
                    debtors_data.opening_bank_balance,
                    debtors_data.closing_bank_balance,
                    debtors_data.opening_imprest,
                    debtors_data.closing_imprest,
                    debtors_data.opening_cash_in_hand,
                    debtors_data.closing_cash_in_hand,
                    debtors_data.opening_debtors,
                    debtors_data.closing_debtors,
                    debtors_data.opening_creditors,
                    debtors_data.closing_creditors_wo_uncleared,
                    debtors_data.opening_security_rent,
                    debtors_data.closing_security_rent,
                    debtors_data.debtor_cycle,
                    debtors_data.creditor_cycle,
                    debtors_data.uncleard_cheque,
                    debtors_data.debtors
                ),
                credit_note as (
                    select
                    ai.branch_id as branch,
                    sum(ai.amount_total) as credit_note
                    from account_invoice ai
                    join res_branch br on br.id = ai.branch_id                    
                    where ai.state in ('open', 'paid') 
                    and ai.type = 'out_refund'                   
                    and ai.date_invoice between '%s' and '%s'
                    group by ai.branch_id
                ),
                credit_note_data as (
                    select
                    creditor_data.branch,
                    COALESCE(sum(creditor_data.opening_bank_balance), 0.0) as opening_bank_balance,
                    COALESCE(sum(creditor_data.closing_bank_balance), 0.0) as closing_bank_balance,
                    COALESCE(sum(creditor_data.opening_imprest), 0.0) as opening_imprest,
                    COALESCE(sum(creditor_data.closing_imprest), 0.0) as closing_imprest,
                    COALESCE(sum(creditor_data.opening_cash_in_hand), 0.0) as opening_cash_in_hand,
                    COALESCE(sum(creditor_data.closing_cash_in_hand), 0.0) as closing_cash_in_hand,
                    COALESCE(sum(creditor_data.opening_debtors), 0.0) as opening_debtors,
                    COALESCE(sum(creditor_data.closing_debtors), 0.0) as closing_debtors,
                    COALESCE(sum(creditor_data.opening_creditors), 0.0) as opening_creditors,
                    COALESCE(sum(creditor_data.closing_creditors_wo_uncleared), 0.0) as closing_creditors_wo_uncleared,
                    COALESCE(sum(creditor_data.opening_security_rent), 0.0) as opening_security_rent,
                    COALESCE(sum(creditor_data.closing_security_rent), 0.0) as closing_security_rent,
                    COALESCE(sum(creditor_data.debtor_cycle), 0.0) as debtor_cycle,
                    COALESCE(sum(creditor_data.creditor_cycle), 0.0) as creditor_cycle,
                    COALESCE(sum(creditor_data.uncleard_cheque), 0.0) as uncleard_cheque,
                    COALESCE(sum(creditor_data.debtors), 0.0) as debtors,
                    COALESCE(sum(creditor_data.creditor), 0.0) as creditor,
                    COALESCE(sum(cn.credit_note), 0.0) as credit_note                
                    from creditor_data
                    left join credit_note cn on creditor_data.branch = cn.branch
                    group by creditor_data.branch,
                    creditor_data.opening_bank_balance,
                    creditor_data.closing_bank_balance,
                    creditor_data.opening_imprest,
                    creditor_data.closing_imprest,
                    creditor_data.opening_cash_in_hand,
                    creditor_data.closing_cash_in_hand,
                    creditor_data.opening_debtors,
                    creditor_data.closing_debtors,
                    creditor_data.opening_creditors,
                    creditor_data.closing_creditors_wo_uncleared,
                    creditor_data.opening_security_rent,
                    creditor_data.closing_security_rent,
                    creditor_data.debtor_cycle,
                    creditor_data.creditor_cycle,
                    creditor_data.uncleard_cheque,
                    creditor_data.debtors,
                    creditor_data.creditor
                    
                )              
                select row_number() OVER (ORDER BY branch) AS id,
                branch, opening_bank_balance, closing_bank_balance, opening_imprest,
                closing_imprest, opening_security_rent, closing_security_rent, opening_cash_in_hand, closing_cash_in_hand, opening_debtors,
                closing_debtors, opening_creditors, closing_creditors_wo_uncleared, debtor_cycle, creditor_cycle, uncleard_cheque,
                debtors, ((debtors * 365) / nullif((debtor_cycle - credit_note), 0.0)) as debtor_cycle_new, creditor, 
                (((creditor / nullif(creditor_cycle, 0.0)) * 365)) as creditor_cycle_new, 
                (closing_debtors - closing_creditors_wo_uncleared) as debtor_creditor_diffrence,
                credit_note, (debtor_cycle - credit_note) as net_sale
                
                from credit_note_data
            """ % (tuple(bank), first_day, tuple(bank), last_day, first_day, last_day,
                   tuple(cash), first_day, tuple(cash), last_day, first_day, last_day,
                   first_day, last_day,
                   first_day,
                   last_day,
                   year_date, last_day,
                   year_date, last_day,
                   first_day, last_day,
                   last_day,
                   last_day,
                   year_date, last_day))

            result = self._cr.fetchall()
            rows = result
            values = ', '.join(map(str, rows))
            sql = ("""INSERT INTO debtor_creditor_report 
                        (id, branch, opening_bank_balance, closing_bank_balance,
                        opening_imprest, closing_imprest, opening_security_rent, closing_security_rent, opening_cash_in_hand,
                        closing_cash_in_hand, opening_debtors, closing_debtors,
                        opening_creditors, closing_creditors_wo_uncleared, debtor_cycle, creditor_cycle, uncleard_cheque,
                        debtors, debtor_cycle_new, creditor, creditor_cycle_new, debtor_creditor_diffrence,
                        credit_note, net_sale
                        ) VALUES {}""".format(values).replace('None', 'null')
                   )
            self._cr.execute(sql)

            action = {
                'type': 'ir.actions.act_window',
                'views': [
                    (tree_view_id, 'tree'), (form_view_id, 'form'),
                    (graph_view_id, 'graph'), (pivot_view_id, 'pivot')
                ],
                'view_mode': 'tree,form',
                'name': _('Debtors & Creditors Report: %s - %s' %(first_day.strftime('%d %b %y'),
                                                                  last_day.strftime('%d %b %y'))),
                'res_model': 'debtor.creditor.report',
                'search_view_id': search_view_ref and search_view_ref.id,
            }
            return action