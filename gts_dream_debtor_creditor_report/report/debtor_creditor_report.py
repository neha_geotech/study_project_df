# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError


class DebtorCreditorReport(models.Model):
    '''
    This is report view table to show Stock Report
    '''
    _name = 'debtor.creditor.report'
    _rec_name = 'branch'

    @api.model
    def default_get(self, fields):
        raise ValidationError(_('No Record Create From this Option!'))
        res = super(DebtorCreditorReport, self).default_get(fields)
        return res

    branch = fields.Many2one('res.branch', string='Branch', readonly=True)
    opening_bank_balance = fields.Float(string='OP Bank Balance', readonly=True)
    closing_bank_balance = fields.Float(string='CL Bank Balance', readonly=True)
    opening_imprest = fields.Float(string='OP Imprest', readonly=True)
    closing_imprest = fields.Float(string='CL Imprest', readonly=True)
    opening_security_rent = fields.Float(string='OP Security', readonly=True)
    closing_security_rent = fields.Float(string='CL Security', readonly=True)
    opening_cash_in_hand = fields.Float(string='OP Cash in Hand', readonly=True)
    closing_cash_in_hand = fields.Float(string='CL Cash in Hand', readonly=True)
    opening_debtors = fields.Float(string='OP Debtors', readonly=True)
    closing_debtors = fields.Float(string='CL Debtors', readonly=True)
    debtor_cycle = fields.Float(string='Sale last Year', readonly=True)
    opening_creditors = fields.Float(string='OP Creditors', readonly=True)
    closing_creditors_wo_uncleared = fields.Float(string='CL Creditors', readonly=True)
    creditor_cycle = fields.Float(string='Purchase', readonly=True)
    debtor_creditor_diffrence = fields.Float(string='DR. & CR. Diff', readonly=True)
    uncleard_cheque = fields.Float(string='Uncleared Cheque')
    debtors = fields.Float(string='Total Debtors')
    creditor = fields.Float(string='Total Creditor')
    debtor_cycle_new = fields.Float(string='Dr. Cycle', readonly=True, group_operator='avg')
    creditor_cycle_new = fields.Float(string='Cr. Cycle', readonly=True, group_operator='avg')
    credit_note = fields.Float(string='Credit Note')
    net_sale = fields.Float(string='Net Sale')
