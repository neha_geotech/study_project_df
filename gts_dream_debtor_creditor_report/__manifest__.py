# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'GTS Debtors & Creditors Report',
    'summary': 'Debtors & Creditors Report',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'website': 'http://www.geotechnosoft.com',
    'category': '',
    'version': '1.0',
    'depends': [
        'gts_dream_purchase'
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/debtor_creditor_report_security.xml',
        'wizard/debtor_creditor_wiz_view.xml',
        'report/debtor_creditor_report_view.xml',
    ],
    'installable': True,
}
