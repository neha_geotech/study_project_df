from odoo import api, fields, models, tools


class ProductProduct(models.Model):
    _inherit = "product.product"

    @api.depends('attribute_value_ids')
    def _get_attribute_name(self):
        for res in self:
            complete_name = ''
            for data in res.attribute_value_ids:
                complete_name += str(data.attribute_id.name + ':' + data.name + ' ')
            res.attribute_name = complete_name

    @api.model
    def _compute_purchase_count(self):
        search_id = self.env['product.product'].search([])
        list = []
        for data in search_id:
            nbr = 0
            list_ = []
            for order in data.purchase_ids:
                list_.append(order.price_unit)
                list_min = min(list_)
                if order.order_id.state != 'cancel':
                    nbr += 1
                data.best_price = list_min
            data.purchase_number = nbr

    attribute_name = fields.Char('Attribute Name', compute='_get_attribute_name', store=True)
    rack_location = fields.Char('Rack Location', copy=False, Index=True)
    purchase_ids = fields.One2many('purchase.order.line', 'product_id', string='Vendors')
    purchase_number = fields.Integer('Total')
    best_price = fields.Float(string='Best Price')
    event_product = fields.Boolean('Event Product')

    # product_stage = fields.Selection([
    #     ('raw_material', 'Raw Material'),
    #     ('wip', 'WIP'),
    #     ('semi_finished', 'Semi Finished'),
    #     ('reusable', 'Reusable'),
    #     ('assets', 'Asset')],
    #     string='Product Stage')


class ProductTemplate(models.Model):
    _inherit = "product.template"

    branch_ids = fields.Many2many('res.branch', 'rel_product_tmp_branch', 'product_tmpl_id', 'branch_tmpl_id', string='Branch')
    rack_location = fields.Char('Rack Location', copy=False, Index=True)
    product_stage = fields.Selection([
        ('raw_material', 'Raw Material'),
        ('wip', 'WIP'),
        ('semi_finished', 'Semi Finished'),
        ('reusable', 'Reusable'),
        ('assets', 'Asset')],
        string='Product Stage', copy=False, Index=True)



