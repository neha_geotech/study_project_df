import json

from odoo import fields, models, api, _
from datetime import datetime,date, timedelta
from odoo.exceptions import Warning, ValidationError
from odoo.tools import float_is_zero, float_compare
from num2words import num2words
from dateutil import relativedelta


class AccountInvoice(models.Model):
    _inherit = "account.invoice"
    _order = "create_date desc"

    @api.one
    @api.depends(
        'invoice_line_ids.price_subtotal',
        'tax_line_ids.amount', 'tax_line_ids.amount_rounding',
        'currency_id', 'company_id', 'date_invoice', 'type',
    )
    def _compute_amount(self):
        res = super(AccountInvoice, self)._compute_amount()
        for inv in self:
            amount_tax_igst = amount_tax_sgst = amount_tax_cgst = amount_tax_tcs = 0.0
            for line in inv.invoice_line_ids:
                amount_tax_igst += line.igst
                amount_tax_tcs += line.tcs
                if not line.hsn_id:
                    amount_tax_sgst += line.sgst
                    amount_tax_cgst += line.cgst
                    # amount_tax_igst = 0.0
                if line.hsn_id:
                    if inv.partner_id.state_id and inv.branch_id.state_id:
                        if inv.partner_id.state_id == inv.branch_id.state_id:
                            amount_tax_sgst += line.sgst
                            amount_tax_cgst += line.cgst
                            # amount_tax_igst = 0.0
                inv.update({
                    'amount_untaxed': round(inv.amount_untaxed),
                    'amount_tax_igst': round(amount_tax_igst),
                    'amount_tax_sgst': round(amount_tax_sgst),
                    'amount_tax_cgst': round(amount_tax_cgst),
                    'amount_tax_tcs': round(amount_tax_tcs),
                    'amount_total': round(inv.amount_untaxed) + round(amount_tax_igst) +
                                    round(amount_tax_sgst) + round(amount_tax_cgst) + round(amount_tax_tcs),
                })

        return res

    @api.depends('date_invoice', 'date_deadline')
    def month_calculation(self):
        date_format_ = '%Y-%m-%d'
        for data in self:
            if data.date_invoice and data.date_deadline:
                a = datetime.strptime(data.date_deadline, date_format_)
                b = datetime.strptime(data.date_invoice, date_format_)
                if (a.month > b.month and a.year == b.year):
                    data.update({
                        'month_selection': 'next_month'
                    })
                if (a.month == b.month and a.year == b.year):
                    data.update({
                        'month_selection': 'current_month'
                    })
                if (a.month < b.month and a.year == b.year):
                    data.update({
                        'month_selection': 'previous_month'
                    })
                if (a.month < b.month and a.year < b.year):
                    data.update({
                        'month_selection': 'previous_month'
                    })
                if (a.month > b.month and a.year > b.year):
                    data.update({
                        'month_selection': 'next_month'
                    })

    @api.one
    @api.depends(
        'state', 'currency_id', 'invoice_line_ids.price_subtotal',
        'move_id.line_ids.amount_residual',
        'move_id.line_ids.currency_id')
    def _compute_residual(self):
        residual = 0.0
        residual_company_signed = 0.0
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        for line in self.sudo().move_id.line_ids:
            if line.account_id == self.account_id:
                residual_company_signed += line.amount_residual
                if line.currency_id == self.currency_id:
                    residual += line.amount_residual_currency if line.currency_id else line.amount_residual
                else:
                    from_currency = (line.currency_id and line.currency_id.with_context(
                        date=line.date)) or line.company_id.currency_id.with_context(date=line.date)
                    residual += from_currency.compute(line.amount_residual, self.currency_id)
        self.residual_company_signed = abs(residual_company_signed) * sign
        self.residual_signed = abs(residual) * sign
        self.residual = abs(round(residual))
        digits_rounding_precision = self.currency_id.rounding
        if float_is_zero(self.residual, precision_rounding=digits_rounding_precision):
            self.reconciled = True
        else:
            self.reconciled = False

    @api.depends('amount_total')
    def _compute_amount_total_words_in(self):
        for invoice in self:
            invoice.amount_total_words = invoice.currency_id.symbol + ' ' + (num2words(
                invoice.amount_total, ordinal=False, lang='en_IN')
            ).title() + ' Only'

    @api.depends('date_invoice', 'date_due')
    def _compute_number_of_due_days(self):
        date_format = '%Y-%m-%d'
        for res in self:
            if res.date_due and res.date_invoice:
                a = datetime.strptime(res.date_due, date_format)
                b = datetime.strptime(res.date_invoice, date_format)
                c = a - b
                res.due_days = c.days


    delays = fields.Integer('Total Delays day of Invoice')
    amount_total_words = fields.Char("Amount in Words", compute="_compute_amount_total_words_in")
    dispatch_document_no = fields.Char(string='Dispatch Document No')
    vehicle_no = fields.Char(string='Vehicle No')
    client_po_no = fields.Char(string='Client PO No')
    dispatched_through = fields.Char(string='Dispatched Through')
    activity_owner = fields.Char(string='Activity Owner')
    po_date = fields.Char(string='PO Date')
    destination = fields.Char(string='Destination')
    date_deadline = fields.Date(related='job_code.date_deadline',string='Expected Closing Date')
    due_days = fields.Integer('Due Days', compute="_compute_number_of_due_days")
    amount_tax_igst = fields.Monetary(
        string='IGST',
        store=True,
        compute='_compute_amount'
    )
    amount_tax_sgst = fields.Monetary(
        string='SGST',
        store=True,
        compute='_compute_amount'
    )
    amount_tax_cgst = fields.Monetary(
        string='CGST',
        store=True,
        compute='_compute_amount'
    )
    amount_tax_tcs = fields.Monetary(
        string='TCS',
        store=True,
        compute='_compute_amount'
    )
    job_code = fields.Many2one(
        'account.analytic.account',
        string='Job Code',
        index=True
    )

    partner_ids = fields.Many2many(
        'res.partner',
        'rel_account_partner',
        'invoice_id',
        'part_id',
        string='Associated Addresses',
    )
    sale_id = fields.Many2one('sale.order', 'Sale Reference')
    bank_details = fields.Text(string='Bank Details')
    new_delivery_address = fields.Boolean('Other Delivery Address', default=False)
    other_name = fields.Char('Name')
    purchase_ref_drive = fields.Char(string='Attachment Link')
    street = fields.Char('Street')
    street2 = fields.Char('Street2')
    zip = fields.Char('Zip', change_default=True)
    city = fields.Char('City')
    state_id = fields.Many2one("res.country.state", string='State')
    place_supply = fields.Char('Place of supply')
    state_code = fields.Char('State Code')
    inv_seq = fields.Char('Sequence', copy=False)
    month_selection = fields.Selection([
        ('previous_month', 'Previous Month'),
        ('current_month', 'Current Month'),
        ('next_month', 'Next Month')
    ], string='Closing Month', compute='month_calculation', store=True)

    inv_type = fields.Selection([
        ('orignal', 'Orignal')
    ])
    event_date = fields.Date('Event Date')
    event_name = fields.Char('Event Name')
    venue = fields.Text('Venue')

    @api.model
    def create(self, values):
        res = super(AccountInvoice, self).create(values)
        post_date_entry = self.env.user.has_group('gts_dream_purchase.post_date_entry')
        if res.type == 'in_invoice':
            if res.date_invoice:
                invoice_date = datetime.strptime(res.date_invoice, "%Y-%m-%d")
                date_diff = datetime.today() - invoice_date
                if not post_date_entry:
                    if date_diff.days > 5:
                        raise ValidationError(_("You have not access to create bill before 5 days"))
        return res

    @api.multi
    def write(self, values):
        res = super(AccountInvoice, self).write(values)
        post_date_entry = self.env.user.has_group('gts_dream_purchase.post_date_entry')
        if values.get('date_invoice'):
            if self.type == 'in_invoice':
                invoice_date = datetime.strptime(values.get('date_invoice'), "%Y-%m-%d")
                date_diff = datetime.today() - invoice_date
                if not post_date_entry:
                    if date_diff.days > 5:
                        raise ValidationError(_("You have not access to create bill before 5 days"))
        return res

    @api.model
    def compute_due_days(self):
        search_id = self.env['account.invoice'].search([])
        date_format_ = '%Y-%m-%d %H:%M:%S'
        date_format = '%Y-%m-%d'
        today_date = fields.Datetime.now()
        for res in search_id:
            if res.date_invoice and today_date:
                a = datetime.strptime(res.date_invoice, date_format)
                b = datetime.strptime(today_date, date_format_)
                c = b - a
                res.delays = c.days

    @api.onchange('new_delivery_address')
    def clear_check_box(self):
        if self.new_delivery_address is not True:
            self.other_name = ''
            self.street = ''
            self.street2 = ''
            self.zip = ''
            self.city = ''
            self.state_id = ''
            self.place_supply = ''
            self.state_code = ''

    @api.onchange('other_name''street', 'street2', 'city', 'place_supply')
    def _dilivery_address_upper(self):
        if self.new_delivery_address is True:
            for rec in self:
                rec.other_name = str(rec.other_name).title()
                rec.street = str(rec.street).title()
                rec.street2 = str(rec.street2).title()
                rec.city = str(rec.city).title()
                rec.place_supply = str(rec.place_supply).title()

    @api.onchange('delivery_address')
    def project_name_title(self):
        for rec in self:
            rec.delivery_address = str(rec.delivery_address).title()

    @api.one
    def _get_outstanding_info_JSON(self):
        self.outstanding_credits_debits_widget = json.dumps(False)
        if self.state == 'open':
            domain = [
                ('branch_id', '=', self.branch_id.id),
                ('account_id', '=', self.account_id.id),
                ('partner_id', '=', self.env['res.partner']._find_accounting_partner(self.partner_id).id),
                ('reconciled', '=', False), '|', ('amount_residual', '!=', 0.0),
                ('amount_residual_currency', '!=', 0.0),
                ('move_id.state', '=', 'posted')
            ]
            if self.type in ('out_invoice', 'in_refund'):
                domain.extend([('credit', '>', 0), ('debit', '=', 0)])
                type_payment = _('Outstanding credits')
            else:
                domain.extend([('credit', '=', 0), ('debit', '>', 0)])
                type_payment = _('Outstanding debits')
            info = {'title': '', 'outstanding': True, 'content': [], 'invoice_id': self.id}
            lines = self.env['account.move.line'].search(domain)
            currency_id = self.currency_id
            if len(lines) != 0:
                for line in lines:
                    # get the outstanding residual value in invoice currency
                    if line.currency_id and line.currency_id == self.currency_id:
                        amount_to_show = abs(line.amount_residual_currency)
                    else:
                        amount_to_show = line.company_id.currency_id.with_context(date=line.date).compute(
                            abs(line.amount_residual), self.currency_id)
                    if float_is_zero(amount_to_show, precision_rounding=self.currency_id.rounding):
                        continue
                    info['content'].append({
                        'journal_name': line.ref or line.move_id.name,
                        'amount': amount_to_show,
                        'currency': currency_id.symbol,
                        'id': line.id,
                        'position': currency_id.position,
                        'digits': [69, self.currency_id.decimal_places],
                        'date':line.move_id.date
                    })
                info['title'] = type_payment
                self.outstanding_credits_debits_widget = json.dumps(info)
                self.has_outstanding = True

    @api.onchange('invoice_line_ids')
    def invoice_line_change(self):
        if self.invoice_line_ids:
            for data in self.invoice_line_ids:
                if self.job_code:
                    if data.product_id.type == 'service':
                        data.account_analytic_id = self.job_code.id

    @api.multi
    def action_move_create(self):
        res = super(AccountInvoice, self).action_move_create()
        for invoice in self:
            context = dict(self.env.context)
            context.pop('default_type', None)
            invoice.invoice_line_ids.with_context(context).asset_create()
            for line in invoice.invoice_line_ids:
                if invoice.type in ('out_invoice', 'out_refund'):
                    if invoice.move_id:
                        invoice.move_id.line_ids.update({
                            'analytic_account_id': invoice.job_code.id,
                            'activity_owner': invoice.activity_owner if invoice.activity_owner else False,
                            'inv_reference': invoice.inv_seq,
                            'inv_date': invoice.date_invoice,
                            'event_date': invoice.sale_id.event_date if invoice.sale_id and invoice.sale_id.event_date else False,
                            'event_name': invoice.sale_id.opportunity_id.name if invoice.sale_id and invoice.sale_id.opportunity_id.name else invoice.event_name,
                            'venue': invoice.venue if invoice.sale_id and invoice.venue else invoice.venue,
                            'basic_amount': invoice.amount_untaxed,
                            'gst_amount': invoice.amount_tax
                        })
                if invoice.type in ('in_invoice', 'in_refund'):
                    if invoice.move_id:
                        invoice.move_id.line_ids.update({
                            'analytic_account_id': invoice.job_code.id,
                            'inv_reference': invoice.inv_seq,
                            'inv_date': invoice.date_invoice,
                            'basic_amount': invoice.amount_untaxed,
                            'gst_amount': invoice.amount_tax
                        })

        return res

    @api.multi
    def check_report(self):
        self.ensure_one()
        for res in self:
            if res.origin:
                search_id = self.env['sale.order'].search([('name','=',res.origin)])
                return self.env.ref('sale.action_report_saleorder').report_action(search_id.id)

    @api.multi
    def proforma_report_invoice(self):
        self.ensure_one()
        for res in self:
            if res.origin:
                search_id = self.env['sale.order'].search([('name', '=', res.origin)])
                return self.env.ref('sale.action_report_pro_forma_invoice').report_action(search_id.id)

    @api.onchange('state', 'partner_id', 'invoice_line_ids')
    def _onchange_allowed_purchase_ids(self):
        '''
        The purpose of the method is to define a domain for the available
        purchase orders.
        '''
        result = {}

        # A PO can be selected only if at least one PO line is not already in the invoice
        purchase_line_ids = self.invoice_line_ids.mapped('purchase_line_id')
        purchase_ids = self.invoice_line_ids.mapped('purchase_id').filtered(lambda r: r.order_line <= purchase_line_ids)

        result['domain'] = {'purchase_id': [
            ('invoice_status', '=', 'to invoice'),
            ('partner_id', 'child_of', self.partner_id.id),
            ('id', 'not in', purchase_ids.ids),
        ]}
        return result

    def _prepare_invoice_line_from_po_line(self, line):
        if line.product_id.purchase_method == 'purchase':
            qty = line.product_qty - line.qty_invoiced
        else:
            qty = line.qty_received - line.qty_invoiced
        if float_compare(
                qty, 0.0, precision_rounding=line.product_uom.rounding) <= 0:
            qty = 0.0
        taxes = line.taxes_id
        invoice_line_tax_ids = line.order_id.fiscal_position_id.map_tax(taxes)
        invoice_line = self.env['account.invoice.line']
        data = {
            'purchase_line_id': line.id,
            'name': line.order_id.name + ': ' + line.name,
            'origin': line.order_id.origin,
            'uom_id': line.product_uom.id,
            'product_id': line.product_id.id,
            'account_id': invoice_line.with_context({'journal_id': self.journal_id.id,'type': 'in_invoice'})._default_account(),
            'price_unit': line.order_id.currency_id.with_context(
                date=self.date_invoice
            ).compute(
                line.price_unit,
                self.currency_id,
                round=False
            ),
            'quantity': qty,
            'discount': 0.0,
            # 'account_analytic_id': line.account_analytic_id.id,
            'analytic_tag_ids': line.analytic_tag_ids.ids,
            'invoice_line_tax_ids': invoice_line_tax_ids.ids,
            's_no': line.s_no
        }
        if line.product_id.type == 'service':
            data['account_analytic_id'] = line.account_analytic_id.id
        account = invoice_line.get_invoice_line_account(
            'in_invoice',
            line.product_id,
            line.order_id.fiscal_position_id,
            self.env.user.company_id
        )
        if account:
            data['account_id'] = account.id
        return data

    @api.multi
    def invoice_validate(self):
        """
        Create sequence for account.invoice
        """
        for invoice in self:
            seq_obj = self.env['ir.sequence']
            if invoice.branch_id and invoice.branch_id.branch_code:
                cus_inv_seq_code = 'customer.invoice.' + invoice.branch_id.branch_code
                vendor_bill_seq_code = 'vendor.bill.' + invoice.branch_id.branch_code
                credit_note_seq_code = 'credit.note.' + invoice.branch_id.branch_code
                debit_note_seq_code = 'debit.note.' + invoice.branch_id.branch_code
                if invoice.date_invoice:
                    date_ = datetime.strptime(invoice.date_invoice, '%Y-%m-%d')
                    month_ = (date_.strftime("%b")).upper()
                    year_ = date_.strftime("%y")
                    if invoice.type == 'out_invoice':
                        if not invoice.inv_seq:
                            invoice.number = 'IN' + '/' + str(
                                invoice.branch_id.branch_code
                            ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                                cus_inv_seq_code
                            )
                            invoice.inv_seq = invoice.number
                        else:
                            invoice.number = invoice.inv_seq
                    if invoice.type == 'out_refund':
                        if not invoice.inv_seq:
                            invoice.number = 'CN' + '/' + str(
                                invoice.branch_id.branch_code
                            ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                                credit_note_seq_code
                            )
                            invoice.inv_seq = invoice.number
                        else:
                            invoice.number = invoice.inv_seq
                    if invoice.type == 'in_invoice':
                        if not invoice.inv_seq:
                            invoice.number = 'VB' + '/' + str(
                                invoice.branch_id.branch_code
                            ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                                vendor_bill_seq_code
                            )
                            invoice.inv_seq = invoice.number
                        else:
                            invoice.number = invoice.inv_seq
                    if invoice.type == 'in_refund':
                        if not invoice.inv_seq:
                            invoice.number = 'DN' + '/' + str(
                                invoice.branch_id.branch_code
                            ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                                debit_note_seq_code
                            )
                            invoice.inv_seq = invoice.number
                        else:
                            invoice.number = invoice.inv_seq
            if not invoice.branch_id.branch_code:
                raise Warning(_(
                    'Branch code is not defined for branch %s'
                ) % invoice.branch_id.name)

        return super(AccountInvoice, self).invoice_validate()

    @api.onchange('partner_id', 'company_id')
    def _onchange_partner_id(self):
        rec = super(AccountInvoice, self)._onchange_partner_id()
        list_ = []
        if self.partner_id.child_ids:
            for data in self.partner_id.child_ids:
                list_.append(data.id)
                partner_data = self.env['res.partner'].search([
                    ('id', 'in', list_)
                ])
                if partner_data:
                    self.update({'partner_ids': partner_data})
                if not partner_data:
                    self.update({'partner_ids': ''})
        return rec


class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    s_no = fields.Char(string='S.No')
    hsn_sac = fields.Char(string='HSN/SAC')
    name = fields.Text(string='Description', required=True, default='')
    type = fields.Selection("Type", related='invoice_id.type')
    date_invoice = fields.Date('Invoice Date', related='invoice_id.date_invoice')

    @api.onchange('name')
    def _compute_upper(self):
        for rec in self:
            rec.name = str(rec.name).title()


