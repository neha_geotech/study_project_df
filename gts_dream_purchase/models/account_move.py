from odoo import models, api, fields, _
from datetime import datetime, date, timedelta
from odoo.exceptions import Warning, UserError, ValidationError


class AccountMove(models.Model):
    _name = 'account.move'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'account.move']

    def get_first_day(self, dt, d_years=0, d_months=0):
        # d_years, d_months are "deltas" to apply to dt
        y, m = dt.year + d_years, dt.month + d_months
        a, m = divmod(m - 1, 12)
        return date(y + a, m + 1, 1)

    # currency_id = fields.Many2one("res.currency", string="Currency")
    job_code = fields.Many2one(
        'account.analytic.account',
        string='Job Code',
        index=True,
        required=False,
        track_visibility='onchange'
    )
    date_deadline = fields.Date(related='job_code.date_deadline',string='Expected Closing Date')
    ref = fields.Char(string='Reference', copy=False, size=100, default='')
    late_entry_reason = fields.Text('Reason For Late Entry')
    is_late_entry = fields.Boolean('Is Late Entry')
    date = fields.Date(required=True, states={'posted': [('readonly', True)]}, index=True,
                       default=fields.Date.context_today, track_visibility='onchange')
    entry_date = fields.Datetime('Entry Date', default=fields.datetime.now())
    amount = fields.Monetary(compute='_amount_compute', store=True, track_visibility='onchange')
    created_by = fields.Many2one('res.users', string='Created by', default=lambda self: self.env.uid, track_visibility='onchange')
    move_seq = fields.Char(string='Sequence')
    asset_id = fields.Many2one('account.asset.asset', 'asset')

    @api.multi
    def write(self, values):
        res = super(AccountMove, self).write(values)
        post_date_entry = self.env.user.has_group('gts_dream_purchase.post_date_entry')
        for rec in self:
            date = datetime.strptime(rec.date, "%Y-%m-%d")
            date_diff = datetime.today() - date
            if not post_date_entry:
                if date_diff.days > 5:
                    raise ValidationError(_("You have not access to create bill before 5 days"))
        return res

    # COMMENT THIS SECTION : NO REQUIREMENT OF LATE ENTRY CONCEPT
    @api.onchange('date')
    def check_entry_date(self):
        date_format_ = '%Y-%m-%d'
        date_format = '%Y-%m-%d %H:%M:%S'
        date_entry = datetime.strptime(self.date, date_format_).date()
        entry_date_ = datetime.strptime(self.entry_date, date_format).date()
        day_first = self.get_first_day(entry_date_)
        if entry_date_.day > 0:
            if day_first > date_entry:
                self.is_late_entry = True
            if day_first.month == date_entry.month:
                self.is_late_entry = False

    @api.onchange('line_ids', 'job_code')
    def line_ids_change(self):
        for data in self.line_ids:
            data.analytic_account_id = self.job_code.id

    @api.onchange('ref')
    def compute_title(self):
        for rec in self:
            rec.ref = rec.ref.title()

            # rec.city = str(rec.city).title()
            # rec.name = str(rec.name).title()

    @api.model
    def create(self, vals):
        ctx = self.env.context
        seq_obj = self.env['ir.sequence']
        branch_obj = self.env['res.branch']
        journal_obj = self.env['account.journal']
        post_date_entry = self.env.user.has_group('gts_dream_purchase.post_date_entry')
        date = datetime.strptime(vals.get('date'), "%Y-%m-%d")
        date_diff = datetime.today() - date
        if not post_date_entry:
            if date_diff.days > 5:
                raise ValidationError(_("You have not access to create bill before 5 days"))

        if 'invoice' in ctx:
            date_format = '%Y-%m-%d %H:%M:%S'
            date_format_ = '%Y-%m-%d'
            bill_date = datetime.strptime(ctx['invoice'].date_invoice, date_format_).date()
            create_bill_date = datetime.strptime(ctx['invoice'].create_date, date_format).date()
            day_first = self.get_first_day(create_bill_date)
            if create_bill_date.day > 7:
                if day_first > bill_date:
                    vals['is_late_entry'] = True
                    vals['late_entry_reason'] = 'Late Billing'
                if day_first.month == bill_date.month:
                    vals['is_late_entry'] = False
                    vals['late_entry_reason'] = False
        if 'branch_id' in vals:
            branch_id_ = branch_obj.search([
                ('id', '=', vals['branch_id'])
            ])
            journal_id_ = journal_obj.search([
                ('id', '=', vals['journal_id'])
            ])
            cus_inv_seq_code = 'move.cus.' + branch_id_.branch_code
            vendor_bill_seq_code = 'move.vb.' + branch_id_.branch_code
            credit_note_seq_code = 'move.cn.' + branch_id_.branch_code
            debit_note_seq_code = 'move.dn.' + branch_id_.branch_code
            if branch_id_.branch_code:
                date_ = datetime.now()
                month_ = (date_.strftime("%b")).upper()
                year_ = date_.strftime("%y")
                if 'invoice' in ctx:
                    if ctx['invoice'].inv_seq:
                        vals['name'] = ctx['invoice'].inv_seq
                    else:
                        if 'type' in ctx:
                            if journal_id_.type == 'purchase' and ctx['type'] == 'in_invoice':
                                vals['name'] = 'VB' + '/' + str(
                                    branch_id_.branch_code
                                ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                                    vendor_bill_seq_code
                                )
                            if journal_id_.type == 'purchase' and ctx['type'] == 'in_refund':
                                vals['name'] = 'DN' + '/' + str(
                                    branch_id_.branch_code
                                ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                                    debit_note_seq_code
                                )
                            if journal_id_.type == 'sale' and ctx['type'] == 'out_invoice':
                                vals['name'] = 'IN' + '/' + str(
                                    branch_id_.branch_code
                                ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                                    cus_inv_seq_code
                                )
                            if journal_id_.type == 'sale' and ctx['type'] == 'out_refund':
                                vals['name'] = 'CN' + '/' + str(
                                    branch_id_.branch_code
                                ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                                    credit_note_seq_code
                                )
            if not branch_id_.branch_code:
                raise Warning(_(
                    'Branch code is not defined for branch %s'
                ) % branch_id_.name)

        return super(AccountMove, self).create(vals)

    @api.multi
    def button_cancel(self):
        res = super(AccountMove, self).button_cancel()
        for rec in self:
            for move in rec.line_ids:
                search_id = self.env['bank.reco.info.line'].search([('move_line_id', '=', move.id)])
                search_id.unlink()
        return res


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'
    _order = 'delays desc'

    name = fields.Char(string="Narration", default='')
    analytic_account_id = fields.Many2one('account.analytic.account', string='Job Code')
    delays = fields.Integer(
        string='Total Delays day of Invoice'
    )
    bank_date_ = fields.Date(string='Bank Receiving Date')

    @api.model
    def compute_number_of_due_days(self):
        journal_ids = self.env['account.journal'].search([('name', 'in',('Tax Invoices','Vendor Bills'))])
        search_id = self.env['account.move.line'].search([('journal_id', 'in', journal_ids.ids)])
        date_format_ = '%Y-%m-%d %H:%M:%S'
        date_format = '%Y-%m-%d'
        today_date = fields.Datetime.now()

        for res in search_id:
            if res.inv_date:
                a = datetime.strptime(res.inv_date, date_format)
                b = datetime.strptime(today_date, date_format_)
                c = b - a
                res.delays = c.days
            else:
                res.delays = 0


    @api.one
    def _prepare_analytic_line(self):
        """ Prepare the values used to create() an account.analytic.line upon validation of an account.move.line having
            an analytic account. This method is intended to be extended in other modules.
        """
        amount = (self.credit or 0.0) - (self.debit or 0.0)
        return {
            'name': self.name,
            'date': self.date,
            'account_id': self.analytic_account_id.id,
            'tag_ids': [(6, 0, self.analytic_tag_ids.ids)],
            'unit_amount': self.quantity,
            'vendor_payout_reco': self.analytic_account_id.vendor_payout,
            'product_id': self.product_id and self.product_id.id or False,
            'product_uom_id': self.product_uom_id and self.product_uom_id.id or False,
            'amount': self.company_currency_id.with_context(date=self.date or fields.Date.context_today(self)).compute(
                amount, self.analytic_account_id.currency_id) if self.analytic_account_id.currency_id else amount,
            'general_account_id': self.account_id.id,
            'ref': self.ref,
            'move_id': self.id,
            'user_id': self.invoice_id.user_id.id or self._uid,
            'branch_id': self.branch_id.id
        }

    @api.onchange('name')
    def compute_title(self):
        for rec in self:
            rec.name = rec.name.title()

    @api.multi
    def _update_check(self):
        """ Raise Warning to cause rollback if the move is posted, some entries are reconciled or the move is older than the lock date"""
        move_ids = set()
        for line in self:
            err_msg = _('Move name (id): %s (%s)') % (line.move_id.name, str(line.move_id.id))
            # if line.move_id.state != 'draft':
            #     raise UserError(_(
            #         'You cannot do this modification on a posted journal entry, you can just change some non legal fields. You must revert the journal entry to cancel it.\n%s.') % err_msg)
            # if line.reconciled and not (line.debit == 0 and line.credit == 0):
            #     raise UserError(_(
            #         'You cannot do this modification on a reconciled entry. You can just change some non legal fields or you must unreconcile first.\n%s.') % err_msg)
            if line.move_id.id not in move_ids:
                move_ids.add(line.move_id.id)
        self.env['account.move'].browse(list(move_ids))._check_lock_date()
        return True

