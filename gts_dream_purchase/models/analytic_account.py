from odoo import fields, models, api, _
from datetime import datetime
from odoo.exceptions import Warning
from odoo.osv import expression


class AccountAnalyticAccount(models.Model):
    _inherit = 'account.analytic.account'
    _rec_name = 'analytic_acc_seq'

    @api.model
    def get_event_date(self):
        search_id = self.env['account.analytic.account'].search([])
        for res in search_id:
            res.event_date = res.opportunity_id.event_date

    analytic_acc_seq = fields.Char(
        'Reference',
        required=True,
        index=True,
        copy=False,
        default='New'
    )
    branch_id = fields.Many2one('res.branch', string='Branch')
    opportunity_id = fields.Many2one('crm.lead', 'Opportunity', copy=False)
    date_deadline = fields.Date(
        related='opportunity_id.date_deadline',
        string='Expected Closing Date', store=True
    )
    event_date = fields.Date(string='Event Date')
    vendor_payout = fields.Boolean("For Vendor Payout", copy=False, index=True, default=False)

    @api.multi
    def name_get(self):
        res = []
        for analytic in self:
            name = analytic.name
            if analytic.analytic_acc_seq:
                name = analytic.analytic_acc_seq + '/' + name
            res.append((analytic.id, name))
        return res

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        if operator not in ('ilike', 'like', '=', '=like', '=ilike'):
            return super(AccountAnalyticAccount, self).name_search(name, args, operator, limit)
        args = args or []
        domain = ['|', ('analytic_acc_seq', operator, name), ('name', operator, name)]
        recs = self.search(domain + args, limit=limit)
        return recs.name_get()

    @api.model
    def create(self, values):
        """
        Create sequence for account.analytic.account
        """
        ctx = self.env.context
        res = super(AccountAnalyticAccount, self).create(values)
        seq_obj = self.env['ir.sequence']
        branch_obj = self.env['res.branch']
        if res.branch_id and res.branch_id.branch_code:
            seq_code = 'account.analytic.account.' + res.branch_id.branch_code
            date_ = datetime.now()
            month_ = (date_.strftime("%b")).upper()
            year_ = date_.strftime("%y")
            res.analytic_acc_seq = 'JC' + '/' + str(
                res.branch_id.branch_code
            ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                seq_code
            )

        if not res.branch_id.branch_code:
            if 'branch' in ctx:
                if ctx['branch']:
                    branch_id_ = branch_obj.search([
                        ('id', '=', ctx['branch'])
                    ])
                    seq_code = 'account.analytic.account.' + branch_id_.branch_code
                    date_ = datetime.now()
                    month_ = (date_.strftime("%b")).upper()
                    year_ = date_.strftime("%y")
                    res.analytic_acc_seq = 'JC' + '/' + str(
                        branch_id_.branch_code
                    ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                        seq_code
                    )
                if not ctx['branch']:
                    raise Warning(_(
                        'Branch code is not defined for branch %s'
                    ) % res.branch_id.name)

        return res


class AccountAnalyticLine(models.Model):
    _inherit = 'account.analytic.line'

    date_deadline = fields.Date(related='account_id.date_deadline', string='Expected Closing Date')
    branch_id = fields.Many2one('res.branch', string='Branch', compute='get_branch', store=True)
    cost = fields.Float('Cost', compute='get_cost')
    bank_rec_date = fields.Date('Bank Receive Date')
    vendor_payout_reco = fields.Boolean("Vendor Payout")

    @api.multi
    def get_branch(self):
        for data in self:
            if data.account_id and data.account_id.branch_id:
                data.branch_id = data.account_id.branch_id

    @api.multi
    def get_cost(self):
        for res in self:
            res.cost = res.product_id.standard_price


