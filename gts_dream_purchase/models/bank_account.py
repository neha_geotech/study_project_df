from odoo import api, fields, models, tools


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    @api.model
    def _get_default_branch(self):
        user_obj = self.env['res.users']
        branch_id = user_obj.browse(self.env.user.id).branch_id
        return branch_id

    branch_id = fields.Many2one('res.branch', 'Branch', required=True, default=_get_default_branch)