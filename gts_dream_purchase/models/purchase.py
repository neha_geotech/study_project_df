from odoo import fields, models, api, _
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.tools.float_utils import float_is_zero, float_compare
from odoo.exceptions import Warning, ValidationError
from num2words import num2words


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"
    _order = "create_date desc"

    @api.depends('order_line.price_total')
    def _amount_all(self):
        res = super(PurchaseOrder, self)._amount_all()
        for order in self:
            amount_tax_igst = amount_tax_sgst = amount_tax_cgst = amount_tax_tcs = 0.0
            for line in order.order_line:
                amount_tax_igst += line.igst
                amount_tax_tcs += line.tcs
                if not line.hsn_id:
                    amount_tax_sgst += line.sgst
                    amount_tax_cgst += line.cgst
                    # amount_tax_igst = 0.0
                if line.hsn_id:
                    if order.partner_id.state_id and order.branch_id.state_id:
                        if order.partner_id.state_id == order.branch_id.state_id:
                            amount_tax_sgst += line.sgst
                            amount_tax_cgst += line.cgst
                            # amount_tax_igst = 0.0
                order.update({
                    'amount_tax_igst': round(amount_tax_igst),
                    'amount_tax_sgst': round(amount_tax_sgst),
                    'amount_tax_cgst': round(amount_tax_cgst),
                    'amount_tax_tcs' : round(amount_tax_tcs),
                    'amount_total': round(order.amount_untaxed) + round(amount_tax_igst) +
                                    round(amount_tax_sgst) + round(amount_tax_cgst) + round(amount_tax_tcs),
                })
        return res

    @api.depends('state', 'order_line.qty_invoiced', 'order_line.qty_received', 'order_line.product_qty')
    def _get_invoiced(self):
        # res = super(PurchaseOrder, self)._get_invoiced()
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        for order in self:
            if order.state not in ('purchase', 'done', 'open'):
                order.invoice_status = 'no'
                continue
            # if order.state == 'open':
            #     order.invoice_status = 'open'
            if any(float_compare(line.qty_invoiced,
                                 line.product_qty if line.product_id.purchase_method == 'purchase' else line.qty_received,
                                 precision_digits=precision) == -1 for line in order.order_line):
                order.invoice_status = 'to invoice'
            elif all(float_compare(line.qty_invoiced,
                                   line.product_qty if line.product_id.purchase_method == 'purchase' else line.qty_received,
                                   precision_digits=precision) >= 0 for line in order.order_line) and order.invoice_ids:
                order.invoice_status = 'invoiced'
            elif order.state == 'open':
                order.invoice_status = 'open'
            if order.invoice_ids:
                if all((data.state == 'paid') for data in order.invoice_ids):
                    order.invoice_status = 'paid'
                if len(order.invoice_ids) > 1 and any((data.state != 'paid') for data in order.invoice_ids):
                    order.invoice_status = 'invoiced'

        # return res

    @api.depends('amount_total')
    def _compute_amount_total_words_in(self):
        for order in self:
            order.amount_total_words = order.currency_id.symbol + ' ' + (num2words(
                order.amount_total, ordinal=False, lang='en_IN')
            ).title() + ' Only'

    @api.multi
    def _calc_exp_date(self):
        return datetime.now() + relativedelta(months=1)

    @api.model
    def _get_purchase_default_branch_to_ship(self):
        user_obj = self.env['res.users']
        ship_to_address = user_obj.browse(self.env.user.id).branch_id.id
        return ship_to_address

    validate_by = fields.Many2one('res.users', string='Validated by')
    purchase_ref_drive = fields.Char(string='Attachment Link')
    ship_to_address = fields.Many2one(
        'res.branch', string='Ship to Branch',
        default=_get_purchase_default_branch_to_ship
    )
    add_new_address = fields.Boolean('Ship to Event/Branch', default=False)

    po_valid = fields.Date(
        string='PO Valid Upto',
        readonly=True,
        default=_calc_exp_date,
    )

    state = fields.Selection([
        ('draft', 'RFQ'),
        ('open', 'Open'),
        ('sent', 'RFQ Sent'),
        ('to approve', 'To Approve'),
        ('purchase', 'Purchase Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled')
    ], string='Status', readonly=True, index=True, copy=False, default='draft', track_visibility='onchange')
    amount_total_words = fields.Char(
        "Amount in Words",
        compute="_compute_amount_total_words_in"
    )
    job_code = fields.Many2one(
        'account.analytic.account',
        string='Job Code',
        index=True,
    )
    branch_code_ = fields.Char(
        string='Branch Code',
        related='branch_id.branch_code'
    )
    po_order_type = fields.Selection([
        ('material', 'Material'),
        ('service', 'Service'),
        ('internal','Internal')],string ='PO Type')

    po_payment_type = fields.Selection([
        ('cash', 'Immediate Payment'),
        ('credit', 'Credit')], string='Payment Type')

    amount_tax_igst = fields.Monetary(
        string='IGST',
        store=True,
        compute='_amount_all'
    )
    amount_tax_sgst = fields.Monetary(
        string='SGST',
        store=True,
        compute='_amount_all'
    )
    amount_tax_tcs = fields.Monetary(
        string='TCS',
        store=True,
        compute='_amount_all'
    )
    amount_tax_cgst = fields.Monetary(
        string='CGST',
        store=True,
        compute='_amount_all'
    )

    street = fields.Char('Street')
    street2 = fields.Char('Street2')
    zip = fields.Char('Zip', change_default=True)
    city = fields.Char('City')
    state_id = fields.Many2one("res.country.state", string='State')
    date_deadline = fields.Date(related='job_code.date_deadline', string='Expected Closing Date')
    event_date = fields.Date(related='job_code.event_date', string='Event Date')
    jc_to_update = fields.Char(string='Job code update', default='')
    payment_term_id = fields.Many2one('account.payment.term', string='Payment Term', store=True)
    invoice_status = fields.Selection([
        ('no', 'Nothing to Bill'),
        ('open', 'Open'),
        ('to invoice', 'Waiting Bills'),
        ('invoiced', 'No Bill to Receive'),
        ('partially_paid', 'Partially Paid'),
        ('paid', 'Paid')], string='Billing Status',
        compute='_get_invoiced', store=True, readonly=True, copy=False, default='no')

    @api.onchange('add_new_address')
    def clear_check_box(self):
        if self.add_new_address is not True:
            self.street = ''
            self.street2 = ''
            self.zip = ''
            self.city = ''
            self.state_id = ''

    @api.multi
    def open_po(self):
        self.write({'state': 'open'})
        return {}

    @api.multi
    def button_confirm(self):
        for order in self:
            context = order._context
            current_uid = context.get('uid')
            user = self.env['res.users'].browse(current_uid)
            order.update({'validate_by': user})
            if order.state not in ['draft', 'sent', 'open']:
                continue
            order._add_supplier_to_product()
            # Deal with double validation process
            if order.company_id.po_double_validation == 'one_step' \
                    or (order.company_id.po_double_validation == 'two_step' \
                        and order.amount_total < self.env.user.company_id.currency_id.compute(
                        order.company_id.po_double_validation_amount, order.currency_id)) \
                    or order.user_has_groups('purchase.group_purchase_manager'):
                order.button_approve()
            else:
                order.write({'state': 'to approve'})
        return True


    @api.onchange('add_new_address')
    def clear_check_box(self):
        if self.add_new_address is  True:
            self.ship_to_address = ''
        if self.add_new_address is not True:
            self.ship_to_address = self.branch_id

    @api.onchange('street', 'street2', 'city')
    def _compute_address_upper(self):
        if self.add_new_address is True:
            for rec in self:
                rec.street = str(rec.street).title()
                rec.street2 = str(rec.street2).title()
                rec.city = str(rec.city).title()

    @api.onchange('partner_id')
    def update_payment_term(self):
        for res in self:
            res.payment_term_id = res.partner_id.property_supplier_payment_term_id.id

    @api.onchange('shipping_address')
    def convert_string(self):
        for res in self:
            res.shipping_address = str(res.shipping_address).title()

    @api.onchange('order_line','job_code')
    def order_line_change(self):
        for data in self.order_line:
            data.account_analytic_id = self.job_code.id

    @api.multi
    def action_view_invoice(self):
        result = super(PurchaseOrder, self).action_view_invoice()
        result['context']['default_job_code'] = self.job_code.id
        result['context']['default_branch_id'] = self.branch_id.id
        return result

    @api.model
    def create(self, vals):
        """
        Create sequence for purchase.order
        """
        seq_obj = self.env['ir.sequence']
        branch_obj = self.env['res.branch']
        if 'branch_id' in vals:
            branch_id_ = branch_obj.search([
                ('id', '=', vals['branch_id'])
            ])
            seq_code = 'purchase.' + branch_id_.branch_code
            if 'date_order' in vals and branch_id_.branch_code:
                date_ = datetime.strptime(
                    vals['date_order'],
                    "%Y-%m-%d %H:%M:%S"
                )
                month_ = (date_.strftime("%b")).upper()
                year_ = date_.strftime("%y")
                vals['name'] = 'PO' + '/' + str(
                    branch_id_.branch_code
                ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                    seq_code
                )
            if 'date_order' not in vals:
                if branch_id_.branch_code:
                    date_ = datetime.now()
                    month_ = (date_.strftime("%b")).upper()
                    year_ = date_.strftime("%y")
                    vals['name'] = 'PO' + '/' + str(
                        branch_id_.branch_code
                    ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                        seq_code
                    )
            if not branch_id_.branch_code:
                raise Warning(_(
                    'Branch code is not defined for branch %s'
                ) % branch_id_.name)
        res = super(PurchaseOrder, self).create(vals)
        post_date_entry = self.env.user.has_group('gts_dream_purchase.post_purchase_order')
        if res.event_date:
            event_date_ = datetime.strptime(res.event_date, "%Y-%m-%d")
            date_diff = datetime.today() - event_date_
            if not post_date_entry:
                if date_diff.days > 5:
                    raise ValidationError(_("You can not create bill after 5 days"))
        return res

    @api.model
    def _prepare_picking(self):
        res = super(PurchaseOrder, self)._prepare_picking()
        for data in self:
            if data.job_code:
                res.update({'job_code': data.job_code.id})
        return res


class PurchaseOrderLine(models.Model):
    _inherit = "purchase.order.line"

    @api.depends('cgst', 'sgst', 'igst')
    def total_tax_po(self):
        for order in self:
            total_tax = 0.0
            order.update({
                'total_tax': total_tax + round(order.cgst) + round(order.sgst) + round(order.igst) + round(order.tcs),
            })

    @api.depends('best_price_purchase', 'price_unit')
    def variance_percentage_cal(self):
        for data in self:
            if data.best_price_purchase:
                data.update({
                    'variance_per': (data.price_unit - data.best_price_purchase) * 100 / data.best_price_purchase,
                })

    # @api.multi
    # def get_best_price(self):
    #     search_id = self.env['product.product'].search(['product_id', '=', self.id])
    #     print(search_id)
    #     list = []
    #     for data in search_id:
    #         list.append(data.price_unit)
    #         print(list)
    #         print("min value element : ", min(list))
    #         data.best_price = min(list)

    @api.depends('total_tax', 'price_subtotal')
    def total_cost_po(self):
        for order in self:
            total_amount = 0.0
            order.update({
                'total_amount': total_amount + round(order.total_tax) +  round(order.price_subtotal),
            })

    s_no = fields.Char(string='S.No')
    hsn_sac = fields.Char(string='HSN/SAC')
    length_ = fields.Char(string='L')
    width = fields.Char(string='W')
    height = fields.Char(string='H')
    size = fields.Char(string='SIZE')
    days = fields.Integer(string='Days')
    name = fields.Text(string='Description', required=True, default='')
    best_price_purchase = fields.Float(string='Best Price')
    total_tax = fields.Float(string='Taxes', compute='total_tax_po', store=True)
    total_amount = fields.Float(string='Total Amount', compute='total_cost_po')
    variance_per = fields.Float(string='% Variance', compute='variance_percentage_cal')
    qty_on_hand = fields.Float(string='On Hand Qty')
    branch_id = fields.Many2one('res.branch', 'Branch')
    product_catg_id = fields.Many2one('product.category', string='Product Category')
    parent_product_catg_id = fields.Many2one('product.category', string='Parent Category')
    payment_term_id = fields.Many2one(related='order_id.payment_term_id', string='Payment Term', store=True)

    @api.onchange('product_id')
    def onchange_product_id(self):
        res = super(PurchaseOrderLine, self).onchange_product_id()
        # for data in self.product_id.variant_seller_ids:
        #     search_id = data.search([('name', '=', 'Best Price Vendor'), ('product_id', '=', self.product_id.id)])
        #     if search_id:
        #         self.best_price = search_id.price
        data_dict = {}
        if self.product_id:
            self.branch_id = self.order_id.branch_id.id
            self.length_ = self.product_id.display_length1
            self.width = self.product_id.display_width1
            self.height = self.product_id.display_height1
            self.qty_on_hand = self.product_id.qty_available
            self.best_price_purchase = self.product_id.best_price
            self.product_catg_id = self.product_id.categ_id.id
            self.parent_product_catg_id = self.product_id.categ_id.parent_id.id
        if self.product_id.attribute_value_ids:
            for data in self.product_id.attribute_value_ids:
                key = data.attribute_id.name
                value = data.name
                data_dict.setdefault(key, []).append(value)
            if 'Size' in data_dict:
                self.size = data_dict['Size'][0]
            if 'size' in data_dict:
                self.size = data_dict['size'][0]
            if 'SIZE' in data_dict:
                self.size = data_dict['SIZE'][0]
            if not ('Size', 'size', 'SIZE') in data_dict:
                pass
        return res

    @api.onchange('name')
    def _compute_upper(self):
        for rec in self:
            rec.name = str(rec.name).title()


class ProductTemplate(models.Model):
    _inherit = "product.template"

    project_categ = fields.Char(related='categ_id.name', string='Category Name')