{
	"name": "Print Journal Voucher",
	"version": "1.0",
	"depends": [
		"base",
		"account"
	], 
	"author": "",
	"website": "",
	"category": "Accounting",
	"description": """\

Features
======================================================================

* Print account move journal voucher
""",
	"data": [
		"report/account_move.xml",
	],
	"installable": True,
	"application": True,
	"auto_install": False,
}