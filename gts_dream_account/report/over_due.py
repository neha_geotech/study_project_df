from odoo import api, fields, models, _
from datetime import datetime,date


# class OverDueReport(models.AbstractModel):
#     '''
#     Inherit model to add fields and new functions
#     '''
#     _name = "report.account.report_overdue_document"
#
#     # delays = fields.Integer(
#     #     string='Total Delays day of Invoice',
#     #     compute='compute_number_of_due_days_of_invoice_new',
#     #     store=True
#     # )
#
#
#     @api.multi
#     def compute_number_of_due_days_of_invoice_new(self):
#         date_format_ = '%Y-%m-%d %H:%M:%S'
#         date_format = '%Y-%m-%d'
#         today_date = fields.Datetime.now()
#         for res in self:
#             if res.date_maturity:
#                 a = datetime.strptime(res.date_maturity, date_format)
#                 b = datetime.strptime(today_date, date_format_)
#                 c = b - a
#                 return c
#
#     @api.model
#     def get_report_values(self, docids, data=None):
#         move = self.env['account.move.line'].browse(docids)
#         return {
#             'doc_ids': docids,
#             'doc_model': 'account.move.line',
#             'data': data,
#             'docs': move,
#             'compute_number_of_due_days_of_invoice_new': self.compute_number_of_due_days_of_invoice_new,
#
#         }
