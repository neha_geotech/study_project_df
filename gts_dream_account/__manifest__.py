{
    'name': 'Account Dream Factory',
    'version': '1.0',
    'category': '',
    'summary': 'Dream Factory ',
    'description': "account changes and modification",
    'website': '',
    'depends': ['account','base'],
    'data': [
        'views/account_move_view.xml',
        'views/over_due_report.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
