from odoo import api, fields, models, _
from datetime import datetime,date


class AccountMoveLine(models.Model):
    '''
    Inherit model to add fields and new functions
    '''
    _inherit = "account.move.line"

    account_balance = fields.Float(
        'Balance', readonly=1, help='This value show balance '
                                    'amount for selected account and partner'
    )

    inv_reference = fields.Char('Invoice Reference')
    inv_date = fields.Date('Invoice Date')
    event_date = fields.Date('Event Date')
    event_name = fields.Char('Event Name')
    venue = fields.Text('Venue')
    activity_owner = fields.Char('Activity Owner')
    basic_amount = fields.Float('Basic Amount')
    gst_amount = fields.Float('GST Amount')

    @api.onchange('account_id','partner_id')
    def onchange_partner_account(self):
        '''
        update balance amount in account move line for selected partner and account
        :return: update balance amount
        '''
        if self.account_id and self.partner_id:
            query="select COALESCE(sum(debit-credit), 0)  from account_move_line where " \
                  "account_id = %s and partner_id=%s"%(self.account_id.id, self.partner_id.id)
            self.env.cr.execute(query)
            results = self.env.cr.fetchall()
            print (results)
            for res in results:
                if res[0]:
                    self.account_balance = res[0]

    @api.onchange('debit', 'credit')
    def change_debit_credit(self):
        '''
        This method check in move line if debit amount enter
        then auto fill 0.0 in credit amount and vice versa
        :return:
        '''
        if self.debit > 0.0:
            # self.debit = round(self.debit)
            self.credit = 0.0
        if self.credit > 0.0:
            # self.credit = round(self.credit)
            self.debit = 0.0
