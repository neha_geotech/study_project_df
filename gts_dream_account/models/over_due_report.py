# -*- coding: utf-8 -*-

import time
from odoo import api, fields, models


class ReportOverdue(models.AbstractModel):
    _inherit = 'report.account.report_overdue'

    # currency_id = fields.Many2one("res.currency", string="Currency")

    def _get_account_move_lines(self, partner_ids):
        res = {x: [] for x in partner_ids}
        self.env.cr.execute(
            "SELECT m.name AS move_id, l.inv_date, l.event_date, l.event_name, l.venue,"
            "l.inv_reference, l.delays, l.date, l.name, l.activity_owner, l.basic_amount, l.gst_amount,"
            "l.ref, l.inv_date, l.partner_id, l.blocked, l.amount_currency, l.currency_id, ac.name as analytic_account_id, "
            "CASE WHEN at.type = 'receivable' "
            "THEN SUM(l.debit) "
            "ELSE SUM(l.credit * -1) "
            "END AS debit, "
            "CASE WHEN at.type = 'receivable' "
            "THEN SUM(l.credit) "
            "ELSE SUM(l.debit * -1) "
            "END AS credit, "
            "CASE WHEN l.inv_date < %s "
            "THEN SUM(l.debit - l.credit) "
            "ELSE 0 "
            "END AS mat "
            "FROM account_move_line l "
            "JOIN account_account_type at ON (l.user_type_id = at.id) "
            "JOIN account_move m ON (l.move_id = m.id) "
            "JOIN account_analytic_account ac ON (l.analytic_account_id = ac.id) "
            "WHERE l.partner_id IN %s AND at.type IN ('receivable', 'payable') AND l.full_reconcile_id IS NULL GROUP BY l.date, l.name, l.ref, l.inv_date, l.partner_id, ac.name, at.type,"
            "l.blocked, l.amount_currency, l.currency_id, l.move_id, m.name, l.delays,"
            "l.inv_date, l.event_date, l.event_name, l.venue,"
            "l.inv_reference, l.date, l.name, l.activity_owner, l.basic_amount, l.gst_amount",
            (((fields.date.today(),) + (tuple(partner_ids),))))
        for row in self.env.cr.dictfetchall():
            res[row.pop('partner_id')].append(row)
        return res
