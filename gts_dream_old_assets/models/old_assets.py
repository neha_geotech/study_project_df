# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import Warning


class AssetOld(models.Model):
    _name = 'account.asset.old'
    _rec_name = 'asset_description'
    _order = 'id'

    date_of_acquisition = fields.Date('Year & Date Acquisition')
    asset_description = fields.Char('Asset Description')
    asset_category = fields.Char('Asset Category')
    life_asset = fields.Float('Asset Life')
    used_life = fields.Float('Used Life')
    remaining_life = fields.Float('Remaining Life')
    gross_block = fields.Float('Gross Block As On 01-04-2015')
    opening_dep = fields.Float('OP. Accu Dep. As On 01-04-2015')
    op_written_value15 = fields.Float('OP. Written Down Value As On 01-04-2015')
    residual_value = fields.Float('Residual Value')
    rate_wdv = fields.Float('Dep. Rate-WDV')
    remaining_life_at_beginning = fields.Float('Life Left')
    op_written_value17 = fields.Float('OP Written Down Value As On 01-04-2017')
    dep_year_end = fields.Float('Dep. for the year ended on 31-03-2018')
    closing_wdv = fields.Float('CL WDV as on 31-03-2018')
    opening_accumulated_dep17 = fields.Float('OP Accumulated Dep. As On 01-04-2017')
    date_of_sale18 = fields.Date('Sale Date Year End On 31-03-2018')
    create_date = fields.Datetime('Created On', default=fields.Datetime.now())