# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'GTS Dream Old Assets',
    'summary': 'Module for Old Assets',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'website': 'http://www.geotechnosoft.com',
    'category': 'Assets',
    'version': '1.0',
    'depends': [],
    'data': [
        'security/ir.model.access.csv',
        'views/old_assets_view.xml',
    ],
    'installable': True,
}
