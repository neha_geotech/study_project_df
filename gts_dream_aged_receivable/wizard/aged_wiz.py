from datetime import date, timedelta, datetime
from dateutil.relativedelta import relativedelta
from odoo import models, fields, api, _


class AgedReportWiz(models.TransientModel):
    _name = 'aged.report.wiz'

    to_date = fields.Date('To Date', default=fields.Datetime.now)

    # end_date = fields.Datetime('End Date', default=fields.Datetime.now)

    @api.multi
    def open_aged_table(self):
        report_search = self.env['aged.report'].search([])
        if report_search:
            report_search.unlink()
        if self.to_date:
            tree_view_id = self.env.ref('gts_dream_aged_receivable.view_aged_report_tree').id
            form_view_id = self.env.ref('gts_dream_aged_receivable.view_aged_report_form').id
            graph_view_id = self.env.ref('gts_dream_aged_receivable.view_aged_report_graph').id
            pivot_view_id = self.env.ref('gts_dream_aged_receivable.view_aged_report_pivot').id
            search_view_ref = self.env.ref('gts_dream_aged_receivable.view_aged_report_search', False)
            date_format = '%Y-%m-%d'

            to_day_ = datetime.strptime(self.to_date, date_format)

            day_0 = to_day_.date()
            day_30 = to_day_.date() - timedelta(days=30)

            day_30_1 = day_30 - timedelta(days=1)
            day_60 = day_30_1 - timedelta(days=30)

            day_60_1 = day_60 - timedelta(days=1)
            day_90 = day_60_1 - timedelta(days=30)

            day_90_1 = day_90 - timedelta(days=1)
            day_120 = day_90_1 - timedelta(days=30)

            day_120_1 = day_120 - timedelta(days=1)
            day_150 = day_120_1 - timedelta(days=30)

            day_150_1 = day_150 - timedelta(days=1)
            day_180 = day_150_1 - timedelta(days=30)

            day_180_1 = day_180 - timedelta(days=1)
            day_210 = day_180_1 - timedelta(days=30)

            year_1 = day_0 - relativedelta(years=1)

            year_1_5 = year_1 - relativedelta(months=6)

            year_2 = year_1_5 - relativedelta(months=6)

            year_2_5 = year_2 - relativedelta(months=6)

            year_3 = year_2_5 - relativedelta(months=6)

            year_3_5 = year_3 - relativedelta(months=6)

            year_4 = year_3_5 - relativedelta(months=6)

            self._cr.execute("""
            select sum(balance), partner_id from account_move_line
            where partner_id is not null
            and user_type_id = 1
            and date < '%s'
            group by partner_id""" % day_0)
            result_partner = self._cr.fetchall()
            partners = []
            for data in result_partner:
                if data[0] == 0.0:
                    partners.append(data[1])
            self._cr.execute(
                """
                with period1 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       sum(al.amount_residual) as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity between '%s' and '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period2 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       sum(al.amount_residual) as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity between '%s' and '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period3 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       sum(al.amount_residual) as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity between '%s' and '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period4 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       sum(al.amount_residual) as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity between '%s' and '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period5 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       sum(al.amount_residual) as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity between '%s' and '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period6 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       sum(al.amount_residual) as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity between '%s' and '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period7 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       sum(al.amount_residual) as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity between '%s' and '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period8 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       sum(al.amount_residual) as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity between '%s' and '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period9 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       sum(al.amount_residual) as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity < '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                older as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       sum(al.amount_residual) as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity < '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                undue_amount as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       sum(al.amount_residual) as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity > '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period10 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       sum(al.amount_residual) as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity < '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period11 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       sum(al.amount_residual) as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity < '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period12 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       sum(al.amount_residual) as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity < '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period13 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       sum(al.amount_residual) as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity < '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period14 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       sum(al.amount_residual) as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity < '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period15 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       sum(al.amount_residual) as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity < '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period16 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       sum(al.amount_residual) as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                       from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity < '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period17 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       sum(al.amount_residual) as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity < '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period18 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       sum(al.amount_residual) as part18,
                       0.0 as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity < '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period19 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       sum(al.amount_residual) as part19,
                       0.0 as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity between '%s' and '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period20 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       sum(al.amount_residual) as part20,
                       0.0 as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity < '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                period21 as (
                    select
                       al.id as account_move_line_id,
                       am.id as account_move_id,
                       al.partner_id as partner_id,
                       rp.parent_id as parent_id,
                       al.branch_id as branch_id,
                       rp.rating as rating,
                       rp.user_id as salesperson,
                       al.invoice_id as invoice_id,
                       0.0 as part1,
                       0.0 as part2,
                       0.0 as part3,
                       0.0 as part4,
                       0.0 as part5,
                       0.0 as part6,
                       0.0 as part7,
                       0.0 as part8,
                       0.0 as part9,
                       0.0 as older,
                       0.0 as total,
                       0.0 as undue,
                       al.inv_date as date_inv,
                       0.0 as part10,
                       0.0 as part11,
                       0.0 as part12,
                       0.0 as part13,
                       0.0 as part14,
                       0.0 as part15,
                       0.0 as part16,
                       0.0 as part17,
                       0.0 as part18,
                       0.0 as part19,
                       0.0 as part20,
                       sum(al.amount_residual) as part21
                    from account_move_line al
                    join res_partner rp on rp.id = al.partner_id
                    left join account_move am on am.id = al.move_id
                    where rp.employee is FALSE
                    and am.state = 'posted'
                    and al.date_maturity < '%s'
                    and al.partner_id not in %s
                    and al.user_type_id = 1
                    and al.amount_residual != 0.0
                    group by al.id, am.id, rp.parent_id, rp.rating, rp.user_id, al.inv_date
                ),
                final_data as (
                    select * from period1
                    UNION
                    select * from period2
                    UNION
                    select * from period3
                    UNION
                    select * from period4
                    UNION
                    select * from period5
                    UNION
                    select * from period6
                    UNION
                    select * from period7
                    UNION
                    select * from period8
                    UNION
                    select * from period9
                    UNION
                    select * from older
                    UNION
                    select * from undue_amount
                    UNION
                    select * from period10
                    UNION
                    select * from period11
                    UNION
                    select * from period12
                    UNION
                    select * from period13
                    UNION
                    select * from period14
                    UNION
                    select * from period15
                    UNION
                    select * from period16
                    UNION
                    select * from period17
                    UNION
                    select * from period18
                    UNION
                    select * from period19
                    UNION
                    select * from period20
                    UNION
                    select * from period21

                )
                select row_number() OVER () AS id, account_move_line_id,
                account_move_id, partner_id, parent_id, branch_id, rating, salesperson, invoice_id, 
                part1, part2, part3,
                part4, part5, part6, part7, part8, part9, part10, part11, part12, part13, part14, part15, part16, part17, part18, part19, part20, part21, older, undue, date_inv,
                (part1+part2+part3+part4+part5+part6+part7+older+undue) as total,
                (part8+part9) as total_
                    from final_data
            """ % (day_30, day_0, tuple(partners),
                   day_60, day_30_1, tuple(partners),
                   day_90, day_60_1, tuple(partners),
                   day_120, day_90_1, tuple(partners),
                   day_150, day_120_1, tuple(partners),
                   day_180, day_150_1, tuple(partners),
                   day_210, day_180_1, tuple(partners),
                   day_180, day_0, tuple(partners),
                   day_180, tuple(partners),
                   day_210, tuple(partners), day_0, tuple(partners),
                   year_1, tuple(partners),
                   year_1_5, tuple(partners),
                   year_2, tuple(partners),
                   year_2_5, tuple(partners),
                   year_3, tuple(partners),
                   year_3_5, tuple(partners),
                   year_4, tuple(partners),
                   day_120, tuple(partners),
                   day_150, tuple(partners),
                   day_150, day_0, tuple(partners),
                   day_60, tuple(partners),
                   day_90, tuple(partners)))
            result = self._cr.fetchall()
            rows = result
            values = ', '.join(map(str, rows))
            sql = ("""INSERT INTO aged_report (id, account_move_line_id,
                    account_move_id, partner_id, parent_id, branch_id, rating, salesperson, invoice_id,
                    part1, part2, part3,
                    part4, part5, part6, part7, part8, part9, part10, part11, part12, part13, part14, part15, part16, part17, part18, part19, part20, part21, older, undue, date_inv, total, total_) VALUES {}""".format(
                values).replace('None', 'null')
                   )
            self._cr.execute(sql)
            action = {
                'type': 'ir.actions.act_window',
                'views': [
                    (pivot_view_id, 'pivot'),
                    (tree_view_id, 'tree'), (form_view_id, 'form'),
                    (graph_view_id, 'graph')
                ],
                'view_mode': 'tree,form',
                'name': _('Aged Receivable Report: %s' %(to_day_.date())),
                'res_model': 'aged.report',
                'search_view_id': search_view_ref and search_view_ref.id,
                'context': {'group_by': ['parent_id', 'partner_id', 'account_move_id']}
            }
            return action