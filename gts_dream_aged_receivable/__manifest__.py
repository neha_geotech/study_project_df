# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'GTS Aged Report',
    'summary': 'Aged Report',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'website': 'http://www.geotechnosoft.com',
    'category': 'accounting',
    'version': '1.0',
    'depends': [
        'account', 'gts_dream_purchase'
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/aged_receivable_security.xml',
        'wizard/aged_view_wiz.xml',
        'wizard/bank_report_wiz_view.xml',
        'report/aged_report_view.xml',
    ],
    'installable': True,
}
