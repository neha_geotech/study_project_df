from odoo import api, fields, models, _
from datetime import date, timedelta, datetime
import struct

class LoanTaxWiz(models.TransientModel):
    _name = 'loan.tax.wiz'

    from_date = fields.Date('Start Date', default=fields.Datetime.now)
    end_date = fields.Date('End Date', default=fields.Datetime.now)

    @api.multi
    def open_tax_loan_table(self):
        report_search = self.env['loan.tax.report'].search([])
        loan_tax_obj = self.env['loan.tax.report']
        total = 0.0
        input = 0.0
        output = 0.0

        branches = self.env['res.branch'].search([])
        self._cr.execute('''
                            delete  
                            from loan_tax_report
                            where branch_id in %s
                        ''', (tuple(branches.ids),))

        if self.from_date and self.end_date:
            tree_view_id = self.env.ref('gts_dream_loan_tax_report.view_loan_tax_report_tree').id
            form_view_id = self.env.ref('gts_dream_loan_tax_report.view_tax_loan_report_form').id
            graph_view_id = self.env.ref('gts_dream_loan_tax_report.view_loan_tax_report_graph').id
            pivot_view_id = self.env.ref('gts_dream_loan_tax_report.view_loan_tax_report_pivot').id
            search_view_ref = self.env.ref('gts_dream_loan_tax_report.view_loan_tax_report_search', False)
            hdfc_loan = self.env['account.account'].search([('name', '=', 'HDFC Bank Loan A/C No.44698138')], limit=1)
            icici_loan = self.env['account.account'].search([('name', '=', 'ICICI Bank Ltd.Loan A/C No. LBDEL00001951418')], limit=1)
            intec_loan = self.env['account.account'].search([('name', '=', 'Intec Capital Ltd.')], limit=1)
            nbfc_loan = self.env['account.account'].search([('name', '=', 'Loan NBFC')], limit=1)
            intrest_machine_loan = self.env['account.account'].search([('name', '=', 'Interest on Machinery Loan')], limit=1)
            intrest_plot_loan = self.env['account.account'].search([('name', '=', 'Interest Plot-21A')], limit=1)
            intrest_vehicle_loan = self.env['account.account'].search([('name', '=', 'Interest on Vehicle Loans')], limit=1)
            # =================account for Tax Data======================================
            tax_paid = self.env['account.account'].search([('name', '=', 'GST Paid')], limit=1)
            account_type = self.env['account.account.type'].search([('name', '=', 'Current Liabilities')], limit=1)
            gst_accounts = self.env['account.account'].search([('name', 'ilike', '%gst'),
                                                               ('user_type_id', '=', account_type.id)])
            input_tax = self.env['account.account'].search([('name', 'in', ('Input SGST','Input CGST','Input IGST'))])
            output_tax = self.env['account.account'].search([('name', 'in', ('Output SGST','Output CGST','Output IGST'))])
            # ==============================TDS receivable==========================
            tds_deducted = self.env['account.account'].search([('name', '=', 'TDS Receivable')], limit=1)
            # tds_paid = self.env['account.account'].search([('name', 'in', ('TDS Payable Rent Company',
            #                                                                'TDS Payable Professinal / Technical',
            #                                                                'TDS Payable Contractor Company',
            #                                                                'TDS Payable Contractor Non Company -1%',
            #                                                                'TDS Payable Rent Non Compay'))])
            tds_paid = self.env['account.account'].search([('name', 'ilike', '%TDS Payable')])
            print(tds_paid)
            # ======================bad debiit=================================================
            bad_debit = self.env['account.account'].search([('name', '=', 'Bad Debts')], limit=1)
            # =========================== Asset Account ==================================
            asset_type_id = self.env['account.account.type'].search([('name', '=', 'Fixed Assets')])
            asset_acc = self.env['account.account'].search([('user_type_id', '=', asset_type_id.id)])
            # =============================CC bank AC==============================
            cc_ac = self.env['account.account'].search([('name', '=', 'State Bank Of India-CC A/C No. 31860447153')], limit=1)
            loan_related_party = self.env['account.account'].search([('name', '=', 'Loan From Related Party')], limit=1)
            branches = self.env['res.branch'].search([])
            for branch in branches:
                self._cr.execute('''
                                    select sum(debit-credit) 
                                    from account_move_line
                                    where date < %s
                                    AND account_id in %s
                                    and branch_id = %s
                                ''', (self.from_date, (loan_related_party.id,hdfc_loan.id,icici_loan.id,intec_loan.id), branch.id))
                sum_open_balance = self._cr.fetchall()
                self._cr.execute('''
                                    select sum(debit) 
                                    from account_move_line
                                    where date >= %s
                                    and date <= %s
                                    AND account_id in %s
                                    and branch_id = %s
                                    
                                ''', (self.from_date, self.end_date, (intrest_machine_loan.id,intrest_plot_loan.id,intrest_vehicle_loan.id), branch.id))
                intrest_loan_balance = self._cr.fetchall()
                self._cr.execute('''
                                    select sum(credit) 
                                    from account_move_line
                                    where date >= %s
                                    and date <= %s
                                    AND account_id in %s
                                    and branch_id = %s
                                     
                                ''', (self.from_date, self.end_date, (loan_related_party.id,nbfc_loan.id), branch.id))
                loan_receive = self._cr.fetchall()
                self._cr.execute('''
                                    select sum(debit) 
                                    from account_move_line
                                    where date >= %s
                                    and date <= %s
                                    AND account_id in %s
                                    and branch_id = %s
                                    
                                ''', (self.from_date, self.end_date, (nbfc_loan.id,loan_related_party.id,hdfc_loan.id,icici_loan.id,intec_loan.id), branch.id))
                loan_paid = self._cr.fetchall()
                self._cr.execute('''
                                    select sum(debit-credit) 
                                    from account_move_line
                                    where date < %s
                                    AND account_id in %s
                                    and branch_id = %s

                                ''', (self.from_date, tuple(gst_accounts.ids), branch.id))
                opening_tax = self._cr.fetchall()
                self._cr.execute('''
                                    select sum(debit-credit) 
                                    from account_move_line
                                    where date >= %s
                                    and date <= %s
                                    AND account_id = %s
                                    and branch_id = %s

                                ''', (self.from_date, self.end_date, tax_paid.id, branch.id))
                paid_tax = self._cr.fetchall()
                self._cr.execute('''
                                    select sum(debit-credit) 
                                    from account_move_line
                                    where date >= %s
                                    and date <= %s
                                    AND account_id in %s
                                    and branch_id = %s

                                ''', (self.from_date, self.end_date, tuple(input_tax.ids), branch.id))
                input_tax_payable = self._cr.fetchall()
                self._cr.execute('''
                                    select sum(credit-debit) 
                                    from account_move_line
                                    where date >= %s
                                    and date <= %s
                                    AND account_id in %s
                                    and branch_id = %s

                                ''', (self.from_date, self.end_date, tuple(output_tax.ids), branch.id))
                output_tax_payable = self._cr.fetchall()
                self._cr.execute('''
                                    select sum(debit) 
                                    from account_move_line
                                    where date >= %s
                                    and date <= %s
                                    AND account_id = %s
                                    and branch_id = %s
                                ''', (self.from_date, self.end_date, tds_deducted.id, branch.id))
                tds_deducted_balance = self._cr.fetchall()
                self._cr.execute('''
                                    select sum(debit) 
                                    from account_move_line
                                    where date >= %s
                                    and date <= %s
                                    AND account_id in %s
                                    and branch_id = %s
                                ''', (self.from_date, self.end_date, tuple(tds_paid.ids), branch.id))
                tds_paid_balance = self._cr.fetchall()
                self._cr.execute('''
                                    select sum(debit) 
                                    from account_move_line
                                    where date >= %s
                                    and date <= %s
                                    AND account_id = %s
                                    and branch_id = %s
                                ''', (self.from_date, self.end_date, bad_debit.id, branch.id))
                bad_debit_balance = self._cr.fetchall()
                self._cr.execute('''
                                    select sum(credit) 
                                    from account_move_line
                                    where date >= %s
                                    and date <= %s
                                    AND account_id in %s
                                    and branch_id = %s
                                ''', (self.from_date, self.end_date, tuple(asset_acc.ids), branch.id))
                assets_balance = self._cr.fetchall()
                self._cr.execute('''
                                    select sum(debit-credit) 
                                    from account_move_line
                                    where date <= %s
                                    
                                    AND account_id = %s
                                    and branch_id = %s
                                ''', (self.end_date, cc_ac.id, branch.id))
                cc_balance = self._cr.fetchall()
                if input_tax_payable[0][0]:
                    input = input_tax_payable[0][0]
                if output_tax_payable[0][0]:
                    output = output_tax_payable[0][0]
                total = output - input
                loan_tax_obj.create({'branch_id': branch.id,
                                     'opening_loan': sum_open_balance[0][0],
                                    'loan_receive': loan_receive[0][0],
                                     'principal_paid': loan_paid[0][0],
                                     'loan_interest_paid': intrest_loan_balance[0][0],
                                     'open_tax': opening_tax[0][0],
                                     'tax_paid': paid_tax[0][0],
                                     'tax_payable': total,
                                     'tds_deducted': tds_deducted_balance[0][0],
                                     'tds_pt_paid': tds_paid_balance[0][0],
                                     'bad_debts': bad_debit_balance[0][0],
                                     'investment': assets_balance[0][0],
                                     'cc_bank_account': cc_balance[0][0]
                                     })
                total = 0.0
                input = 0.0
                output = 0.0
            date_format = '%Y-%m-%d'
            first_day_ = datetime.strptime(self.from_date, date_format)
            last_day_ = datetime.strptime(self.end_date, date_format)
            first_day = first_day_
            last_day = last_day_
            bank_accounts = self.env['account.account'].search([
                ('group_id.name', '=', 'Bank Accounts'),
                ('name', '!=', 'Bank')
            ])
            bank = []
            for data in bank_accounts:
                bank.append(data.id)
            cash_accounts = self.env['account.account'].search([
                ('group_id.name', '=', 'Cash-In-Hand'),
                ('name', '!=', 'Dummy Cash')
            ])
            cash = []

            action = {
                'type': 'ir.actions.act_window',
                'views': [
                    (tree_view_id, 'tree'), (form_view_id, 'form'),
                    (graph_view_id, 'graph'), (pivot_view_id, 'pivot')
                ],
                'view_mode': 'tree,form',
                'name': _('Loan & Tax Report : %s - %s' %(first_day.strftime('%d %b %y'),
                                                                  last_day.strftime('%d %b %y'))),
                'res_model': 'loan.tax.report',
                'search_view_id': search_view_ref and search_view_ref.id,
            }
            return action

