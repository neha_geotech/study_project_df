# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'GTS Loan & tax Report',
    'summary': 'Loan & Tax Report',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'website': 'http://www.geotechnosoft.com',
    'category': '',
    'version': '1.0',
    'depends': [
        'gts_dream_purchase'
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/security_view.xml',
        'wizard/loan_tax_wiz_view.xml',
        'views/loan_tax_report_view.xml',
    ],
    'installable': True,
}
