from odoo import api, fields, models

class LoanTaxReport(models.Model):
    _name = 'loan.tax.report'
    _rec_name = 'branch_id'

    @api.depends('opening_loan', 'loan_receive', 'principal_paid')
    def calculate_loan_closing(self):
        for rec in self:
            rec.closing_laon = (rec.opening_loan + rec.loan_receive) - rec.principal_paid

    @api.depends('open_tax', 'tax_payable', 'tax_paid')
    def calculate_tax_close(self):
        for rec in self:
            total = 0.0
            total = (rec.open_tax + rec.tax_payable) - rec.tax_paid
            rec.closing_tax = total


    branch_id = fields.Many2one('res.branch', 'Branch')
    opening_loan = fields.Float('Opening Loan Amt.')
    loan_receive = fields.Float('Loan Received')
    principal_paid = fields.Float('Loan & Principal Paid')
    loan_interest_paid = fields.Float('Loan Interest Paid')
    closing_laon = fields.Float('Closing Loan Amt.',compute="calculate_loan_closing", store=True)
    open_tax = fields.Float('Opening Tax')
    tax_payable = fields.Float('Tax Payable')
    tax_paid = fields.Float('Tax Paid')
    closing_tax = fields.Float('Closing Tax', compute="calculate_tax_close", store=True)
    tds_deducted = fields.Float('TDS Deducted')
    tds_pt_paid = fields.Float('TDS/PT Paid')
    investment = fields.Float('Investment & Assets')
    bad_debts = fields.Float('Bad Debts/Short Pass')
    cc_bank_account = fields.Float('CC Bank Account')