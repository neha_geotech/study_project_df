# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'GTS Stock Opening Closing Report',
    'summary': 'Stock Opening Closing Report',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'website': 'http://www.geotechnosoft.com',
    'category': 'Manufacturing',
    'version': '1.0',
    'depends': [
        'mrp', 'analytic', 'gts_dream_mrp','gts_dream_purchase'
    ],
    'data': [
        # 'security/ir.model.access.csv',
        # 'security/variance_security.xml',
        'wizard/stock_open_wiz_view.xml',
        'report/stock_opening_closing_view.xml',
    ],
    'installable': True,
}
