from odoo import api, fields, models, tools, _
from datetime import datetime, date, time, timedelta
from odoo.exceptions import UserError, ValidationError


class StockOpeningClosing(models.Model):
    '''
    This is report view table to show Stock Report
    '''
    _name = 'stock.opening.closing.wiz'

    from_date = fields.Date('Start Date', default=fields.Datetime.now)
    end_date = fields.Date('End Date', default=fields.Datetime.now)

    @api.multi
    def stock_opening_closing(self):
        report_search = self.env['stock.opening.closing'].search([])
        self._cr.execute('''
                                delete  
                                from stock_opening_closing

                            ''', )
        if self.from_date and self.end_date:
            tree_view_id = self.env.ref('gts_dream_stock_opening_closing.view_stock_report_tree').id
            form_view_id = self.env.ref('gts_dream_stock_opening_closing.view_stock_opening_report_form').id
            # graph_view_id = self.env.ref('gts_dream_stock_opening_closing.view_opening_report_graph').id
            # pivot_view_id = self.env.ref('gts_dream_stock_opening_closing.view_opening_report_pivot').id
            # search_view_ref = self.env.ref('gts_dream_stock_opening_closing.view_opening_report_search', False)
            date_format = '%Y-%m-%d'
            first_day_ = datetime.strptime(self.from_date, date_format)
            last_day_ = datetime.strptime(self.end_date, date_format)
            first_day = first_day_
            # day_last_strip = str(last_day_).strip(':0')
            last_day = last_day_
            # last_day_it = day_last_strip + '23:59:59'

            self._cr.execute("""
                    with product_detail_inward as (
                        select                            
                            sl.branch_id as branch_id,
                            sm.product_id as product_id,
                            sum(sm.product_uom_qty) as purchase_quantity,
                            
                            0.0 as outward_quantity,
                            pr.product_stage as product_stage,
                            sm.date as date
                            from stock_move sm
                            join stock_location sl on sl.id = sm.location_dest_id
                            join res_branch br on br.id = sl.branch_id
                            join product_product pr on sm.product_id = pr.id
                            where location_dest_id in (16, 73, 79, 91, 85)
                            and picking_type_id in (1, 61, 79, 73,67)
                            and state = 'done'
                            and sm.date <= '%s'
                            and picking_id is not null
                            group by sm.product_id, sl.branch_id, pr.product_stage, sm.date
                    ),

                    product_detail_outward as (
                        select
                            sl.branch_id as branch_id,
                            sm.product_id as product_id,
                            0.0 as purchase_quantity,                            
                            sum(sm.product_uom_qty) as outward_quantity,                            
                            pr.product_stage as product_stage,
                            sm.date as date
                            from stock_move sm
                            join stock_location sl on sl.id = sm.location_dest_id
                            join res_branch br on br.id = sl.branch_id
                            join product_product pr on sm.product_id = pr.id
                            where location_dest_id in (102, 100, 99, 95, 104)
                            and picking_type_id in (4, 64, 70, 76, 82)
                            and state = 'done'
                            and sm.date between '%s' and '%s'
                            and picking_id is not null
                            group by sm.product_id, sl.branch_id, pr.product_stage, sm.date
                    ),


                    final_data as (

                        select * from product_detail_inward
                        UNION
                        select * from product_detail_outward

                    )


                    select row_number() OVER (ORDER BY product_id) AS id, branch_id, product_id, purchase_quantity,                    
                    outward_quantity, product_stage as product_stage_, date

                    from final_data
            """ % (
                first_day,
                first_day, last_day

            ))

            result = self._cr.fetchall()
            rows = result
            values = ', '.join(map(str, rows))
            sql = ("""INSERT INTO stock_opening_closing
                            (id, branch_id, product_id,
                            purchase_quantity, outward_quantity,
                            product_stage_, date
                            ) VALUES {}""".format(values).replace('None', 'null')
                   )
            self._cr.execute(sql)

            action = {
                'type': 'ir.actions.act_window',
                'views': [
                    (tree_view_id, 'tree'), (form_view_id, 'form'),

                ],
                'view_mode': 'tree,form',
                'name': _('Simple Stock Report: %s - %s' % (first_day.strftime('%d %b %y'),
                                                            last_day.strftime('%d %b %y'))),
                'res_model': 'stock.opening.closing',
                # 'search_view_id': search_view_ref and search_view_ref.id,
                'context': {'group_by': ['branch_id']}
            }
            return action