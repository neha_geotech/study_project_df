# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from datetime import timedelta
from odoo.exceptions import UserError


class ResPartner(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'

    partner_group_label = fields.Char(
        compute='_compute_partner_group_label',
        help='The label to display on partner ledger button, in form view'
    )
    # group_partner = fields.Boolean(
    #     'Group Partner', compute='_compute_group',
    #     default=True, store=True
    # )

    @api.depends('supplier', 'customer')
    def _compute_partner_group_label(self):
        for record in self:
            if record.supplier == record.customer:
                record.partner_group_label = _('Group Ledger')
            elif record.supplier:
                record.partner_group_label = _('Group Supplier')
            else:
                record.partner_group_label = _('Group Customer')

    @api.multi
    def open_partner_group(self):
        report_search = self.env['group.partner.report'].search([])
        if report_search:
            report_search.unlink()
        where_clause = ''
        partners = []
        for data in self:
            if data.supplier:
                where_clause = 'al.user_type_id = 2'
            if data.customer:
                where_clause = 'al.user_type_id = 1'
            if data.supplier == data.customer:
                where_clause = 'al.user_type_id in (1, 2)'
            search_id = self.env['res.partner'].search([('parent_id', '=', data.id)])
            for res in search_id:
                partners.append(res.id)
            if not search_id:
                raise UserError(_('Unable to show Group Ledger!'))
        self._cr.execute(
            """
            with data_partner as (
                select
                   al.id as account_move_line_id,
                   am.id as account_move_id,
                   al.partner_id as partner_id,
                   rp.parent_id as parent_id,
                   al.branch_id as branch_id,
                   al.invoice_id as invoice_id,
                   al.date_maturity as date_maturity,
                   al.delays as delays,
                   sum(al.balance) as balance,
                   sum(al.credit) as credit,
                   sum(al.debit) as debit,
                   al.account_id as account_id,
                   al.date as date,
                   al.event_name as event_name,
                   al.venue as venue,
                   al.activity_owner as activity_owner
                from account_move_line al
                join res_partner rp on rp.id = al.partner_id
                left join account_move am on am.id = al.move_id
                where rp.employee is FALSE
                and am.state = 'posted'
                and al.partner_id in %s
                and %s
                group by al.id, am.id, rp.parent_id
            )
            select row_number() OVER () AS id, account_move_line_id, account_move_id, partner_id, parent_id,
            branch_id, invoice_id, date_maturity, delays, balance, credit, debit, account_id, date, event_name, venue,
            activity_owner from data_partner""" % (tuple(partners), where_clause))
        result = self._cr.fetchall()
        rows = result
        values = ', '.join(map(str, rows))
        sql = ("""INSERT INTO group_partner_report (id, account_move_line_id, account_move_id, partner_id, parent_id,
            branch_id, invoice_id, date_maturity, delays, balance, credit, debit, account_id, date, event_name,
             venue, activity_owner) VALUES {}""".format(values).replace('None', 'null')
               )
        self._cr.execute(sql)

        tree_view_id = self.env.ref('gts_dream_group_partner.view_group_partner_report_tree').id
        form_view_id = self.env.ref('gts_dream_group_partner.view_group_partner_report_form').id
        graph_view_id = self.env.ref('gts_dream_group_partner.view_group_partner_report_graph').id
        pivot_view_id = self.env.ref('gts_dream_group_partner.view_group_partner_report_pivot').id
        search_view_ref = self.env.ref('gts_dream_group_partner.view_group_partner_report_search', False)
        action = {
            'type': 'ir.actions.act_window',
            'views': [
                (tree_view_id, 'tree'),
                (pivot_view_id, 'pivot'),
                (form_view_id, 'form'),
                (graph_view_id, 'graph')
            ],
            'view_mode': 'tree,form',
            'name': _('Group Partner Report'),
            'res_model': 'group.partner.report',
            'search_view_id': search_view_ref and search_view_ref.id,
            'context': {'group_by': ['parent_id', 'partner_id']}
        }
        return action


class GroupPartnerReport(models.Model):
    _name = 'group.partner.report'
    _rec_name = 'parent_id'

    partner_id = fields.Many2one(
        'res.partner',
        string='Partner', readonly=True
    )
    parent_id = fields.Many2one(
        'res.partner',
        string='Group Partner', readonly=True
    )
    account_move_id = fields.Many2one(
        'account.move', string='Journal Entry',
        readonly=True
    )
    account_move_line_id = fields.Many2one(
        'account.move.line', string='Journal Item',
        readonly=True
    )
    account_id = fields.Many2one(
        'account.account', string='Account',
        readonly=True
    )
    branch_id = fields.Many2one(
        'res.branch', string='Branch',
        readonly=True
    )
    invoice_id = fields.Many2one(
        'account.invoice', string='Invoice Number',
        readonly=True
    )
    debit = fields.Float('Debit', readonly=True)
    credit = fields.Float('Credit', readonly=True)
    balance = fields.Float('Balance', readonly=True)
    event_date = fields.Date('Event Date', readonly=True)
    date = fields.Date('Date', readonly=True)
    delays = fields.Integer('Due Days', readonly=True)
    date_maturity = fields.Date('Due Date', readonly=True)
    event_name = fields.Char('Event Name', readonly=True)
    venue = fields.Text('Venue', readonly=True)
    activity_owner = fields.Char('Activity Owner', readonly=True)


