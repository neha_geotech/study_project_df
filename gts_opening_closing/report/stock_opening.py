# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError


class SimpleStockReport(models.Model):
    '''
    This is report view table to show Stock Report
    '''
    _name = 'simple.stock.report.new'

    product_id = fields.Many2one('product.product', string='Product')
    location_dest_id = fields.Many2one('stock.location', string='Location')
    branch_id = fields.Many2one('res.branch', string='Branch', readonly=True)
    purchase_quantity = fields.Float(string='Purchase Quantity')
    purchase_value = fields.Float(string='Purchase Cost(Avg Cost)')
    outward_quantity = fields.Float('Consume Quantity')
    outward_value = fields.Float('Consume Cost')
    theoretical_quantity = fields.Float(string='Stock Quantity')
    theoretical_cost = fields.Float(string='Stock Value')
    product_stage_ = fields.Selection([
        ('raw_material', 'Raw Material'),
        ('wip', 'WIP'),
        ('semi_finished', 'Semi Finished'),
        ('reusable', 'Reusable')],
        string='Product Stage')
    date = fields.Date(string='Stock Move Date')
    purchase_price = fields.Float(string='Unit Cost(Real)')
    purchase_cost_price = fields.Float(string='Purchase Cost(Real)')
    # product_uom_qty = fields.Float(string='PO Quantity')
