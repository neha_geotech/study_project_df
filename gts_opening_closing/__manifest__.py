# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'GTS Opening Closing',
    'summary': 'Stock Opening Closing',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'website': 'http://www.geotechnosoft.com',
    'category': 'Inventory',
    'version': '1.0',
    'depends': [
        'gts_dream_purchase'
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/security_view.xml',
        'wizard/stock_opening_wiz.xml',
        'wizard/stock_report_wiz_view.xml',
        'report/stock_opening_view.xml',
        'report/stock_report_view.xml',

    ],
    'installable': True,
}
