from odoo import models, fields, api, _
from datetime import datetime, date, time, timedelta
from dateutil.relativedelta import relativedelta


class GTSStockReportWizardDetailed(models.TransientModel):
    _name = 'stock.report.wizard.detailed'

    from_date = fields.Date('Start Date', default=fields.Datetime.now)
    end_date = fields.Date('End Date', default=fields.Datetime.now)

    @api.multi
    def open_product_detail_table_new(self):
        report_search = self.env['stock.report.wizard'].search([])
        self._cr.execute('''
                            delete  
                            from stock_report_wizard
                        ''',)
        if self.from_date and self.end_date:
            tree_view_id = self.env.ref('gts_opening_closing.view_stock_report_wizard_tree').id
            form_view_id = self.env.ref('gts_opening_closing.view_stock_report_wizard_form').id
            date_format = '%Y-%m-%d'
            first_day_ = datetime.strptime(self.from_date, date_format)
            last_day_ = datetime.strptime(self.end_date, date_format)
            first_day = first_day_
            last_day = last_day_
            self._cr.execute("""
                with stock_detailed_opening as (
                    select                            
                        sl.branch_id as branch_id,
                        sq.product_id as product_id,
                        sum(sq.quantity) as physical_op_stock_qty,
                        pr.product_stage as product_stage,
                        sq.in_date as date
                        from stock_quant sq
                        join stock_location sl on sl.id = sq.location_id
                        join res_branch br on br.id = sl.branch_id
                        join product_product pr on sq.product_id = pr.id
                        where sq.location_id in (select id from stock_location where usage='internal')
                        and sq.in_date < '%s'
                        group by sq.product_id, sl.branch_id, pr.product_stage, sq.in_date
                ),
                final_data as (
                    select * from stock_detailed_opening
                )
                select row_number() OVER (ORDER BY product_id) AS id, branch_id, product_id, physical_op_stock_qty, 
                date from final_data
                """ % (
                       first_day
                       ))

            result = self._cr.fetchall()
            print('===result', result)
            rows = result
            print('===rows', rows)
            values = ', '.join(map(str, rows))
            print('===values', values)
            sql = ("""INSERT INTO stock_report_wizard(id, branch_id, product_id, physical_op_stock_qty, date) 
            VALUES {}""".format(values).replace('None', 'null')
                   )
            self._cr.execute(sql)
            action = {
                'type': 'ir.actions.act_window',
                'views': [
                    (tree_view_id, 'tree'), (form_view_id, 'form'),
                ],
                'view_mode': 'tree,form',
                'name': _('NEW Stock Report: %s - %s' % (first_day.strftime('%d %b %y'),
                                                      last_day.strftime('%d %b %y'))),
                'res_model': 'stock.report.wizard',
                'context': {'group_by': ['branch_id','date', 'product_id']}
            }
            return action