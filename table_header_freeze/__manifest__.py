# -*- coding: utf-8 -*-

{
    'name': 'Table header freeze',
    'version': '1.1',
    'sequence': 33,
    'category': 'Extra Tools',
    #'website': 'http://manangewall.mn',
    'author': 'InceptionMara',
    'price': 15.0,
    'license':'LGPL-3',
    'currency': 'USD',
    'description': """
        - Easy use branch
        - Tree view header freeze
        - One2many tree view header freeze """,
    'images': [
        'static/description/icon.jpg',
    ],
    'depends': [
        'gts_dream_purchase',
        'gts_dream_quotation',
        'gts_dream_mrp',
        'gts_profit_loss_report',
        'gts_payment_bank_charge_tds',
        'gts_hr_batch_expense',
        'gts_employee_expense_advance',
        'gts_dream_variance_report',
        'gts_dream_project_profit',
        'gts_dream_old_assets',
        'gts_dream_material',
        'gts_dream_crm',
        'gts_dream_budget',
        'gts_journal_entry_report',
        'gts_job_code_detail',
        'gts_dream_stock_detail',
    ],
    'summary': '',
    'data': [
        'views/widget_path.xml',
        'views/inherited_views.xml'
    ],
    'installable': True,
    'application': True,
    'qweb': ['static/xml/*.xml'],
}
