from odoo import fields, models, api, _
from odoo.exceptions import UserError


class Picking(models.Model):
    _inherit = "stock.picking"

    @api.model
    def get_total_amt(self):
        search_id = self.env['stock.picking'].search([])
        for res in search_id:
            total_amount = 0.0
            for line in res.move_lines:
                total_amount += line.total_cost
                res.update({
                    'total_it_amount':  total_amount
                })

    job_code = fields.Many2one(
        'account.analytic.account',
        string='Job Code',
        index=True,
    )
    # total_purchase_amount = fields.Float(string='Total Cost')
    picking_type = fields.Selection(
        related='picking_type_id.code',
        string='Picking Type',
        index=True,
    )
    expected_closing = fields.Date(
        compute='get_date_deadline',
        string='Expected Closing Date',
    )


    sale_id = fields.Many2one(
        'sale.order', 'Sale',
        readonly=True, copy=False, index=True
    )
    total_amt = fields.Float('Total Amount')
    total_it_amount = fields.Float('Total Amount')

    @api.multi
    def get_date_deadline(self):
        for pic in self:
            if pic.job_code and pic.job_code.date_deadline:
                pic.expected_closing = pic.job_code.date_deadline

    @api.onchange('move_lines')
    def move_line_change(self):
        for data in self.move_lines:
            data.branch_id = self.branch_id.id
            if self.job_code:
                data.job_code = self.job_code.id

    @api.onchange('branch_id')
    def branch_change(self):
        for data in self.move_lines:
            if self.branch_id:
                data.branch_id = self.branch_id.id

    @api.onchange('job_code')
    def job_code_change(self):
        for data in self.move_lines:
            if self.job_code:
                data.job_code = self.job_code.id

    @api.multi
    def button_validate(self):
        data = super(Picking, self).button_validate()
        if self.picking_type_id.code == 'internal' and self.location_dest_id.manufacturing_location is True:
            for line in self.move_lines:
                res = {
                    'name': self.name,
                    'date': self.scheduled_date,
                    'account_id': line.job_code.id,
                    'unit_amount': line.product_uom_qty,
                    'product_id': line.product_id and line.product_id.id or False,
                    'product_uom_id': line.product_uom and line.product_uom.id or False,
                    'amount': -(line.product_id.standard_price * line.product_uom_qty),
                    'user_id': self._uid,
                    'expense_type': line.product_id.expense_type.id,
                    'branch_id': line.branch_id.id
                }
                self.env['account.analytic.line'].create(res)
        return data


class StockMove(models.Model):
    _inherit = "stock.move"

    @api.model
    def create(self, values):
        if values.get('raw_material_production_id'):
            production = values.get('raw_material_production_id')
            production_id = self.env['mrp.production'].browse(production)
            if production_id.bom_id.closing_date:
                values['date'] = production_id.bom_id.closing_date
            if production_id.picking_type_id:
                values['picking_type_id'] = production_id.picking_type_id.id
        if values.get('production_id'):
            mo = values.get('production_id')
            mo_id = self.env['mrp.production'].browse(mo)
            if mo_id.expected_closing_date:
                values['date'] = mo_id.expected_closing_date
            if mo_id.picking_type_id:
                values['picking_type_id'] = mo_id.picking_type_id.id
        return super(StockMove, self).create(values)

    @api.model
    def get_total_cost(self):
        search_id = self.env['stock.move'].search([])
        for move in search_id:
            move.update({
                'cost': move.product_id.standard_price,
            })
            if move.state != 'done':
                move.update({
                    'total_cost': move.cost * move.product_uom_qty,
                })
            else:
                move.update({
                    'total_cost': move.cost * move.quantity_done,
                })

    @api.depends('purchase_price', 'move_line_ids.qty_done', 'move_line_ids.product_uom_id', 'move_line_nosuggest_ids.qty_done')
    def compute_purchase_cost(self):
        for res in self:
            res.purchase_cost_price = res.purchase_price * res.quantity_done

    expense_type = fields.Many2one(
        'mrp.bom.expense', string='Expense Type'
    )
    job_code = fields.Many2one(
        'account.analytic.account',
        string='Job Code',
        index=True,
    )
    picking_type = fields.Selection(
        related='picking_type_id.code',
        string='Picking Type',
        index=True,
    )
    stock_location_qty = fields.Float(
        string='Stock Quantity',
        index=True,
        readonly=True
    )
    mrp_location_qty = fields.Float(
        string='Manufacturing Quantity',
        index=True,
        readonly=True
    )
    cost = fields.Float(string='Cost')
    total_cost = fields.Float(string='Total Cost')
    purchase_cost_price = fields.Float(string='Cost', compute='compute_purchase_cost', store=True)
    purchase_price = fields.Float(string='Unit Price')

    @api.onchange('product_id', 'product_qty')
    def onchange_quantity(self):
        res = super(StockMove, self).onchange_quantity()
        quant_obj = self.env['stock.quant']
        for data in self:
            data.expense_type = data.product_id.expense_type.id
            if data.picking_type_id.code == 'internal':
                search_id = quant_obj.search([
                    ('product_id', '=', data.product_id.id),
                    ('location_id', '=', data.location_id.id)
                ])
                search_dest_id = quant_obj.search([
                    ('product_id', '=', data.product_id.id),
                    ('location_id', '=', data.location_dest_id.id)
                ])
                done_qty = 0.0
                mrp_done = 0.0
                for qty_ in search_dest_id:
                    mrp_done += qty_.quantity
                    data.mrp_location_qty = mrp_done
                for rec in search_id:
                    done_qty += rec.quantity
                    data.stock_location_qty = done_qty
                    # if done_qty < data.quantity_done:
                    # return {
                    #     'warning': {
                    #         'title': _('Stock Availability Information'),
                    #         'message': _('Available quantity of the product %s is %s') % (
                    #             data.product_id.display_name, done_qty)
                    #     }
                    # }
        return res


class StockLocation(models.Model):
    _inherit = "stock.location"

    manufacturing_location = fields.Boolean(
        string='Is a Manufacturing Location?',
    )


class StockQuant(models.Model):
    _inherit = 'stock.quant'

    cost_value = fields.Float(string='Cost')
    total_cost = fields.Float(string='Total Cost')
    cost_total = fields.Float(string='Cost Total')

    # @api.multi
    # def cost_value_updation(self):
    #     for res in self:
    #         res.cost_value = res.product_id.standard_price

    @api.model
    def get_cost_total(self):
        search_id = self.env['stock.quant'].search([])
        for data in search_id:
            data.cost_value = data.product_id.standard_price
            data.cost_total = data.product_id.standard_price * data.quantity


class InventoryLine(models.Model):
    _inherit = "stock.inventory.line"

    total_theoretical_cost = fields.Float(string='Total Theoretical Cost', compute='total_cost_new', store=True)
    total_real_cost = fields.Float(string='Total Real Cost', compute='total_cost_new', store=True)
    date_line = fields.Datetime(string='Date', related='inventory_id.date', store=True)
    state_line= fields.Selection(string='Status', related='inventory_id.state', store=True)

    @api.constrains('product_id')
    def _check_product_id(self):
        """ As no quants are created for consumable products, it should not be possible do adjust
        their quantity.
        """
        for line in self:
            if line.product_id.type != 'product':
                raise UserError(_("You can only adjust stockable products %s: %s"
                                  %(line.product_id.name, line.product_id)))


    @api.depends('product_id')
    def total_cost_new(self):
        for rec in self:
            rec.total_theoretical_cost = rec.product_id.standard_price * rec.theoretical_qty
            rec.total_real_cost = rec.product_id.standard_price * rec.product_qty