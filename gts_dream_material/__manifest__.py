{
    'name': 'Material Dream Factory',
    'version': '1.0',
    'category': '',
    'sequence': 75,
    'summary': 'Dream Factory ',
    'description': "",
    'website': '',
    'depends': ['crm','analytic','stock','branch','product','base', 'gts_dream_mrp', 'gts_dream_purchase'],
    'data': [
        'data/stock_data.xml',
        'views/stock_view.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
