# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'GTS Dream MRP',
    'summary': 'MRP Bill of Material Changes',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'sequence': 12,
    'website': 'http://www.geotechnosoft.com',
    'category': 'MRP',
    'version': '1.0',
    'depends': [
        'mrp', 'purchase', 'stock', 'branch', 'product', 'account', 'analytic', 'gts_dream_purchase'
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/schedular_data.xml',
        'data/manufacturing_sequence.xml',
        'views/mrp_view.xml',
        'views/bom_view.xml',
        'views/bom_account_view.xml',
        'views/bom_inventory_view.xml',
        'views/account_invoice_view.xml',
    ],
    'installable': True,
}
