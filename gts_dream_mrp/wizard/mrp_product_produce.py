from odoo import api, fields, models, _


class MrpProductProduce(models.TransientModel):
    _inherit = "mrp.product.produce"

    @api.multi
    def do_produce(self):
        res = super(MrpProductProduce, self).do_produce()
        self.production_id.bom_id.write({
            'related_mo': self.production_id.id,
            'mo_created': True,
        })
        return res
