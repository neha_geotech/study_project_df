import json

from odoo import fields, models, api, _
from datetime import datetime,date, timedelta
from odoo.exceptions import Warning, ValidationError
from odoo.tools import float_is_zero, float_compare
from num2words import num2words
from dateutil import relativedelta


class ProductTemplate(models.Model):
    _inherit = "product.template"

    expense_type = fields.Many2one('mrp.bom.expense', string='Expense Type', copy=False)


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    expense_type = fields.Many2one(
        'mrp.bom.expense', string='Expense Type'
    )

    @api.multi
    def action_view_invoice(self):
        result = super(PurchaseOrder, self).action_view_invoice()
        result['context']['default_expense_type'] = self.expense_type.id
        return result

class PurchaseOrderLine(models.Model):
    _inherit = "purchase.order.line"

    expense_type = fields.Many2one('mrp.bom.expense', string='Expense Type')


    @api.onchange('product_id')
    def onchange_product_id(self):
        res = super(PurchaseOrderLine, self).onchange_product_id()
        if self.product_id:
            self.expense_type = self.product_id.expense_type.id


class AccountInvoice(models.Model):
    _inherit = "account.invoice"


    expense_type = fields.Many2one('mrp.bom.expense', 'Expense Type')

    @api.multi
    def action_move_create(self):
        res = super(AccountInvoice, self).action_move_create()
        for invoice in self:
            context = dict(self.env.context)
            context.pop('default_type', None)
            invoice.invoice_line_ids.with_context(context).asset_create()
            for line in invoice.invoice_line_ids:
                if invoice.type in ('out_invoice', 'out_refund'):
                    if invoice.move_id:
                        invoice.move_id.line_ids.update({
                            'expense_type': '4',
                            'analytic_account_id': invoice.job_code.id,
                            'activity_owner': invoice.activity_owner if invoice.activity_owner else False,
                            'inv_reference': invoice.inv_seq,
                            'inv_date': invoice.date_invoice,
                            'event_date': invoice.sale_id.event_date if invoice.sale_id and invoice.sale_id.event_date else False,
                            'event_name': invoice.sale_id.opportunity_id.name if invoice.sale_id and invoice.sale_id.opportunity_id.name else invoice.event_name,
                            'venue': invoice.venue if invoice.sale_id and invoice.venue else invoice.venue,
                            'basic_amount': invoice.amount_untaxed,
                            'gst_amount': invoice.amount_tax
                        })
                        if line.product_id.type == 'service':
                            for move_line_ in invoice.move_id.line_ids:
                                for analytic_ in move_line_.analytic_line_ids:
                                    analytic_.update({
                                        'expense_type': 4,
                                    })
                if invoice.type in ('in_invoice', 'in_refund'):
                    if invoice.move_id:
                        invoice.move_id.line_ids.update({
                            'expense_type': line.expense_type.id,
                            'analytic_account_id': invoice.job_code.id,
                            'inv_reference': invoice.inv_seq,
                            'inv_date': invoice.date_invoice,
                            'basic_amount': invoice.amount_untaxed,
                            'gst_amount': invoice.amount_tax
                        })
                        if line.product_id.type == 'service':
                            for move_line_ in invoice.move_id.line_ids:
                                for analytic_ in move_line_.analytic_line_ids:
                                    analytic_.update({
                                        'expense_type': line.expense_type.id,
                                    })
        return res


    def _prepare_invoice_line_from_po_line(self, line):
        if line.product_id.purchase_method == 'purchase':
            qty = line.product_qty - line.qty_invoiced
        else:
            qty = line.qty_received - line.qty_invoiced
        if float_compare(
                qty, 0.0, precision_rounding=line.product_uom.rounding) <= 0:
            qty = 0.0
        taxes = line.taxes_id
        invoice_line_tax_ids = line.order_id.fiscal_position_id.map_tax(taxes)
        invoice_line = self.env['account.invoice.line']
        data = {
            'purchase_line_id': line.id,
            'name': line.order_id.name + ': ' + line.name,
            'origin': line.order_id.origin,
            'uom_id': line.product_uom.id,
            'product_id': line.product_id.id,
            'account_id':
                invoice_line.with_context({'journal_id': self.journal_id.id, 'type': 'in_invoice'})._default_account(),
            'price_unit': line.order_id.currency_id.with_context(
                date=self.date_invoice
            ).compute(
                line.price_unit,
                self.currency_id,
                round=False
            ),
            'quantity': qty,
            'discount': 0.0,
            # 'account_analytic_id': line.account_analytic_id.id,
            'analytic_tag_ids': line.analytic_tag_ids.ids,
            'invoice_line_tax_ids': invoice_line_tax_ids.ids,
            'expense_type': line.expense_type.id,
            's_no': line.s_no
        }
        if line.product_id.type == 'service':
            data['account_analytic_id'] = line.account_analytic_id.id
        account = invoice_line.get_invoice_line_account(
            'in_invoice',
            line.product_id,
            line.order_id.fiscal_position_id,
            self.env.user.company_id
        )
        if account:
            data['account_id'] = account.id
        return data


class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    expense_type = fields.Many2one('mrp.bom.expense', string='Expense Type')

class HRExpense(models.Model):
    _inherit = 'hr.expense'

    expense_type = fields.Many2one('mrp.bom.expense', string='Expense Type')

    @api.multi
    def _prepare_move_line(self, line):
        '''
        This function prepares move line of account.move related to an expense
        '''
        partner_id = self.employee_id.address_home_id.commercial_partner_id.id
        return {
            'date_maturity': line.get('date_maturity'),
            'partner_id': partner_id,
            'name': line['name'][:64],
            'debit': line['price'] > 0 and line['price'],
            'credit': line['price'] < 0 and - line['price'],
            'account_id': line['account_id'],
            'analytic_line_ids': line.get('analytic_line_ids'),
            'amount_currency': line['price'] > 0 and abs(line.get('amount_currency')) or - abs(
                line.get('amount_currency')),
            'currency_id': line.get('currency_id'),
            'tax_line_id': line.get('tax_line_id'),
            'tax_ids': line.get('tax_ids'),
            'quantity': line.get('quantity', 1.00),
            'product_id': line.get('product_id'),
            'product_uom_id': line.get('uom_id'),
            'analytic_account_id': line.get('analytic_account_id'),
            'payment_id': line.get('payment_id'),
            'expense_id': line.get('expense_id'),
            'branch_id': self.branch_id.id,
            'expense_type': self.expense_type.id,
        }

class AccountAnalyticLine(models.Model):
    _inherit = 'account.analytic.line'

    expense_type = fields.Many2one('mrp.bom.expense', string='Expense Type')

class AccountMove(models.Model):
    _inherit = 'account.move'
    expense_type = fields.Many2one('mrp.bom.expense', string='Expense Type')


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'
    _order = 'delays desc'

    expense_type = fields.Many2one('mrp.bom.expense', string='Expense Type')

    @api.one
    def _prepare_analytic_line(self):
        """ Prepare the values used to create() an account.analytic.line upon validation of an account.move.line having
            an analytic account. This method is intended to be extended in other modules.
        """
        amount = (self.credit or 0.0) - (self.debit or 0.0)
        return {
            'name': self.name,
            'date': self.date,
            'account_id': self.analytic_account_id.id,
            'tag_ids': [(6, 0, self.analytic_tag_ids.ids)],
            'unit_amount': self.quantity,
            'vendor_payout_reco': self.analytic_account_id.vendor_payout,
            'product_id': self.product_id and self.product_id.id or False,
            'product_uom_id': self.product_uom_id and self.product_uom_id.id or False,
            'amount': self.company_currency_id.with_context(date=self.date or fields.Date.context_today(self)).compute(
                amount, self.analytic_account_id.currency_id) if self.analytic_account_id.currency_id else amount,
            'general_account_id': self.account_id.id,
            'ref': self.ref,
            'move_id': self.id,
            'user_id': self.invoice_id.user_id.id or self._uid,
            'expense_type': self.expense_type.id,
            'branch_id': self.branch_id.id
        }
