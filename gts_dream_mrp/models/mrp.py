# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import Warning


class MrpBom(models.Model):
    _inherit = 'mrp.bom'
    _order = 'create_date desc'

    # Function: Schedular Fun to update Closing Date
    @api.multi
    def update_closing_date(self):
        bom_ids = self.env['mrp.bom'].search([('active', '=', True)])
        for bom in bom_ids:
            if bom.job_code and bom.job_code.date_deadline:
                bom.update({
                    'closing_date': bom.job_code.date_deadline,
                })

    @api.multi
    def _get_quotation_ids(self):
        for data in self:
            search_id = self.env['sale.order'].search([
                ('analytic_account_id', '=', data.job_code.id)
            ])
            if search_id:
                data.update({
                    'quotation_ids': search_id,
                })

    @api.multi
    def _get_quotation_value(self):
        for data in self:
            if data.sale_id.state == 'sale':
                quot_value = data.sale_id.amount_untaxed_new
                data.update({
                    'quotation_value': quot_value
                })

    @api.depends('quotation_value', 'total_planned_amount_bom')
    def _get_bom_wrt_quotation(self):
        for data in self:
            if data.sale_id.state == 'sale':
                val = 0.0
                if data.quotation_value != 0.0:
                    val = (data.total_planned_amount_bom / data.quotation_value) * 100
                data.update({
                    'bom_wrt_quotation': val
                })

    @api.model
    def _get_default_branch(self):
        user_obj = self.env['res.users']
        branch_id = user_obj.browse(self.env.user.id).branch_id
        return branch_id

    @api.depends('bom_line_ids')
    def _calc_total(self):
        for data in self:
            total_amount = total_recycle_value = total_return_value = \
                total_planned_amount_bom = total_fund_required = \
                lumpsum_fund_required = total_fund_approval = 0.0
            for lines in data.bom_line_ids:
                total_amount += lines.amount
                total_recycle_value += lines.recycle_value
                total_return_value += lines.return_value
                total_planned_amount_bom += lines.planned_amount_bom
                total_fund_required += lines.fund_required
                total_fund_approval += lines.fund_approval
                lumpsum_fund_required += lines.fund_required
                data.update({
                    'total_amount': total_amount,
                    'total_recycle_value': total_recycle_value,
                    'total_return_value': total_return_value,
                    'total_planned_amount_bom': total_planned_amount_bom,
                    'total_fund_required': total_fund_required,
                    'total_fund_approval': total_fund_approval,
                    'lumpsum_fund_required': lumpsum_fund_required
                })

    job_code = fields.Many2one(
        'account.analytic.account',
        string='Job Code',
        index=True,
        track_visibility='onchange',
        copy=False
    )
    branch_id = fields.Many2one(
        'res.branch',
        string='Branch',
        track_visibility='onchange',
        default=_get_default_branch
    )
    related_mo = fields.Many2one('mrp.production', string='Related MO', track_visibility='onchange', copy=False)
    mo_created = fields.Boolean(string='MO Created', default=False, copy=False, track_visibility='onchange')
    currency_id = fields.Many2one("res.currency", string="Currency")
    bom_total_ids = fields.One2many(
        'mrp.bom.total',
        'bom_total_id',
        'BoM Total Lines',
        copy=False,
        readonly=True,
        track_visibility='onchange'
    )
    total_amount = fields.Monetary(string='Amount', compute='_calc_total', track_visibility='onchange', store=True)
    total_recycle_value = fields.Monetary(string='Recycle Value', compute='_calc_total', track_visibility='onchange',store=True)
    total_return_value = fields.Monetary(string='Return Value', compute='_calc_total', track_visibility='onchange', store=True)
    total_planned_amount_bom = fields.Monetary(string='Planned Amount', compute='_calc_total', track_visibility='onchange', store=True)
    total_fund_required = fields.Monetary(string='Fund Required', compute='_calc_total', track_visibility='onchange', store=True)
    total_fund_approval = fields.Monetary(string='Fund Approved', compute='_calc_total', track_visibility='onchange', store=True)
    lumpsum_fund_required = fields.Monetary(string='Fund Required', compute='_calc_total', track_visibility='onchange', store=True)
    lumpsum_fund_approval = fields.Float(string='Fund Approved', track_visibility='onchange')
    bom_line_ids = fields.One2many('mrp.bom.line', 'bom_id', 'BoM Lines', copy=True, track_visibility='onchange')
    mode_of_approval = fields.Selection([
        ('cheque_adv', 'Chq. Advance'),
        ('cheque_old_payment', 'Chq. Old Pymt'),
        ('cheque_full_payment', 'Chq. Full Pymt'),
        ('cheque_pdc', 'Chq. PDC'),
        ('credit', 'Credit'),
        ('branch_transfer', 'Chq. Pymt-Local'),
        ('cash_payment_direct', 'Cash Pymt-Direct'),
        ('transfer_adv', 'NEFT Advance Pymt'),
        ('transfer_full', 'NEFT Full Pymt'),
        ('transfer_old', 'NEFT Old Pymt'),
        ('paytm_imps_direct', 'Paytm IMPS-Direct'),
    ], string='Mode of Approval', track_visibility='onchange')
    type = fields.Selection([
        ('normal', 'Manufacture This Product'),
        ('phantom', 'Kit')], 'BoM Type',
        default='normal', required=True, track_visibility='onchange')
    description = fields.Text('Description', track_visibility='onchange')
    quotation_ids = fields.Many2many(
        'sale.order',
        'rel_mrp_sale',
        'mrp_bom_id',
        'sale_id',
        'Quotations',
        copy=False,
        compute='_get_quotation_ids',
        track_visibility='onchange'
    )
    quotation_value = fields.Float(
        'SO Value',
        compute='_get_quotation_value',
        track_visibility='onchange'
    )
    bom_wrt_quotation = fields.Float('% BOM WRT Quotation', compute='_get_bom_wrt_quotation', track_visibility='onchange')
    advance_expected = fields.Float('Advance Expected', track_visibility='onchange')
    advance_received = fields.Float('Advance Received', track_visibility='onchange')
    advance_type = fields.Selection([
        ('new_payment', 'New Payment'),
        ('old_payment', 'Old Payment')
    ], string='Advance Payment', track_visibility='onchange')
    client_name = fields.Many2one('res.partner', 'Customer', readonly=True, track_visibility='onchange')
    event_date = fields.Date(string='Event Date', track_visibility='onchange')
    closing_date = fields.Date(
        compute='get_date_deadline',
        string='Expected Closing Date',
        track_visibility='onchange',
        store=True
    )
    venue = fields.Text('Venue', track_visibility='onchange')
    opportunity_id = fields.Many2one(
        'crm.lead', string='Opportunity'
    )
    sale_id = fields.Many2one(
        'sale.order', 'Quotation',
        readonly=True, copy=False,
        track_visibility='onchange'
    )
    project_managers = fields.Many2one('res.users', string='Project Managers', track_visibility='onchange')
    production_order_created = fields.Boolean(string='Production Created', default=False, copy=False, track_visibility='onchange')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('bom_confirm', 'Confirm'),
        ('bom_close', 'Close'),
        ('cancel', 'Cancelled'),
    ], string='Status', readonly=True, copy=False,
        track_visibility='onchange', default='draft', compute='compute_mo_state', store=True)
    create_date = fields.Datetime('Create Date', default=fields.datetime.now())
    mrp_bom_ref = fields.Char('Attachment Link')

    # function: to update BOM state as per MO state
    @api.multi
    @api.depends('related_mo.state')
    def compute_mo_state(self):
        for line in self:
            if line.related_mo:
                if line.related_mo.state == 'done':
                    line.state = 'bom_close'

    @api.multi
    @api.depends('job_code','job_code.date_deadline')
    def get_date_deadline(self):
        for bom in self:
            if bom.job_code and bom.job_code.date_deadline:
                bom.closing_date = bom.job_code.date_deadline

    @api.onchange('job_code')
    def get_opportunity(self):
        for bom in self:
            if bom.job_code and bom.job_code.opportunity_id:
                bom.opportunity_id = bom.job_code.opportunity_id

    @api.multi
    def action_draft(self):
        # orders = self.filtered(lambda s: s.state in ['cancel', 'sent'])
        return self.write({
            'state': 'draft',
        })

    @api.multi
    def action_cancel(self):
        return self.write({'state': 'cancel'})

    @api.multi
    def action_confirm(self):
        # for order in self.filtered(lambda order: order.partner_id not in order.message_partner_ids):
        #     order.message_subscribe([order.partner_id.id])
        self.write({
            'state': 'bom_confirm',
            # 'confirmation_date': fields.Datetime.now()
        })

        return True

    @api.multi
    def button_create_production(self):
        view_ref = self.env[
            'ir.model.data'
        ].get_object_reference(
            'mrp', 'mrp_production_form_view'
        )
        view_id = view_ref[1] if view_ref else False
        if view_id:
            rec = {
                'type': 'ir.actions.act_window',
                'res_model': 'mrp.production',
                'view_type': 'form',
                'view_mode': 'form',
                'view_id': view_id,
                'target': 'current',
                'context': {
                    'default_product_id': self.product_id.id,
                    'default_client_name': self.client_name.id,
                    'default_venue': self.venue,
                    'default_event_date': self.event_date,
                    'default_project_managers': self.project_managers.id,
                    'default_bom_id': self.id
                }
            }
            return rec

    @api.model
    def create(self, values):
        res = super(MrpBom, self).create(values)
        expense_ids = set(res.bom_line_ids.mapped('expense_type').ids)
        for _id in expense_ids:
            recycle_value = amount = return_value = planned_amount_bom = \
                fund_required = fund_approval = 0.0
            for data in res.bom_line_ids:
                if _id == data.expense_type.id:
                    recycle_value += data.recycle_value
                    amount += data.amount
                    return_value += data.return_value
                    planned_amount_bom += data.planned_amount_bom
                    fund_required += data.fund_required
                    fund_approval += data.fund_approval
            vals = {
                'bom_total_id': res.id,
                'recycle_value': recycle_value,
                'expense_type': _id,
                'return_value': return_value,
                'planned_amount_bom': planned_amount_bom,
                'fund_required': fund_required,
                'fund_approval': fund_approval,
                'amount': amount
            }
            self.env['mrp.bom.total'].create(vals)
        active_ids = self.env.context.get('active_ids')
        if active_ids:
            # crm_id = self.env['crm.lead'].browse(active_ids)
            # crm_id.write({
            #     'related_bom': res.id,
            #     'bom_created': True
            # })
            sale_id = self.env['sale.order'].browse(active_ids)
            sale_id.write({
                'bom_id': res.id,
                'bom_created': True
            })
        return res

    @api.multi
    def write(self, values):
        res = super(MrpBom, self).write(values)
        expense_ids = set(self.bom_line_ids.mapped('expense_type').ids)
        for _id in expense_ids:
            recycle_value = amount = return_value = planned_amount_bom = \
                fund_required = fund_approval = 0.0
            for data in self.bom_line_ids:
                if _id == data.expense_type.id:
                    recycle_value += data.recycle_value
                    amount += data.amount
                    return_value += data.return_value
                    planned_amount_bom += data.planned_amount_bom
                    fund_required += data.fund_required
                    fund_approval += data.fund_approval
            vals = {
                'recycle_value': recycle_value,
                'expense_type': _id,
                'return_value': return_value,
                'planned_amount_bom': planned_amount_bom,
                'fund_required': fund_required,
                'fund_approval': fund_approval,
                'amount': amount
            }
            search_id = self.bom_total_ids.search([
                ('expense_type', '=', _id),
                ('bom_total_id', '=', self.id)
            ])
            search_id.write(vals)
            if not search_id:
                vals['bom_total_id'] = self.id
                self.env['mrp.bom.total'].create(vals)

        return res


class MrpBomTotal(models.Model):
    _name = 'mrp.bom.total'

    expense_type = fields.Many2one('mrp.bom.expense', string='Expense Type', copy=False)
    amount = fields.Float(string='Average Cost', copy=False)
    recycle_value = fields.Float(string='Recycle Value', copy=False)
    return_value = fields.Float(string='Return Value', copy=False)
    planned_amount_bom = fields.Float(string='Planned Amount', copy=False, )
    fund_required = fields.Float(string='Fund Required', copy=False)
    fund_approval = fields.Float(string='Fund Approved', copy=False)
    bom_total_id = fields.Many2one(
        'mrp.bom', 'Parent BoM',
        index=True, required=True)


class MrpBomLine(models.Model):
    _inherit = 'mrp.bom.line'

    @api.onchange('product_id')
    def branch_onchange(self):
        result = {}
        prod_list = []
        product_list = []
        product_ids = self.env['product.product'].search([])
        products = self.env['product.product'].search([('event_product', '=', True)])
        for rec in self:
            if rec.bom_id.branch_id.name == 'DF-Retail':
                for prod in product_ids:
                    prod_list.append(prod.id)
                result['domain'] = {'product_id': [
                    ('id', 'in', prod_list)
                ]}
                # result.update({'domain': {'product_id': [('product_tmpl_id', 'in', prod_list)]}})
            else:
                for prod1 in products:
                    product_list.append(prod1.id)
                result['domain'] = {'product_id': [
                    ('id', 'in', product_list)
                ]}
            #     # result.update({'domain': {'product_id': [('product_tmpl_id', 'in', product_list)]}})
        return result

    @api.depends('rate', 'product_qty')
    def _compute_amount(self):
        for rec in self:
            rec.amount = rec.rate * rec.product_qty
            rec.planned_amount_bom = rec.rate * rec.product_qty

    @api.depends('product_id', 'product_id.expense_type')
    def _compute_expense_type(self):
        for rec in self:
            rec.expense_type = rec.product_id.expense_type.id

    @api.depends('product_id', 'product_id.standard_price')
    def _compute_rate_base(self):
        for rec in self:
            rec.rate_base = rec.product_id.standard_price

    @api.multi
    def _compute_availabe_qty(self):
        for res in self:
            search_id = self.env['stock.warehouse'].search([
                ('branch_id', '=', res.bom_id.branch_id.id)
            ])
            if search_id and res.bom_id.branch_id.id in (6,7,8):
                picking_search_id = self.env['stock.picking.type'].search([
                    ('warehouse_id', '=', search_id.id),
                    ('code', '=', 'internal')
                ])
                location_search_id = self.env['stock.location'].search([
                    ('id', '=', picking_search_id.default_location_src_id.id)
                ])
                for data in location_search_id.quant_ids:
                    if data.product_id == res.product_id:
                        res.update({'available_qty': data.quantity})
            else:
                picking_search_id = self.env['stock.picking.type'].search([
                    ('warehouse_id', '=', 1),
                    ('code', '=', 'internal')
                ])
                location_search_id = self.env['stock.location'].search([
                    ('id', '=', picking_search_id.default_location_src_id.id)
                ])
                for data in location_search_id.quant_ids:
                    if data.product_id == res.product_id:
                        res.update({'available_qty': data.quantity})

    @api.depends('ecs_quantity')
    def _compute_ecs_value(self):
        for rec in self:
            rec.ecs_value = rec.rate_base * rec.ecs_quantity

    bom_id = fields.Many2one(
        'mrp.bom', 'Parent BoM',
        index=True, ondelete='cascade', required=True, track_visibility='onchange')
    length_ = fields.Char(string='L')
    width = fields.Char(string='W')
    height = fields.Char(string='H')
    size = fields.Char(string='Size')
    rate = fields.Float(string='Rate')
    rate_base = fields.Float(
        string='Base Rate',
        compute='_compute_rate_base', store=True
    )
    available_qty = fields.Float(string='Available QTY', compute='_compute_availabe_qty')
    amount = fields.Float(string='Average Cost', compute='_compute_amount', store=True)
    expense_type = fields.Many2one(
        'mrp.bom.expense', string='Expense Type'
    )
    planned_amount_bom = fields.Float(string='Planned Amount')
    nature_of_consumption = fields.Selection([
        ('purchase_n_consume', 'Purchase & Consume'),
        ('purchase_n_return', 'Purchase & Return'),
        ('purchase_n_scrap', 'Purchase & Scrap'),
        ('stock_n_consume', 'Stock & Consume'),
        ('stock_n_return', 'Stock & Return'),
        ('stock_n_recycle', 'Recycle & Return'),
        ('stock_n_scrap', 'Stock & Scrap'),
        ('recycle_n_consume', 'Recycle & Consume'),
        ('recycle_n_scrap', 'Recycle & Scrap'),
    ], string='Nature of Consumption')
    recycle_value = fields.Float(string='Recycle Value')
    return_value = fields.Float(string='Return Value')
    fund_required = fields.Float(string='Fund Required')
    fund_approval = fields.Float(string='Fund Approval')
    vendor = fields.Many2one('res.partner', string='Vendor')
    return_quantity = fields.Float(string='Return Quantity')
    mode_of_approval = fields.Selection([
        ('cheque_adv', 'Chq. Advance'),
        ('cheque_old_payment', 'Chq. Old Pymt'),
        ('cheque_full_payment', 'Chq. Full Pymt'),
        ('cheque_pdc', 'Chq. PDC'),
        ('credit', 'Credit'),
        ('branch_transfer', 'Chq. Pymt-Local'),
        ('cash_payment_direct', 'Cash Pymt-Direct'),
        ('transfer_adv', 'NEFT Advance Pymt'),
        ('transfer_full', 'NEFT Full Pymt'),
        ('transfer_old', 'NEFT Old Pymt'),
        ('paytm_imps_direct', 'Paytm IMPS-Direct'),
    ], string='Mode of Approval')
    product_description = fields.Text('Description')
    days = fields.Text('Days')
    move_id = fields.Many2one('stock.move', 'Stock Move')
    ecs_quantity = fields.Float('ECS Qty')
    ecs_value = fields.Float('ECS Value', compute='_compute_ecs_value', store=True)
    related_mo_id = fields.Many2one('mrp.production', related="bom_id.related_mo", store=True, string='Related Mo')


    @api.multi
    def get_return_value(self):
        """
        Method to create internal transfer from manufacturing location to stock location
        """
        location_obj = self.env['stock.location']
        for data in self:
            warehouse_id = self.env['stock.warehouse'].search([
                ('branch_id', '=', data.bom_id.branch_id.id),
            ])
            picking_type = self.env['stock.picking.type'].search([
                ('warehouse_id', '=', warehouse_id.id),
                ('code', '=', 'internal'),
            ])
            source_location_id = location_obj.search([
                ('branch_id', '=', data.bom_id.branch_id.id),
                ('usage', '=', 'internal'),
                ('manufacturing_location', '=', True)
            ])
            dest_location_id = location_obj.search([
                ('branch_id', '=', data.bom_id.branch_id.id),
                ('usage', '=', 'internal'),
                ('manufacturing_location', '=', False)
            ])
            res = {
                'origin': 'Return' + ': ' + str(data.product_id.name),
                'job_code': data.bom_id.job_code.id,
                'branch_id': data.bom_id.branch_id.id,
                'name': 'Return- ' + str(data.product_id.name) + '(' + str(data.bom_id.product_tmpl_id.name) + ')'
            }
            if data.move_id.branch_id.id in (6, 7, 8):
                res['picking_type_id'] = 4
                res['location_id'] = 95
                res['location_dest_id'] = 16
            else:
                res['picking_type_id'] = picking_type.id
                res['location_id'] = source_location_id.id
                res['location_dest_id'] = dest_location_id.id
            if data.return_quantity > 0.0:
                trans_id = self.env['stock.picking'].create(res)
                dict_ = {
                    'product_id': data.product_id.id,
                    'branch_id': data.bom_id.branch_id.id,
                    'job_code': data.bom_id.job_code.id,
                    'expense_type': data.product_id.expense_type.id,
                    'quantity_done': data.return_quantity,
                    'product_uom': data.product_uom_id.id,
                    'picking_id': trans_id.id,
                    'name': 'Return- ' + str(data.product_id.name) + '(' + str(data.bom_id.product_tmpl_id.name) + ')',
                }
                if data.move_id.branch_id.id in (6, 7, 8):
                    dict_['location_id'] = 95
                    dict_['location_dest_id'] = 16
                else:
                    dict_['location_id'] = source_location_id.id
                    dict_['location_dest_id'] = dest_location_id.id
                move_id = self.env['stock.move'].create(dict_)
                if trans_id:
                    trans_id.button_validate()
                if move_id:
                    data.write({
                        'return_value': data.return_quantity * data.rate_base,
                        'move_id': move_id.id
                    })
            else:
                raise Warning(_('Please Enter Return quantity to proceed!'))

    @api.onchange('return_quantity', 'rate_base')
    def onchange_return_qty(self):
        print('=====', self.rate_base)
        print('=====', self.return_quantity)
        if self.return_quantity and self.rate_base:
            self.return_value = self.return_quantity * self.rate_base


    @api.onchange('nature_of_consumption')
    def onchange_nature_of_consumption(self):
        if self.nature_of_consumption in (
                'recycle_n_consume',
                'recycle_n_recycle',
                'recycle_n_scrap'
        ):
            self.recycle_value = self.amount
            self.planned_amount_bom = 0.0
        else:
            self.recycle_value = 0.0
            self.planned_amount_bom = self.amount

    @api.onchange('product_id')
    def onchange_product_id(self):
        """
        we have used this method of standard to add the functionality related
        with product variants and and other fields related with
        the product dimensions
        """
        data_dict = {}
        if self.product_id:
            self.product_uom_id = self.product_id.uom_id.id
            self.length_ = self.product_id.display_length1
            self.width = self.product_id.display_width1
            self.height = self.product_id.display_height1
            self.rate = self.product_id.standard_price
            self.expense_type = self.product_id.expense_type.id
        if self.product_id.attribute_value_ids:
            for data in self.product_id.attribute_value_ids:
                key = data.attribute_id.name
                value = data.name
                data_dict.setdefault(key, []).append(value)
            if 'Size' in data_dict:
                self.size = data_dict['Size'][0]
            if 'size' in data_dict:
                self.size = data_dict['size'][0]
            if 'SIZE' in data_dict:
                self.size = data_dict['SIZE'][0]
            if not ('Size', 'size', 'SIZE') in data_dict:
                pass


class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.depends('rate', 'product_uom_qty')
    def _compute_amount(self):
        for rec in self:
            rec.amount = rec.rate * rec.product_uom_qty

    length_ = fields.Char(string='L')
    width = fields.Char(string='W')
    height = fields.Char(string='H')
    size = fields.Char(string='Size')
    rate = fields.Float(string='Rate')
    amount = fields.Float(string='Average Cost', compute='_compute_amount')
    expense_type = fields.Many2one('mrp.bom.expense', string='Expense Type')
    planned_amount_bom = fields.Float(string='Planned Amount')
    nature_of_consumption = fields.Selection([
        ('purchase_n_consume', 'Purchase & Consume'),
        ('purchase_n_return', 'Purchase & Return'),
        ('purchase_n_recycle', 'Purchase & Recycle'),
        ('purchase_n_scrap', 'Purchase & Scrap'),
        ('stock_n_consume', 'Stock & Consume'),
        ('stock_n_return', 'Stock & Return'),
        ('stock_n_recycle', 'Stock & Recycle'),
        ('stock_n_scrap', 'Stock & Scrap'),
        ('recycle_n_consume', 'Recycle & Consume'),
        ('recycle_n_recycle', 'Recycle & Recycle'),
        ('recycle_n_scrap', 'Recycle & Scrap'),
    ], string='Nature of Consumption')
    recycle_value = fields.Float(string='Recycle Value')
    return_value = fields.Float(string='Return Value')
    fund_required = fields.Float(string='Fund Required')
    fund_approval = fields.Float(string='Fund Approval')
    vendor = fields.Many2one('res.partner', string='Vendor')
    return_quantity = fields.Char(string='Return Quantity')
    mode_of_approval = fields.Selection([
        ('cheque_adv', 'Chq. Advance'),
        ('cheque_old_payment', 'Chq. Old Pymt'),
        ('cheque_full_payment', 'Chq. Full Pymt'),
        ('cheque_pdc', 'Chq. PDC'),
        ('credit', 'Credit'),
        ('branch_transfer', 'Chq. Pymt-Local'),
        ('cash_payment_direct', 'Cash Pymt-Direct'),
        ('transfer_adv', 'NEFT Advance Pymt'),
        ('transfer_full', 'NEFT Full Pymt'),
        ('transfer_old', 'NEFT Old Pymt'),
        ('paytm_imps_direct', 'Paytm IMPS-Direct'),
    ], string='Mode of Approval')


class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    @api.multi
    def _compute_amount_total(self):
        for data in self:
            for rec in data.move_raw_ids:
                data.amount_total += rec.amount

    job_code = fields.Many2one('account.analytic.account', string='Job Code')
    amount_total = fields.Float(string='Amount Total', compute='_compute_amount_total')
    branch_id = fields.Many2one('res.branch', string='Branch')
    related_picking = fields.Many2one('stock.picking', string='Related Picking', copy=False)
    branch_code_ = fields.Char(
        string='Branch Code',
        related='branch_id.branch_code'
    )
    service_line = fields.One2many(
        'service.line',
        'service_product_id',
        string='Service Lines', copy=False
    )
    client_name = fields.Many2one('res.partner', 'Customer', readonly=True)
    event_date = fields.Date(string='Event Date')
    expected_closing_date = fields.Date(
        compute='get_date_deadline',
        string='Expected Closing Date',
        store=True
    )
    venue = fields.Text('Venue')
    delay = fields.Char(string='Delay', compute='_compute_number_of_delays_days')
    date_planned_finished = fields.Datetime(
        'Deadline End', copy=False,
        index=True, default='',
        states={'confirmed': [('readonly', False)]})
    project_managers = fields.Many2one('res.users', string='Project Managers')
    product_uom_id = fields.Many2one(
        'product.uom', 'Product Unit of Measure',
         required=False,
        states={'confirmed': [('readonly', False)]})
    process_stage = fields.Selection([
        ('po_from_client', 'PO from Client'),
        ('bom', 'BOM'),
        ('procurement', 'Procurement'),
        ('grn', 'GRN'),
        ('issue_of_material', 'Issue of Material'),
        ('machining', 'Machining'),
        ('carpentry', 'Carpentry'),
        ('metal', 'Metal'),
        ('paint_and_polish', 'Paint and Polish'),
        ('assembly', 'Assembly'),
        ('qc_1_factory', 'QC 1-Factory'),
        ('packing', 'Packing'),
        ('dispatch', 'Dispatch'),
        ('site_installation', 'Site Installation'),
        ('qc_1_onsite', 'QC 1-Onsite'),
        ('rectification_1', 'Rectification 1'),
        ('qc_2', 'QC 2'),
        ('rectification_2', 'Rectification 2'),
        ('qc3', 'QC 3'),
        ('final_ho', 'Final HO'),
        ('billing_fixtures', 'Billing Fixtures'),
        ('final_billing', 'Final Billing'),
        ('bill_received_by_client', 'Bill Received by Client'),
        ('bill_reconciled', 'Bill Reconciled'),
    ], string='Process Stage')

    @api.multi
    def get_date_deadline(self):
        for prod in self:
            if prod.job_code and prod.job_code.date_deadline:
                prod.expected_closing_date = prod.job_code.date_deadline

    @api.depends('date_planned_finished', 'date_planned_start')
    def _compute_number_of_delays_days(self):
        date_format = '%Y-%m-%d %H:%M:%S'
        for res in self:
            if res.branch_id.id == 7:
                if res.date_planned_finished and res.date_planned_start:
                    a = datetime.strptime(res.date_planned_finished, date_format)
                    b = datetime.strptime(res.date_planned_start, date_format)
                    c = a - b
                    res.delay = c.days

    @api.multi
    def action_assign(self):
        for production in self:
            if not production.related_picking and production.branch_id.id not in (1, 7):
                print('erererer', production)
                ser_warehouse_id = self.env['stock.warehouse'].search(
                    [('branch_id', '=', production.branch_id.id)])
                ser_picking_type = self.env['stock.picking.type'].search(
                    [('warehouse_id', '=', ser_warehouse_id.id),
                     ('code', '=', 'internal')])
                rec = {
                    'job_code': production.job_code.id,
                    'company_id': production.company_id.id,
                    'branch_id': production.branch_id.id,
                    # 'location_id': production.location_dest_id.id,
                    # 'location_dest_id': production.location_src_id.id,
                    'name': production.name
                }
                if production.branch_id.id in (6, 7, 8):
                    rec['picking_type_id'] = 4
                    rec['location_id'] = 16
                    rec['location_dest_id'] = 95
                else:
                    rec['picking_type_id'] = ser_picking_type.id
                    rec['location_id'] = production.location_dest_id.id
                    rec['location_dest_id'] = production.location_src_id.id

                picking_create_id_ = self.env['stock.picking'].create(rec)
                print(picking_create_id_)
                for line in production.move_raw_ids:
                    if not line.product_uom_qty:
                        bom_line_ids = self.env['mrp.bom.line'].search([('product_id', '=', line.product_id.id),
                                                                        ('bom_id', '=', production.bom_id.id)])
                        if len(bom_line_ids) > 1:
                            line.product_uom_qty = 0
                            for bom_line_id in bom_line_ids:
                                line.product_uom_qty = line.product_uom_qty + bom_line_id.product_qty
                        else:
                            line.product_uom_qty = bom_line_ids.product_qty
                    dict_ = {
                        'product_id': line.product_id.id,
                        'branch_id': production.branch_id.id,
                        'job_code': production.job_code.id,
                        # 'product_uom_qty': line.product_uom_qty,
                        'product_uom': line.product_uom.id,
                        'picking_id': picking_create_id_.id,
                        'name': line.name,
                        # 'location_id': 16,
                        # 'location_dest_id': 95,
                        'quantity_done': line.product_uom_qty,
                    }
                    if production.branch_id.id in (6, 7, 8):
                        dict_['location_id'] = 16
                        dict_['location_dest_id'] = 95
                    else:
                        dict_['location_id'] = production.location_dest_id.id
                        dict_['location_dest_id'] = production.location_src_id.id
                    production.move_raw_ids.create(dict_)
                if picking_create_id_:
                    production.related_picking = picking_create_id_.id
                    # production.write({
                    #     'related_picking': picking_create_id_.id,
                    # })
                    picking_create_id_.button_validate()
        res = super(MrpProduction, self).action_assign()
        return res

    @api.onchange('product_id', 'picking_type_id', 'company_id')
    def onchange_product_id(self):
        res = super(MrpProduction, self).onchange_product_id()
        for production in self:
            production.job_code = production.bom_id.job_code.id
            production.branch_id = production.bom_id.branch_id.id
            ser_warehouse_id = self.env['stock.warehouse'].search(
                [('branch_id', '=', production.branch_id.id)])
            if production.branch_id.id in (6, 7, 8):
                ser_picking_type = self.env['stock.picking.type'].search([
                    ('code', '=', 'mrp_operation'),
                    ('warehouse_id.company_id', 'in', [self.env.context.get(
                         'company_id', self.env.user.company_id.id
                     ), False])],
                    limit=1)
            if production.branch_id.id not in (6, 7, 8):
                ser_picking_type = self.env['stock.picking.type'].search([
                    ('code', '=', 'mrp_operation'),
                    ('warehouse_id', '=', ser_warehouse_id.id),
                    ('warehouse_id.company_id', 'in', [self.env.context.get(
                        'company_id', self.env.user.company_id.id
                    ), False])],
                    limit=1)
            production.update({
                'picking_type_id': ser_picking_type.id,
                'location_src_id': ser_picking_type.default_location_src_id.id,
                'location_dest_id': ser_picking_type.default_location_dest_id.id
            })
        return res

    def _generate_raw_move(self, bom_line, line_data):
        print('bom_line===', bom_line)
        print('line_data===', line_data)
        quantity = line_data['qty']
        # alt_op needed for the case when you explode phantom bom and all the lines will be consumed in the operation given by the parent bom line
        alt_op = line_data['parent_line'] and line_data['parent_line'].operation_id.id or False
        if bom_line.child_bom_id and bom_line.child_bom_id.type == 'phantom':
            return self.env['stock.move']
        if bom_line.product_id.type not in ['product', 'consu']:
            return self.env['stock.move']
        if self.routing_id:
            routing = self.routing_id
        else:
            routing = self.bom_id.routing_id
        if routing and routing.location_id:
            source_location = routing.location_id
        else:
            source_location = self.location_src_id
        original_quantity = self.product_qty - self.qty_produced
        data = {
            'sequence': bom_line.sequence,
            'name': self.name,
            'date': self.date_planned_start,
            'picking_type_id': self.picking_type_id.id,
            'date_expected': self.date_planned_start,
            'bom_line_id': bom_line.id,
            'product_id': bom_line.product_id.id,
            'product_uom_qty': bom_line.product_qty or bom_line.ecs_quantity,
            'product_uom': bom_line.product_uom_id.id,
            'location_id': source_location.id,
            'location_dest_id': self.product_id.property_stock_production.id,
            'raw_material_production_id': self.id,
            'company_id': self.company_id.id,
            'operation_id': bom_line.operation_id.id or alt_op,
            'price_unit': bom_line.product_id.standard_price,
            'procure_method': 'make_to_stock',
            'origin': self.name,
            'warehouse_id': source_location.get_warehouse().id,
            'group_id': self.procurement_group_id.id,
            'propagate': self.propagate,
            'unit_factor': quantity / original_quantity,
            'rate': bom_line.rate,
            'length_': bom_line.length_,
            'width': bom_line.width,
            'height': bom_line.height,
            'size': bom_line.size,
            'expense_type': bom_line.expense_type.id,
            'planned_amount_bom': bom_line.planned_amount_bom,
            'nature_of_consumption': bom_line.nature_of_consumption,
            'recycle_value': bom_line.recycle_value,
            'return_value': bom_line.return_value,
            'job_code': self.job_code.id,
            'fund_approval': bom_line.fund_approval,
            'mode_of_approval': bom_line.mode_of_approval,
            'fund_required': bom_line.fund_required,
            'vendor': bom_line.vendor.id
        }
        print('move create',data)
        return self.env['stock.move'].create(data)

    @api.multi
    def _workorders_create(self, bom, bom_data):
        workorders = super(
            MrpProduction, self
        )._workorders_create(bom, bom_data)
        for rec in workorders:
            rec.update({
                'job_code': self.job_code.id,
                'branch_id': self.branch_id.id,
            })
        return workorders

    @api.model
    def create(self, values):
        """
        Create sequence for mrp.production
        """
        seq_obj = self.env['ir.sequence']
        branch_obj = self.env['res.branch']
        # res = super(MrpProduction, self).create(values)
        if 'branch_id' in values:
            branch_id_ = branch_obj.search([
                ('id', '=', values['branch_id'])
            ])
            seq_code = 'mrp.production.' + branch_id_.branch_code
            if 'date_planned_start' in values and branch_id_.branch_code:
                date_ = datetime.strptime(
                    values['date_planned_start'],
                    "%Y-%m-%d %H:%M:%S"
                )
                month_ = (date_.strftime("%b")).upper()
                year_ = date_.strftime("%y")
                values['name'] = 'MO' + '/' + str(
                    branch_id_.branch_code
                ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                    seq_code
                )
            if 'date_planned_start' not in values:
                if branch_id_.branch_code:
                    date_ = datetime.now()
                    month_ = (date_.strftime("%b")).upper()
                    year_ = date_.strftime("%y")
                    values['name'] = 'MO' + '/' + str(
                        branch_id_.branch_code
                    ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                        seq_code
                    )
            if not branch_id_.branch_code:
                raise Warning(_(
                    'Branch code is not defined for branch %s'
                ) % branch_id_.name)
            res = super(MrpProduction, self).create(values)
            print('asdasd',self._context.get('active_ids'))
            active_ids = self.env.context.get('active_ids')
            print(active_ids)
            if active_ids:
                mrp_bom_id = self.env['mrp.bom'].browse(active_ids)
                mrp_bom_id.write({
                    'related_mo': res.id,
                    'mo_created': True,
                    'production_order_created': True
                })
                print(mrp_bom_id)
            for rec in res.bom_id.bom_line_ids:
                if rec.product_id.type == 'service':
                    dict_ = {
                        'product_id': rec.product_id.id,
                        'service_product_id': res.id,
                        'product_uom': rec.product_uom_id.id,
                        'length_': rec.length_,
                        'width': rec.width,
                        'height': rec.height,
                        'size': rec.size,
                        'product_uom_qty': rec.product_qty,
                        'rate': rec.rate,
                        'expense_type': rec.expense_type.id,
                        'nature_of_consumption': rec.nature_of_consumption,
                        'return_quantity': rec.return_quantity
                    }
                    self.env['service.line'].create(dict_)
        return res


class MrpWorkorder(models.Model):
    _inherit = 'mrp.workorder'

    job_code = fields.Many2one(
        'account.analytic.account',
        string='Job Code',
        index=True,
    )
    work_seq = fields.Char(
        'Workorder Reference',
        required=True,
        index=True,
        copy=False,
        default='New'
    )
    branch_id = fields.Many2one('res.branch', string='Branch')

    @api.model
    def create(self, values):
        """
        Create sequence for mrp.workorder
        """
        seq_obj = self.env['ir.sequence']
        prod_obj = self.env['mrp.production']
        date_ = datetime.now()
        month_ = (date_.strftime("%b")).upper()
        year_ = date_.strftime("%y")
        if 'production_id' in values:
            data_ = prod_obj.search([
                ('id', '=', values['production_id'])
            ])
            if data_.branch_id.branch_code:
                values['work_seq'] = 'WO' + '/' + str(
                    data_.branch_id.branch_code
                ) + '/' + month_ + year_ + '/' + seq_obj.next_by_code(
                    'mrp.workorder'
                )
        return super(MrpWorkorder, self).create(values)


class MrpBomExpense(models.Model):
    _name = 'mrp.bom.expense'

    name = fields.Char(string='Name')
    code = fields.Char(string='Code')
    type = fields.Selection(string="Type", selection=[
        ('Material_Expense', 'Material Expense'),
        ('Non_Material_Expense', 'Non-Material Expense'),
        ('Skilled_Expense', 'Skilled Expense'),
        ('Income', 'Income'),
        ('Admin_Expense', 'Admin Expense'),
        ('Not_Applicable', 'Not Applicable'),
    ], required=False, )


class ServiceLine(models.Model):
    _name = 'service.line'

    service_product_id = fields.Many2one('mrp.production', string='Order Reference')
    product_id = fields.Many2one('product.product',string='Product')
    product_uom = fields.Many2one('product.uom',string='Unit of Measure')
    length_ = fields.Char(string='L')
    width = fields.Char(string='W')
    height = fields.Char(string='H')
    size = fields.Char(string='Size')
    product_uom_qty = fields.Float(string='To Consume')
    rate = fields.Float(string='Rate')
    expense_type = fields.Many2one('mrp.bom.expense', string='Expense Type')
    nature_of_consumption = fields.Selection([
        ('purchase_n_consume', 'Purchase & Consume'),
        ('purchase_n_return', 'Purchase & Return'),
        ('purchase_n_recycle', 'Purchase & Recycle'),
        ('purchase_n_scrap', 'Purchase & Scrap'),
        ('stock_n_consume', 'Stock & Consume'),
        ('stock_n_return', 'Stock & Return'),
        ('stock_n_recycle', 'Stock & Recycle'),
        ('stock_n_scrap', 'Stock & Scrap'),
        ('recycle_n_consume', 'Recycle & Consume'),
        ('recycle_n_recycle', 'Recycle & Recycle'),
        ('recycle_n_scrap', 'Recycle & Scrap'),
    ], string='Nature of Consumption')
    return_quantity = fields.Char(string='Return Quantity')
