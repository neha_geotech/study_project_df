# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft

from odoo import api, fields, models, tools
from datetime import datetime, date, timedelta
from odoo.exceptions import UserError, ValidationError


class JournalEntryReport(models.Model):
    '''
    This is report view table to show Variance Report
    '''
    _name = 'journal.entry.report'
    _auto = False
    # _rec_name = 'date'

    @api.model
    def default_get(self, fields):
        raise ValidationError(_('No Record Create From this Option!'))
        res = super(JournalEntryReport, self).default_get(fields)
        return res

    @api.multi
    def get_journal_ids(self):
        move_ids = self.env['account.move'].search([''])

    job_code = fields.Many2one(
        'account.analytic.account',
        string='Job Code', readonly=True
    )
    ref = fields.Char('Reference', readonly=True)
    # account_id = fields.Many2one('account.account', string='Account', readonly=True)
    amount = fields.Float('Amount', readonly=True)
    account_move_id = fields.Many2one('account.move', string='Journal Entry', readonly=True)
    date = fields.Date('Date', readonly=True)
    create_date = fields.Datetime('Created On', readonly=True)
    branch = fields.Many2one("res.branch", string='Branch', readonly=True)
    late_entry_reason = fields.Text('Reason For Late Entry')
    create_uid = fields.Many2one('res.users', string='Created By', readonly=True)
    journal_id = fields.Many2one("account.journal", string="Journal", readonly=True)

    def get_first_day(self, dt, d_years=0, d_months=0):
        # d_years, d_months are "deltas" to apply to dt
        y, m = dt.year + d_years, dt.month + d_months
        a, m = divmod(m - 1, 12)
        return date(y + a, m + 1, 1)

    @api.model_cr
    def init(self):
        """
        Query to create table for variance report
        """

        date_format = '%Y-%m-%d %H:%M:%S'
        date_today = fields.Datetime.now()
        dt = datetime.strptime(date_today, date_format).date()
        day_first = self.get_first_day(dt)
        tools.drop_view_if_exists(self._cr, 'journal_entry_report')
        self._cr.execute("""
            CREATE or REPLACE VIEW journal_entry_report AS (
                with entries as (
                select
                am.id as account_move_id,
                am.name as ref,
                am.amount as amount,
                am.branch_id as branch,
                am.job_code as job_code,
                am.date as date,
                am.create_uid as create_uid,
                am.create_date as create_date,
                am.late_entry_reason,
                am.journal_id
                from account_move as am
                where am.is_late_entry=True
                )
                select
                row_number() OVER (ORDER BY job_code) AS id,
                ref, amount, branch, job_code, create_uid, late_entry_reason, account_move_id ,date, create_date, journal_id
                from entries
                )
        """)
