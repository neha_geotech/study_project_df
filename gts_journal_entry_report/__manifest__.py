# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'GTS Journal Entry Report',
    'summary': 'Journal Entry Report',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'website': 'http://www.geotechnosoft.com',
    'category': 'Manufacturing',
    'version': '1.0',
    'depends': [
        'account','branch'
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/journal_entry_report_security.xml',
        'report/journal_entry_report_view.xml',
    ],
    'installable': True,
}
