# -*- coding: utf-8 -*-
###########################################################################
#    Module Writen to odoo
#
#    Copyright (C) 2016 - Turkesh Patel. <http://www.almightycs.com>
#
#    @author Turkesh Patel <info@almightycs.com>
###########################################################################

from . import models
from . import wizard
