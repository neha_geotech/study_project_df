from odoo import api, fields, models, tools, _


class HSNMaster(models.Model):
    _name = "hsn.master"

    name = fields.Char('Name/Code', required=True, default=' ')
    description = fields.Char('Description')
    product_category = fields.Many2one('product.category', "Category")
    cgst_sale = fields.Many2one('account.tax', "CGST Sale")
    sgst_sale = fields.Many2one('account.tax', "SGST Sale")
    cgst_purchase = fields.Many2one('account.tax', "CGST Purchase")
    sgst_purchase = fields.Many2one('account.tax', "SGST Purchase")
    igst_sale = fields.Many2one('account.tax', "IGST Sale")
    igst_purchase = fields.Many2one('account.tax', "IGST Purchase")

    @api.onchange('name')
    def _compute_upper(self):
        for rec in self:
            rec.name = str(rec.name).title()

