# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft

from odoo import api, fields, models, tools


class BudgetVarianceReport(models.Model):
    _name = 'budget.variance.report'
    _auto = False

    product_id = fields.Many2one('product.product', string='Product')
    job_code = fields.Many2one(
        'account.analytic.account',
        string='Job Code',
    )
    planned_amount = fields.Float(string='Planned Amount', readonly=True)
    total_qty = fields.Float(string='Total Quantity', readonly=True)
    expense_type = fields.Many2one(
        'mrp.bom.expense',
        string='Expense Type',
        readonly=True
    )
    actual_qty = fields.Float(string='Actual Quantity', readonly=True)
    actual_amount = fields.Float(string='Actual Amount', readonly=True)
    diff_amount = fields.Float(string='Difference Amount', readonly=True)
    date = fields.Datetime(string='Transaction Date', readonly=True)
    open_expense = fields.Float(string='Open Expense', readonly=True)
    total_diff = fields.Float(string='Total Difference', readonly=True)
    nature_of_consumption = fields.Text(
        string='Nature of Consumption',
        readonly=True
    )
    recycle_value = fields.Float(string='Recycle Value', readonly=True)
    fund_required = fields.Float(string='Fund Required', readonly=True)
    fund_approval = fields.Float(string='Fund Approved', readonly=True)
    mode_of_approval = fields.Text(string='Payment Mode', readonly=True)
    est_value = fields.Float(string='Estimated Value', readonly=True)
    planned_value = fields.Float(string='Planned Value', readonly=True)
    process_stage = fields.Text(string='Completion Stage')
    invoice_value = fields.Float(string='Invoice Value', readonly=True)
    payment_received = fields.Float(string='Payment Received', readonly=True)
    bom_percentage = fields.Float(string='% BOM', readonly=True)
    average_cost = fields.Float(string='Average Cost', readonly=True)
    variance = fields.Float(string='Variance', readonly=True)
    variance_percentage = fields.Float(string='% Variance', readonly=True)
    difference = fields.Float(string='Difference Amount', readonly=True)
    difference_percentage = fields.Float(string='% Difference', readonly=True)
    total_cost = fields.Float(string='Total Cost', readonly=True)
    return_value = fields.Float(string='Return Value', readonly=True)
    asset_value = fields.Float(string='Asset Value', readonly=True)
    partner_id = fields.Many2one(
        'res.partner',
        string='Customer',
        readonly=True
    )
    branch_id = fields.Many2one(
        'res.branch',
        string='Branch',
        readonly=True
    )
    project_managers = fields.Many2one(
        'res.users',
        string='Project Managers',
        readonly=True
    )

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'budget_variance_report')
        self._cr.execute("""
                CREATE or REPLACE VIEW budget_variance_report AS (
                    with mrp_line as (
                        select
                            mbl.nature_of_consumption as nature_of_consumption,
                            mbl.recycle_value as recycle_value,
                            mbl.fund_required as fund_required,
                            mbl.fund_approval as fund_approval,
                            mbl.mode_of_approval as mode_of_approval,
                            mbl.product_id as product_id,
                            mb.job_code as job_code,
                            mbl.planned_amount_bom as planned_amount,
                            mbl.product_qty as total_qty,
                            mbl.expense_type as expense_type,
                            mbl.create_date as date,
                            0.0 as actual_qty,
                            0.0 as actual_amount,
                            0.0 as diff_amount,
                            0.0 as open_expense,
                            0.0 as total_diff,
                            0.0 as est_value,
                            ''::text as process_stage,
                            0.0 as invoice_value,
                            0.0 as payment_received,
                            0.0 as bom_percentage,
                            mbl.amount as average_cost,
                            0.0 as variance,
                            0.0 as variance_percentage,
                            0.0 as difference,
                            0.0 as difference_percentage,
                            0.0 as total_cost,
                            mb.client_name as partner_id,
                            mb.branch_id as branch_id,
                            mb.project_managers as project_managers,
                            0.0 as return_value,
                            0.0 as asset_value
                        from mrp_bom_line mbl
                        inner join product_product p on mbl.product_id = p.id
                        join product_template pt on pt.id = p.product_tmpl_id
                        left join mrp_bom mb on mbl.bom_id = mb.id where mbl.bom_id is not null
                ),
                    analytic_line as (
                        select
                            ''::text as nature_of_consumption,
                            0.0 as recycle_value,
                            0.0 as fund_required,
                            0.0 as fund_approval,
                            ''::text as mode_of_approval,
                            al.product_id as product_id,
                            al.account_id as job_code,
                            0.0 as planned_amount,
                            0.0 as total_qty,
                            al.expense_type as expense_type,
                            al.create_date as date,
                            al.unit_amount as actual_qty,
                            -(al.amount) as actual_amount,
                            0.0 as diff_amount,
                            0.0 as open_expense,
                            0.0 as total_diff,
                            0.0 as est_value,
                            ''::text as process_stage,
                            0.0 as invoice_value,
                            0.0 as payment_received,
                            0.0 as bom_percentage,
                            0.0 as average_cost,
                            0.0 as variance,
                            0.0 as variance_percentage,
                            0.0 as difference,
                            0.0 as difference_percentage,
                            0.0 as total_cost,
                            null::int as partner_id,
                            null::int as branch_id,
                            null::int as project_managers,
                            0.0 as return_value,
                            0.0 as asset_value
                        from account_analytic_line al
                        inner join product_product p on al.product_id = p.id
                        join product_template pt on pt.id = p.product_tmpl_id
                ),
                    purchase_line as (
                        select
                            ''::text as nature_of_consumption,
                            0.0 as recycle_value,
                            0.0 as fund_required,
                            0.0 as fund_approval,
                            ''::text as mode_of_approval,
                            l.product_id as product_id,
                            po.job_code as job_code,
                            0.0 as planned_amount,
                            0.0 as total_qty,
                            l.expense_type as expense_type,
                            l.create_date as date,
                            0.0 as actual_qty,
                            0.0 as actual_amount,
                            0.0 as diff_amount,
                            l.price_subtotal as open_expense,
                            0.0 as total_diff,
                            0.0 as est_value,
                            ''::text as process_stage,
                            0.0 as invoice_value,
                            0.0 as payment_received,
                            0.0 as bom_percentage,
                            0.0 as average_cost,
                            0.0 as variance,
                            0.0 as variance_percentage,
                            0.0 as difference,
                            0.0 as difference_percentage,
                            0.0 as total_cost,
                            null::int as partner_id,
                            null::int as branch_id,
                            null::int as project_managers,
                            0.0 as return_value,
                            0.0 as asset_value
                        from purchase_order_line l
                        inner join product_product p on l.product_id = p.id
                        join product_template pt on pt.id = p.product_tmpl_id
                        left join purchase_order po on l.order_id = po.id where l.order_id is not null and po.state in ('draft', 'sent', 'to_approve')
                ),
                    production_line as (
                        select
                            ''::text nature_of_consumption,
                            0.0 as recycle_value,
                            0.0 as fund_required,
                            0.0 as fund_approval,
                            ''::text as mode_of_approval,
                            sm.product_id as product_id,
                            mp.job_code as job_code,
                            0.0 as planned_amount,
                            0.0 as total_qty,
                            sm.expense_type as expense_type,
                            null::date as date,
                            0.0 as actual_qty,
                            0.0 as actual_amount,
                            0.0 as diff_amount,
                            0.0 as open_expense,
                            0.0 as total_diff,
                            0.0 as est_value,
                            mp.process_stage as process_stage,
                            0.0 as invoice_value,
                            0.0 as payment_received,
                            0.0 as bom_percentage,
                            0.0 as average_cost,
                            0.0 as variance,
                            0.0 as variance_percentage,
                            0.0 as difference,
                            0.0 as difference_percentage,
                            0.0 as total_cost,
                            null::int as partner_id,
                            null::int as branch_id,
                            null::int as project_managers,
                            0.0 as return_value,
                            0.0 as asset_value
                        from stock_move sm
                        inner join product_product p on sm.product_id = p.id
                        join product_template pt on pt.id = p.product_tmpl_id
                        left join mrp_production mp on sm.raw_material_production_id = mp.id where sm.raw_material_production_id is not null
                ),
                    quotation_line as (
                        select
                            ''::text as nature_of_consumption,
                            0.0 as recycle_value,
                            0.0 as fund_required,
                            0.0 as fund_approval,
                            ''::text as mode_of_approval,
                            null::int as product_id,
                            so.analytic_account_id as job_code,
                            0.0 as planned_amount,
                            0.0 as total_qty,
                            null::int as expense_type,
                            null::date as date,
                            0.0 as actual_qty,
                            0.0 as actual_amount,
                            0.0 as diff_amount,
                            0.0 as open_expense,
                            0.0 as total_diff,
                            so.amount_untaxed_new as est_value,
                            ''::text as process_stage,
                            0.0 as invoice_value,
                            0.0 as payment_received,
                            0.0 as bom_percentage,
                            0.0 as average_cost,
                            0.0 as variance,
                            0.0 as variance_percentage,
                            0.0 as difference,
                            0.0 as difference_percentage,
                            0.0 as total_cost,
                            null::int as partner_id,
                            null::int as branch_id,
                            null::int as project_managers,
                            0.0 as return_value,
                            0.0 as asset_value
                        from sale_order so
                    ),
                    account_inv_line as (
                        select
                            ''::text as nature_of_consumption,
                            0.0 as recycle_value,
                            0.0 as fund_required,
                            0.0 as fund_approval,
                            ''::text as mode_of_approval,
                            ail.product_id as product_id,
                            ail.account_analytic_id as job_code,
                            0.0 as planned_amount,
                            0.0 as total_qty,
                            ail.expense_type as expense_type,
                            ail.create_date as date,
                            0.0 as actual_qty,
                            0.0 as actual_amount,
                            0.0 as diff_amount,
                            0.0 as open_expense,
                            0.0 as total_diff,
                            0.0 as est_value,
                            ''::text as process_stage,
                            sum(amount_total) as invoice_value,
                            0.0 as payment_received,
                            0.0 as bom_percentage,
                            0.0 as average_cost,
                            0.0 as variance,
                            0.0 as variance_percentage,
                            0.0 as difference,
                            0.0 as difference_percentage,
                            0.0 as total_cost,
                            null::int as partner_id,
                            null::int as branch_id,
                            null::int as project_managers,
                            0.0 as return_value,
                            0.0 as asset_value
                        from account_invoice_line ail
                        inner join product_product p on ail.product_id = p.id
                        join product_template pt on pt.id = p.product_tmpl_id
                        left join account_invoice ai on ail.invoice_id = ai.id where ail.invoice_id is not null
                        group by ail.product_id, ail.account_analytic_id, ail.expense_type, ail.create_date
                    ),
                    account_move as (
                        select
                            ''::text as nature_of_consumption,
                            0.0 as recycle_value,
                            0.0 as fund_required,
                            0.0 as fund_approval,
                            ''::text as mode_of_approval,
                            aml.product_id as product_id,
                            aml.analytic_account_id as job_code,
                            0.0 as planned_amount,
                            0.0 as total_qty,
                            aml.expense_type as expense_type,
                            aml.create_date as date,
                            0.0 as actual_qty,
                            0.0 as actual_amount,
                            0.0 as diff_amount,
                            0.0 as open_expense,
                            0.0 as total_diff,
                            0.0 as est_value,
                            ''::text as process_stage,
                            0.0 as invoice_value,
                            am.amount as payment_received,
                            0.0 as bom_percentage,
                            0.0 as average_cost,
                            0.0 as variance,
                            0.0 as variance_percentage,
                            0.0 as difference,
                            0.0 as difference_percentage,
                            0.0 as total_cost,
                            null::int as partner_id,
                            null::int as branch_id,
                            null::int as project_managers,
                            0.0 as return_value,
                            0.0 as asset_value
                        from account_move_line aml
                        left join account_move am on aml.move_id = am.id where aml.move_id is not null and aml.partner_id is not null
                    ),
                    final_data as (
                        select * from mrp_line
                        UNION
                        select * from analytic_line
                        UNION
                        select * from purchase_line
                        UNION
                        select * from production_line
                        UNION
                        select * from quotation_line
                        UNION
                        select * from account_inv_line
                        UNION
                        select * from account_move
                )
                    select row_number() OVER (ORDER BY job_code) AS id,
                    nature_of_consumption, recycle_value, fund_required, fund_approval,
                    mode_of_approval, product_id,
                    job_code, planned_amount, total_qty,
                    expense_type, date, actual_qty, actual_amount,
                    (planned_amount + actual_amount) as diff_amount,
                    open_expense, (planned_amount + actual_amount - open_expense) as total_diff,
                    est_value, process_stage, invoice_value, payment_received,
                    ((planned_amount / nullif(est_value, 0)) * 100) as bom_percentage,
                    average_cost, (planned_amount - total_cost) as variance,
                    (((planned_amount - total_cost) * 100) / nullif(planned_amount, 0)) as variance_percentage,
                    (total_cost - invoice_value) as difference,
                    (((total_cost - invoice_value) * 100) / nullif(total_cost, 0)) as difference_percentage,
                    (actual_amount + open_expense) as total_cost, partner_id, branch_id, project_managers,
                    return_value, asset_value
                    from final_data
                )
        """)
