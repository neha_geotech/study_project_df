from odoo import models, fields, api, _
from datetime import date, timedelta, datetime


class AdminReportWiz(models.TransientModel):
    _name = 'admin.report.wiz'

    from_date = fields.Date('Start Date', default=str(date.today()))
    end_date = fields.Date('End Date', default=str(date.today()))

    @api.multi
    def open_admin_table(self):
        report_search = self.env['admin.account.report'].search([])
        if report_search:
            report_search.unlink()
        if self.from_date and self.end_date:
            tree_view_id = self.env.ref('gts_admin_report.view_admin_report_tree').id
            form_view_id = self.env.ref('gts_admin_report.view_admin_report_form').id
            graph_view_id = self.env.ref('gts_admin_report.view_admin_report_graph').id
            pivot_view_id = self.env.ref('gts_admin_report.view_admin_report_pivot').id
            search_view_ref = self.env.ref('gts_admin_report.view_admin_report_search', False)
            date_format = '%Y-%m-%d'
            first_day_ = datetime.strptime(self.from_date, date_format)
            last_day_ = datetime.strptime(self.end_date, date_format)
            first_day = first_day_
            last_day = last_day_
            self._cr.execute("""
                with account_ as (
                    select
                       acc.id as account_account,
                       ag.id as account_group,
                       0.0 as head_office,
                       0.0 as delhi,
                       0.0 as bangalore,
                       0.0 as mumbai,
                       0.0 as chennai,
                       0.0 as kolkata,
                       0.0 as retail,
                       0.0 as sogno,
                       0.0 as branch_total
                    from account_account acc
                    join account_group ag on ag.id = acc.group_id
                    where ag.parent_id = 72
                    group by acc.id, ag.id
                ),
                head_office as (
                    select
                        al.account_id as account_account,
                        aa.group_id as account_group,
                        sum(al.balance) as head_office,
                        0.0 as delhi,
                        0.0 as bangalore,
                        0.0 as mumbai,
                        0.0 as chennai,
                        0.0 as kolkata,
                        0.0 as retail,
                        0.0 as sogno,
                        0.0 as branch_total
                    from account_move_line al
                    join account_account aa on aa.id = al.account_id
                    join account_group ag on ag.id = aa.group_id
                    left join account_move am on am.id = al.move_id
                    where al.date between '%s' and '%s' and ag.parent_id = 72
                    and al.branch_id = 9 and am.state = 'posted'
                    group by al.account_id, aa.group_id
                ),
                delhi as (
                    select
                        al.account_id as account_account,
                        aa.group_id as account_group,
                        0.0 as head_office,
                        sum(al.balance) as delhi,
                        0.0 as bangalore,
                        0.0 as mumbai,
                        0.0 as chennai,
                        0.0 as kolkata,
                        0.0 as retail,
                        0.0 as sogno,
                        0.0 as branch_total
                    from account_move_line al
                    join account_account aa on aa.id = al.account_id
                    join account_group ag on ag.id = aa.group_id
                    left join account_move am on am.id = al.move_id
                    where al.date between '%s' and '%s' and ag.parent_id = 72
                    and al.branch_id = 1 and am.state = 'posted'
                    group by al.account_id, aa.group_id
                ),
                bangalore as (
                    select
                        al.account_id as account_account,
                        aa.group_id as account_group,
                        0.0 as head_office,
                        0.0 as delhi,
                        sum(al.balance) as bangalore,
                        0.0 as mumbai,
                        0.0 as chennai,
                        0.0 as kolkata,
                        0.0 as retail,
                        0.0 as sogno,
                        0.0 as branch_total
                    from account_move_line al
                    join account_account aa on aa.id = al.account_id
                    join account_group ag on ag.id = aa.group_id
                    left join account_move am on am.id = al.move_id
                    where al.date between '%s' and '%s' and ag.parent_id = 72
                    and al.branch_id = 4 and am.state = 'posted'
                    group by al.account_id, aa.group_id
                ),
                mumbai as (
                    select
                        al.account_id as account_account,
                        aa.group_id as account_group,
                        0.0 as head_office,
                        0.0 as delhi,
                        0.0 as bangalore,
                        sum(al.balance) as mumbai,
                        0.0 as chennai,
                        0.0 as kolkata,
                        0.0 as retail,
                        0.0 as sogno,
                        0.0 as branch_total
                    from account_move_line al
                    join account_account aa on aa.id = al.account_id
                    join account_group ag on ag.id = aa.group_id
                    left join account_move am on am.id = al.move_id
                    where al.date between '%s' and '%s' and ag.parent_id = 72
                    and al.branch_id = 2 and am.state = 'posted'
                    group by al.account_id, aa.group_id
                ),
                chennai as (
                    select
                        al.account_id as account_account,
                        aa.group_id as account_group,
                        0.0 as head_office,
                        0.0 as delhi,
                        0.0 as bangalore,
                        0.0 as mumbai,
                        sum(al.balance) as chennai,
                        0.0 as kolkata,
                        0.0 as retail,
                        0.0 as sogno,
                        0.0 as branch_total
                    from account_move_line al
                    join account_account aa on aa.id = al.account_id
                    join account_group ag on ag.id = aa.group_id
                    left join account_move am on am.id = al.move_id
                    where al.date between '%s' and '%s' and ag.parent_id = 72
                    and al.branch_id = 5 and am.state = 'posted'
                    group by al.account_id, aa.group_id
                ),
                kolkata as (
                    select
                        al.account_id as account_account,
                        aa.group_id as account_group,
                        0.0 as head_office,
                        0.0 as delhi,
                        0.0 as bangalore,
                        0.0 as mumbai,
                        0.0 as chennai,
                        sum(al.balance) as kolkata,
                        0.0 as retail,
                        0.0 as sogno,
                        0.0 as branch_total
                    from account_move_line al
                    join account_account aa on aa.id = al.account_id
                    join account_group ag on ag.id = aa.group_id
                    left join account_move am on am.id = al.move_id
                    where al.date between '%s' and '%s' and ag.parent_id = 72
                    and al.branch_id = 3 and am.state = 'posted'
                    group by al.account_id, aa.group_id
                ),
                retail as (
                    select
                        al.account_id as account_account,
                        aa.group_id as account_group,
                        0.0 as head_office,
                        0.0 as delhi,
                        0.0 as bangalore,
                        0.0 as mumbai,
                        0.0 as chennai,
                        0.0 as kolkata,
                        sum(al.balance) as retail,
                        0.0 as sogno,
                        0.0 as branch_total
                    from account_move_line al
                    join account_account aa on aa.id = al.account_id
                    join account_group ag on ag.id = aa.group_id
                    left join account_move am on am.id = al.move_id
                    where al.date between '%s' and '%s' and ag.parent_id = 72
                    and al.branch_id = 7 and am.state = 'posted'
                    group by al.account_id, aa.group_id
                ),
                sogno as (
                    select
                        al.account_id as account_account,
                        aa.group_id as account_group,
                        0.0 as head_office,
                        0.0 as delhi,
                        0.0 as bangalore,
                        0.0 as mumbai,
                        0.0 as chennai,
                        0.0 as kolkata,
                        0.0 as retail,
                        sum(al.balance) as sogno,
                        0.0 as branch_total
                    from account_move_line al
                    join account_account aa on aa.id = al.account_id
                    join account_group ag on ag.id = aa.group_id
                    left join account_move am on am.id = al.move_id
                    where al.date between '%s' and '%s' and ag.parent_id = 72
                    and al.branch_id = 8 and am.state = 'posted'
                    group by al.account_id, aa.group_id
                ),
                branch_total as (
                    select
                        al.account_id as account_account,
                        aa.group_id as account_group,
                        0.0 as head_office,
                        0.0 as delhi,
                        0.0 as bangalore,
                        0.0 as mumbai,
                        0.0 as chennai,
                        0.0 as kolkata,
                        0.0 as retail,
                        0.0 as sogno,
                        sum(al.balance) as branch_total
                    from account_move_line al
                    join account_account aa on aa.id = al.account_id
                    join account_group ag on ag.id = aa.group_id
                    left join account_move am on am.id = al.move_id
                    where al.date between '%s' and '%s' and ag.parent_id = 72
                    and am.state = 'posted'
                    group by al.account_id, aa.group_id
                ),
                final_data as (
                        select * from account_
                        UNION
                        select * from head_office
                        UNION
                        select * from delhi
                        UNION
                        select * from bangalore
                        UNION
                        select * from mumbai
                        UNION
                        select * from chennai
                        UNION
                        select * from kolkata
                        UNION
                        select * from retail
                        UNION
                        select * from sogno
                        UNION
                        select * from branch_total
                )
                select row_number() OVER (ORDER BY account_account) AS id,
                    account_account, account_group, head_office,
                    delhi, bangalore, mumbai,
                    chennai, kolkata,
                    retail, sogno, branch_total
                    from final_data
                
                """ % (
                first_day, last_day,
                first_day, last_day,
                first_day, last_day,
                first_day, last_day,
                first_day, last_day,
                first_day, last_day,
                first_day, last_day,
                first_day, last_day,
                first_day, last_day
                )

            )
            result = self._cr.fetchall()
            rows = result
            values = ', '.join(map(str, rows))
            sql = ("""INSERT INTO admin_account_report 
                        (id, account_account, account_group, head_office, delhi,
                        bangalore, mumbai,
                        chennai, kolkata,
                        retail, sogno, branch_total) VALUES {}""".format(values).replace('None', 'null')
            )
            self._cr.execute(sql)

            action = {
                'type': 'ir.actions.act_window',
                'views': [
                    (pivot_view_id, 'pivot'),
                    (tree_view_id, 'tree'), (form_view_id, 'form'),
                    (graph_view_id, 'graph')
                ],
                'view_mode': 'tree,form',
                'name': _('Admin Report: %s - %s' % (first_day.strftime('%d %b %y'), last_day.strftime('%d %b %y'))),
                'res_model': 'admin.account.report',
                'search_view_id': search_view_ref and search_view_ref.id,
                'context': {'group_by': 'account_group'}
            }
            return action