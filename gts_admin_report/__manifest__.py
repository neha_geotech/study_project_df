# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'GTS Admin Report',
    'summary': 'Admin Report',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'website': 'http://www.geotechnosoft.com',
    'category': 'Account',
    'version': '1.0',
    'depends': [
        'mrp', 'analytic', 'gts_dream_purchase'
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/admin_report_security.xml',
        'wizard/report_admin_wiz_view.xml',
        'report/report_admin_view.xml',
    ],
    'installable': True,
}
