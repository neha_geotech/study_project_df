# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError

from calendar import monthrange
from datetime import date, timedelta, datetime


class AdminReport(models.Model):
    '''
    This is report view table to show Admin account data  branch wise
    '''
    _name = 'admin.account.report'
    _rec_name = 'account_account'

    @api.model
    def default_get(self, fields):
        raise ValidationError(_('No Record Create From this Option!'))
        res = super(AdminReport, self).default_get(fields)
        return res

    account_account = fields.Many2one('account.account', string='Accounts', readonly=True)
    account_group = fields.Many2one('account.group', string='Group', readonly=True)
    head_office = fields.Float(string='HO', readonly=True)
    delhi = fields.Float(string='DEL', readonly=True)
    bangalore = fields.Float(string='BLR', readonly=True)
    mumbai = fields.Float(string='MUM', readonly=True)
    chennai = fields.Float(string='CHN', readonly=True)
    kolkata = fields.Float(string='KOL', readonly=True)
    retail = fields.Float(string='RET', readonly=True)
    sogno = fields.Float(string='SOGNO', readonly=True)
    branch_total = fields.Float(string='ALL BRANCHES')