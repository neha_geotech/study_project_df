# -*- coding: utf-8 -*-
from odoo import models, api
from lxml import etree
from odoo.osv.orm import setup_modifiers
from odoo.tools.safe_eval import safe_eval
from odoo.exceptions import UserError

class product_product(models.Model):
    _inherit = 'product.product'

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        result = super(product_product, self).fields_view_get(view_id, view_type, toolbar=toolbar, submenu=submenu)
        if view_type == "form":
            doc = etree.XML(result['arch'])
            field_exist_in_view = False
            first_node = None
            for node in doc.iter(tag="field"):
                if not first_node:
                    first_node = node
                if node.get("name") == 'state':
                    field_exist_in_view = True
                if 'readonly' in node.attrib.get("modifiers", ''):
                    attrs = node.attrib.get("attrs", '')
                    if 'readonly' in attrs:
                        attrs_dict = safe_eval(node.get('attrs'))
                        r_list = attrs_dict.get('readonly',)
                        if type(r_list) == list:
                            r_list.insert(0, ('state', '=', 'confirmed'))
                            if len(r_list) > 1:
                                r_list.insert(0, '|')
                        attrs_dict.update({'readonly': r_list})
                        if node.get("name") in result['fields']:
                            node.set('attrs', str(attrs_dict))
                            setup_modifiers(node, result['fields'][node.get("name")])
                        continue
                    else:
                        continue

                if node.get("name") in result['fields']:
                    node.set('attrs', "{'readonly':[('state','=','confirmed')]}")
                    setup_modifiers(node, result['fields'][node.get("name")])
            if not field_exist_in_view:
                first_node.addnext(etree.Element('field', {'name': 'state', 'invisible': "1"}))
            result['arch'] = etree.tostring(doc)
        return result

    @api.multi
    def confirm_product(self):
        self.write({'state': 'confirmed'})
        return True

    @api.multi
    def product_reset_to_draft(self):
        self.write({'state': 'draft'})
        return True

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        if not args:
            args = []
        args += [['state', '=', 'confirmed']]
        res = super(product_product, self).name_search(name, args, operator, limit)
        return res
