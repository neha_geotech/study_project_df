# -*- coding: utf-8 -*-
{
    "name": "Product Approval WorkFlow",
    "version": "0.1",
    "author": "Nilesh Sheliya",
    "category": "Product",
    "description": """
        This module will add state in the products, if products state is in draft, than it can't be salable and once it is confirmed, it becomes salable and can't modify by anyone.
    """,
    "depends": ['product', 'stock', 'sale', 'purchase', 'mrp'],
    "price": 30.00,
    "currency": "EUR",
    "data": [
              "security/security.xml",
              "views/product_product.xml",
              "views/product_template.xml",
              "views/menus.xml"
            ],
    "images": ['static/description/not-approved.png'],
    "installable": True,
    "auto_install": False,
    "application": True,
}
