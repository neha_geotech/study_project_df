{
    'name': 'Gts Expense',
    'version': '1.0',
    'category': '',
    "sequence": 14,
    'complexity': "easy",
    'category': 'Generic Modules/Others',
    'description': """

    """,
    'author': 'Geo Technosoft.',
    'website': 'geotechnosoft.com',
    'depends': ['hr_expense', 'gts_employee_expense_advance'],
    'init_xml': [],
    'data': [
               #'security/ir.model.access.csv',
                'security/security.xml',
               'views/expense_view.xml',
               #"views/report_leave_templates.xml",


    ],


    'installable': True,
    'auto_install': False,
}
