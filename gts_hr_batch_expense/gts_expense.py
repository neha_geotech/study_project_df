# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import float_compare, float_is_zero
import time


class HrExpense(models.Model):
    _inherit = "hr.expense"

    @api.depends('sheet_id', 'sheet_id.account_move_id', 'sheet_id.state')
    def _compute_state(self):
        for expense in self:
            if not expense.sheet_id:
                expense.state = "draft"
            elif expense.sheet_id.state == "cancel":
                expense.state = "refused"
            elif not expense.sheet_id.account_move_id:
                expense.state = "reported"
            else:
                expense.state = "done"

    @api.model
    def _get_default_branch(self):
        user_obj = self.env['res.users']
        branch_id = user_obj.browse(self.env.user.id).branch_id
        return branch_id

    @api.model
    def default_get(self, fields):
        result = super(HrExpense, self).default_get(fields)
        product = self.env['product.product'].search([('name', '=', 'Expenses')])
        if product:
            result['product_id'] = product.id
        return result

    expenseline_ids = fields.One2many(
        'hr.expense', 'expense_id', 'Expense Lines', copy=False)
    expense_id = fields.Many2one('hr.expense')
    branch_id = fields.Many2one(
        'res.branch',
        string='Branch',
        copy=False,
        default=_get_default_branch,
    )
    expense_type = fields.Many2one(
        'mrp.bom.expense',
        string='Expense Type',
        copy=False
    )
    advance_amount_line = fields.Float('Advance Amount')
    date_from = fields.Date(string='Date From')
    date_to = fields.Date(string='Date To')

    @api.multi
    def submit_expenses(self):
        if any(expense.state != 'draft' for expense in self):
            raise UserError(_("You cannot report twice the same line!"))
        if len(self.mapped('employee_id')) != 1:
            raise UserError(_("You cannot report expenses for different employees in the same report!"))
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'hr.expense.sheet',
            'target': 'current',
            'context': {
                'default_expense_line_ids': [line.id for line in self.expenseline_ids],
                'default_employee_id': self[0].employee_id.id,
                'default_branch_id': self[0].branch_id.id,
                'default_name': self[0].name if len(self.ids) == 1 else '',
            }
        }

    @api.onchange('product_id')
    def _onchange_product_id(self):
        if self.product_id:
            self.name = self.product_id.display_name or ''
            self.unit_amount = self.product_id.price_compute('standard_price')[self.product_id.id]
            self.product_uom_id = self.product_id.uom_id
            self.tax_ids = self.product_id.supplier_taxes_id
            self.expense_type = self.product_id.expense_type.id
            account = self.product_id.product_tmpl_id._get_product_accounts()['expense']
            if account:
                self.account_id = account

    @api.onchange('advance_expense')
    def advance_expense_change(self):
        list_ = []
        for line in self.advance_expense.advance_expense_line_ids:
            res = {
                'product_id': line.product_id.id,
                'name': line.description,
                'expense_type': line.expense_type.id,
                'analytic_account_id': line.job_code.id,
                'unit_amount': line.unit_amount,
                'quantity': line.quantity,
                'product_uom_id': line.product_uom_id.id,
                'advance_amount_line': line.total_amount,
                'account_id': self.advance_expense.account_id.id,
                'advance_expense': self.advance_expense.id,
                'state': 'draft',
                'currency_id': line.currency_id.id
            }
            list_.append(res)
        self.update({'expenseline_ids': list_})

    # def _prepare_move_line(self, line):
    #     res = super(HrExpense, self)._prepare_move_line(line)
    #     if self.advance_expense:
    #         print("=============", line)
    #         sdfghjk
        # partner_id = self.employee_id.address_home_id.commercial_partner_id.id
        # return {
        #     'date_maturity': line.get('date_maturity'),
        #     'partner_id': partner_id,
        #     'name': line['name'][:64],
        #     'debit': line['price'] > 0 and line['price'],
        #     'credit': line['price'] < 0 and - line['price'],
        #     'account_id': line['account_id'],
        #     'analytic_line_ids': line.get('analytic_line_ids'),
        #     'amount_currency': line['price'] > 0 and abs(line.get('amount_currency')) or - abs(
        #         line.get('amount_currency')),
        #     'currency_id': line.get('currency_id'),
        #     'tax_line_id': line.get('tax_line_id'),
        #     'tax_ids': line.get('tax_ids'),
        #     'quantity': line.get('quantity', 1.00),
        #     'product_id': line.get('product_id'),
        #     'product_uom_id': line.get('uom_id'),
        #     'analytic_account_id': line.get('analytic_account_id'),
        #     'payment_id': line.get('payment_id'),
        #     'expense_id': line.get('expense_id'),
        # }
        # return res

    # @api.multi
    # def action_move_create(self):
    #     # res = super(HrExpense, self).action_move_create()
    #     if any(expense.advance_expense for expense in self):
    #         created_moves = self.env['account.move']
    #         move_vals = {}
    #         for line in self:
    #             exp_date = fields.Date.context_today(self)
    #             ref = line.employee_id.name + ': ' + line.name.split('\n')[0][:64]
    #             credit_acc_id = line.mapped('account_id')
    #             company_currency = line.company_id.currency_id
    #             current_currency = line.currency_id
    #             move_line_credit = {
    #                 'name': ref,
    #                 'account_id': line.account_id.id,
    #                 'credit': line.total_amount,
    #                 'debit': 0.0,
    #                 'journal_id': line.sheet_id.journal_id.id,
    #                 'partner_id': line.employee_id.user_id.partner_id.id,
    #                 'currency_id': company_currency != current_currency and current_currency.id or False,
    #                 'amount_currency': company_currency != current_currency and line.total_amount_expense or 0.0,
    #                 'branch_id': line.branch_id.id,
    #                 'analytic_account_id': line.analytic_account_id.id,
    #                 'expense_type': line.expense_type.id
    #             }
    #             print()
    #             move_line_debit = {
    #                 'name': ref,
    #                 'account_id': line.product_id.property_account_expense_id.id,
    #                 'debit': line.total_amount,
    #                 'credit': 0.0,
    #                 'journal_id': line.sheet_id.journal_id.id,
    #                 'partner_id': line.employee_id.user_id.partner_id.id,
    #                 'currency_id': company_currency != current_currency and current_currency.id or False,
    #                 'amount_currency': company_currency != current_currency and - 1.0 * line.total_amount_expense or 0.0,
    #                 'branch_id': line.branch_id.id,
    #                 'analytic_account_id': line.analytic_account_id.id,
    #                 'expense_type': line.expense_type.id
    #             }
    #             move_vals = {
    #                 'ref': line.name,
    #                 'date': exp_date or False,
    #                 'journal_id': line.sheet_id.journal_id.id,
    #                 'line_ids': [(0, 0, move_line_debit), (0, 0, move_line_credit)],
    #                 'branch_id': line.branch_id.id,
    #                 # 'job_code':
    #             }
    #             # for data in line.expenseline_ids:
    #             #     move_vals['job_code'] = data.analytic_account_id.id
    #             print("=========", move_vals)
    #         move = self.env['account.move'].create(move_vals)
    #         line.write({
    #             'move_id': move.id,
    #             'move_check': True,
    #             'state': 'done',
    #             'account_validate_date': time.strftime('%Y-%m-%d'),
    #             'account_by_id': line.env.user.id,
    #             'is_paid': True,
    #             'branch_id': move.branch_id.id,
    #         })
    #             # created_moves |= move
    #         # return [x.id for x in created_moves]
    #     else:
    #         move_group_by_sheet = {}
    #         for expense in self:
    #             journal = expense.sheet_id.bank_journal_id if expense.payment_mode == 'company_account' else expense.sheet_id.journal_id
    #             # create the move that will contain the accounting entries
    #             acc_date = expense.sheet_id.accounting_date or expense.date
    #             if not expense.sheet_id.id in move_group_by_sheet:
    #                 move = self.env['account.move'].create({
    #                     'journal_id': journal.id,
    #                     'company_id': self.env.user.company_id.id,
    #                     'date': acc_date,
    #                     'ref': expense.sheet_id.name,
    #                     # force the name to the default value, to avoid an eventual 'default_name' in the context
    #                     # to set it to '' which cause no number to be given to the account.move when posted.
    #                     'name': '/',
    #                 })
    #                 move_group_by_sheet[expense.sheet_id.id] = move
    #             else:
    #                 move = move_group_by_sheet[expense.sheet_id.id]
    #             company_currency = expense.company_id.currency_id
    #             diff_currency_p = expense.currency_id != company_currency
    #             # one account.move.line per expense (+taxes..)
    #             move_lines = expense._move_line_get()
    #
    #             # create one more move line, a counterline for the total on payable account
    #             payment_id = False
    #             total, total_currency, move_lines = expense._compute_expense_totals(company_currency, move_lines,
    #                                                                                 acc_date)
    #             if expense.payment_mode == 'company_account':
    #                 if not expense.sheet_id.bank_journal_id.default_credit_account_id:
    #                     raise UserError(_("No credit account found for the %s journal, please configure one.") % (
    #                     expense.sheet_id.bank_journal_id.name))
    #                 emp_account = expense.sheet_id.bank_journal_id.default_credit_account_id.id
    #                 journal = expense.sheet_id.bank_journal_id
    #                 # create payment
    #                 payment_methods = (
    #                                           total < 0) and journal.outbound_payment_method_ids or journal.inbound_payment_method_ids
    #                 journal_currency = journal.currency_id or journal.company_id.currency_id
    #                 payment = self.env['account.payment'].create({
    #                     'payment_method_id': payment_methods and payment_methods[0].id or False,
    #                     'payment_type': total < 0 and 'outbound' or 'inbound',
    #                     'partner_id': expense.employee_id.address_home_id.commercial_partner_id.id,
    #                     'partner_type': 'supplier',
    #                     'journal_id': journal.id,
    #                     'payment_date': expense.date,
    #                     'state': 'reconciled',
    #                     'currency_id': diff_currency_p and expense.currency_id.id or journal_currency.id,
    #                     'amount': diff_currency_p and abs(total_currency) or abs(total),
    #                     'name': expense.name,
    #                 })
    #                 payment_id = payment.id
    #             else:
    #                 if not expense.employee_id.address_home_id:
    #                     raise UserError(_("No Home Address found for the employee %s, please configure one.") % (
    #                     expense.employee_id.name))
    #                 emp_account = expense.employee_id.address_home_id.property_account_payable_id.id
    #
    #             aml_name = expense.employee_id.name + ': ' + expense.name.split('\n')[0][:64]
    #             move_lines.append({
    #                 'type': 'dest',
    #                 'name': aml_name,
    #                 'price': total,
    #                 'account_id': emp_account,
    #                 'date_maturity': acc_date,
    #                 'amount_currency': diff_currency_p and total_currency or False,
    #                 'currency_id': diff_currency_p and expense.currency_id.id or False,
    #                 'payment_id': payment_id,
    #                 'expense_id': expense.id,
    #             })
    #
    #             # convert eml into an osv-valid format
    #             lines = [(0, 0, expense._prepare_move_line(x)) for x in move_lines]
    #             move.with_context(dont_create_taxes=True).write({'line_ids': lines})
    #             expense.sheet_id.write({'account_move_id': move.id})
    #             if expense.payment_mode == 'company_account':
    #                 expense.sheet_id.paid_expense_sheets()
    #         for move in move_group_by_sheet.values():
    #             move.post()
    #         return True


class HrExpenseSheet(models.Model):
    _inherit = "hr.expense.sheet"

    branch_id = fields.Many2one('res.branch', string='Branch')

    @api.one
    @api.constrains('expense_line_ids', 'employee_id')
    def _check_employee(self):
        employee_ids = self.expense_line_ids.mapped('employee_id')
        if len(employee_ids) > 1 or (len(employee_ids) == 1 and employee_ids != self.employee_id):
            pass

    @api.multi
    def action_sheet_move_create(self):
        # res = super(HrExpenseSheet, self).action_move_create()
        if any(expense.advance_expense for expense in self.expense_line_ids):
            created_moves = self.env['account.move']
            exp_date = fields.Date.context_today(self)
            prec = self.env['decimal.precision'].precision_get('Account')
            for line in self:
                ref = line.employee_id.name + ': ' + line.name.split('\n')[0][:64]
                credit_acc_id = line.expense_line_ids.mapped('account_id')
                company_currency = line.company_id.currency_id
                current_currency = line.currency_id
                amount = current_currency.compute(line.total_amount, company_currency)
                move_line_credit = {
                    'name': ref,
                    'account_id': credit_acc_id.id,
                    'credit': line.total_amount,
                    'debit': 0.0,
                    'journal_id': line.journal_id.id,
                    # 'partner_id': line.partner_id.id,
                    'currency_id': company_currency != current_currency and current_currency.id or False,
                    'amount_currency': company_currency != current_currency and line.total_amount_expense or 0.0,
                    'branch_id': line.branch_id.id,
                }
                move_line_debit = {
                    'name': ref,
                    # 'account_id': line.journal_id.default_credit_account_id.id,
                    'debit': line.total_amount,
                    'credit': 0.0,
                    'journal_id': line.journal_id.id,
                    # 'partner_id': line.partner_id.id,
                    'currency_id': company_currency != current_currency and current_currency.id or False,
                    'amount_currency': company_currency != current_currency and - 1.0 * line.total_amount_expense or 0.0,
                    'branch_id': line.branch_id.id,
                }
                move_vals = {
                    'ref': line.name,
                    'date': exp_date or False,
                    'journal_id': line.journal_id.id,
                    # 'narration': line.reason_for_advance,
                    'line_ids': [(0, 0, move_line_debit), (0, 0, move_line_credit)],
                    'branch_id': line.branch_id.id,
                }
                for data in line.expense_line_ids:
                    move_line_debit['account_id'] = data.product_id.property_account_expense_id.id
                    move_line_credit['expense_type'] = data.expense_type.id
                    move_line_debit['analytic_account_id'] = data.analytic_account_id.id
                    move_line_credit['analytic_account_id'] = data.analytic_account_id.id
                    move_line_debit['expense_type'] = data.expense_type.id
                    move_line_debit['partner_id'] = data.employee_id.user_id.partner_id.id
                    move_line_credit['partner_id'] = data.employee_id.user_id.partner_id.id
                    move_vals['job_code'] = data.analytic_account_id.id
                    print("=========", move_line_credit, move_line_debit, data.product_id.property_account_expense_id.name, credit_acc_id.name)
                move = self.env['account.move'].create(move_vals)
                line.write({
                    'move_id': move.id,
                    'move_check': True,
                    'state': 'done',
                    'account_validate_date': time.strftime('%Y-%m-%d'),
                    'account_by_id': line.env.user.id,
                    'is_paid': True,
                    'branch_id': move.branch_id.id,
                })
                created_moves |= move
                print("============", created_moves)
            return [x.id for x in created_moves]
        else:
            if any(sheet.state != 'approve' for sheet in self):
                raise UserError(_("You can only generate accounting entry for approved expense(s)."))

            if any(not sheet.journal_id for sheet in self):
                raise UserError(_("Expenses must have an expense journal specified to generate accounting entries."))

            expense_line_ids = self.mapped('expense_line_ids') \
                .filtered(lambda r: not float_is_zero(r.total_amount, precision_rounding=(
                    r.currency_id or self.env.user.company_id.currency_id).rounding))
            res = expense_line_ids.action_move_create()

            if not self.accounting_date:
                self.accounting_date = self.account_move_id.date

            if self.payment_mode == 'own_account' and expense_line_ids:
                self.write({'state': 'post'})
            else:
                self.write({'state': 'done'})
            return res
        # return res
