# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields


class Company(models.Model):
    """Inheriting company ko get sender id."""

    _inherit = "res.company"

    localtext_sms_sender_id = fields.Char('TextLocal SMS Sender Id')
