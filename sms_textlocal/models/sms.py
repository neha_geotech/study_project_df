# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, exceptions, fields, models, _
from odoo.exceptions import UserError, except_orm
from urllib import parse, request
import urllib.parse
from urllib.request import urlopen
import requests
from lxml import etree
import re, ast, json
from ast import literal_eval

import logging
_logger = logging.getLogger(__name__)


class SMS(models.Model):
    _name = 'sms.sms'
    _rec_name = 'phone_number'

    content = fields.Char(string="Content", required=True)
    phone_number = fields.Char(string="Phone Number")
    state = fields.Selection([
        ('draft', 'Draft'),
        ('sent', 'Sent'),
        ('failed', 'Failed')], "States",
        default='draft')
    response_status = fields.Char("Response Status")
    response_message = fields.Text("Response")

    @api.multi
    def send(self):
        """Send the sms through textlocal API."""
        # Fetch the key
        localtext_key = self.env['ir.config_parameter'].get_param('textlocal_api_key')
        sms_base_url = self.env['ir.config_parameter'].get_param('textlocal_url')
        if localtext_key and sms_base_url:
            params = {
                'apikey': localtext_key,
                'numbers': self.phone_number,
                'message': self.content,
                'sender': self.env.user.company_id.localtext_sms_sender_id
            }
            data = urllib.parse.urlencode(params)
            data = data.encode('utf-8')
            request_ = urllib.request.Request(str(sms_base_url))
            f = urlopen(request_, data)
            fr = f.read()
            fr_parse = json.loads(fr.decode('utf-8'))
            if 'errors' in fr_parse:
                self.write({'state': 'failed', 'response_message': fr, 'response_status': fr_parse['status']})
            if 'errors' not in fr_parse:
                self.write({'state': 'sent', 'response_message': fr, 'response_status': fr_parse['status']})
            return fr
