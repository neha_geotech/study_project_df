# -*- coding: utf-8 -*-
# Part of GTS. See LICENSE file for full copyright and licensing details.
{
    'name' : 'GTS SMS',
    'version' : '11.0',
    'summary': 'Module for sending SMS',
    'author' : "Geo Technosoft",
    'sequence': 30,
    'description': """ """,
    'category' : '',
    'website': 'www.geotechnosoft.com',
    'depends' : ['project'],
    'data': [
        'security/ir.model.access.csv',
        'views/sms_view.xml',
        'views/company.xml'
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}