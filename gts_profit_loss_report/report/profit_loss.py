# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError


class ProfitLoss(models.Model):
    '''
    This is report view table to show Profit and Loss Report
    '''
    _name = 'profit.loss.report'
    # _auto = False
    _rec_name = 'branch'

    @api.model
    def default_get(self, fields):
        raise ValidationError(_('No Record Create From this Option!'))
        res = super(ProfitLoss, self).default_get(fields)
        return res

    branch = fields.Many2one('res.branch', string='Branch', readonly=True)
    curr_total_invoice = fields.Float(string='CURRENT MTH INVOICE', readonly=True)
    curr_total_credit_note = fields.Float(string='CURRENT MTH CN', readonly=True)
    curr_mnth_invoice = fields.Float(string='CURR MTH INV "-CN"', readonly=True)
    closing_opening_unbilled = fields.Float(string='8 CL-OP UNBILL DIFF', readonly=True)
    so_ap_closing = fields.Float(string='1 SO AS PER CLOSING', readonly=True)
    cl_op_stock_difference = fields.Float(string='6 CL-OP STOCK DIFF', readonly=True)
    project_expense = fields.Float(string='2 PROJECT EXPENSE', readonly=True)
    percent_pe = fields.Float(string='% PE', readonly=True)
    overheads_last_month = fields.Float(string='4 OVERHEADS', readonly=True)
    net_profit = fields.Float(string='5 NET PROFIT', readonly=True)
    profit_percentage = fields.Float(string='% PROFIT', readonly=True)
    overhead_percentage = fields.Float(string='% OH', readonly=True)
    opening_unbilled = fields.Float(string='OP-UNBILLED', readonly=True)
    curr_month_billed = fields.Float(string='10 CURR MTH-BILLED IN CURRENT', readonly=True)
    previous_month_billed = fields.Float(string='11 PREVIOUS MTH-BILLED IN CURRENT', readonly=True)
    current_left_unbilled = fields.Float(string='12 CURR LEFT UNBILLED', readonly=True)
    cl_unbilled = fields.Float(string='CL-UNBILLED 9-10-11+12', readonly=True)
    op_stock = fields.Float(string='OP-STOCK', readonly=True)
    cl_stock = fields.Float(string='CL-STOCK', readonly=True)
    closing_unbilled = fields.Float(string='CL UNBILLED', readonly=True)
    opening_unbilled_ = fields.Float(string='9 OP-UNBILLED', readonly=True)
    open_expense = fields.Float(string='3 OPEN EXPENSES', readonly=True)
    assets_and_investment = fields.Float(string='7 ASSETS & INVESTMENT', readonly=True)
