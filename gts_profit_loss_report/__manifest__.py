# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'GTS P&L Report',
    'summary': 'P&L Report',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'website': 'http://www.geotechnosoft.com',
    'category': '',
    'version': '1.0',
    'depends': [
        'gts_dream_purchase'
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/profit_loss_security.xml',
        'wizard/profit_loss_view_wiz.xml',
        'report/profit_loss_view.xml',
    ],
    'installable': True,
}
