from odoo import models, fields, api, _
from datetime import date, timedelta, datetime


class ProfitLossWiz(models.TransientModel):
    _name = 'profit.loss.wiz'

    from_date = fields.Date('Start Date', default=fields.Datetime.now)
    end_date = fields.Date('End Date', default=fields.Datetime.now)

    @api.multi
    def open_pnl_table(self):
        report_search = self.env['profit.loss.report'].search([])
        branches = self.env['res.branch'].search([])
        self._cr.execute('''
                            delete  
                            from profit_loss_report
                            where branch in %s
                        ''', (tuple(branches.ids),))
        if self.from_date and self.end_date:
            tree_view_id = self.env.ref('gts_profit_loss_report.view_profit_loss_report_tree').id
            form_view_id = self.env.ref('gts_profit_loss_report.view_profit_loss_report_form').id
            graph_view_id = self.env.ref('gts_profit_loss_report.view_profit_loss_report_graph').id
            pivot_view_id = self.env.ref('gts_profit_loss_report.view_profit_loss_report_pivot').id
            search_view_ref = self.env.ref('gts_profit_loss_report.view_profit_loss_report_search', False)

            date_format = '%Y-%m-%d'
            first_day_ = datetime.strptime(self.from_date, date_format)
            last_day_ = datetime.strptime(self.end_date, date_format)
            # dt = datetime.strptime(today_date, date_format)
            # dt_inv = datetime.strptime(today_date, date_format).date()
            first_day = first_day_
            day_last_strip = str(last_day_).strip(':0')
            last_day = last_day_
            last_day_it = day_last_strip + '23:59:59'
            # first_day_inv = self.get_first_day(dt_inv)
            # last_day_inv = self.get_last_day(dt_inv)

            self._cr.execute("""
                with branch_ as (
                    select
                    br.id as branch
                    from res_branch br
                    group by br.id
                ),
                inv_val as (
                    select
                    ai.branch_id as branch,
                    sum(ai.amount_untaxed) as curr_total_invoice
                    from account_invoice ai
                    join res_branch br on br.id = ai.branch_id 
                    where ai.state in ('open', 'paid') and ai.type = 'out_invoice'
                    and ai.date_invoice between '%s' and '%s'
                    group by ai.branch_id
                ),
                data_inv as (
                    select
                    branch_.branch,
                    sum(iv.curr_total_invoice) as curr_total_invoice
                    from branch_
                    left join inv_val iv on branch_.branch = iv.branch
                    group by branch_.branch
                ),
                credit_note_val as (
                    select
                    ai.branch_id as branch,
                    sum(ai.amount_untaxed) as curr_total_credit_note
                    from account_invoice ai
                    join res_branch br on br.id = ai.branch_id 
                    where ai.state in ('open', 'paid') and ai.type = 'out_refund'
                    and ai.date_invoice between '%s' and '%s'
                    group by ai.branch_id
                ),
                data_credit_note_inv as (
                    select
                    data_inv.branch,
                    sum(data_inv.curr_total_invoice) as curr_total_invoice,
                    sum(iv.curr_total_credit_note) as curr_total_credit_note
                    from data_inv
                    left join credit_note_val iv on data_inv.branch = iv.branch
                    group by data_inv.branch
                ),
                sale as (
                    select
                    so.branch_id as branch,
                    sum(so.amount_untaxed_new) as so_ap_closing
                    from sale_order so
                    join res_branch br on br.id = so.branch_id
                    join account_analytic_account ac on ac.id = so.analytic_account_id
                    where so.state not in ('draft', 'sent', 'cancel')
                    and ac.date_deadline between '%s' and '%s'
                    group by so.branch_id
                ),
                data_sale as (
                    select
                    data_credit_note_inv.branch,
                    sum(data_credit_note_inv.curr_total_invoice) as curr_total_invoice,
                    sum(data_credit_note_inv.curr_total_credit_note) as curr_total_credit_note,
                    sum(s.so_ap_closing) as so_ap_closing
                    from data_credit_note_inv
                    left join sale s on data_credit_note_inv.branch = s.branch
                    group by data_credit_note_inv.branch
                ),
                cl_unbill as (
                    select
                    so.branch_id as branch,
                    sum(so.amount_untaxed_new) as closing_unbilled
                    from sale_order so
                    join res_branch br on br.id = so.branch_id 
                    where so.invoice_status in ('to invoice', 'no') and so.state = 'sale'
                    and so.create_date between '%s' and '%s'
                    group by so.branch_id
                ),
                data_cl_unbill as (
                    select
                        data_sale.branch,
                        sum(data_sale.curr_total_invoice) as curr_total_invoice,
                        sum(data_sale.curr_total_credit_note) as curr_total_credit_note,
                        sum(data_sale.so_ap_closing) as so_ap_closing,
                        sum(cl.closing_unbilled) as closing_unbilled
                        from data_sale
                        left join cl_unbill cl on data_sale.branch = cl.branch
                        group by data_sale.branch
                ),
                op_unbill as (
                    select
                    so.branch_id as branch,
                    sum(so.amount_untaxed_new) as opening_unbilled_
                    from sale_order so
                    join res_branch br on br.id = so.branch_id 
                    where so.invoice_status in ('to invoice', 'no') and so.state = 'sale'
                    and so.create_date < '%s'
                    group by so.branch_id
                ),
                data_op_unbill as (
                    select
                        data_cl_unbill.branch,
                        sum(data_cl_unbill.curr_total_invoice) as curr_total_invoice,
                        sum(data_cl_unbill.curr_total_credit_note) as curr_total_credit_note,
                        sum(data_cl_unbill.so_ap_closing) as so_ap_closing,
                        sum(data_cl_unbill.closing_unbilled) as closing_unbilled,
                        sum(op.opening_unbilled_) as opening_unbilled_
                        from data_cl_unbill
                        left join op_unbill op on data_cl_unbill.branch = op.branch
                        group by data_cl_unbill.branch
                ),
                curr_billed_in_curr as (
                    select
                    ai.branch_id as branch,
                    sum(ai.amount_untaxed) as curr_month_billed
                    from account_invoice ai
                    join res_branch br on br.id = ai.branch_id
                    join account_analytic_account ac on ac.id = ai.job_code
                    where ai.state = 'open' and ai.type = 'out_invoice'
                    and ai.create_date between '%s' and '%s'
                    and ac.date_deadline between '%s' and '%s'
                    group by ai.branch_id
                ),
                data_curr_billed_in_curr as (
                    select
                        data_op_unbill.branch,
                        sum(data_op_unbill.curr_total_invoice) as curr_total_invoice,
                        sum(data_op_unbill.curr_total_credit_note) as curr_total_credit_note,
                        sum(data_op_unbill.so_ap_closing) as so_ap_closing,
                        sum(data_op_unbill.closing_unbilled) as closing_unbilled,
                        sum(data_op_unbill.opening_unbilled_) as opening_unbilled_,
                        sum(cur.curr_month_billed) as curr_month_billed
                        from data_op_unbill
                        left join curr_billed_in_curr cur on data_op_unbill.branch = cur.branch
                        group by data_op_unbill.branch
                ),
                pre_billed_in_curr as (
                    select
                    ai.branch_id as branch,
                    sum(ai.amount_untaxed) as previous_month_billed
                    from account_invoice ai
                    join res_branch br on br.id = ai.branch_id
                    join account_analytic_account ac on ac.id = ai.job_code
                    where ai.state = 'open' and ai.type = 'out_invoice'
                    and ac.date_deadline < '%s'
                    and ai.date_invoice between '%s' and '%s'
                    group by ai.branch_id
                ),
                data_pre_billed_in_curr as (
                    select
                        data_curr_billed_in_curr.branch,
                        sum(data_curr_billed_in_curr.curr_total_invoice) as curr_total_invoice,
                        sum(data_curr_billed_in_curr.curr_total_credit_note) as curr_total_credit_note,
                        sum(data_curr_billed_in_curr.so_ap_closing) as so_ap_closing,
                        sum(data_curr_billed_in_curr.closing_unbilled) as closing_unbilled,
                        sum(data_curr_billed_in_curr.opening_unbilled_) as opening_unbilled_,
                        sum(data_curr_billed_in_curr.curr_month_billed) as curr_month_billed,
                        sum(pre.previous_month_billed) as previous_month_billed
                        from data_curr_billed_in_curr
                        left join pre_billed_in_curr pre on data_curr_billed_in_curr.branch = pre.branch
                        group by data_curr_billed_in_curr.branch
                ),
                current_left_unbill as (
                    select
                    so.branch_id as branch,
                    sum(so.amount_untaxed_new) as current_left_unbilled
                    from sale_order so
                    join res_branch br on br.id = so.branch_id 
                    where so.invoice_status in ('to invoice', 'no') and so.state = 'sale'
                    and so.create_date <= '%s'
                    group by so.branch_id
                ),
                data_current_left_unbill as (
                    select
                        data_pre_billed_in_curr.branch,
                        sum(data_pre_billed_in_curr.curr_total_invoice) as curr_total_invoice,
                        sum(data_pre_billed_in_curr.curr_total_credit_note) as curr_total_credit_note,
                        sum(data_pre_billed_in_curr.so_ap_closing) as so_ap_closing,
                        sum(data_pre_billed_in_curr.closing_unbilled) as closing_unbilled,
                        sum(data_pre_billed_in_curr.opening_unbilled_) as opening_unbilled_,
                        sum(data_pre_billed_in_curr.curr_month_billed) as curr_month_billed,
                        sum(data_pre_billed_in_curr.previous_month_billed) as previous_month_billed,
                        sum(curr.current_left_unbilled) as current_left_unbilled
                        from data_pre_billed_in_curr
                        left join current_left_unbill curr on data_pre_billed_in_curr.branch = curr.branch
                        group by data_pre_billed_in_curr.branch
                ),
                project_expenses as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) as project_expense
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    join account_account aa on aa.id = al.account_id
                    join account_group ag on ag.id = aa.group_id
                    left join account_move am on am.id = al.move_id
                    where al.date between '%s' and '%s' and ag.parent_id = 73
                    and am.state = 'posted'
                    group by al.branch_id
                ),
                data_project_expenses as (
                    select
                        data_current_left_unbill.branch,
                        sum(data_current_left_unbill.curr_total_invoice) as curr_total_invoice,
                        sum(data_current_left_unbill.curr_total_credit_note) as curr_total_credit_note,
                        sum(data_current_left_unbill.so_ap_closing) as so_ap_closing,
                        sum(data_current_left_unbill.closing_unbilled) as closing_unbilled,
                        sum(data_current_left_unbill.opening_unbilled_) as opening_unbilled_,
                        sum(data_current_left_unbill.curr_month_billed) as curr_month_billed,
                        sum(data_current_left_unbill.previous_month_billed) as previous_month_billed,
                        sum(data_current_left_unbill.current_left_unbilled) as current_left_unbilled,
                        sum(pro.project_expense) as project_expense
                        from data_current_left_unbill
                        left join project_expenses pro on data_current_left_unbill.branch = pro.branch
                        group by data_current_left_unbill.branch
                ),
                overhead_last_month as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) as overheads_last_month
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    join account_account aa on aa.id = al.account_id
                    join account_group ag on ag.id = aa.group_id
                    left join account_move am on am.id = al.move_id
                    where al.date between '%s' and '%s' and ag.parent_id = 72
                    and am.state = 'posted'
                    group by al.branch_id
                ),
                data_overhead_last_month as (
                    select
                        data_project_expenses.branch,
                        sum(data_project_expenses.curr_total_invoice) as curr_total_invoice,
                        sum(data_project_expenses.curr_total_credit_note) as curr_total_credit_note,
                        sum(data_project_expenses.so_ap_closing) as so_ap_closing,
                        sum(data_project_expenses.closing_unbilled) as closing_unbilled,
                        sum(data_project_expenses.opening_unbilled_) as opening_unbilled_,
                        sum(data_project_expenses.curr_month_billed) as curr_month_billed,
                        sum(data_project_expenses.previous_month_billed) as previous_month_billed,
                        sum(data_project_expenses.current_left_unbilled) as current_left_unbilled,
                        sum(data_project_expenses.project_expense) as project_expense,
                        sum(olm.overheads_last_month) as overheads_last_month
                        from data_project_expenses
                        left join overhead_last_month olm on data_project_expenses.branch = olm.branch
                        group by data_project_expenses.branch
                ),
                open_expense as (
                    select
                    po.branch_id as branch,
                    sum(po.amount_untaxed) as open_expense
                    from purchase_order po
                    join account_analytic_account ac on ac.id = po.job_code
                    where po.invoice_status not in ('no', 'invoiced', 'paid', 'partially_paid')
                    and po.state != 'cancel'
                    and po.create_date between '%s' and '%s'
                    and po.invoice_count = 0
                    group by po.branch_id
                ),
                data_open_expense as (
                    select 
                    data_overhead_last_month.branch,
                    sum(data_overhead_last_month.curr_total_invoice) as curr_total_invoice,
                    sum(data_overhead_last_month.curr_total_credit_note) as curr_total_credit_note,
                    sum(data_overhead_last_month.so_ap_closing) as so_ap_closing,
                    sum(data_overhead_last_month.closing_unbilled) as closing_unbilled,
                    sum(data_overhead_last_month.opening_unbilled_) as opening_unbilled_,
                    sum(data_overhead_last_month.curr_month_billed) as curr_month_billed,
                    sum(data_overhead_last_month.previous_month_billed) as previous_month_billed,
                    sum(data_overhead_last_month.current_left_unbilled) as current_left_unbilled,
                    sum(data_overhead_last_month.project_expense) as project_expense,
                    sum(data_overhead_last_month.overheads_last_month) as overheads_last_month,
                    sum(oe.open_expense) as open_expense
                    from data_overhead_last_month
                    left join open_expense oe on data_overhead_last_month.branch = oe.branch
                    group by data_overhead_last_month.branch
                ),
                internal_trans as (
                    select
                    al.branch_id as branch,
                    sum(al.amount) * -1 as cl_op_stock_difference
                    from account_analytic_line al
                    join account_analytic_account ac on ac.id = al.account_id
                    where al.amount < 0.0 and al.product_id is not null
                    and al.date between '%s' and '%s'
                    group by al.branch_id
                ),
                data_internal_trans as (
                    select 
                    data_open_expense.branch,
                    sum(data_open_expense.curr_total_invoice) as curr_total_invoice,
                    sum(data_open_expense.curr_total_credit_note) as curr_total_credit_note,
                    sum(data_open_expense.so_ap_closing) as so_ap_closing,
                    sum(data_open_expense.closing_unbilled) as closing_unbilled,
                    sum(data_open_expense.opening_unbilled_) as opening_unbilled_,
                    sum(data_open_expense.curr_month_billed) as curr_month_billed,
                    sum(data_open_expense.previous_month_billed) as previous_month_billed,
                    sum(data_open_expense.current_left_unbilled) as current_left_unbilled,
                    sum(data_open_expense.project_expense) as project_expense,
                    sum(data_open_expense.overheads_last_month) as overheads_last_month,
                    sum(data_open_expense.open_expense) as open_expense,
                    sum(iv.cl_op_stock_difference) as cl_op_stock_difference
                    from data_open_expense
                    left join internal_trans iv on data_open_expense.branch = iv.branch
                    group by data_open_expense.branch
                ),
                assets as (
                    select
                    al.branch_id as branch,
                    sum(al.balance) as assets_and_investment
                    from account_move_line al
                    join res_branch br on br.id = al.branch_id
                    join account_account aa on aa.id = al.account_id
                    join account_group ag on ag.id = aa.group_id
                    where al.date between '%s' and '%s' 
                    and aa.user_type_id = 8
                    group by al.branch_id
                ),
                final_data as (
                    select
                    data_internal_trans.branch,
                    COALESCE(sum(data_internal_trans.curr_total_invoice), 0.0) as curr_total_invoice,
                    COALESCE(sum(data_internal_trans.curr_total_credit_note), 0.0) as curr_total_credit_note,
                    COALESCE(sum(data_internal_trans.so_ap_closing), 0.0) as so_ap_closing,
                    COALESCE(sum(data_internal_trans.so_ap_closing), 0.0) as closing_unbilled,
                    COALESCE(sum(data_internal_trans.opening_unbilled_), 0.0) as opening_unbilled_,
                    COALESCE(sum(data_internal_trans.curr_month_billed), 0.0) as curr_month_billed,
                    COALESCE(sum(data_internal_trans.previous_month_billed), 0.0) as previous_month_billed,
                    COALESCE(sum(data_internal_trans.current_left_unbilled), 0.0) as current_left_unbilled,
                    COALESCE(sum(data_internal_trans.project_expense), 0.0) as project_expense,
                    COALESCE(sum(data_internal_trans.overheads_last_month), 0.0) as overheads_last_month,
                    COALESCE(sum(data_internal_trans.open_expense), 0.0) as open_expense,
                    COALESCE(sum(data_internal_trans.cl_op_stock_difference), 0.0) as cl_op_stock_difference,
                    COALESCE(sum(ast.assets_and_investment), 0.0) as assets_and_investment
                    from data_internal_trans
                    left join assets ast on data_internal_trans.branch = ast.branch
                    group by data_internal_trans.branch
                )
                select row_number() OVER (ORDER BY branch) AS id,
                branch,
                (curr_total_invoice - curr_total_credit_note) as curr_mnth_invoice,
                so_ap_closing, closing_unbilled, opening_unbilled_, open_expense,
                cl_op_stock_difference, assets_and_investment,
                (((opening_unbilled_ - previous_month_billed) + (so_ap_closing - previous_month_billed)) - opening_unbilled_) as closing_opening_unbilled,
                curr_month_billed, previous_month_billed, (so_ap_closing - previous_month_billed) as current_left_unbilled,
                ((opening_unbilled_ - curr_month_billed - previous_month_billed) + (so_ap_closing - previous_month_billed)) as cl_unbilled,
                project_expense, overheads_last_month,
                (((project_expense + open_expense) / COALESCE(nullif(so_ap_closing, 0.0))) * 100.0) as percent_pe,
                ((overheads_last_month / COALESCE(nullif(so_ap_closing, 0.0))) * 100.0) as overhead_percentage,
                (((so_ap_closing - project_expense - overheads_last_month - open_expense) / COALESCE(nullif(so_ap_closing, 0.0))) * 100.0) as profit_percentage,
                (so_ap_closing - project_expense - overheads_last_month - open_expense) as net_profit
                from final_data  
        """ % (first_day, last_day_it,
               first_day, last_day_it,
               first_day, last_day_it,
               first_day, last_day_it,
               first_day,
               first_day, last_day_it, first_day, last_day_it,
               first_day, first_day, last_day_it,
               last_day_it,
               first_day, last_day_it,
               first_day, last_day_it,
               first_day, last_day_it,
               first_day, last_day_it,
               first_day, last_day_it)

            )
            result = self._cr.fetchall()
            rows = result
            values = ', '.join(map(str, rows))
            sql = ("""INSERT INTO profit_loss_report 
            (id, branch, curr_mnth_invoice,
            so_ap_closing, closing_unbilled,
            opening_unbilled_, open_expense,
            cl_op_stock_difference, assets_and_investment, closing_opening_unbilled,
            curr_month_billed, previous_month_billed,
            current_left_unbilled, cl_unbilled, project_expense,
            overheads_last_month, percent_pe, overhead_percentage,
            profit_percentage, net_profit) VALUES {}""".format(values).replace('None', 'null'))
            self._cr.execute(sql)
            # print('first_day.date()===', first_day.strftime('%d %b %y'))
            action = {
                'type': 'ir.actions.act_window',
                'views': [
                    (tree_view_id, 'tree'), (form_view_id, 'form'),
                    (graph_view_id, 'graph'), (pivot_view_id, 'pivot')
                ],
                'view_mode': 'tree,form',
                'name': _('Profit Loss Report : %s - %s' % (first_day.strftime('%d %b %y'),
                                                           last_day.strftime('%d %b %y'))),
                'res_model': 'profit.loss.report',
                'search_view_id': search_view_ref and search_view_ref.id,
                'context': {'today_date': self.from_date}
            }
            return action
